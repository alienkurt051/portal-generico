-- portal SS version 1.2 Se agrega bitcora de eventos
-- catalogo de acciones para bitacora


CREATE TABLE IF NOT EXISTS c_acciones_bitacora (
id_accion INTEGER NOT NULL,
d_accion  VARCHAR(255),
PRIMARY KEY(id_accion)
);

-- se agregan movimientos
INSERT INTO c_acciones_bitacora VALUES (1,"Inicio de sesión");
INSERT INTO c_acciones_bitacora VALUES (2,"Cierre de sesión");
INSERT INTO c_acciones_bitacora VALUES (3,"Cambio de contraseña");
INSERT INTO c_acciones_bitacora VALUES (4,"Consulta de perfil");
INSERT INTO c_acciones_bitacora VALUES (5,"Actualización de datos de perfil");
INSERT INTO c_acciones_bitacora VALUES (6,"Consulta de documentos");
INSERT INTO c_acciones_bitacora VALUES (7,"Descarga de XML");
INSERT INTO c_acciones_bitacora VALUES (8,"Descarga de PDF");
INSERT INTO c_acciones_bitacora VALUES (9,"Extracción de reporte");
INSERT INTO c_acciones_bitacora VALUES (10,"Alta de usuario");
INSERT INTO c_acciones_bitacora VALUES (11,"Cambio de usuario");
INSERT INTO c_acciones_bitacora VALUES (12,"Desbloqueo de usuario");
INSERT INTO c_acciones_bitacora VALUES (13,"Cambio de contraseña de usuario");
INSERT INTO c_acciones_bitacora VALUES (14,"Inicia recuperación de contraseña");
INSERT INTO c_acciones_bitacora VALUES (15,"Error en recuperación de contraseña");
INSERT INTO c_acciones_bitacora VALUES (16,"Recuperación de contraseña exitoso");
INSERT INTO c_acciones_bitacora VALUES (17,"Bloqueo de usuario");
INSERT INTO c_acciones_bitacora VALUES (18,"Cambio de contraseña de usuario exitoso");
INSERT INTO c_acciones_bitacora VALUES (19,"Cambio de contraseña de usuario con error");
INSERT INTO c_acciones_bitacora VALUES (20,"Ejecución de consulta Mis Comprobantes");
INSERT INTO c_acciones_bitacora VALUES (21,"Intento de inicio de sesión con cuenta bloqueada");
INSERT INTO c_acciones_bitacora VALUES (22,"Intento de inicio de sesión con login o contraseña equivocada");
INSERT INTO c_acciones_bitacora VALUES (23,"Ejecuta consulta de Bitácora");
INSERT INTO c_acciones_bitacora VALUES (24,"Consulta de movimientos de bitácora");
INSERT INTO c_acciones_bitacora VALUES (25,"Extracción de reporte de bitácora");
INSERT INTO c_acciones_bitacora VALUES (26,"Busqueda de transacción para factura");
INSERT INTO c_acciones_bitacora VALUES (27,"Confirma transacción para facturar");
INSERT INTO c_acciones_bitacora VALUES (28,"Envia transacción a facturar");


-- bitacora


CREATE TABLE IF NOT EXISTS pss_bitacora (
id_bitacora   INTEGER NOT NULL AUTO_INCREMENT,
id_accion     INTEGER,
id_usuario    INTEGER,
fecha_hora    TIMESTAMP,
dir_ip        VARCHAR(20),
observaciones MEDIUMTEXT,
PRIMARY KEY(id_bitacora)
);

-- vista para consultar bitacora
drop view if exists v_pss_bitacora;

create view v_pss_bitacora as
select
bic.id_bitacora     ,
bic.id_usuario      ,
usu.login           ,
usu.nombre          ,
usu.apellido_paterno,
usu.email           ,
bic.fecha_hora      ,
bic.id_accion       ,
acc.d_accion        ,
bic.dir_ip          ,
bic.observaciones   
FROM pss_bitacora bic left outer join pss_usuario usu on bic.id_usuario = usu.id_usuario_pss
inner join c_acciones_bitacora acc on bic.id_accion = acc.id_accion
ORDER BY bic.id_bitacora DESC;
-- Agregar columna que identificará al sistema móvil
ALTER TABLE pss_usuario ADD id_sistema_movil varchar(32);
-- Agregar unique a columna 
ALTER TABLE pss_usuario ADD CONSTRAINT constraint_id_sistema_movil UNIQUE (id_sistema_movil);
-- Agregar Índice 
ALTER TABLE pss_usuario ADD INDEX index_product_id (id_sistema_movil);

-- columna que se utiliza cuando se timbra y hace la actualizacion del registro
-- del xml
ALTER TABLE emi_trx33_xml ADD COLUMN estatus int(11);

-- Tabla que se ocupa cuando se manda a timbrar por WS
CREATE TABLE IF NOT EXISTS adm_c_urlpac (
 id int(11) NOT NULL AUTO_INCREMENT,
 orden int(11) NOT NULL DEFAULT '0',
 url_pac varchar(500) NOT NULL,
 intentos int(2) DEFAULT NULL,
 PRIMARY KEY (id)
) ;

-- Se agrega la columna que contendrá la url del componente generador de pdf birt
-- el cual se utiliza cuando se factura por servicio movil
ALTER TABLE pss_config_portal ADD COLUMN url_generador_pdf MEDIUMTEXT;

-- Para mejor la búsqueda de transacción
CREATE INDEX idx_busqueda_transac ON emi_trx33_r (id_trx33_r,id_sucursal,id_tipo_de_comprobante);  
CREATE INDEX idx_id_trx33 ON emi_trx33_inf_adic (id_trx33);
CREATE INDEX idx_busqueda_transac ON emi_trx33_inf_adic (id_flex_header,valor);     

-- Para mejorar performance en busqueda de cliente
CREATE INDEX idx_rfc    ON c_clientes(rfc);             
CREATE index idx_c_id_dato ON pss_fh_transaccion(id_tipo_dato,id_flex_header);

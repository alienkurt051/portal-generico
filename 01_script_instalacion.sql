-- TABLAS PARA EL Portal de autoservicio

-- configuracion del portal
drop table if exists pss_config_portal;

create table pss_config_portal (
id_config_portal               SMALLINT NOT NULL DEFAULT 1,
plantilla_portal               VARCHAR(50), -- plantilla para usar
titulo_pantalla_principal      VARCHAR(100),
titulo_menu                    VARCHAR(20),
activar_captcha                SMALLINT DEFAULT 0, -- 0 inactivo, 1 activo
activar_autoregistro           SMALLINT DEFAULT 0, -- 0 inactivo, 1 activo
activar_autodesbloqueo         SMALLINT DEFAULT 0, -- 0 inactivo, 1 activo
notif_trx_no_encontrada        SMALLINT DEFAULT 0, -- 0 inactivo, 1 activo
usar_email_como_login          SMALLINT DEFAULT 1, -- 0 el login no es el correo electronico, 1 el login es el correo electronico
usar_contrasena                SMALLINT DEFAULT 1, -- 0 no se requiere contrasena, 1 se requiere contrasena
activar_fecha_max_facturar     SMALLINT DEFAULT 0, -- 0 no hay  fecha maxima para facturar, 1 existe fecha maxima para facturar
fecha_max_para_facturar        VARCHAR(20), -- escribir una cadena tipo CRON
facturar_ticket_en_global      SMALLINT DEFAULT 0, -- 0 no permitir facturar ticket que ya esta en factura global, 1 si permitirlo
usar_concepto_generico         SMALLINT, -- 0 no se usa, va desglosado en la transaccion, 1 se usa concepto generico
clave_prod_serv_generico       VARCHAR(10), -- clave producto o servicio que se asociara al producto generico
unidad_medida_generico         VARCHAR(5), -- unidad de medida que se asociara al producto generico
descripcion_generico           VARCHAR(255), -- descripcion del producto generico
activar_elegir_uso_cfdi        SMALLINT, -- 0 no se elige, viene en la transaccion, 1 se elige
activar_elegir_metodo_pago     SMALLINT, -- 0 no se elige, viene en la transaccion; 1 se elige
id_cliente_autofactura         INTEGER, -- id de cliente con el que estan las transacciones sin facturar
fecha_config                   TIMESTAMP,
ip_config                      VARCHAR(20),
activar_elegir_forma_pago      SMALLINT, -- 0 no se puede elegir forma de pago, 1 se puede elegir forma de pago
aviso_login                    TEXT, -- texto en la pagina del login
aviso_principal                TEXT, -- texto en la pagina principal
url_ws_facturacion             MEDIUMTEXT, -- url del ws de facturacion
primary key(id_config_portal)
);

delete from pss_config_portal;

-- configuracion por defecto          
--  id, captcha, autoregistro, trxnoenc, emailogin, usar contrasena, activar fecha max, fecha max, fact ticket global, usar concepto generico, clave prod serv gen, um gen, desc gen, elegir uso, elegir metodo pago, fecha con, ip
insert into pss_config_portal values
--  id, captcha, autoregistro, autodesbloqueo, trxnoenc, emailogin, usar contrasena, activar fecha max, fecha max, fact ticket global, usar concepto generico, clave prod serv gen, um gen, desc gen, elegir uso, elegir metodo pago, fecha con, ip
(    1, "sto",      "<b>STO</b> Portal autoservicio","STO",0,            0,           0,             0,         0,               1,                 0,      null,                  0,                      0,               null , null  ,  null   ,   0,   0, 1, now(),"127.0.0.1",0,"Mensaje en Login","Mensaje en pantalla principal","http://localhost:8080/NeonEmisionWS_20180324/NeonEmisionWS?wsdl");

alter table pss_config_portal add modo_facturacion smallint default 0;

-- delete from pss_config_portal;

-- Tabla para establecer tipos de usuario
DROP TABLE IF EXISTS `pss_c_tipo_usuario`;
CREATE TABLE pss_c_tipo_usuario (
  id_tipo_usuario smallint(6) NOT NULL,
  clave_tipo_usuario varchar(10) COLLATE utf8_general_ci DEFAULT NULL,
  d_tipo_usuario varchar(20) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_tipo_usuario`)
);

INSERT INTO pss_c_tipo_usuario (id_tipo_usuario, clave_tipo_usuario, d_tipo_usuario) VALUES ('1', 'Administrador STO', 'Usuario administrador de STO');
INSERT INTO pss_c_tipo_usuario (id_tipo_usuario, clave_tipo_usuario, d_tipo_usuario) VALUES ('2', 'Administrador Sistema', 'Usuario administrador del sistema');
INSERT INTO pss_c_tipo_usuario (id_tipo_usuario, clave_tipo_usuario, d_tipo_usuario) VALUES ('3', 'Operador de tienda', 'Usuario operador de sucursal');
INSERT INTO pss_c_tipo_usuario (id_tipo_usuario, clave_tipo_usuario, d_tipo_usuario) VALUES ('4', 'Cliente', 'Usuario tipo cliente');


DROP TABLE IF EXISTS c_entidades;
CREATE TABLE `c_entidades`  (
  `id_entidad` int(11) NOT NULL AUTO_INCREMENT,
  `entidad` varchar(360) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `numero_entidad` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rfc` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `numero_exterior` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `calle` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `numero_interior` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `colonia` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `localidad` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `referencia` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `municipio` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `estado` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pais` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `codigo_postal` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_tipo_entidad` int(11) NOT NULL,
  `id_entidad_padre` int(11) NULL DEFAULT NULL,
  `id_regimen` smallint(6) NOT NULL,
  `estatus` smallint(1) NOT NULL,
  PRIMARY KEY (`id_entidad`) USING BTREE);
-- --------------
DROP TABLE IF EXISTS c_series_entidad;
CREATE TABLE `c_series_entidad`  (
  `id_entidad` int(11) NOT NULL,
  `id_tipo_documento` smallint(6) NOT NULL,
  `serie` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `secuencia` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_entidad`, `id_tipo_documento`, `serie`) USING BTREE);
-- -------------
DROP TABLE IF EXISTS emi_c_info_adicionales;
CREATE TABLE `emi_c_info_adicionales`  (
  `id_info_adicional` int(11) NOT NULL AUTO_INCREMENT,
  `campo_adicional` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nivel` smallint(1) NOT NULL,
  PRIMARY KEY (`id_info_adicional`) USING BTREE);
-- ---------------------------------------------

DROP TABLE IF EXISTS `c_clientes`;
CREATE TABLE `c_clientes`  (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `cliente` varchar(360) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rfc` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `numero_cliente` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `numero_exterior` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `numero_interior` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `calle` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `colonia` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `localidad` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `referencia` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `municipio` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `estado` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pais` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `codigo_postal` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `estatus` smallint(6) NOT NULL DEFAULT 1,
  `num_reg_id_trib` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `enviar_email_cliente` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cliente`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5630 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

DROP TABLE IF EXISTS `c_preguntas_recuperacion`;
CREATE TABLE `c_preguntas_recuperacion`  (
  `id_pregunta_recuperacion` smallint(6) NOT NULL AUTO_INCREMENT,
  `pregunta` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estatus` smallint(1) NOT NULL,
  PRIMARY KEY (`id_pregunta_recuperacion`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- series usadas para facturar en portal ss
drop table if exists pss_series_entidades;

create table pss_series_entidades (
id_serie_entidad    INTEGER NOT NULL AUTO_INCREMENT,
serie               varchar(20),
id_entidad          INTEGER,
primary key(id_serie_entidad)
);


-- 20180327 se agrega tipo (para poder poner factura, nota de credito, ambas)
ALTER TABLE pss_series_entidades add tipo int;

-- vista para consultar las series por entidad y configurar
drop view if exists v_pss_series_entidades;
create view v_pss_series_entidades as
select 
  pse.id_serie_entidad,
  ent.id_entidad      , 
  ent.rfc             , 
  ent.entidad         ,
  if(ent.id_entidad_padre is null, "MATRIZ","SUCURSAL") as tipo_entidad,
  ser.serie           ,
  ser.secuencia       ,
  pse.tipo            
from pss_series_entidades pse INNER JOIN c_entidades ent on
  pse.id_entidad = ent.id_entidad
INNER JOIN c_series_entidad ser ON
  pse.serie = ser.serie
  and pse.id_entidad = ser.id_entidad;
  

-- tipos de dato para campos flex de portal ss
drop table if exists pss_c_tipo_dato;

create table pss_c_tipo_dato (
id_tipo_dato    SMALLINT,
clave_tipo_dato VARCHAR(10),
d_tipo_dato     VARCHAR(20),
primary key(id_tipo_dato)
);

create unique index idx_uni_psstipodato ON pss_c_tipo_dato(clave_tipo_dato);

insert into pss_c_tipo_dato values (1,"text","Alfanumérico");
insert into pss_c_tipo_dato values (2,"date","Fecha");
insert into pss_c_tipo_dato values (3,"number","Numérico");
insert into pss_c_tipo_dato values (4,"time","Hora");

-- tabla para definir los flex headers que daran unicidad a la transaccion
drop table if exists pss_fh_transaccion;

create table pss_fh_transaccion (
id_fh_transaccion     SMALLINT NOT NULL AUTO_INCREMENT,
id_flex_header        INTEGER,
etiqueta_flex_header  VARCHAR(100),
id_tipo_dato          SMALLINT,
primary key(id_fh_transaccion)
);

create unique index idx_uni_fhtrxpss on pss_fh_transaccion(id_flex_header);

-- 20180326 se agrega columna para agregar placeholder en los campos
alter table pss_fh_transaccion add placeholder varchar(100);


-- vista para generar pantalla de facturacion
drop view if exists v_pss_campos_transaccion;

-- 20180326 se agrega campo placeholder
create view v_pss_campos_transaccion AS
SELECT  fht.id_fh_transaccion
       ,fht.id_flex_header
       ,cia.campo_adicional
       ,cia.descripcion
       ,fht.etiqueta_flex_header
       ,fht.id_tipo_dato
       ,fht.placeholder
       ,ctd.clave_tipo_dato
       ,ctd.d_tipo_dato
FROM pss_fh_transaccion fht INNER JOIN emi_c_info_adicionales cia
     ON fht.id_flex_header = cia.id_info_adicional
INNER JOIN pss_c_tipo_dato ctd
     ON fht.id_tipo_dato = ctd.id_tipo_dato
ORDER BY fht.id_fh_transaccion;

-- catalogo de estatus
drop table if exists pss_c_estatus;

create table pss_c_estatus(
id_estatus    SMALLINT,
estatus       VARCHAR(20),
primary key(id_estatus)
);

insert into pss_c_estatus values (0,"Inactivo"); -- esta inactivo y no se puede desbloquear ni nada, hasta que lo haga el administrador
insert into pss_c_estatus values (1,"Activo"); -- activa, en uso
insert into pss_c_estatus values (2,"Bloqueado"); -- bloqueada pero puede desbloquearse automaticamente



-- tabla de usuarios del portal
drop table if exists pss_usuario;

create table pss_usuario (
id_usuario_pss                   INTEGER NOT NULL AUTO_INCREMENT,
login                            VARCHAR(50),
contrasena                       VARCHAR(40),
email                            VARCHAR(50),
nombre                           VARCHAR(50),
apellido_paterno                 VARCHAR(50),
apellido_materno                 VARCHAR(50),
id_pregunta_recuperacion         SMALLINT,
respuesta_recuperar_contrasena   VARCHAR(30),
fecha_alta                       TIMESTAMP,
fecha_ultima_sesion              TIMESTAMP,
primer_inicio_sesion             SMALLINT, -- booleana que indica si no ha hecho sesion nunca
solicitar_cambio_contrasena      SMALLINT, -- booleana que indica si se debe solicitar al usuario que cambie su contrasena en el siguiente inicio de sesion
id_estatus                       SMALLINT, -- estatus de la cuenta 0 - inactivo, 1 activo
dir_ip                           VARCHAR(20),
tipo_usuario                     SMALLINT,
primary key(id_usuario_pss)
);

create unique index idx_uni_pssusuario ON pss_usuario(login);

insert into pss_usuario values (0,"administrador",MD5("neonsm"),"administrador@correo.com","Administrador del sistema",null,null,1,"azul",now(),null,1,1,1,"172.20.5.123",2);

-- token de activacion de cuenta de usuario nueva
drop table if exists pss_token_cuenta_nueva;

create table pss_token_cuenta_nueva (
id_token         integer not null auto_increment,
id_usuario_pss   INTEGER,
token            VARCHAR(30),
fecha_creacion   date,
primary key(id_token)
);



-- relacion de usuarios con clientes
drop table if exists pss_r_usuario_cliente;

create table pss_r_usuario_cliente (
id_r_usuario_cliente    INTEGER NOT NULL AUTO_INCREMENT,
id_usuario              INTEGER,
id_cliente              INTEGER,
fecha_alta              TIMESTAMP,
primary key(id_r_usuario_cliente)
);

create unique index idx_uni_pssusucli on pss_r_usuario_cliente(id_usuario, id_cliente);

-- datos de prueba
insert into pss_r_usuario_cliente values (0,1,1,now());

-- catalogo de sepomex para codigos postales, estados y colonias
drop table if exists c_sepomex;

CREATE TABLE c_sepomex (
  d_codigo varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  d_asenta varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  d_tipo_asenta varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  d_mnpio varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  d_estado varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  d_ciudad varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  d_cp varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  c_estado varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  c_oficina varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  c_cp varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  c_tipo_asenta varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  c_mnpio varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  id_asenta_cpcons varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  d_zona varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  c_cve_ciudad varchar(255) COLLATE utf8_general_ci DEFAULT NULL
);

create index ixd_cpsepomex on c_sepomex(d_codigo,d_asenta);

-- vista para mostrar las entidades en el portal ss
drop view if exists v_pss_entidades;
create view v_pss_entidades as
select
id_entidad,
entidad
from c_entidades;

-- tabla de consulta de comprobantes en pss
drop table if exists pss_docto_xml;

create table pss_docto_xml (
  id_docto int(10) unsigned NOT NULL AUTO_INCREMENT,
  version varchar(5) DEFAULT NULL,
  xml mediumtext,
  comentario varchar(300) DEFAULT NULL,
  id_forma_ingreso int(11) DEFAULT NULL,
  PRIMARY KEY (id_docto)
);

drop table if exists pss_docto_pdf;

create table pss_docto_pdf (
id_pdf    INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
id_docto  INTEGER UNSIGNED,
pdf       mediumblob,
primary key(id_pdf)
);

drop table if exists pss_docto_encabezado;

create table pss_docto_encabezado (
id_docto            INTEGER UNSIGNED,
rfc_emisor          VARCHAR(15),
nombre_emisor       VARCHAR(255),
rfc_receptor        VARCHAR(15),
nombre_receptor     VARCHAR(255),
pais_residencia     VARCHAR(3),
num_reg_id_trib     VARCHAR(40),
serie               VARCHAR(40),
folio               VARCHAR(40),
uuid                VARCHAR(50),
fecha_emision       TIMESTAMP,
fecha_timbrado      TIMESTAMP,
estatus             SMALLINT,
fecha_cancelacion   TIMESTAMP,
moneda              VARCHAR(50),
tipo_cambio         DECIMAL(12,6),
monto               DECIMAL(22,6),
tipo_comprobante    VARCHAR(20),
forma_pago          VARCHAR(10),
metodo_pago         VARCHAR(20),
comentarios         TEXT,
primary key(id_docto)
);

drop table if exists pss_docto_detalle;

create table pss_docto_detalle (
id_docto_detalle INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
id_docto         INTEGER UNSIGNED,
num_linea        SMALLINT,
clave_prod_serv  VARCHAR(20),
concepto         TEXT,
unidad_medida    VARCHAR(20),
unidad           VARCHAR(20),
precio_unitario  DECIMAL(22,6),
descuento        DECIMAL(22,6),
importe          DECIMAL(22,6),
primary key(id_docto_detalle)
);

drop table if exists pss_docto_forma_ingreso;

create table pss_docto_forma_ingreso (
  id_forma_ingreso int(10) unsigned NOT NULL AUTO_INCREMENT,
  forma_ingreso    varchar(20) DEFAULT NULL,
  fecha_ingreso    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  id_usuario       int(10) unsigned DEFAULT NULL,
  comentarios      text,
  archivo_original varchar(500) DEFAULT NULL,
  PRIMARY KEY (id_forma_ingreso)
);

-- vista para consultar los comprobantesde boveda
drop view if exists v_pss_listado_comprobantes_boveda;
create view v_pss_listado_comprobantes_boveda as
select
  xml.id_docto         ,
  xml.version          ,
  enc.rfc_emisor       ,
  enc.nombre_emisor    ,
  enc.rfc_receptor     ,
  enc.nombre_receptor  ,
  enc.pais_residencia  ,
  enc.num_reg_id_trib  ,
  enc.serie            ,
  enc.folio            ,
  enc.uuid             ,
  enc.fecha_emision    ,
  enc.fecha_timbrado   ,
  enc.estatus          ,
  enc.fecha_cancelacion,
  enc.moneda           ,
  enc.tipo_cambio      ,
  enc.monto            ,
  enc.tipo_comprobante ,
  enc.forma_pago       ,
  enc.metodo_pago      ,
  enc.comentarios      
FROM pss_docto_xml xml
INNER JOIN pss_docto_encabezado enc ON xml.id_docto = enc.id_docto;

-- ============================================ estas tablas son solamente para el envio de correo del portal de boveda de CFDI =====================================================
drop table if exists envio_correo_remitente;
-- tabla para registro de envio de correo. Cuentas con las que se envia correo. Debe haber una por default
create table envio_correo_remitente (
id_remitente      SMALLINT,
d_remitente       VARCHAR(255),
usuario_imap      varchar(100),
contrasena_imap   varchar(100),
servidor_imap     varchar(100),
puerto_imap       varchar(10),
root_folder       varchar(50),
success_folder    varchar(50),
failed_folder     varchar(50),
protocolo         varchar(10),
es_default        SMALLINT,
primary key(id_remitente)
);

/*
mail.smtp.host=smtpcorp.com
mail.smtp.starttls.enable=false
mail.smtp.port=25
mail.smtp.mail.sender=notificaciones@stofactura.com
mail.smtp.user=notificaciones@stofactura.com
mail.smtp.pass=F4cT$t0C0nsult1ng
mail.smtp.auth=true
*/
create unique index uni_idx_envremitente on envio_correo_remitente (usuario_imap);

insert into envio_correo_remitente values (1,"Boveda CFDI STO","notificaciones@stofactura.com", "F4cT$t0C0nsult1ng", "smtpcorp.com", "25", "inbox","success","failed","None",1);
-- insert into envio_correo_remitente values (1,'Boveda CFDI STO', 'stodccrm@outlook.com', 'StoFactura2016', 'smtp-mail.outlook.com', '587', 'inbox', 'success', 'failed', 'STARTTLS', 1);

drop table if exists envio_correo;

create table envio_correo (
id_envio_correo          bigint NOT NULL AUTO_INCREMENT,
id_transaccion           BIGINT,
id_proceso               int,
id_remitente             smallint,
procesado                smallint,
fecha_registro           timestamp,
fecha_proceso          timestamp,
enviar_adjuntos          SMALLINT,
asunto                   VARCHAR(255),
cuerpo                   MEDIUMTEXT,
primary key(id_envio_correo)
);

-- destinatarios del correo
drop table if exists envio_correo_destinatario;

create table envio_correo_destinatario (
id_correo_destinatario BIGINT NOT NULL AUTO_INCREMENT,
id_envio_correo        BIGINT,
destinatario           varchar(200),
fecha_proceso          timestamp,
estatus_envio          smallint,
cod_error              smallint,
d_error                MEDIUMTEXT,
num_intentos           SMALLINT, -- numero de intentos fallidos para marcar error de envio
primary key(id_correo_destinatario)
);

-- adjuntos del correo
drop table if exists envio_correo_adjuntos;

create table envio_correo_adjuntos (
id_correo_adjunto      BIGINT NOT NULL AUTO_INCREMENT,
id_envio_correo        BIGINT,
tipo_adjunto           SMALLINT, -- 1 text, 2 blob
forma_adjunto          SMALLINT, -- 1 archivo, 2 obtener de bd
adjunto_text           MEDIUMTEXT,
adjunto_blob           blob,
nombre_adjunto         VARCHAR(200),
primary key(id_correo_adjunto)
);

-- vista para obtener datos de envio de correo
drop view if exists v_listado_envio_correo;

create view v_listado_envio_correo AS
SELECT
   rem.usuario_imap    ,
   rem.contrasena_imap ,
   rem.servidor_imap   ,
   rem.puerto_imap     ,
   rem.protocolo       ,
   env.id_envio_correo ,
   env.asunto          ,
   env.cuerpo          ,
   des.id_correo_destinatario,
   des.destinatario    ,
   des.num_intentos
FROM envio_correo env
     INNER JOIN envio_correo_remitente rem
         ON env.id_remitente = rem.id_remitente
     INNER JOIN envio_correo_destinatario des
         ON env.id_envio_correo = des.id_envio_correo
     WHERE des.estatus_envio = 1 AND des.num_intentos < 4
     ;
     
-- tabla para guardar la ultima consulta realizada por el usuario
drop table if exists pss_boveda_consulta_cfdi;

create table pss_boveda_consulta_cfdi (
id_consulta          INTEGER NOT NULL AUTO_INCREMENT,
id_usuario           INTEGER,
fecha_consulta       TIMESTAMP,
ultima_consulta      TEXT,
estatus_consulta     smallint,
descripcion_estatus  varchar(200),
url_descarga         MEDIUMTEXT,
notificar_por_correo smallint,
correo_notificacion varchar(250),
primary key(id_consulta)
);

-- vista para consulta de entidades en portal de autofactura para asignacion de series para facturacion
drop view if exists v_pss_c_entidades;

create view v_pss_c_entidades as
select
  ent.id_entidad      ,
  ent.rfc             , 
  ent.entidad         ,
  if(ent.id_entidad_padre is null, "MATRIZ","SUCURSAL") as tipo_entidad
from c_entidades ent;

DROP TABLE IF EXISTS pss_frecuencia_max_fac;
CREATE TABLE pss_frecuencia_max_fac(
id_frecuencia_max_fac       TINYINT(2) UNSIGNED NOT NULL AUTO_INCREMENT,
frecuencia          varchar(30) CHARACTER SET UTF8 COLLATE utf8_bin UNIQUE,
dias                SMALLINT(3) UNSIGNED DEFAULT 1,
dias_posteriores    TINYINT(2) UNSIGNED NOT NULL DEFAULT 0,
cambio_anio         TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0=false 1=true',

CONSTRAINT pk_frecuencia PRIMARY KEY (id_frecuencia_max_fac)
);

-- Posibles valores de la frecuencia
INSERT INTO pss_frecuencia_max_fac (frecuencia)       values ('FECHA TRANSACCIÓN');
INSERT INTO pss_frecuencia_max_fac (frecuencia,dias)  values ('FECHA CALENDARIO',0);


-- Se agrega el campo a la tabla de configuración del portal
ALTER TABLE pss_config_portal
  ADD COLUMN id_frecuencia_max_fac TINYINT(2) UNSIGNED NOT NULL DEFAULT 2,
  ADD FOREIGN KEY fk_frecuencia_max_fac(id_frecuencia_max_fac) 
    REFERENCES pss_frecuencia_max_fac(id_frecuencia_max_fac);

-- ----------------------------
-- Table structure for c_prioridades_procesos
-- ----------------------------
DROP TABLE IF EXISTS `c_prioridades_procesos`;
CREATE TABLE `c_prioridades_procesos`  (
  `id_prioridad` smallint(6) NOT NULL AUTO_INCREMENT,
  `prioridad` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `color` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_prioridad`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;  
-- ----------------------------
-- Table structure for c_grupos_procesos
-- ----------------------------
DROP TABLE IF EXISTS `c_grupos_procesos`;
CREATE TABLE `c_grupos_procesos`  (
  `id_grupo_proceso` smallint(6) NOT NULL AUTO_INCREMENT,
  `clave` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `estatus` int(1) NOT NULL,
  PRIMARY KEY (`id_grupo_proceso`) USING BTREE,
  UNIQUE INDEX `clave`(`clave`) USING BTREE,
  INDEX `nombre`(`nombre`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;  
-- ----------------------------
-- Table structure for c_procesos
-- ----------------------------
DROP TABLE IF EXISTS `c_procesos`;
CREATE TABLE `c_procesos`  (
  `id_proceso` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `estatus` int(1) NOT NULL,
  `id_grupo_proceso` smallint(6) NOT NULL,
  `id_prioridad` smallint(6) NOT NULL,
  PRIMARY KEY (`id_proceso`) USING BTREE,
  INDEX `fk_c_procesos_c_grupos_procesos1_idx`(`id_grupo_proceso`) USING BTREE,
  INDEX `fk_c_procesos_c_prioridades_procesos1_idx`(`id_prioridad`) USING BTREE,
  CONSTRAINT `fk_c_procesos_c_grupos_procesos1` FOREIGN KEY (`id_grupo_proceso`) REFERENCES `c_grupos_procesos` (`id_grupo_proceso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_c_procesos_c_prioridades_procesos1` FOREIGN KEY (`id_prioridad`) REFERENCES `c_prioridades_procesos` (`id_prioridad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

INSERT INTO `c_preguntas_recuperacion`(`id_pregunta_recuperacion`, `pregunta`, `estatus`) VALUES (1, 'Cuál es tu color favorito?', 1);
INSERT INTO `c_preguntas_recuperacion`(`id_pregunta_recuperacion`, `pregunta`, `estatus`) VALUES (2, 'Cuál es tu número favorito?', 1);
INSERT INTO `c_preguntas_recuperacion`(`id_pregunta_recuperacion`, `pregunta`, `estatus`) VALUES (3, 'Cuál es tu equipo favorito?', 1);
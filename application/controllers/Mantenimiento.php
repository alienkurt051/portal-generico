<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mantenimiento extends CI_Controller {

	/**
	 * Inicio de sesion
	 *
	 */
	public function index()
	{
        $data = array();
        
		$config_portal = Model\Pss_config_portal::find(1, false);
		$data["config_portal"] = $config_portal;
        cargar_interfaz_grafica_login($this, $data, "view_mantenimiento", null);
	}

   
}

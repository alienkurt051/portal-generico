
-- Datos de la addenda que no se encuentran en la BD de neon y se desean modificar
create table pss_emi_trx33_addenda(

    id_trx33                INT(11) NOT NULL,
    orden_compra            varchar(35) NOT NULL,
    gln_comprador           varchar(13) NOT NULL,
    seccion_departamento    varchar(256),
    gln_proveedor           varchar(13) NOT NULL,
    numero_proveedor        varchar(8) NOT NULL,
    gln_entrega             varchar(13)	NOT NULL,
    dias_plazo_pago         TINYINT UNSIGNED,
    entidad                 varchar(360),
    estado                  varchar(35),
    calle                   varchar(35),
    codigo_postal           varchar(5),
    
    PRIMARY KEY (id_trx33),
    CONSTRAINT fk_pss_id_trx33_xml FOREIGN KEY (id_trx33)
    REFERENCES emi_trx33_xml(id_trx33)
);
-- Datos extras que requiere el cliente para incluir en la addenda para cada concepto.
-- Se hace una relación 1-1 con la tabla emi_trx33_conceptos para no modificar
-- la estructura de esta última.
create table pss_emi_trx33_conceptos(

    id_trx33_conceptos      INT(11) NOT NULL,
    id_trx33                INT(11) NOT NULL,
    orden_compra            varchar(35) NOT NULL DEFAULT '0',
    numero_identificacion   varchar(14) NOT NULL DEFAULT '0',
    
    PRIMARY KEY (id_trx33_conceptos),
    CONSTRAINT fk_pss_id_trx33_conceptos FOREIGN KEY (id_trx33_conceptos)
    REFERENCES emi_trx33_conceptos(id_trx33_conceptos),
    CONSTRAINT fk_pss_id_trx33 FOREIGN KEY (id_trx33) 
    REFERENCES emi_trx33 (id_trx33)
);

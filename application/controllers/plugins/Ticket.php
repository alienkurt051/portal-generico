<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/* Ruta y nombre de reporte html que se descargará */
define('RUTA_ARCHIVO_HTML', FCPATH.'/reports/Reporte_tickets_');

class Ticket extends CI_Controller {
 
    function __construct() {
        parent::__construct();

        // si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
        $existe_sesion = $this->session->userdata("id_usuario");
        if (empty($existe_sesion)) {
            redirect(site_url(), 'refresh');
        }
    }

    public function index() {

        // se creal el arreglo para paso de parametros
        $data = array();
        
        $data['loading_img'] = base_url() . "assets/imgcustom/loading.gif";
        
        // se obtienen los datos del usuario
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;


        $config_portal = Model\Pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;

        // url para recuperar la contrasena
        $url_anterior = base_url("index.php/" . URL_PANTALLA_PRINCIPAL);

        $url_vista_previa = base_url("index.php/facturar/busca_transaccion");
        $data["url_vista_previa"] = $url_vista_previa;

        // url de la imagen guia del ticket
        $url_guia_ticket = base_url() . "assets/imgcustom/ticket.jpg";
        $data["url_guia_ticket"] = $url_guia_ticket;

        // se obtiene la lista de entidades que si tienen serie definida para facturar
        $series_entidades = Model\Pss_series_entidades::all();

        // si no hay sucursales con series definidas
        if (empty($series_entidades)) {
            $url_anterior = base_url("index.php/" . URL_PANTALLA_PRINCIPAL);
            $data["url_anterior"] = $url_anterior;

            $mensaje_error = "Ninguna de las entidades emisoras han sido configuradas con una serie para facturar. No es posible continuar con el proceso de facturación.";
            $data["mensaje_error"] = $mensaje_error;
            cargar_interfaz_grafica($this, $data, 'facturar_ticket/view_content_wrapper_error_config_facturacion', null);
        } else {
            $entidades_in = array();
            $i = 0;
            foreach ($series_entidades as $serie_entidad) {
                $entidades_in[$i] = $serie_entidad->id_entidad;
                $i++;
            }

            // arreglo de entidades
            $this->db->where_in("id_entidad", $entidades_in);
            $this->db->order_by("entidad");
            $arr_entidades = Model\V_pss_entidades::all();
            $data["arr_entidades"] = $arr_entidades;

            // se transfieren los parametros al arreglo
            $data["url_anterior"] = $url_anterior;

            // campos para busqueda de transaccion
            $arr_campos_transaccion = Model\V_pss_campos_transaccion::all();
            $data["arr_campos_transaccion"] = $arr_campos_transaccion;

            if ($this->session->flashdata('titulo') != null) {
                $data["titulo"] = $this->session->flashdata('titulo');
                $data["mensaje"] = $this->session->flashdata('mensaje');
                $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
            }

            cargar_interfaz_grafica($this, $data, 'plugins/ticket/view_content_wrapper_busqueda', 'plugins/ticket/view_content_wrapper_busqueda_script');
        }
    }

    public function cargar_resultados() {
        /* Recupera los datos enviados del formulario */
        $data = $this->recuperar_datos_form();
  
        $data['loading_img'] = base_url() . "assets/imgcustom/loading.gif";

        /* Carga la vista que despliega la lista de facturas */
        $this->load->view('plugins/ticket/view_content_wrapper_resultados', $data);
    }

    public function cargar_pagina($rowno = 0, $rowperpage = 5) {

        $this->datos_consulta = $this->recuperar_datos_form();

        /* Offset del paginado */
        if ($rowno != 0) {
            $rowno = ($rowno - 1) * $rowperpage;
        }
        $this->load->model("plugins/Model_ticket");
        $this->load->library('pagination');
                
        /* Se obtiene el total de datos consultados */
        $total = $this->Model_ticket->total_registros($this->datos_consulta);
        /* Se obtienen la lista de datos consultados */
        $listado_tickets= $this->Model_ticket->obtener_tickets($rowno, $rowperpage, $this->datos_consulta);

        
        /* Se configura la paginación y se inicializa */
        $config = $this->cargar_configuracion_pag($total, $rowperpage);
        $this->pagination->initialize($config);

        // Datos que se regresan
        $data['pagination'] = $this->pagination->create_links();
        $data['result']     = $listado_tickets;
        $data['row']        = $rowno;
        $data['total']      = $total;
        
        echo json_encode($data);
    }

    public function cargar_configuracion_pag($total, $rowperpage) {

        $config['uri_segment'] = 4; // Número de segmentos después de base_url
        $config['num_links'] = 5;
        /* Si el total de registros es menor que el número de paginación, no
         * coloca barra de paginación
         *         */
        if ($total < $rowperpage) {
            $config['num_links'] = 0;
        }

        $config['base_url']             = base_url().'index.php/plugins/ticket/cargar_pagina';
        $config['use_page_numbers']     = TRUE;
        $config['total_rows']           = $total;
        $config['per_page']             = $rowperpage;
        $config['full_tag_open']        = "<ul class='pagination'>";
        $config['full_tag_close']       = "</ul>";
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';
        $config['cur_tag_open']         = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close']        = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open']        = "<li>";
        $config['next_tagl_close']      = "</li>";
        $config['prev_tag_open']        = "<li>";
        $config['prev_tagl_close']      = "</li>";
        $config['first_tag_open']       = "<li>";
        $config['first_tagl_close']     = "</li>";
        $config['last_tag_open']        = "<li>";
        $config['last_tagl_close']      = "</li>";
        $config['first_link']           = '<<';
        $config['last_link']            = '>>';

        return $config;
    }

    public function recuperar_datos_form() {
        return [
            'rows'              => (int) $this->input->post('numero_registros'),
            'sucursal'          => (int)$this->input->post('sucursal'),
            'fecha_inicio'      => $this->input->post('fecha_inicio'),
            'fecha_fin'         => $this->input->post('fecha_fin'),
            'hora_inicio'       => $this->input->post('hora_inicio'),
            'hora_fin'          => $this->input->post('hora_fin'),            
            'caja'              => $this->input->post('caja'),
            'transaccion'       => $this->input->post('transaccion')
            
        ];
    }

    public function generar_reporte() {
        ini_set('memory_limit', '250M');
        /* Se obtiene los datos que se envían como queryString*/
        $this->datos_consulta = [
            'sucursal'          => (int)$this->input->get('sucursal'),
            'fecha_inicio'      => $this->input->get('fecha_inicio'),
            'fecha_fin'         => $this->input->get('fecha_fin'),
            'hora_inicio'       => $this->input->get('hora_inicio'),
            'hora_fin'          => $this->input->get('hora_fin'),
            'caja'              => $this->input->get('caja'),
            'transaccion'       => $this->input->get('transaccion')
        ];

        /* Carga el modelo y obtiene los datos */
        $this->load->model("plugins/Model_ticket");        
        /* Se obtiene el total de datos consultados */
        $total = $this->Model_ticket->total_registros($this->datos_consulta);
        /* Se obtienen la lista de datos consultados */
        $listado_tickets= $this->Model_ticket->obtener_tickets(0,$total,$this->datos_consulta);

        /* Se genera el archivo html y se descarga */
        $config_portal = Model\Pss_config_portal::find(1);
        $this->crear_html($listado_tickets,$config_portal->titulo_menu);
        
    }
    
    /* Se crea archivo html y se descarga */
    public function crear_html($datos,$cliente = "Decathlon",$mensaje = "Reporte de tickets") {
        
        $nombre_archivo = RUTA_ARCHIVO_HTML.time().'.xls';
        $numero_fila = 1;
        /* Encabezados html y de la tabla */
        $html = "<!DOCTYPE html> <html> <head> "
                . "<title>Reporte de tickets</title> "
                . "<meta charset='utf-8'>"
                . "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css'>"
                . "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>"
                . "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js'></script> "             
                . "</head> <body>"
                . "<div class='jumbotron text-center'>"
                . "<h1>$cliente</h1>"
                . "<p>$mensaje</p>"
                .  "</div>"
                . "<div class='container'>";
        
        $table = "<div class='table-responsive'>
            <table class='table table-condensed' width='75%'>
                <thead>
                    <tr class='bg-primary'>
                        <th scope='col'>#</th>
                        <th scope='col'>Id STO</th>
                        <th scope='col'>".NOMBRE_ID_ERP."</th>
                        <th scope='col'>Fecha</th>
                        <th scope='col'>Sucursal</th>
                        <th scope='col'>Subtotal</th>
                        <th scope='col'>Total Impuestos Transaladados</th>
                        <th scope='col'>Total</th>
						<th scope='col'>Estatus facturación</th>
						<th scope='col'>RFC Receptor</th>
						<th scope='col'>Nombre receptor</th>
                    </tr>
                </thead><tbody>";
        
        /* Se crea y se escribe el contenido en el archivo */
        $archivo = fopen($nombre_archivo, "w");
            fwrite($archivo, $html.$table);
        
            /* Se crea cada fila con sus respectivos datos */
            foreach ($datos as $key => $value) {            
                $fila = "<tr class='bg-info'>"
                    ."<td>" . $numero_fila                         . "</td>"
                    ."<td>" . $value['id_trx33_r']                 . "</td>"
                    ."<td>" . $value['id_trx_erp']                 . "</td>"
                    ."<td>" . $value['fecha']                      . "</td>"
                    ."<td>" . $value['id_sucursal']                . "</td>"
                    ."<td>" . $value['subtotal']                   . "</td>"
                    ."<td>" . $value['totalImpuestosTrasladados']  . "</td>"
                    ."<td>" . $value['total']                      . "</td>"
					."<td>" . $value['estatus_facturacion']        . "</td>"
					."<td>" . $value['rfc_receptor']               . "</td>"
					."<td>" . $value['nombre_receptor']            . "</td>"
                    ."</tr>";
                fwrite($archivo, $fila);
                $numero_fila++;
            }
            $close_tag= "  </table> </div></body> </html>";
            fwrite($archivo, $close_tag);
        fclose($archivo);
        
        /* Cabeceras para descargar el archivo */        
        if (file_exists($nombre_archivo)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($nombre_archivo).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($nombre_archivo));
            readfile($nombre_archivo);
            exit;
        }        
    }

}

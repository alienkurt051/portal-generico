-- vista de consulta de comprobantes portal con clausula para TUA
 drop view if exists v_pss_listado_comprobantes_tua;
 CREATE VIEW v_pss_listado_comprobantes_tua
 AS
   SELECT DISTINCT trx33.id_trx33 AS id_trx33,
          trx33.version AS version,
          trx33.rfc_emisor AS rfc_emisor,
          trx33.nombre_emisor AS nombre_emisor,
          trx33r.id_receptor AS id_cliente,
          trx33.rfc_receptor AS rfc_receptor,
          trx33.nombre_receptor AS nombre_receptor,
          trx33.residencia_fiscal AS residencia_fiscal,
          trx33.num_reg_id_trib AS num_reg_id_trib,
          trx33.serie AS serie,
          trx33.folio AS folio,
          if((xml33.codigo = '10'),xml33.uuid,xml33.descripcion) AS uuid,
          trx33.fecha AS fecha,
          xml33.fecha_timbrado AS fecha_timbrado,
          NULL ,
          NULL ,
          if ( trx33.tipo_comprobante = 'P', ExtractValue(tr33cpm.xml, '/pago10:Pagos/pago10:Pago/@MonedaP'),trx33.moneda) AS moneda,
          trx33.tipo_cambio AS tipo_cambio,
          if ( trx33.tipo_comprobante = 'P', ExtractValue(tr33cpm.xml, '/pago10:Pagos/pago10:Pago/@Monto'),trx33.total) AS total,
          trx33.tipo_comprobante AS tipo_comprobante,
          trx33.forma_pago AS forma_pago,
          trx33.metodo_pago AS metodo_pago,
          NULL AS pdf
          from 
          emi_trx33 trx33 join emi_trx33_r trx33r
          on trx33.id_trx33 = trx33r.id_trx33_r
          INNER JOIN emi_trx33_conceptos con
		          ON con.id_trx33 = trx33r.id_trx33_r
                  AND con.descripcion REGEXP '0210|0209'
          join emi_trx33_xml xml33 
          on trx33.id_trx33 = xml33.id_trx33
          LEFT OUTER JOIN emi_trx33_complementos_r tr33cpm
          ON trx33.id_trx33 = tr33cpm.id_trx33_r;
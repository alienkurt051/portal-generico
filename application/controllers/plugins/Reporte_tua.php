
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporte_tua extends CI_Controller {

 function __construct() {
        parent::__construct();
        
        // si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
        $existe_sesion = $this->session->userdata("id_usuario");
        if ( empty( $existe_sesion ) ) {
            redirect(site_url(),'refresh');
        }
    }
    /**
     * Inicio de sesion
     *
     */
    public function index() {
        
        // se creal el arreglo para paso de parametros
        $data = array();
        
       
        // url para consulta
        $url_consulta_tua = base_url("index.php/plugins/reporte_tua/consultaTUA");

        // se transfieren los parametros al arreglo
        $data["url_consulta_tua"]           = $url_consulta_tua;
        
        if ( $this->session->flashdata('titulo') != null ) {
          $data["titulo"]       = $this->session->flashdata('titulo');
          $data["mensaje"]      = $this->session->flashdata('mensaje');
          $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
        }
        
        cargar_interfaz_grafica($this, $data, 'plugins/consultaTUA/view_content_wrapper_consulta_tua', null);

    }
    
    public function consultaTUA() {
        // se obtienen las fechas de consulta
        $fecha_inicial  = $this->input->post("fecha_inicial");
        $fecha_final    = $this->input->post("fecha_final");
        
        // si la fecha final no es mayor que la inicial, se devuelve error
        if ( $fecha_inicial > $fecha_final ) {
            $this->session->set_flashdata('titulo', "Consulta de TUA");
            $this->session->set_flashdata('mensaje', "La fecha final debe ser mayor o igual que la fecha inicial");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-warning alert-dismissible');
        
            $url_reporte_tua = base_url()."index.php/plugins/reporte_tua/index";
            redirect($url_reporte_tua);
        }
        
        // los parametros de consulta son correctos
        $consulta = "SELECT r.serie
                     	,r.fecha
                     	,r.folio
                     	,r.id_tipo_de_comprobante
                     	,r.total
                     	,x.uuid
                     	,t.rfc_emisor
                     	,t.rfc_receptor
                     	,t.nombre_receptor
                     	,r.id_trx_erp AS numero_factura
                     	,con.descripcion
                     FROM emi_trx33_r r
                     INNER JOIN emi_trx33_xml x ON x.id_trx33 = r.id_trx33_r
                     INNER JOIN emi_trx33_conceptos con ON con.id_trx33 = r.id_trx33_r
                     	AND con.descripcion REGEXP '0210|0209'
                     LEFT JOIN emi_trx33 t ON t.id_trx33 = r.id_trx33_r
                     WHERE x.codigo = '10' AND cast(r.fecha as date) BETWEEN '".$fecha_inicial."' AND '".$fecha_final."' 
                     ORDER BY r.fecha";
                     
        $consulta = "SELECT r.serie
                     	,r.fecha
                     	,r.folio
                     	,r.id_tipo_de_comprobante
                     	,r.total
                     	,x.uuid
                     	,t.rfc_emisor
                     	,t.rfc_receptor
                     	,t.nombre_receptor
                     	,r.id_trx_erp AS numero_factura
                     	,con.descripcion
                     FROM emi_trx33_r r
                     INNER JOIN emi_trx33_xml x ON x.id_trx33 = r.id_trx33_r
                     INNER JOIN emi_trx33_conceptos con ON con.id_trx33 = r.id_trx33_r
                     LEFT JOIN emi_trx33 t ON t.id_trx33 = r.id_trx33_r
                     WHERE x.codigo = '10' AND r.fecha BETWEEN '".$fecha_inicial."' AND '".$fecha_final."' 
                     ORDER BY r.fecha limit 10";

        
        $resultset = $this->db->query($consulta);
        
        
        // si no hay datos
        if ( $resultset->num_rows() < 1 ) {
            $this->session->set_flashdata('titulo', "Consulta de TUA");
            $this->session->set_flashdata('mensaje', "No se encontraron resultados con los parametros definidos");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-warning alert-dismissible');
        
            $url_reporte_tua = base_url()."index.php/plugins/reporte_tua/index";
            redirect($url_reporte_tua);
        } else {
            // hay datos, se genera reporte en excel
            $path = APPPATH."/reports";
            $arrcomprobantes         = Model\V_pss_listado_comprobantes_autofactura::all();
            $arr_transacciones       = $arrcomprobantes;
            $id_consulta             = $this->input->post('id_consulta');
            $id_checklist            = $this->input->post('check_list');
            
            $Consultas_excel = explode(',', $id_consulta);
            
            // se verifica si no existe el directorio
            if ( !file_exists($path) ) {
                // se crea si no existe
                mkdir($path, 0755, true);
            }
            
            //echo getcwd ();
            //die();
            
            // carga de la biblioteca para generar zip
            $this->load->library('zip');
            //Generar el reporte y obtener su nombre completo
            require_once APPPATH.'/controllers/lib/PHPExcel/PHPExcel.php';//Se llama a la libreria
            
            // Se crea el objeto PHPExcel
            $objPHPExcel = new PHPExcel();
            
            // Se asignan las propiedades del libro
            $objPHPExcel->getProperties()->setCreator("STO") //Autor
                       ->setLastModifiedBy("STO") //Ultimo usuario que lo modificó
                       ->setTitle("Reporte TUA")
                       ->setSubject("Reporte TUA")
                       ->setDescription("Reporte TUA")
                       ->setKeywords("reporte")
                       ->setCategory("Reporte TUA");
            
            $tituloReporte = "Reporte TUA descarga ".date("Y-m-d-H:i:s"); 
            $titulosColumnas = array('SERIE', 'FECHA', 'FOLIO', 'TIPO DE COMPROBANTE', 'TOTAL', 'UUID', 'RFC EMISOR', 'RFC RECEPTOR', 'NOMBRE RECEPTOR', 'NUMERO DE FACTURA', 'DESCRIPCION');
            
            $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells('A1:K1');//Esto genera que la columna B1 quede sobre B:1,C:1,D:1,E:1,F:1,G:1,G:1,H:1 centrada
                    
            // Se agregan los titulos del reporte
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1',$tituloReporte)
                        ->setCellValue('A2',  $titulosColumnas[0])
                        ->setCellValue('B2',  $titulosColumnas[1])
                        ->setCellValue('C2',  $titulosColumnas[2])
                        ->setCellValue('D2',  $titulosColumnas[3])
                        ->setCellValue('E2',  $titulosColumnas[4])
                        ->setCellValue('F2',  $titulosColumnas[5])
                        ->setCellValue('G2',  $titulosColumnas[6])
                        ->setCellValue('H2',  $titulosColumnas[7])
                        ->setCellValue('I2',  $titulosColumnas[8])
                        ->setCellValue('J2',  $titulosColumnas[9])
                        ->setCellValue('K2',  $titulosColumnas[10]);
            
            //Se agregan los datos 
            $conteo = sizeof($arr_transacciones);
            $i = 3;
            $aux = $i+$conteo;
                               
                           
            // para cada registro de TUA             
            foreach ($resultset->result() as $row) {
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $row->serie)
                            ->setCellValue('B'.$i, $row->fecha)
                            ->setCellValue('C'.$i, $row->folio)
                            ->setCellValue('D'.$i, $row->id_tipo_de_comprobante)
                            ->setCellValue('E'.$i, $row->total)
                            ->setCellValue('F'.$i, $row->uuid)
                            ->setCellValue('G'.$i, $row->rfc_emisor)
                            ->setCellValue('H'.$i, $row->rfc_receptor)
                            ->setCellValue('I'.$i, $row->nombre_receptor)
                            ->setCellValue('J'.$i, $row->numero_factura)
                            ->setCellValue('K'.$i, $row->descripcion);
                $i++;

            }
            $estiloTituloReporte = array(
                  'font' => array(
                    'name'      => 'Verdana',
                      'bold'      => true,
                      'italic'    => false,
                        'strike'    => false,
                        'size' =>16,
                        'color'     => array(
                            'rgb' => 'FFFFFF'
                          )
                    ),
                  'fill' => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'D73636')
              ),
                    'borders' => array(
                        'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_NONE                    
                        )
                    ), 
                    'alignment' =>  array(
                      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                      'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                      'rotation'   => 0,
                      'wrap'          => TRUE
                )
                );
            $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($estiloTituloReporte);
            
            for($i = 'A'; $i <= 'K'; $i++){
              $objPHPExcel->setActiveSheetIndex(0)      
                ->getColumnDimension($i)->setAutoSize(TRUE);
            }
            
            // Se asigna el nombre a la hoja
            $objPHPExcel->getActiveSheet()->setTitle('TUA');
            
            // Se activa la hoja para que sea la que se muestre cuando el archivo se abre
            $objPHPExcel->setActiveSheetIndex(0);
            // Inmovilizar paneles 
            //$objPHPExcel->getActiveSheet(0)->freezePane('A4');
            $objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,3);
            
            $filenamexls = date("Ymd_His").".xlsx";
            $filename_for_zipxls = $path."/".$filenamexls;      
                   
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            
            $objWriter->save($filename_for_zipxls);//Guarda el archivo en la carpeta de descarga
            
            $this->zip->read_file($filename_for_zipxls);
            // descarga de zip
            $filename_for_zip = date("Ymd_His").".zip";;
            $this->zip->download($filename_for_zip);
        }
        
    }
}
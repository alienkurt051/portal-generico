<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require('nodos_addenda.php'); // Cambiar nodos dependiendo del cliente  
class Addenda extends CI_Controller {
    
    private $x_path_addenda     = '//cfdi:Comprobante//'.ADDENDA;
    private $x_path_comprobante = '//cfdi:Comprobante';
    private $reglas_line_item = [];
    
    function __construct() {
        parent::__construct();
        // si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
        $existe_sesion = $this->session->userdata("id_usuario");
        if (empty($existe_sesion)) {
            redirect(site_url(), 'refresh');
        }
    }

    public function index() {

        // se creal el arreglo para paso de parametros
        $data = array();
        $data['loading_img'] = base_url() . "assets/imgcustom/loading.gif";

        // se obtienen los datos del usuario
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;


        $config_portal = Model\Pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;

        // url pantalla anterior
        $url_anterior = base_url("index.php/" . URL_PANTALLA_PRINCIPAL);

        // se transfieren los parametros al arreglo
        $data["url_anterior"] = $url_anterior;

        if ($this->session->flashdata('titulo') != null) {
            $data["titulo"] = $this->session->flashdata('titulo');
            $data["mensaje"] = $this->session->flashdata('mensaje');
            $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
        }

        cargar_interfaz_grafica($this, $data, 'plugins/addenda/view_content_wrapper_busqueda', 'plugins/addenda/view_content_wrapper_busqueda_script');
    }

    public function cargar_resultados() {
        /* Recupera los datos enviados del formulario */
        $data   = $this->recuperar_datos_form();
        $vista  = 'plugins/addenda/view_content_wrapper_resultados';
        $script = 'plugins/addenda/view_content_wrapper_resultados_script';
        $data['loading_img'] = base_url() . "assets/imgcustom/loading.gif";

        /* Carga la vista que despliega la lista de facturas y su script de JS */
        $this->load->view($vista, $data);
        $this->load->view($script);
    }

    public function cargar_pagina($rowno = 0, $rowperpage = 5) {

        $this->datos_consulta = $this->recuperar_datos_form();
        /* Offset del paginado */
        if ($rowno != 0) {
            $rowno = ($rowno - 1) * $rowperpage;
        }
        $this->load->model("plugins/Model_comprobantes_midea");
        $this->load->library('pagination');

        /* Se obtiene el total de datos consultados */
        $total = $this->Model_comprobantes_midea->total_registros($this->datos_consulta);
        /* Se obtienen la lista de datos consultados */
        $listado_tickets = $this->Model_comprobantes_midea->buscar_comprobantes($rowno, $rowperpage, $this->datos_consulta);

        /* Se configura la paginación y se inicializa */
        $config = $this->cargar_configuracion_pag($total, $rowperpage);
        $this->pagination->initialize($config);

        // Datos que se regresan
        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $listado_tickets;
        $data['row'] = $rowno;
        $data['total'] = $total;

        echo json_encode($data);
    }

    public function cargar_configuracion_pag($total, $rowperpage) {

        $config['uri_segment'] = 4; // Número de segmentos después de base_url
        $config['num_links'] = 5;
        /* Si el total de registros es menor que el número de paginación, no
         * coloca barra de paginación
         *         */
        if ($total < $rowperpage) {
            $config['num_links'] = 0;
        }

        $config['base_url']             = base_url() . 'index.php/plugins/Ticket/cargar_pagina';
        $config['use_page_numbers']     = TRUE;
        $config['total_rows']           = $total;
        $config['per_page']             = $rowperpage;
        $config['full_tag_open']        = "<ul class='pagination'>";
        $config['full_tag_close']       = "</ul>";
        $config['num_tag_open']         = '<li>';
        $config['num_tag_close']        = '</li>';
        $config['cur_tag_open']         = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close']        = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open']        = "<li>";
        $config['next_tagl_close']      = "</li>";
        $config['prev_tag_open']        = "<li>";
        $config['prev_tagl_close']      = "</li>";
        $config['first_tag_open']       = "<li>";
        $config['first_tagl_close']     = "</li>";
        $config['last_tag_open']        = "<li>";
        $config['last_tagl_close']      = "</li>";
        $config['first_link']           = '<<';
        $config['last_link']            = '>>';

        return $config;
    }

    public function recuperar_datos_form() {
        return [
            'rows'              => 5,
            'rfc_emisor'        => $this->input->post('rfc_emisor'),
            'nombre_emisor'     => $this->input->post('nombre_emisor'),
            'rfc_receptor'      => $this->input->post('rfc_receptor'),
            'nombre_receptor'   => $this->input->post('nombre_receptor'),
            'serie'             => $this->input->post('serie'),
            'folio_inicio'      => $this->input->post('folio_inicio'),
            'folio_fin'         => $this->input->post('folio_fin')
        ];
    }
    public function editar($id_trx33) {
        $data   = array();
        $vista               = 'plugins/addenda/view_content_wrapper_edicion';
        $script              = 'plugins/addenda/view_content_wrapper_edicion_script';          
        $data['id_trx33']    = $id_trx33;
        $data['loading_img'] = base_url() . "assets/imgcustom/loading.gif";
        $data['url_home']    = base_url()."index.php/".URL_PANTALLA_PRINCIPAL;
        $this->validaciones_line_item();
        $data['reglasOrdenCom'] = json_encode($this->reglas_line_item['ordenCompra']);
        $data['reglasNumeroIde'] = json_encode($this->reglas_line_item['numeroIdentificacion']);
        cargar_interfaz_grafica($this, $data, $vista, $script);
    }
    
    public function informacion_documento($id_trx33){
        $this->load->model("plugins/Model_comprobantes_midea");
        $informacion_documento = $this->Model_comprobantes_midea->obtener_informacion_documento($id_trx33);
        echo json_encode($informacion_documento);
    }
    public function direccion_sucursal($id_trx33) {
        $this->load->model("plugins/Model_comprobantes_midea");
        $direccion_sucursal = $this->Model_comprobantes_midea->direccion_sucursal($id_trx33);
        echo json_encode($direccion_sucursal);
    }

    public function informacion_encabezado_addenda($id_trx33) {
        $this->load->model("plugins/Model_comprobantes_midea");
        $datos_addenda = $this->Model_comprobantes_midea->obtener_datos_addenda($id_trx33);
        echo json_encode($datos_addenda);
    }
    public function conceptos_documento($id_trx33) {
        $id_trx33 = (int) $id_trx33;
        $this->load->model("plugins/Model_comprobantes_midea");
        $datos_addenda = $this->Model_comprobantes_midea->obtener_conceptos($id_trx33);
        echo json_encode($datos_addenda);
    }
    public function generar_addenda() {
        $datos_encabezado   = $this->input->post('encabezado');
        $datos_conceptos    = $this->input->post('conceptos');
        $id_trx33           = $this->input->post('id_trx33');
        $nodo_nuevo_addenda = $this->generar_nodo_addenda($datos_encabezado, $datos_conceptos, $id_trx33);
        $this->load->model("plugins/Model_comprobantes_midea");
        $xml_timbrado       = $this->Model_comprobantes_midea->obtener_xml_timbrado($id_trx33);
        $this->Model_comprobantes_midea->actualizar_addenda($id_trx33,$datos_encabezado,$datos_conceptos);
        $nuevo_xml = $this->agregar_addenda_a_xml($xml_timbrado,$nodo_nuevo_addenda);
        $this->actualizar_xml_timbrado($id_trx33,$nuevo_xml);
    }

    public function actualizar_xml_timbrado($id_trx33, $nuevo_xml) {
        $this->load->model("plugins/Model_comprobantes_midea");
        $this->Model_comprobantes_midea->actualizar_xml($id_trx33, $nuevo_xml);
    }

    public function agregar_addenda_a_xml($xml_timbrado, $nodo_nuevo_addenda) {
        /* Se crea, carga y configura el xml con DOMDocument */
        $doc_xml_tim                        = new DOMDocument();
        $doc_xml_tim->preserveWhiteSpace    = false;
        $doc_xml_tim->formatOutput          = true;
        $doc_xml_tim->loadXML($xml_timbrado);

        /* Se busca el nodo addenda con xpath ya que contienen prefijo */
        $xpath = new DOMXPath($doc_xml_tim);
        $nodos_addenda_tim = $xpath->query($this->x_path_addenda);

        /* Es necesario importar el nuevo nodo para introducir en el nuevo xml */
        $nueva_addenda = $doc_xml_tim->importNode($nodo_nuevo_addenda, true);

        /* Si existe el nodo addenda se reemplaza */
        if ($nodos_addenda_tim->length == 1) {
            foreach ($nodos_addenda_tim as $nodo_addenda_tim) {
                $nodo_comprobante = $nodo_addenda_tim->parentNode;
                $nodo_comprobante->replaceChild($nueva_addenda, $nodo_addenda_tim);
            }
            return $doc_xml_tim->saveXML();
        }
        /* Sino existe el nodo addenda se agrega */
        $comprobantes = $xpath->query($this->x_path_comprobante);
        foreach ($comprobantes as $comprobante) {
            $comprobante->appendChild($nueva_addenda);
        }
        return $doc_xml_tim->saveXML();
    }

    public function generar_nodo_addenda($datos_encabezado, $datos_conceptos, $id_trx33) {
        global $REQUEST_FOR_PAYMENT_ATTR;

        $dom                        = new DOMDocument();
        $dom->preserveWhiteSpace    = false;
        $dom->formatOutput          = true;

        $addenda            = $dom->createElement(ADDENDA);
        $requestForPayment  = $dom->createElement(REQUEST_FOR_PAYMENT);

        /* ENCABEZADO */
        $this->load->model("plugins/Model_comprobantes_midea");
        $datos_encabezado_trx33 = $this->Model_comprobantes_midea->obtener_datos_encabezado_nodo($id_trx33);
        $REQUEST_FOR_PAYMENT_ATTR['DeliveryDate'] = substr($datos_encabezado_trx33['fecha'],0,10);
        $this->colocar_atributos($REQUEST_FOR_PAYMENT_ATTR, $requestForPayment);        
        $this->generar_encabezado($dom, $requestForPayment, $datos_encabezado, $datos_encabezado_trx33);

        /* LINEITEM */
        foreach ($datos_conceptos as $key => $datos_concepto) {
            $concepto = $this->cargar_datos_conceptos($datos_concepto['id_trx33_conceptos']);
            $this->generar_lineitem($dom, $requestForPayment, $datos_concepto, $concepto);
        }
        /* TOTALES */
        $this->generar_totales($dom, $requestForPayment, $datos_encabezado_trx33);

        /* Insertamos el nuevo elemento como raíz (hijo del documento)*/
        $addenda->appendChild($requestForPayment);
        
        return $addenda;
    }

    public function generar_encabezado($dom, $nodo_agregar, $datos_encabezado, $datos_encabezado_trx33) {
        global $SPECIAL_INSTRUCTION_ATTR;
        global $REFERENCE_IDENTIFICATION_ORDER_ATTR;
        global $REFERENCE_IDENTIFICATION_ADDI_ATTR;
        global $ALTERNATE_PARTY_IDENTIFICATION_ATTR;
        global $CURRENCY_ATTR;
        global $PAYMENT_TERMS_ATTR;
        global $NET_PAYMENT_ATTR;
        global $TIME_PERIOD_DUE_ATTR;

        $requestForPaymentIdentification = $dom->createElement(REQUEST_FOR_PAYMENT_IDENTIFICATION);
        $requestForPaymentIdentification->appendChild($dom->createElement(ENTITY_TYPE,ENTITY_TYPE_VAL));
        $requestForPaymentIdentification->appendChild($dom->createElement(
                        UNIQUE_CREATOR_IDENTIFICATION, $datos_encabezado_trx33['serie'] . $datos_encabezado_trx33['folio']
        ));

        $specialInstruction = $dom->createElement(SPECIAL_INSTRUCTION);
        $specialInstruction->appendChild($dom->createElement(TEXT,$datos_encabezado_trx33['importe_letra']));

        $orderIdentification = $dom->createElement(ORDER_IDENTIFICATION);
        $referenceIdentificationOrder = $dom->createElement(REFERENCE_IDENTIFICATION,$datos_encabezado['orden_compra']);
        $orderIdentification->appendChild($referenceIdentificationOrder);
        $orderIdentification->appendChild($dom->createElement(REFERENCE_DATE,substr($datos_encabezado_trx33['fecha'],0,10)));

        $AdditionalInformation = $dom->createElement(ADDITIONAL_INFORMATION);
        $referenceIdentificationAddi = $dom->createElement(REFERENCE_IDENTIFICATION,REFERENCE_IDENTIFICATION_VAL);
        $AdditionalInformation->appendChild($referenceIdentificationAddi);

        $buyer = $dom->createElement(BUYER);
        $contactInformation = $dom->createElement(CONTACT_INFORMATION);
        $personOrDepartmentName = $dom->createElement(PERSON_OR_DEPARTMENT_NAME);
        $personOrDepartmentName->appendChild($dom->createElement(
                        TEXT, $datos_encabezado['seccion_departamento']));
        $contactInformation->appendChild($personOrDepartmentName);

        $buyer->appendChild($dom->createElement(
                        GLN, $datos_encabezado['gln_comprador']));
        $buyer->appendChild($contactInformation);


        $seller = $dom->createElement(SELLER);
        $seller->appendChild($dom->createElement(
                        GLN, $datos_encabezado['gln_proveedor']));
        $alternatePartyIdentification = $dom->createElement(
                ALTERNATE_PARTY_IDENTIFICATION, $datos_encabezado['numero_proveedor']);
        $seller->appendChild($alternatePartyIdentification);

        $nameAndAddress = $dom->createElement(NAME_AND_ADDRESS);
        $nameAndAddress->appendChild($dom->createElement(NAME,$datos_encabezado['entidad']));
        $nameAndAddress->appendChild($dom->createElement(STREET_ADDRESS_ONE,$datos_encabezado['calle']));
        $nameAndAddress->appendChild($dom->createElement(CITY,$datos_encabezado['estado']));
        $nameAndAddress->appendChild($dom->createElement(POSTAL_CODE,$datos_encabezado['codigo_postal']));

        $shipTo = $dom->createElement(SHIPTO);
        $shipTo->appendChild($dom->createElement(
                        GLN, $datos_encabezado['gln_entrega']));
        $shipTo->appendChild($nameAndAddress);


        $currency = $dom->createElement(CURRENCY);
        $currency->appendChild($dom->createElement(CURRENCY_FUNCTION, CURRENCY_FUNCTION_VAL));
        $currency->appendChild($dom->createElement(RATE_OF_CHANGE, RATE_OF_CHANGE_VAL));

        $timePeriodDue = $dom->createElement(TIME_PERIOD_DUE);
        $timePeriodDue->appendChild($dom->createElement(
                        VALUE, $datos_encabezado['dias_plazo_pago']));

        $paymentTimePeriod = $dom->createElement(PAYMENT_TIME_PERIOD);
        $paymentTimePeriod->appendChild($timePeriodDue);

        $netPayment = $dom->createElement(NET_PAYMENT);
        $netPayment->appendChild($paymentTimePeriod);

        $paymentTerms = $dom->createElement(PAYMENT_TERMS);
        $paymentTerms->appendChild($netPayment);

        $nodo_agregar->appendChild($requestForPaymentIdentification);
        $nodo_agregar->appendChild($specialInstruction);
        $nodo_agregar->appendChild($orderIdentification);
        $nodo_agregar->appendChild($AdditionalInformation);
        $nodo_agregar->appendChild($buyer);
        $nodo_agregar->appendChild($seller);
        $nodo_agregar->appendChild($shipTo);
        $nodo_agregar->appendChild($currency);
        $nodo_agregar->appendChild($paymentTerms);


        $this->colocar_atributos($SPECIAL_INSTRUCTION_ATTR, $specialInstruction);
        $this->colocar_atributos($REFERENCE_IDENTIFICATION_ORDER_ATTR, $referenceIdentificationOrder);
        $this->colocar_atributos($REFERENCE_IDENTIFICATION_ADDI_ATTR, $referenceIdentificationAddi);
        $this->colocar_atributos($ALTERNATE_PARTY_IDENTIFICATION_ATTR, $alternatePartyIdentification);
        $this->colocar_atributos($CURRENCY_ATTR, $currency);

        $this->colocar_atributos($PAYMENT_TERMS_ATTR, $paymentTerms);
        $this->colocar_atributos($NET_PAYMENT_ATTR, $netPayment);
        $this->colocar_atributos($TIME_PERIOD_DUE_ATTR, $timePeriodDue);
    }

    public function generar_totales($dom, $nodo, $datos_encabezado_trx33) {
        global $TAX_ATTR;
        $totalAmount = $dom->createElement(TOTAL_AMOUNT);
        $totalAmount->appendChild($dom->createElement(
                        AMOUNT, $datos_encabezado_trx33['subtotal']));

        $baseAmount = $dom->createElement(BASE_AMOUNT);
        $baseAmount->appendChild($dom->createElement(
                        AMOUNT, $datos_encabezado_trx33['subtotal']));

        $tax = $dom->createElement(TAX);
        $tax->appendChild($dom->createElement(
                        TAX_PERCENTAGE, $datos_encabezado_trx33['tasa_o_cuota']));
        $tax->appendChild($dom->createElement(
                        TAX_AMOUNT, $datos_encabezado_trx33['importe']));
        $tax->appendChild($dom->createElement(
                        TAX_CATEGORY, TAX_CATEGORY_VAL));

        $payableAmount = $dom->createElement(PAYABLE_AMOUNT);
        $payableAmount->appendChild($dom->createElement(
                        AMOUNT, $datos_encabezado_trx33['total']));

        $nodo->appendChild($totalAmount);
        $nodo->appendChild($baseAmount);
        $nodo->appendChild($tax);
        $nodo->appendChild($payableAmount);

        $this->colocar_atributos($TAX_ATTR, $tax);
    }

    public function generar_lineitem($dom, $requestForPayment, $datos_concepto, $concepto) {
        global $LINE_ITEM_ATTR;
        global $ALTERNATE_TRADE_ITEM_IDENTIFICATION_ATTR;
        global $INVOICED_QUANTITY_ATTR;
        global $REFERENCE_IDENTIFICATION_LINEITEM_ATTR;

        $lineItem = $dom->createElement(LINE_ITEM);

        $tradeItemIdentification = $dom->createElement(TRADE_ITEM_IDENTIFICATION);
        $tradeItemIdentification->appendChild($dom->createElement(
                        GTIN, $datos_concepto['numero_identificacion']));


        $alternateTradeItemIdentification = $dom->createElement(
                ALTERNATE_TRADE_ITEM_IDENTIFICATION, $datos_concepto['numero_identificacion']);

        $tradeItemDescriptionInformation = $dom->createElement(TRADE_ITEM_DESCRIPTION_INFORMATION);
        $tradeItemDescriptionInformation->appendChild($dom->createElement(LONG_TEXT,$concepto['descripcion']));

        $invoicedQuantity = $dom->createElement(
                INVOICED_QUANTITY, $concepto['importe']);

        $grossPrice = $dom->createElement(GROSS_PRICE);
        $grossPrice->appendChild($dom->createElement(
                        AMOUNT, $concepto['valor_unitario']));

        $netPrice = $dom->createElement(NET_PRICE);
        $netPrice->appendChild($dom->createElement(
                        AMOUNT, $concepto['valor_unitario']));

        $AdditionalInformation = $dom->createElement(ADDITIONAL_INFORMATION);
        $referenceIdentification = $dom->createElement(
                REFERENCE_IDENTIFICATION, $datos_concepto['orden_compra']);
        $AdditionalInformation->appendChild($referenceIdentification);

        $totalLineAmount = $dom->createElement(TOTAL_LINE_AMOUNT);
        $grossAmount = $dom->createElement(GROSS_AMOUNT);
        $grossAmount->appendChild($dom->createElement(
                        AMOUNT, $concepto['importe']));
        $netAmount = $dom->createElement(NET_AMOUNT);
        $netAmount->appendChild($dom->createElement(
                        AMOUNT, $concepto['importe']));
        $totalLineAmount->appendChild($grossAmount);
        $totalLineAmount->appendChild($netAmount);

        $lineItem->appendChild($tradeItemIdentification);
        $lineItem->appendChild($alternateTradeItemIdentification);
        $lineItem->appendChild($tradeItemDescriptionInformation);
        $lineItem->appendChild($invoicedQuantity);
        $lineItem->appendChild($grossPrice);
        $lineItem->appendChild($netPrice);
        $lineItem->appendChild($AdditionalInformation);
        $lineItem->appendChild($totalLineAmount);
        $requestForPayment->appendChild($lineItem);

        $LINE_ITEM_ATTR['number'] = $concepto['numero_linea'];
        $this->colocar_atributos($LINE_ITEM_ATTR, $lineItem);
        $this->colocar_atributos($ALTERNATE_TRADE_ITEM_IDENTIFICATION_ATTR, $alternateTradeItemIdentification);
        $this->colocar_atributos($INVOICED_QUANTITY_ATTR, $invoicedQuantity);
        $this->colocar_atributos($REFERENCE_IDENTIFICATION_LINEITEM_ATTR, $referenceIdentification);
    }

    public function colocar_atributos($array_atributos, $elemento) {
        foreach ($array_atributos as $key => $value) {
            $elemento->setAttribute($key, $value);
        }
    }

    public function cargar_datos_conceptos($id_trx33_conceptos) {
        $this->load->model("plugins/Model_comprobantes_midea");
        $concepto = $this->Model_comprobantes_midea->obtener_concepto($id_trx33_conceptos);
        return $concepto;
    }
    /* Reglas de validación para los campos de la addenda que se ingresan 
     * manualmente. Se validan los datos que se envían vía post
     *      */
    public function validar_addenda() {

        $this->form_validation->set_rules('orden_compra', '<b>Orden de compra</b>', 'required|numeric|max_length[35]');
        $this->form_validation->set_rules('gln_comprador', '<b>GLN TCM</b>', 'required|numeric|exact_length[13]');
        $this->form_validation->set_rules('seccion_departamento', '<b>Sección o Depto.</b>', 'required');
        $this->form_validation->set_rules('gln_proveedor', '<b>GLN Proveedor</b>', 'required|numeric|exact_length[13]');
        $this->form_validation->set_rules('numero_proveedor', '<b>Número de Proveedor</b>', 'required|numeric|max_length[8]');
        $this->form_validation->set_rules('gln_entrega', '<b>GLN de Sucursal de Entrega</b>', 'required|numeric|exact_length[13]');
        $this->form_validation->set_rules('dias_plazo_pago', '<b>Días de Plazo pago</b>', 'min_length[1]|max_length[5]');

        $this->form_validation->set_rules('entidad', '<b>Sucursal</b>', 'min_length[1]|max_length[35]');
        $this->form_validation->set_rules('estado', '<b>Ciudad</b>', 'min_length[1]|max_length[35]');
        $this->form_validation->set_rules('calle', '<b>Calle</b>', 'min_length[1]|max_length[35]');
        $this->form_validation->set_rules('codigo_postal', '<b>Código postal</b>', 'numeric|exact_length[5]');
        /* Si existe errores de validación */
        if ($this->form_validation->run() == FALSE) {
            /* Lo errores están en una cadena */
            $errors = explode("</p>", validation_errors());
            /* Se separa la cadena, se toma únicamente el primer error y se envía */
            echo json_encode(['error' => $errors[0]]);
            return;
        }
        echo json_encode(['success' => '200']);
    }
    /* Coloca las validaciones para los campos modificables en la lista de conceptos
     *      */
    private function validaciones_line_item(){
        $this->reglas_line_item['ordenCompra'] = [
            "attributes" => [
                "pattern"   => "[0-9]{1,35}",
                "title"     => "Únicamente números, longitud máxima 35"
            ],
            "props" => [
                "required" => true
            ],
            "class" => "orden_compra"
        ];
        $this->reglas_line_item['numeroIdentificacion'] = [
            "attributes" => [
                "pattern"   => "[0-9]{1,14}",
                "title"     => "Únicamente números, longitud máxima 14"
            ],
            "props" => [
                "required"  => true
            ],
            "class" => "numero_identificacion"
        ];                
    }
}

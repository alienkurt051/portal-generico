<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Constantes para la descarga de los detalles de la factura */
defined('RUTA_DETALLE_XML') OR 
define('RUTA_DETALLE_XML', "/interface/aicm/archivos/");
defined('INFIJO_ARCHIVO_DET') OR define('INFIJO_ARCHIVO_DET','MEX');
defined('EXTENSION_ARCHIVO_DET') OR define('EXTENSION_ARCHIVO_DET','.xls');

class Mis_comprobantes_aicm extends CI_Controller {
    
 function __construct() {
        parent::__construct();

        // si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
        $existe_sesion = $this->session->userdata("id_usuario");
        if (empty($existe_sesion)) {
            redirect(site_url(), 'refresh');
        }
    }

    /* controlador para gestionar las cuentas de usuario */
  public function index() {
        $data = array();
        // se obtienen los datos del usuario
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;

        // solo si el usuario es cliente se valida si tiene al menos un RFC asignado
        if ($pss_usuario->tipo_usuario == PERFIL_USUARIO_CLIENTE) {
            // se asume que no tiene RFCs asignados
            $sinRFCAsignado = true;

            // se obtiene la relacion de clientes asociados al usuario
            $this->db->limit(1);
            $arr_r_usuario_cliente = Model\Pss_r_usuario_cliente::find_by_id_usuario($pss_usuario->id_usuario_pss);

            $tieneIdCliente = "select count(*) as conteo from pss_r_usuario_cliente where id_usuario = " . $pss_usuario->id_usuario_pss . " limit 1";
            $rescliente     = $this->db->query($tieneIdCliente);
            $elCliente      = $rescliente->row();

            if ($elCliente->conteo == null || $elCliente->conteo < 1) {
                $sinRFCAsignado = true;
            } else {
                // al menos tiene un RFC
                $sinRFCAsignado = false;
            }
        } else {
            // es un perfil con privilegios y puede ver todo
            $sinRFCAsignado = false;
        }

        // si no hay clientes asignados
        if ($sinRFCAsignado) {
            $url_anterior = base_url("index.php/" . URL_PANTALLA_PRINCIPAL);
            $data["url_anterior"] = $url_anterior;

            $mensaje_error = "Aún no cuentas con RFCs relacionados a tu cuenta para facturar. Por favor accede a la sección Mi Perfil y crea al menos el registro de un RFC con datos fiscales.";
            $data["mensaje_error"] = $mensaje_error;
            cargar_interfaz_grafica($this, $data, 'facturar_ticket/view_content_wrapper_error_config_comprobantes', null);
        } else {
            if ($this->session->flashdata('titulo') != null) {
                $data["titulo"]         = $this->session->flashdata('titulo');
                $data["mensaje"]        = $this->session->flashdata('mensaje');
                $data["tipo_mensaje"]   = $this->session->flashdata('tipo_mensaje');
            }

            $id_usuario = $this->session->userdata("id_usuario");
            $data["id_usuario"] = $id_usuario;

            $url_anterior = base_url() . "index.php/" . URL_PANTALLA_PRINCIPAL;
            $data["url_anterior"] = $url_anterior;

            // se obtiene la lista de comprobantes del usuario firmado    
            $url_consultar_comprobantes_pss = base_url() . "index.php/plugins/mis_comprobantes_aicm/consultar_comprobantes_pss";
            $data["url_consultar_comprobantes_pss"] = $url_consultar_comprobantes_pss;

            // se arma la vista de asignatura
            cargar_interfaz_grafica($this, $data, "plugins/mis_comprobantes_aicm/view_content_wrapper_mis_comprobantes_autofactura", "plugins/mis_comprobantes_aicm/view_content_wrapper_mis_comprobantes_script");
        }
    }

    public function consultar_comprobantes_pss() {
      set_time_limit(300);
      // se obtienen los parametros de busqueda
      $rfc_emisor            = $this->input->post("rfc_emisor");
      $nombre_emisor         = $this->input->post("nombre_emisor");
      $rfc_receptor          = $this->input->post("rfc_receptor");
      $nombre_receptor       = $this->input->post("nombre_receptor");
      $pais_residencia       = $this->input->post("pais_residencia");
      $num_reg_id_trib       = $this->input->post("num_reg_id_trib");
      $serie                 = $this->input->post("serie");
      $folio_inicio          = $this->input->post("folio_inicio");
      $folio_fin             = $this->input->post("folio_fin");
      $uuid                  = $this->input->post("uuid");
      $fecha_emision_desde   = $this->input->post("fecha_emision_desde");
      $fecha_emision_hasta   = $this->input->post("fecha_emision_hasta");
      $fecha_timbrado_desde  = $this->input->post("fecha_timbrado_desde");
      $fecha_timbrado_hasta  = $this->input->post("fecha_timbrado_hasta");
      $moneda                = $this->input->post("moneda");
      $monto_desde           = $this->input->post("monto_desde");
      $monto_hasta           = $this->input->post("monto_hasta");
      $tipo_comprobante      = $this->input->post("tipo_comprobante");
      $concepto              = $this->input->post("concepto");
      $clave_prod_serv       = $this->input->post("clave_prod_serv");
      $activos               = $this->input->post("solo_activos");
      $cancelados            = $this->input->post("solo_cancelados");
      
      //Revisar que haya al menos tres campos seleccionados
      $numeroDeCampos = 0;
      if($rfc_emisor != null){
        $numeroDeCampos++;
      }
      if($nombre_emisor != null){
        $numeroDeCampos++;
      }
      if($rfc_receptor != null){
        $numeroDeCampos++;
      }
      if($nombre_receptor != null){
        $numeroDeCampos++;
      }
      if($pais_residencia != null){
        $numeroDeCampos++;
      }
      if($num_reg_id_trib != null){
        $numeroDeCampos++;
      }
      if($serie != null){
        $numeroDeCampos++;
      }
      if($folio_inicio != null){
        $numeroDeCampos++;
      }
      if($folio_fin != null){
        $numeroDeCampos++;
      }
      if($uuid != null){
        $numeroDeCampos++;
      }
      if($fecha_emision_desde != null && $fecha_emision_hasta != null){
        $numeroDeCampos++;
      }
      if($fecha_timbrado_desde != null && $fecha_timbrado_hasta != null){
        $numeroDeCampos++;
      }
      if($moneda != null){
        $numeroDeCampos++;
      }
      if($monto_desde != null){
        $numeroDeCampos++;
      }
      if($monto_hasta != null){
        $numeroDeCampos++;
      }
      
    if ( $numeroDeCampos < 0) { //Forza al usuario a utilizar al menos 2 campos para la busqueda
        $this->session->set_flashdata('titulo', "Consulta  de comprobantes");
        $this->session->set_flashdata('mensaje', "Debes seleccionar al menos 3 elementos de búsqueda.");
        $this->session->set_flashdata('tipo_mensaje', 'alert alert-warning alert-dismissible');
                
        // se regresa al formulario de alta con los datos para que el usuario corrija      
        $url_mis_cmprobantes = base_url()."index.php/plugins/Mis_comprobantes_aicm/index";
          redirect($url_mis_cmprobantes);
        }
    else{
      // se validan los campos
      if ( $folio_inicio != "" || $folio_fin != "" ) {
          $this->form_validation->set_rules('folio_inicio', 'Folio inicial', 'required');
          $this->form_validation->set_rules('folio_fin', 'Folio final', 'required');
      }
      
      if ( $fecha_emision_desde != null || $fecha_emision_hasta != null ) {
          $this->form_validation->set_rules('fecha_emision_desde', 'Fecha de emisión inicial', 'required');
          $this->form_validation->set_rules('fecha_emision_hasta', 'Fecha de emisión final', 'required');
      }
      
      if ( $fecha_timbrado_desde != null || $fecha_timbrado_hasta != null ) {
          $this->form_validation->set_rules('fecha_timbrado_desde', 'Fecha de timbrado inicial', 'required');
          $this->form_validation->set_rules('fecha_timbrado_hasta', 'Fecha de timbrado final', 'required');
      }
      
      if ( $monto_desde != null || $monto_hasta != null ) {
          $this->form_validation->set_rules('monto_desde', 'Monto inicial', 'required|numeric');
          $this->form_validation->set_rules('monto_hasta', 'Monto final', 'required|numeric');
      }
      
      // si no se enviaron los parametros de consulta correctamente
      if ($this->form_validation->run() == false )
      {
         // se redirige a la consulta
         $this->session->set_flashdata('titulo', "Consulta de comprobantes");
         
         // si no hay mensaje es porque no se usaron filtros
         if ( validation_errors() != "" ) {
             $this->session->set_flashdata('mensaje', validation_errors());
             $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
                   
             // se regresa al formulario de alta con los datos para que el usuario corrija      
           $url_mis_cmprobantes = base_url()."index.php/plugins/Mis_comprobantes_aicm/index";
             redirect($url_mis_cmprobantes);
         }
      }
    }
      // se obtienen los datos del usuario
      $id_usuario = $this->session->userdata("id_usuario");
      
      // se obtiene la configuracion de la cuenta de usuario
      $pss_usuario = Model\Pss_usuario::find($id_usuario, false);

      $data["id_usuario"] = $id_usuario;

      // se obtiene la configuracion del portal
      $config_portal = Model\pss_config_portal::find(1);

	  
      // si el portal esta configurado a que sea por login con RFC
      if ( $config_portal->usar_contrasena == CONTROL_ACCESO_SOLO_LOGIN_RFC ) {
          // el login es el RFC del usuario
          $where_rfcs        = " rfc_receptor = '".$pss_usuario->login."'";
          $where_id_clientes = " 1 = 1 ";
      } else {
		  $where_rfcs        = " 1 = 1 ";
          $where_id_clientes = " 1 = 1 ";
	  }
 
    $data = array();
      
    $url_consultar_comprobantes_pss = base_url()."index.php/plugins/mis_comprobantes_aicm/consultar_comprobantes_pss";
    $data["url_consultar_comprobantes_pss"] = $url_consultar_comprobantes_pss;
    
    $this->load->model("model_mis_comprobantes_pss");
    // se cuenta cuantos registros seran
    
    $respuesta_consulta = $this->model_mis_comprobantes_pss->buscar_comprobantes($id_usuario, $pss_usuario->tipo_usuario, 2, $where_id_clientes, $where_rfcs, $rfc_emisor, $nombre_emisor, $rfc_receptor, $nombre_receptor, $pais_residencia, $num_reg_id_trib, $serie, $folio_inicio, $folio_fin, $uuid, $fecha_emision_desde, $fecha_emision_hasta, $fecha_timbrado_desde, $fecha_timbrado_hasta, $moneda, $monto_desde, $monto_hasta, $tipo_comprobante, $concepto, $clave_prod_serv);
    $arrcomprobantes = $respuesta_consulta["arr_transacciones"];
    $data["arrcomprobantes"] = $arrcomprobantes;
      
    // urls para descarga de pdf y xml
    $url_descarga_pdf           = base_url()."index.php/mis_comprobantes_pss/descargar_pdf";
    $url_descarga_xml           = base_url()."index.php/mis_comprobantes_pss/descargar_xml";
    $url_descarga_detalle_xml   = base_url()."index.php/plugins/mis_comprobantes_aicm/validar_existencia_archivo/";
    $url_envio_correo           = base_url()."index.php/mis_comprobantes_pss/enviar_correo";
    $url_envio_correo_masivo    = base_url()."index.php/mis_comprobantes_pss/url_envio_correo_masivo";
    $url_reporte_excel          = base_url()."index.php/mis_comprobantes_pss/reporte_excelpss";
    $url_docs_anexos            = base_url()."index.php/mis_comprobantes_pss/anexos_descarga";
    $url_download_masivo        = base_url()."index.php/mis_comprobantes_pss/download_masivo";
    $url_merge_masivo           = base_url()."index.php/mis_comprobantes_pss/merge_masivo";
    $url_acuse_cancelacion      = base_url()."index.php/mis_comprobantes_pss/acuse_cancelacion";
    $url_acuse_cancelacion_PDF  = base_url()."index.php/mis_comprobantes_pss/acuse_cancelacion_PDF";
    global $arrcomprobantes;
    $data["url_download_masivo"]        = $url_download_masivo;
    $data["url_merge_masivo"]           = $url_merge_masivo;
    $data["url_descarga_pdf"]           = $url_descarga_pdf;
    $data["url_descarga_xml"]           = $url_descarga_xml;
    $data["url_descarga_detalle_xml"]   = $url_descarga_detalle_xml;
    $data["infijo_archivo_det"]         = INFIJO_ARCHIVO_DET;
    $data["extension_archivo_det"]      = EXTENSION_ARCHIVO_DET;
    $data["url_envio_correo"]           = $url_envio_correo;
    $data["url_envio_correo_masivo"]    = $url_envio_correo_masivo;
    $data["url_reporte_excelpss"]       = $url_reporte_excel; 
    $data["url_docs_anexos"]            = $url_docs_anexos;   
    $data["url_acuse_cancelacion"]      = $url_acuse_cancelacion;
    $data["url_acuse_cancelacion_PDF"]  = $url_acuse_cancelacion_PDF;
    
    // se arma la vista de asignatura
    cargar_interfaz_grafica($this, $data, "plugins/mis_comprobantes_aicm/view_content_wrapper_mis_comprobantes_autofactura", "plugins/mis_comprobantes_aicm/view_content_wrapper_mis_comprobantes_script");
  }    
    /* Función para descargar un archivo 
     * 
     * @param nombre_archivo Nombre del archivo que se intenta descargar
     *    */

    public function descargar_archivo($nombre_archivo) {
        /* Cabeceras para descargar el archivo */
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . basename($nombre_archivo) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($nombre_archivo));
        readfile($nombre_archivo);
        exit;
    }

    /* Valida si el archivo ya existe en el servidor, si existe lo intenta descargar
     * 
     * @param $nombre_archivo Nombre del archivo que se validará e intentará descargar
     * @return json           Estatus de la descarga
     *    */

    public function validar_existencia_archivo($pathname) {
        $ruta_archivo = RUTA_DETALLE_XML . $pathname;
        if (file_exists($ruta_archivo)) {
            /* Si existe el archivo lo descarga */
            $this->descargar_archivo($ruta_archivo);
            return;
        }
         $this->session->set_flashdata('titulo', "Error al descargar detalle de la factura");
         $this->session->set_flashdata('mensaje', 'Por favor consulte al área de finanzas');
         $this->session->set_flashdata('tipo_mensaje', 'alert alert-warning alert-dismissible');

        // se regresa al formulario de alta con los datos para que el usuario corrija      
        $url_mis_cmprobantes = base_url()."index.php/plugins/Mis_comprobantes_aicm/index";
        redirect($url_mis_cmprobantes);        
    }

}
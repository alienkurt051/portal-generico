<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Liberar_ticket_decathlon extends CI_Controller {

 function __construct() {
        parent::__construct();
        
        // si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
        $existe_sesion = $this->session->userdata("id_usuario");
        if ( empty( $existe_sesion ) ) {
            redirect(site_url(),'refresh');
        }
    }
    
    /**
     * Inicio de sesion
     *
     */
    public function index() {
		// se crea el arreglo para paso de parametros
		$data = array();
				
		// url de la vista siguiente, si es correcta la informacion
		$url_vista_previa = base_url("index.php/plugins/liberar_ticket_decathlon/busqueda_ticket");
		$data["url_vista_previa"] = $url_vista_previa;

		// url de la imagen guia del ticket
		$url_guia_ticket = base_url()."assets/imgcustom/ticket.jpg";
		$data["url_guia_ticket"] = $url_guia_ticket;
		
		 // se obtiene la lista de entidades que si tienen serie definida para facturar
        $series_entidades = Model\Pss_series_entidades::all();

        // si no hay sucursales con series definidas
        if ( empty($series_entidades) ) {
            $url_anterior = base_url("index.php/".URL_PANTALLA_PRINCIPAL);
            $data["url_anterior"] = $url_anterior;
            
            $mensaje_error = "Ninguna de las entidades emisoras han sido configuradas con una serie para facturar. No es posible continuar con el proceso de edición de ticket.";
            $data["mensaje_error"] = $mensaje_error;
            cargar_interfaz_grafica($this, $data, 'plugins/modifica_ticket/view_content_wrapper_error_config_facturacion', null);
        } else {
            $entidades_in = array();
            $i = 0;
            foreach ($series_entidades as $serie_entidad) {
                $entidades_in[$i] = $serie_entidad->id_entidad;
                $i++;
            }

            // arreglo de entidades
            $this->db->where_in("id_entidad",$entidades_in);
            $arr_entidades = Model\V_pss_entidades::all();
            $data["arr_entidades"] = $arr_entidades;
            
            $url_anterior = base_url("index.php/".URL_PANTALLA_PRINCIPAL);
            $data["url_anterior"] = $url_anterior;
            
            // campos para busqueda de transaccion
            $arr_campos_transaccion = Model\V_pss_campos_transaccion::all();
            $data["arr_campos_transaccion"] = $arr_campos_transaccion;
            
            if ( $this->session->flashdata('titulo') != null ) {
              $data["titulo"]       = $this->session->flashdata('titulo');
              $data["mensaje"]      = $this->session->flashdata('mensaje');
              $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
            }
            
            cargar_interfaz_grafica($this, $data, 'plugins/modifica_ticket/view_content_wrapper_captura_busqueda_ticket', 'plugins/modifica_ticket/view_script_captura_facturacion');
		}
	}
    
	// funcion que verifica si la transaccion existe
    public function busqueda_ticket() {
		
		// se crea el arreglo para paso de parametros
		$data = array();
        
        $id_entidad = $this->input->post("sucursal");
        $arr_campos = array();
        
        // se obtiene la configuracion del portal
        $config_portal = Model\Pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;
        
		// se obtiene la lista de campos flex
        $arr_campos_transaccion = Model\V_pss_campos_transaccion::all();
		$data["arr_campos_transaccion"] = $arr_campos_transaccion;
		//print_r($data["arr_campos_transaccion"]);
        //die();
		$i = 1;
        foreach ( $arr_campos_transaccion as $campo ) {
         	$arr_campos[$i]["id_flex_header"]  = $campo->id_flex_header;
            $arr_campos[$i]["campo_adicional"] = $campo->campo_adicional;
            $arr_campos[$i]["valor"]           = $this->input->post($campo->campo_adicional);
            $i++;
        }
        
		// valor de campos para transaccion a liberar
        $data["arr_valor"] = $arr_campos;
		//print_r($data["arr_valor"]);
		//die();
		
        $this->load->model("model_buscar_transaccion");
        $arr_transacciones = $this->model_buscar_transaccion->obtener_idtrx33_transaccion($arr_campos, $id_entidad);
		
        // se definen las urls
        $url_transaccion_no_encontrada = base_url("index.php/plugins/liberar_ticket_decathlon/index");
                
       // si no se encontro la transaccion
       if ( count($arr_transacciones) < 1) {
		   $this->session->set_flashdata('titulo', "Transacción no encontrada");
           $this->session->set_flashdata('mensaje', "No se encontraron transacciones con los datos utilizados para la búsqueda. Por favor, verifique sus datos e intente nuevamente.");
           $this->session->set_flashdata('tipo_mensaje', 'alert alert-warning alert-dismissible');
		   redirect($url_transaccion_no_encontrada);
        } else {
            // si existe mas de una coincidencia
            if ( count($arr_transacciones) > 1 ) {
                $this->session->set_flashdata('titulo', "Error en búsqueda de transacción");
                $this->session->set_flashdata('mensaje', "Ocurrió un error al buscar su transacción. Los datos proporcionados generaron más de un caso correcto. El administrador ha sido notificado.");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
                redirect($url_transaccion_no_encontrada);
            } else {
				// transaccion encontrada. Se obtienen los datos de la transaccion a liberar
				// recuperar datos del ticket, serie, folio, subtotal y total
                $transaccion_buscada = $arr_transacciones[1];
                $transaccion = Model\Emi_trx33_r::find($transaccion_buscada["id_trx33"], false);
				$data["transaccion"] = $transaccion;
                
                $config_portal = Model\Pss_config_portal::find(1);
                $data["config_portal"] = $config_portal;
                
                // conceptos de la transaccion
                $conceptos = $this->db->query("SELECT * FROM emi_trx33_concepto_r WHERE id_trx33_r =".$transaccion->id_trx33_r);
                $data["conceptos"] = $conceptos->result();
                
                // url para confirmar la liberacion/edicion del ticket
                $url_confirmar_liberar_modificar = base_url("index.php/plugins/liberar_ticket_decathlon/");
                
				// se asignan las urls para proceder
                // si no ha sido facturado, se puede editar
                if( $transaccion->id_receptor != $config_portal->id_cliente_autofactura ) {
                    $url_liberar_modificar_ticket            = base_url()."index.php/plugins/liberar_ticket_decathlon/confirmar_liberar_modificar_ticket/".$transaccion_buscada["id_trx33"]."/1";
                    redirect($url_liberar_modificar_ticket);
                } else {
                    // no se puede editar, solo se puede liberar
                    $url_liberar_modificar_ticket            = base_url()."index.php/plugins/liberar_ticket_decathlon/confirmar_liberar_modificar_ticket/".$transaccion_buscada["id_trx33"]."/2";
                    redirect($url_liberar_modificar_ticket);
                }
            }
        }
    }
	
    public function confirmar_liberar_modificar_ticket($id_trx33, $operacion) {
		//$this->output->enable_profiler(TRUE);
        // se crea el arreglo para paso de parametros
        $data = array();
        
        // se obtiene la configuracion del portal
        $config_portal = Model\Pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;
        
        $transaccion = Model\Emi_trx33_r::find($id_trx33, false);
        $data["transaccion"] = $transaccion;
        
        // conceptos de la transaccion
        $conceptos = $this->db->query("SELECT * FROM emi_trx33_concepto_r WHERE id_trx33_r = ".$id_trx33);
        $data["conceptos"] = $conceptos->result();
        
        // url para confirmar la liberacion/edicion del ticket
        $url_confirmar_liberar_modificar = base_url("index.php/plugins/liberar_ticket_decathlon/");
        
        $data["url_liberar_ticket"]   = base_url("index.php/plugins/liberar_ticket_decathlon/liberar_ticket");
        $data["url_modificar_ticket"] = base_url("index.php/plugins/liberar_ticket_decathlon/aplicar_edicion_ticket");
        $data["url_anterior"]         = base_url("index.php/plugins/liberar_ticket_decathlon/index");
        
        
       
        // operacion de prueba
        /*
		$operacion = 2;
        
        // si se trata de edicion de ticket, se guardan los datos
        $this->load->model("model_editar_ticket");
        $this->model_editar_ticket->respalda_transaccion($id_trx33, $this->session->userdata("id_usuario"));
        																		  //,$numero_linea,$cantidad
        $this->model_editar_ticket->modifica_cantidad_transaccion_concepto($id_trx33, 1, 6);
		$this->model_editar_ticket->modifica_cantidad_transaccion_concepto($id_trx33, 2, 4);
		$this->model_editar_ticket->modifica_cantidad_transaccion_concepto($id_trx33, 3, 3);
		
		$this->model_editar_ticket->elimina_transaccion_concepto($id_trx33, 8);
		
        //manda a llamar la funcion borrar trx33 
        $this->load->model("model_editar_ticket");
        $this->model_editar_ticket->borrar_trx33($id_trx33);
        
        $this->load->model("model_editar_ticket");
        $this->model_editar_ticket->ejecuta_sp_mueve_trx($id_trx33);
        */





        
        
        
        
        $data["operacion"] = $operacion;
        
		
		
		// url para cancelar ticket
		$url_cancelar_ticket = base_url()."index.php/plugins/liberar_ticket_decathlon/cancelar_ticket/".$id_trx33;
		$data["url_cancelar_ticket"] = $url_cancelar_ticket;
        
        // se abre la ventana para confirmar la liberacion, edicion de ticket
        cargar_interfaz_grafica($this, $data, 'plugins/modifica_ticket/view_content_wrapper_confirmar_liberar_modificar', 'plugins/modifica_ticket/view_content_wrapper_confirmar_liberar_modificar_script');

    }
	
	/* funcion que edita los registros conforme se realizo en la ventana de edicion */
	public function aplicar_edicion_ticket() {
		// se obtienen los datos de la transaccion
		$id_trx33    = $this->input->post("id_trx33");
        $transaccion = Model\Emi_trx33_r::find($id_trx33, false);
        
        // conceptos de la transaccion
        $conceptos = $this->db->query("SELECT * FROM emi_trx33_concepto_r WHERE id_trx33_r = ".$id_trx33);
		
        // si se trata de edicion de ticket, se guardan los datos
        $this->load->model("model_editar_ticket");
        $this->model_editar_ticket->respalda_transaccion($id_trx33, $this->session->userdata("id_usuario"));
        																		  //,$numero_linea,$cantidad
		
		$num_linea = 1;
        // query para recalcular impuestos
        $this->db->where("id_trx33_r",$id_trx33);
        $conceptos = Model\Emi_trx33_concepto_r::all();
        
        foreach ($conceptos as $concepto) {
			// obtiene el estatus y la cantidad de la linea modificada.
			// =================== IMPORTANTE =====================
			// numero_linea contiene el id_trx33_concepto_r, no el numero de linea porque en instalaciones viejas, la columna num_linea puede estar vacia
			$id_estatus_linea  = "id_estatus_linea".$num_linea;
			$id_cantidad_linea = "cantidad".$num_linea;
			$id_numero_linea   = "numero_linea".$num_linea;
			$estatus_linea     = $this->input->post($id_estatus_linea);
			$cantidad_linea    = $this->input->post($id_cantidad_linea);
			$numero_linea      = $this->input->post($id_numero_linea);
			
			// si la linea se debe borrar
			if ( $estatus_linea == 0 ) {
				$this->model_editar_ticket->elimina_transaccion_concepto($id_trx33, $numero_linea);
			} else {
				// se actualiza la cantidad
				$this->model_editar_ticket->modifica_cantidad_transaccion_concepto($id_trx33, $numero_linea, $cantidad_linea);
			}
			
			$num_linea++;
		}
		
        //manda a llamar la funcion borrar trx33, para que recargue la informacion en la emi_trx33 editada formateada
        $this->model_editar_ticket->borrar_trx33($id_trx33);
        
		// se actualizan los datos
        $this->model_editar_ticket->ejecuta_sp_mueve_trx($id_trx33);
		
	    $this->session->set_flashdata('titulo', "Datos transacción");
	    $this->session->set_flashdata('mensaje', "Los datos del ticken han sido actualizados correctamente");
	    $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
	    $url_inicio_liberar_ticket = base_url()."index.php/plugins/liberar_ticket_decathlon/";
	    redirect($url_inicio_liberar_ticket);
	}
	
	// funcion que cambia de estatus un ticket de  facturable a no facturable
	public function cancelar_ticket($id_trx33) {
		// se carga el modelo
		$this->load->model("model_editar_ticket");
        $this->model_editar_ticket->cancelar_ticket($id_trx33);
		
		// se confirma la operacion en pantalla
	    $this->session->set_flashdata('titulo', "Cancelación de ticket");
	    $this->session->set_flashdata('mensaje', "El ticket ha sido cancelado exitosamente. Ya no es facturable.");
	    $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
	    $url_inicio_liberar_ticket = base_url()."index.php/plugins/liberar_ticket_decathlon/";
	    redirect($url_inicio_liberar_ticket);
	}
	
	/* funcion que obtiene los datos de un ticket y regenera un layout para que sea reimportado al sistema */
	public function liberar_ticket(){
		
		$id_trx33_r_liberar = $this->input->post("id_trx33");
		
		$trx33 = Model\Emi_trx33::find_by_id_trx33($id_trx33_r_liberar);
		$trx33_r = Model\Emi_trx33_r::find_by_id_trx33_r($id_trx33_r_liberar);
		
        // se obtienen los parametros de configuracion del portal
        $config_portal = Model\pss_config_portal::find(1);
        
        // se obtiene la ruta de importacion del proceso
        $this->db->where("id_proceso",$config_portal->id_proceso_liberacion_ticket);
        $this->db->where("secuencia = 1");
        $etapas_procesos = Model\C_etapas_procesos::all();
        
        foreach ( $etapas_procesos as $etapa ) {
            $directorio_carga_tickets = $etapa->ruta_entrada;
            break;
        }
        
		//EMISOR
		foreach ($trx33 as $trx33) {
				$emisor_rfc                      = $trx33->rfc_emisor;  
				$emisor_nombre                   = $trx33->nombre_emisor;
				$emisor_regimen_fiscal           = $trx33->regimen_fiscal;
				$total_impuestos_retenidos 		 = $trx33->total_impuestos_retenidos;
				$total_impuestos_trasladados 	 = $trx33->total_impuestos_trasladados;
		}
		 
		 //DOCUMENTO
		 foreach ($trx33_r as $trx33_r) {
			$fecha                  = $trx33_r->fecha;
			//$fecha                  = date('Y-m-d H:i:s');
			$forma_de_pago          = $trx33_r->id_forma_pago;
			$condiciones_de_pago    = $trx33_r->condiciones_de_pago;
			$tipo_cambio            = $trx33_r->tipo_cambio; 
			$moneda                 = $trx33_r->id_moneda;
			$metodo_pago            = 'PUE';//$trx33_r->id_metodo_pago;
			$lugar_expedicion       = $trx33_r->id_lugar_expedicion; 
			$tipo_comprobante       = $trx33_r->id_tipo_de_comprobante;
			$subtotal               = $trx33_r->subtotal;
			$descuento              = $trx33_r->descuento;
			if ( $descuento == '' || $descuento == null ) {
				$descuento=0;
			}
			$enviar_xml                 = $trx33_r->envia_xml;
			$enviar_pdf                 = $trx33_r->envia_pdf;
			$enviar_zip                 = $trx33_r->envia_zip;
			$email_envio                = $trx33_r->email_envio;
			$total                      = $trx33_r->total;
			$confirmacion               = $trx33_r->confirmacion;
			$tipo_documento             = $trx33_r->id_tipo_documento;
			$id_trx_erp                 = $trx33_r->id_trx_erp;
			$totalImpuestosRetenidos    = $trx33_r->totalImpuestosRetenidos;
			$totalImpuestosTrasladados  = $trx33_r->totalImpuestosTrasladados;
			$serie_original             = $trx33_r->serie;
			$folio_original             = $trx33_r->folio;
			$emisor_id_emisor_sto       = $trx33_r->id_emisor;
			$serie                      = null;
			$folio                      = null;
			$id_sucursal                = $trx33_r->id_sucursal;
		 }
		  
		 // si se tiene sucursal
         if ( $trx33_r->id_sucursal != null ) {
             $entidades = $this->db->query("SELECT * FROM c_entidades WHERE id_entidad = ".$id_sucursal. ";");
         } else {
             // se hizo emision desde la matriz
             $entidades = $this->db->query("SELECT * FROM c_entidades WHERE id_entidad = ".$emisor_id_emisor_sto. ";");
         }

		 foreach($entidades->result() as $entidad){
				$rfc_sucursal = $entidad->rfc;
				$entidad_sucursal = $entidad->entidad;
				$id_regimen_sucursal = $entidad->id_regimen;
				$numero_interior_sucursal = $entidad->numero_interior;
				$numero_exterior_sucursal = $entidad->numero_exterior;
				$calle_sucursal = $entidad->calle;
				$colonia_sucrusal = $entidad->colonia;
				$localidad_sucursal = $entidad->localidad;
				$referencia_sucursal = $entidad->referencia;
				$municipio_sucursal = $entidad->municipio;
				$estado_sucursal = $entidad->estado;
				$pais_sucursal = $entidad->pais;
				$codigo_postal_sucursal = $entidad->codigo_postal;
				$email_sucursal = $entidad->email;
				$id_tipo_emisor_sucursal = $entidad->id_tipo_entidad;
				$id_tipo_entidad_padre_sucursal = $entidad->id_entidad_padre;
				$estatus_sucursal = $entidad->estatus;
			}
			
		//Recuperar id_cliente de la tabla de confoguracion del portal, para agregar datos del receptor.
		$pss_config = Model\Pss_config_portal::find(1, false);
		$id_cliente_receptor = $pss_config->id_cliente_autofactura;
		
		//RECEPTOR
		$receptor = Model\C_clientes::find_by_id_cliente($id_cliente_receptor);
		foreach($receptor as $receptorD){
			$rfc_receptor               = $receptorD->rfc;
			$estatus                    = $receptorD->estatus;
		}
		
		//FLEX_HEADERS
		$flex_headers = "";
		$flex_header = Model\Emi_trx33_inf_adic::find_by_id_trx33($id_trx33_r_liberar);

		foreach($flex_header as $flex_headerV){
			$flex_headers .=<<<XML
			<FLEX_HEADER clave="$flex_headerV->id_flex_header" nombre="$flex_headerV->id_flex_header" valor="$flex_headerV->valor"/>				
XML;
		}

		
		//CONCEPTOS
		$c = 1;
		$conceptos = "";
		$concepto_r = Model\Emi_trx33_concepto_r::find_by_id_trx33_r($id_trx33_r_liberar);
		foreach($concepto_r as $concepto){
			
			$descripcion_concepto = htmlspecialchars($concepto->descripcion, ENT_QUOTES, 'UTF-8');
			
			$conceptos_impuestos = false;
			$conceptos .=<<<XML
		<CONCEPTO numero_linea="$c" clave_prod_serv="$concepto->id_claveprodserv" cantidad="$concepto->cantidad" clave_unidad="$concepto->id_clave_unidad" unidad="$concepto->unidad" num_identificacion="$concepto->numero_identificacion" descripcion="$descripcion_concepto" valor_unitario="$concepto->valor_unitario" importe="$concepto->importe" descuento="$concepto->descuento" >
XML;
			$concepto_impuesto_r = Model\Emi_trx33_con_impuestos_r::find_by_id_trx33_concepto_r($concepto->id_trx33_concepto_r);
			
			foreach($concepto_impuesto_r as $concepto_i){
				if($conceptos_impuestos == false){
					$conceptos .="\n".<<<XML
				<IMPUESTOS>
XML;
				}
				//trasladado
				if($concepto_i->tipo_impuesto == 1){
					$conceptos_impuestos = true;
					$conceptos .="\n".<<<XML
					<TRASLADADOS>
						<IMPUESTO base="$concepto_i->base" impuesto="$concepto_i->impuesto" tipo_factor="$concepto_i->tipo_factor" tasa_o_cuota="$concepto_i->tasa_cuota" importe="$concepto_i->importe"/>
					</TRASLADADOS>				
XML;
				}else
					if($concepto_i->tipo_impuesto == 2){
						//retenido
						$conceptos_impuestos = true;
						$conceptos .="\n".<<<XML
					<RETENIDOS>
						<IMPUESTO base="$concepto_i->base" impuesto="$concepto_i->impuesto" tipo_factor="$concepto_i->tipo_factor" tasa_o_cuota="$concepto_i->tasa_cuota" importe="$concepto_i->importe" />
					</RETENIDOS>
XML;
					}
					
					if($conceptos_impuestos == true){
					$conceptos .="\n".<<<XML
				</IMPUESTOS>
XML;
					}
					// si tiene datos de pedimento
					if ( $concepto->info_aduanera_num_ped != null && $concepto->info_aduanera_num_ped != "") {
						$conceptos.= "\n".<<<XML
				         <ADUANA numero_pedimento="$concepto->info_aduanera_num_ped"/>
XML;
					}
			}

			$conceptos .="\n".<<<XML
			</CONCEPTO>
XML;
			$c++;
		}
		
		//IMPUESTOS
		$impuestos = Model\Emi_trx33_impuestos_r::find_by_id_trx33_r($id_trx33_r_liberar);
		$trasladados = "";
		$retenidos = "";
		foreach($impuestos as $impuesto){
			//trasladado
			if($impuesto->tipo_impuesto == 1){
				$trasladados =<<<XML
		<IMPUESTO impuesto="$impuesto->impuesto" tipo_factor="$impuesto->tipo_factor" tasa_o_cuota="$impuesto->tasa_o_cuota" importe="$impuesto->importe"/>
XML;
			}else
				if($impuestos->tipo_impuesto == 2){
					//retenido
					$retenidos =<<<XML
		<IMPUESTO impuesto="$impuesto->impuesto" tipo_factor="$impuesto->tipo_factor" tasa_o_cuota="$impuesto->tasa_o_cuota" importe="$impuesto->importe"/>		
XML;
				}
		}
        
        // cadena del emisor. Si se tiene sucursal
        if ( $trx33_r->id_sucursal != null ) {
            $cadena_emisor = <<<XML
			<EMISOR rfc="$emisor_rfc" nombre="$emisor_nombre" regimen_fiscal="$emisor_regimen_fiscal" id_emisor_sto="$emisor_id_emisor_sto" id_emisor_erp="$emisor_id_emisor_sto">
				<SUCURSAL rfc="$rfc_sucursal" nombre="$entidad_sucursal" regimen_fiscal="$id_regimen_sucursal" id_emisor_sto="$id_sucursal" id_emisor_erp="$emisor_id_emisor_sto" numero_interior="$numero_interior_sucursal" numero_exterior="$numero_exterior_sucursal" calle="$calle_sucursal"
					colonia="$colonia_sucrusal" localidad="$localidad_sucursal" referencia="$referencia_sucursal" municipio="$municipio_sucursal" estado="$estado_sucursal" pais="$pais_sucursal" codigo_postal="$codigo_postal_sucursal" email="$email_sucursal" id_tipo_emisor="$id_tipo_emisor_sucursal" id_emisor_padre="$id_tipo_entidad_padre_sucursal" estatus_registro="$estatus_sucursal"/>
			</EMISOR>
XML;
        } else {
            // solo se tiene emisor
            $cadena_emisor = <<<XML
			<EMISOR rfc="$emisor_rfc" nombre="$emisor_nombre" regimen_fiscal="$emisor_regimen_fiscal" id_emisor_sto="$emisor_id_emisor_sto" id_emisor_erp="$emisor_id_emisor_sto">
			</EMISOR>
XML;
        }
			
		 $archivo_importacion= <<<XML
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<DOCUMENTOS>
	<DOCUMENTO serie="" folio="" fecha="$fecha" forma_de_pago="$forma_de_pago" tipo_cambio="$tipo_cambio" moneda="$moneda" metodo_pago="$metodo_pago" lugar_expedicion="$lugar_expedicion" tipo_comprobante="$tipo_comprobante" 
				subtotal="$subtotal" descuento="$descuento" total="$total" tipo_documento="$tipo_documento">
		<DOCUMENTO_ERP id_trx_erp="$id_trx_erp"/>
		<ENVIO_CFDI enviar_xml="$enviar_xml" enviar_pdf="$enviar_pdf" enviar_zip="$enviar_zip" email="$email_envio"/>
        $cadena_emisor 
		<RECEPTOR rfc="XAXX010101000" nombre="" residencia_fiscal="" num_reg_id_trib="" uso_cfdi="G01" id_receptor_sto="$id_cliente_receptor" id_receptor_erp="$id_cliente_receptor" numero_exterior="" calle="" numero_interior="" colonia="" localidad="" referencia="" municipio="" estado="" pais="" codigo_postal="" email="" id_tipo_receptor="" id_receptor_padre="" estatus_registro="1" >
		</RECEPTOR>
XML;
		if($flex_headers != ""){
		$archivo_importacion .= <<<XML
		<FLEX_HEADERS>
				$flex_headers
		</FLEX_HEADERS>
XML;
		}
		if($conceptos != ""){
		$archivo_importacion .= "\n". <<<XML
		<CONCEPTOS>
	$conceptos
		</CONCEPTOS>
XML;
		}
		$archivo_importacion .="\n".<<<XML
		<IMPUESTOS
XML;
		if($total_impuestos_retenidos != "" && $total_impuestos_retenidos != null){
	$archivo_importacion .=<<<XML
	totalImpuestosRetenidos="$total_impuestos_retenidos"
XML;
		}
		if($total_impuestos_trasladados != "" && $total_impuestos_trasladados != null){
	$archivo_importacion .=<<<XML
	totalImpuestosTrasladados="$total_impuestos_trasladados"
XML;
		}
		
		$archivo_importacion .=<<<XML
>
XML;
		if($trasladados != ""){
			$archivo_importacion .="\n".<<<XML
			<TRASLADADOS>
		$trasladados
			</TRASLADADOS>
XML;
		}
		
		if($retenidos != ""){
			$archivo_importacion .="\n".<<<XML
			<RETENIDOS>
				$retenidos
			</RETENIDOS>
XML;
		}
		
		$archivo_importacion .="\n".<<<XML
		</IMPUESTOS>
	</DOCUMENTO>
</DOCUMENTOS>
XML;

		//Modicar flex_header de la transaccion
		$verificar_flex = $this->db->query("SELECT IF(SUBSTRING(valor, -1) = 'L', 'T', 'F') as resultado FROM emi_trx33_inf_adic WHERE id_trx33 = ".$id_trx33_r_liberar.";");
		foreach($verificar_flex->result() as $verfica){
				$resultado_flex = $verfica->resultado;
				if($resultado_flex == "F"){
                                    $this->load->model("model_buscar_transaccion");
                                    // Si no se pueden actualizar los flex 
                                    if(!$this->model_buscar_transaccion->actualizar_flex_liberacion($id_trx33_r_liberar)){
                                        $this->session->set_flashdata('titulo', 
                                            "Error actualización de información");
                                        $this->session->set_flashdata('mensaje', 
                                            "No fue posible actualizar la información del ticket con id: "
                                            .$id_trx33_r_liberar
                                            ." para proceder a con la liberación");
                                        $this->session->set_flashdata('tipo_mensaje', 
                                            'alert alert-error alert-dismissible');
                                        $url_login = base_url()."index.php/principal_ss";
                                        redirect($url_login);
                                    }
                                    break; // Si se actualizan se sale de blucle
			}
		}
		
		// se marca el id_trx_erp (ticket) como liberado
		$trx33_r->id_trx_erp = $trx33_r->id_trx_erp."L".$trx33_r->id_trx33_r;
		$trx33_r->save();
		
		//Guardarlos en la ruta para mandar el xml generado a Importacion XML
		$folder = $directorio_carga_tickets;
                $name='re_fac_'.$id_trx33_r_liberar."_trx_".$id_trx33_r_liberar.".xml";
                $file_name = $folder.$name;
                $control = fopen($file_name,"w+");                
                if(file_exists($file_name)){
                    if(fwrite($control, $archivo_importacion)){			           
	                    $this->session->set_flashdata('titulo', "Datos transacción");
	                    $this->session->set_flashdata('mensaje', "Los datos han sido actualizados y el XML creado correctamente: ".$file_name);
	                    $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
	                    $url_login = base_url()."index.php/principal_ss";
	                    redirect($url_login);
			        }else{
			            $this->session->set_flashdata('titulo', "Datos transacción");
	                    $this->session->set_flashdata('mensaje', "No se pudo Escribir el XML correctamente");
	                    $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
	                    $url_login = base_url()."index.php/principal_ss";
	                    redirect($url_login);
			        }
                    #  Se hace el ciere para no sobre escribir datos 
                    
                }else{
                    $this->session->set_flashdata('titulo', "Datos transacción");
                    $this->session->set_flashdata('mensaje', "No se pudo crear el archivo xml, intente más tarde: ".$file_name);
                    $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
                    $url_login = base_url()."index.php/principal_ss";
                    redirect($url_login);
                }
                fclose($control);
	}	
}
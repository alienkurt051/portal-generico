<?php

/* Archivo que almacena las constantes que se ocupan para formar el nodo
 * ADDENDA
 * DATOS PARA CLIENTE MIDEA
 * 
 * @author  Iván Mondragón
 *  */

/* Encabezado */
const ADDENDA                               = 'cfdi:Addenda';
/* Número de factura */
const REQUEST_FOR_PAYMENT                   = 'requestForPayment';
const REQUEST_FOR_PAYMENT_IDENTIFICATION    = 'requestForPaymentIdentification';
const ENTITY_TYPE                           = 'entityType';
const UNIQUE_CREATOR_IDENTIFICATION         = 'uniqueCreatorIdentification';

const SPECIAL_INSTRUCTION                   = 'specialInstruction';
const TEXT                                  = 'text';

/* Orden de compra referencia */
const ORDER_IDENTIFICATION                  = 'orderIdentification';
const REFERENCE_IDENTIFICATION              = 'referenceIdentification';
const REFERENCE_DATE                        = 'ReferenceDate';

/* Número de aprovación SAT */
const ADDITIONAL_INFORMATION                = 'AdditionalInformation';
//const REFERENCE_IDENTIFICATION              = 'referenceIdentification';

/* GLN TCM */
const BUYER                                 = 'buyer';
const GLN                                   = 'gln';
const CONTACT_INFORMATION                   = 'contactInformation';
const PERSON_OR_DEPARTMENT_NAME             = 'personOrDepartmentName';
/* Sección o depto. */
//const TEXT                                  = 'text';

/* GLN proveedor */
const SELLER                                = 'seller';
//const GLN                                   = 'gln';
/* Número de proveedor */
const ALTERNATE_PARTY_IDENTIFICATION        = 'alternatePartyIdentification';

/* GLN de sucursal de entrega */
const SHIPTO                                = 'shipTo';
//const GLN                                   = 'gln';
/* Dirección sucursal */
const NAME_AND_ADDRESS                      = 'nameAndAddress';
const NAME                                  = 'name';
const STREET_ADDRESS_ONE                    = 'streetAddressOne';
const CITY                                  = 'city';
const POSTAL_CODE                           = 'postalCode';

/* Tipo de cambio moneda (constante)*/
const CURRENCY                              = 'currency';
const CURRENCY_FUNCTION                     = 'currencyFunction';
const RATE_OF_CHANGE                        = 'rateOfChange';

/* Días de plazo pago */
const PAYMENT_TERMS                         = 'paymentTerms';
const NET_PAYMENT                           = 'netPayment';
const PAYMENT_TIME_PERIOD                   = 'paymentTimePeriod';
const TIME_PERIOD_DUE                       = 'timePeriodDue';
const VALUE                                 = 'value';

/* Nodos de totales */
/* Subtotal factura */
const TOTAL_AMOUNT                          = 'totalAmount';
const AMOUNT                                = 'Amount';

const BASE_AMOUNT                           = 'baseAmount';
//const AMOUNT                                = 'Amount';

/* Impuesto, porcentaje e importe */
const TAX                                   = 'tax';
const TAX_PERCENTAGE                        = 'taxPercentage';
const TAX_AMOUNT                            = 'taxAmount';
const TAX_CATEGORY                          = 'taxCategory';

/* Total factura */
const PAYABLE_AMOUNT                        = 'payableAmount';
//const AMOUNT                                = 'Amount';


/* Nodo para cada lineItem */
const LINE_ITEM = 'lineItem';

const TRADE_ITEM_IDENTIFICATION             = 'tradeItemIdentification';
const GTIN                                  = 'gtin';

const ALTERNATE_TRADE_ITEM_IDENTIFICATION   = 'alternateTradeItemIdentification';

const TRADE_ITEM_DESCRIPTION_INFORMATION    = 'tradeItemDescriptionInformation';
const LONG_TEXT                             = 'longText';

const INVOICED_QUANTITY                     = 'invoicedQuantity';

const GROSS_PRICE                           = 'grossPrice';
//const AMOUNT                                = 'Amount';

const NET_PRICE                             = 'netPrice';
//const AMOUNT                                = 'Amount';

//const ADDITIONAL_INFORMATION                = 'AdditionalInformation';
//const REFERENCE_IDENTIFICATION              = 'referenceIdentification';

const TOTAL_LINE_AMOUNT                     = 'totalLineAmount';
const GROSS_AMOUNT                          = 'grossAmount';
//const AMOUNT                                = 'Amount';
const NET_AMOUNT                            = 'netAmount';
//const AMOUNT                                = 'Amount';

/* Atributos de nodos en encabezado de addenda */
$REQUEST_FOR_PAYMENT_ATTR = [
    'type'                      => 'SimpleInvoiceType',
    'contentVersion'            => '1.3.1',
    'documentStructureVersion'  => 'AMC7.1',
    'documentStatus'            => 'ORIGINAL', //constante por requerimiento
    'DeliveryDate'              => null,
];
$SPECIAL_INSTRUCTION_ATTR = [
    'code'                      => 'ZZZ'
];
$REFERENCE_IDENTIFICATION_ORDER_ATTR = [
    'type'                      => 'ON'
];
/* del nodo additionalinformation checar si no es necesario repetir */
$REFERENCE_IDENTIFICATION_ADDI_ATTR = [
    'type' => 'ATZ'
];
$ALTERNATE_PARTY_IDENTIFICATION_ATTR = [
    'type'                      => 'SELLER_ASSIGNED_IDENTIFIER_FOR_A_PARTY'
];
$CURRENCY_ATTR = [
    'currencyISOCode'           => "MXN" //Constante
];
$PAYMENT_TERMS_ATTR = [
    'paymentTermsEvent'         => 'DATE_OF_INVOICE',
    'paymentTermsRelationTime'  => 'REFERENCE_AFTER'    
];
$NET_PAYMENT_ATTR = [
    'netPaymentTermsType'       => 'BASIC_NET'
];
$TIME_PERIOD_DUE_ATTR = [
    'timePeriod'                => "DAYS"
];

/* Atributos de nodo lineItem */
$LINE_ITEM_ATTR = [
    'type'                      => 'SimpleInvoiceLineItemType', 
    'number'                    => null
];
/* Atributos constantes en el concepto que está exento de iva */
$ALTERNATE_TRADE_ITEM_IDENTIFICATION_ATTR = [
    'type'                      => 'BUYER_ASSIGNED'
];
$INVOICED_QUANTITY_ATTR = [
    'unitOfMeasure'             => 'PCE'
];
/* del nodo additionalinformation checar si no es necesario repetir */
$REFERENCE_IDENTIFICATION_LINEITEM_ATTR = [
    'type' => 'ON'
];

/* Atributos de totales */
$TAX_ATTR = [
    'type'                      => 'VAT'
];
/* Valores constantes de nodos en addenda */
const CURRENCY_FUNCTION_VAL                     = 'BILLING_CURRENCY';
const RATE_OF_CHANGE_VAL                        = '1.00';

const TAX_CATEGORY_VAL                          = 'TRANSFERIDO';
const ENTITY_TYPE_VAL                           = 'INVOICE';
const REFERENCE_IDENTIFICATION_VAL              = 0;
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facturar extends CI_Controller {

 function __construct() {
        parent::__construct();
        
        // si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
        $existe_sesion = $this->session->userdata("id_usuario");
        if ( empty( $existe_sesion ) ) {
            redirect(site_url(),'refresh');
        }
    }
    
    /**
     * Inicio de sesion
     * 20180705 Se quita el id_cliente que es usado para determinar si la transaccion ya ha sido facturada de la busqueda de id_clientes disponibles para el usuario
     * 
     */
    public function index() {

    
        // se crea el arreglo para paso de parametros
        $data = array();
        
        // se obtiene la configuracion del portal
        $config_portal = Model\Pss_config_portal::find(1);
        
        // se obtienen los datos del usuario
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;
        
        // se obtiene la relacion de clientes asociados al usuario
		$this->load->model("model_facturar_pss");
		$arr_clientes = $this->model_facturar_pss->buscar_clientes_por_usuario($pss_usuario->id_usuario_pss, $pss_usuario->tipo_usuario, $config_portal->id_cliente_autofactura, null, null,30);

        $data["arr_clientes"] = $arr_clientes;

        // si no se encuentra el cliente
        if ( empty($arr_clientes) ) {
            $url_anterior = base_url("index.php/".URL_PANTALLA_PRINCIPAL);
            $data["url_anterior"] = $url_anterior;
            
            $mensaje_error = "Aún no cuentas con RFCs relacionados a tu cuenta para facturar. Por favor accede a la sección Mi Perfil y crea al menos el registro de un RFC con datos fiscales.";
            $data["mensaje_error"] = $mensaje_error;
            cargar_interfaz_grafica($this, $data, 'facturar_ticket/view_content_wrapper_error_config_facturacion', null);
        } else {
            // si existe solo un cliente, se envia a facturar
            if ( count($arr_clientes) == 1 ) {
                $url_facturar = base_url()."index.php/facturar/captura_datos_facturacion/".$arr_clientes[1]["id_cliente"];
                redirect($url_facturar);
            } else {
                // se envia a pantalla para que elija con que RFC quiere facturar
            
                // url anterior
                $url_anterior = base_url("index.php/".URL_PANTALLA_PRINCIPAL);
                
                $url_captura_datos_facturacion = base_url("index.php/facturar/captura_datos_facturacion");
                $data["url_captura_datos_facturacion"] = $url_captura_datos_facturacion;
                     
                $url_busqueda_rfc = base_url("index.php/facturar/busqueda_rfc");
                $data["url_busqueda_rfc"] = $url_busqueda_rfc;       
                // se transfieren los parametros al arreglo
                $data["url_anterior"]                = $url_anterior;
            
                if ( $this->session->flashdata('titulo') != null ) {
                  $data["titulo"]       = $this->session->flashdata('titulo');
                  $data["mensaje"]      = $this->session->flashdata('mensaje');
                  $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
                }
                
                cargar_interfaz_grafica($this, $data, 'facturar_ticket/view_content_wrapper_elegir_rfc_facturacion', null);
            }
        }

    }
    
    public function busqueda_rfc($rfc_busqueda, $tipo){

        $data = array();
        $url_captura_datos_facturacion = base_url("index.php/facturar/captura_datos_facturacion");
        $data["url_captura_datos_facturacion"] = $url_captura_datos_facturacion;

        $rfc_busqueda = str_replace("_", " ", $rfc_busqueda);
		
		// se decodifica la url
		$rfc_busqueda = urldecode($rfc_busqueda);
		$rfc = null;
		$razon_social = null;
		
		// busqueda por rfc
		if ( $tipo == 1 ) {
		    $rfc = $rfc_busqueda;
		    $razon_social = null;
		} else {
		    $rfc = null;
		    $razon_social = $rfc_busqueda;
		}
		
		
        $url_busqueda_rfc = base_url("index.php/facturar/busqueda_rfc");
        $data["url_busqueda_rfc"] = $url_busqueda_rfc; 
        $data['rfc_a_buscar'] = $rfc_busqueda;
		
        // se obtienen los datos del usuario
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;
		
        $url_anterior = base_url("index.php/".URL_PANTALLA_PRINCIPAL);
        $data["url_anterior"]=$url_anterior;
        
        // se obtiene la configuracion del portal
        $config_portal = Model\Pss_config_portal::find(1);
        
		$this->load->model("model_facturar_pss");
		$arr_clientes = $this->model_facturar_pss->buscar_clientes_por_usuario($pss_usuario->id_usuario_pss, $pss_usuario->tipo_usuario, $config_portal->id_cliente_autofactura, $rfc, $razon_social, 100);
		
        $data["arr_clientes"] = $arr_clientes;
        cargar_interfaz_grafica($this, $data, 'facturar_ticket/view_content_wrapper_elegir_rfc_facturacion', 'facturar_ticket/view_script_captura_facturacion');
    }

    public function captura_datos_facturacion($id_cliente) {

    
        // se creal el arreglo para paso de parametros
        $data = array();
        
        // se obtienen los datos del usuario
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;
        
        // se obtienen los datos fiscales del usuario
        $cliente = Model\C_clientes::find($id_cliente);
        $data["cliente"] = $cliente;
        
        $config_portal = Model\Pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;
        
        // catalogo uso de cfdi
        // se filtan segun si el rfc es persona fisica o moral
        if ( strlen($cliente->rfc) > 12 ) {
            // persona fisica
            $this->db->where("fisica <> 'No'");
        } else {
            // persona moral
            $this->db->where("moral <> 'No'");
        }
        
        // el uso de cfdi por omision es G03
        $uso_cfdi_elegido = "G03";
        $data["uso_cfdi_elegido"] = $uso_cfdi_elegido;
        
        $arr_uso_cfdi = Model\Emi_c_uso_cfdi::all();
        $data["arr_uso_cfdi"] = $arr_uso_cfdi;

        // url para recuperar la contrasena
        $url_anterior = base_url("index.php/".URL_PANTALLA_PRINCIPAL);
        
        $url_vista_previa = base_url("index.php/facturar/busca_transaccion");
        $data["url_vista_previa"] = $url_vista_previa;
        
        // url de la imagen guia del ticket, depende de la plantilla del portal
        $url_guia_ticket = base_url("assets") . ($config_portal->plantilla_portal == "sto" 
                ? "" : "_".$config_portal->plantilla_portal) . "/imgcustom/ticket.jpg";
        $data["url_guia_ticket"] = $url_guia_ticket;
        
        // se obtiene la lista de entidades que si tienen serie definida para facturar
        $series_entidades = Model\Pss_series_entidades::all();

        // si no hay sucursales con series definidas
        if ( empty($series_entidades) ) {
            $url_anterior = base_url("index.php/".URL_PANTALLA_PRINCIPAL);
            $data["url_anterior"] = $url_anterior;
            
            $mensaje_error = "Ninguna de las entidades emisoras han sido configuradas con una serie para facturar. No es posible continuar con el proceso de facturación.";
            $data["mensaje_error"] = $mensaje_error;
            cargar_interfaz_grafica($this, $data, 'facturar_ticket/view_content_wrapper_error_config_facturacion', null);
        } else {
            $entidades_in = array();
            $i = 0;
            foreach ($series_entidades as $serie_entidad) {
                $entidades_in[$i] = $serie_entidad->id_entidad;
                $i++;
            }

            // arreglo de entidades
            $this->db->where_in("id_entidad",$entidades_in);
            $this->db->order_by("entidad"); 
            $arr_entidades = Model\V_pss_entidades::all();
            $data["arr_entidades"] = $arr_entidades;
            
            // se transfieren los parametros al arreglo
            $data["url_anterior"]                = $url_anterior;
            
            // campos para busqueda de transaccion
            $arr_campos_transaccion = Model\V_pss_campos_transaccion::all();
            $data["arr_campos_transaccion"] = $arr_campos_transaccion;

			// se obtiene la lista de flex headers que se agregan como campos adicionales
            $this->load->model("Model_buscar_transaccion");

            //$arr_campos_fh = $this->db->query("SELECT * FROM emi_c_info_adicionales WHERE estatus = 1;");

            $arr_campos_fh = $this->Model_buscar_transaccion->obtener_estatus();


            $data["arr_campos_fh"] = $arr_campos_fh;				
            
            if ( $this->session->flashdata('titulo') != null ) {
              $data["titulo"]       = $this->session->flashdata('titulo');
              $data["mensaje"]      = $this->session->flashdata('mensaje');
              $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
            }
            
            cargar_interfaz_grafica($this, $data, 'facturar_ticket/view_content_wrapper_captura_facturacion', 'facturar_ticket/view_script_captura_facturacion');
        }
        


    }

    
    public function ajax_obtener_cat_cod_postal($cod_postal) {
        // se asigna uso de utf en los nombres
        $this->db->simple_query("SET NAMES \'utf8\'");
        
        // se obtienen los datos del codigo postal
        $this->db->where("d_codigo = ", $cod_postal);
        $this->db->order_by("d_asenta");
        $cat_cod_postales = Model\C_sepomex::all();
        
        // se devuelve el arreglo en formato json
        $codigos_postales = array();
        foreach ($cat_cod_postales as $cod_postal) {
            $cod_postales["d_codigo"] = $cod_postal->d_codigo;
            $cod_postales["d_asenta"] = $cod_postal->d_asenta;
            $cod_postales["d_mnpio"]  = $cod_postal->d_mnpio;
            $cod_postales["d_estado"] = $cod_postal->d_estado;
            $cod_postales["d_ciudad"] = $cod_postal->d_ciudad;
            array_push($codigos_postales, $cod_postales);
        }

        echo json_encode($codigos_postales);
        
    }
    
    public function obtener_cat_cod_postal($cod_postal) {
        // se asigna uso de utf en los nombres
        $this->db->simple_query("SET NAMES \'utf8\'");
        
        // se obtienen los datos del codigo postal
        $this->db->where("d_codigo = ", $cod_postal);
        $this->db->order_by("d_asenta");
        $cat_cod_postales = Model\C_sepomex::all();

        return $cat_cod_postales;
    }
    
    // funcion que verifica si la transaccion existe
    public function busca_transaccion() {

//        $this->output->enable_profiler(TRUE);
        
        $id_cliente_para_facturar    = $this->input->post("id_cliente_para_facturar");
        $id_entidad                  = $this->input->post("sucursal");
        $uso_cfdi                    = $this->input->post("uso_cfdi");
        $arr_campos = array();
        
        // se registra evento en la bitacora
        $observaciones = "Id cliente para facturar: ".$id_cliente_para_facturar;
        registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), BUSCAR_TRANSACCION_FACTURAR, $observaciones);
        
        // se obtiene la configuracion del portal
        $config_portal = Model\Pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;
        // se obtiene la lista de campos flex
        $arr_campos_transaccion = Model\V_pss_campos_transaccion::all();
		$this->load->model("model_buscar_transaccion");

        $i = 1;
        foreach ( $arr_campos_transaccion as $campo ) {
            $arr_campos[$i]["id_flex_header"]  = $campo->id_flex_header;
            $arr_campos[$i]["campo_adicional"] = $campo->campo_adicional;
            $arr_campos[$i]["valor"]           = $this->input->post($campo->campo_adicional);
            $i++;
        }
        //print_r($arr_campos);
        //echo "<br>Entidad: ".$id_entidad;
        //die();
        
        $this->load->model("model_buscar_transaccion");
        $arr_transacciones = $this->model_buscar_transaccion->obtener_idtrx33_transaccion($arr_campos, $id_entidad);
        //print_r($arr_transacciones);
        //die($this->model_buscar_transaccion->obtener_idtrx33_transaccion($arr_campos, $id_entidad));
        
        $url_facturar = base_url()."index.php/facturar/captura_datos_facturacion/".$id_cliente_para_facturar;
        //die('id_cl: '.$id_cliente_para_facturar);
        // si no se encontro la transaccion
        
        // se verifica si el rfc es 
		$array_campos_fh = $this->model_buscar_transaccion->obtener_estatus();      

        //observaciones
        //envia los datos por la url
        $num=0;
        $tamano = count($array_campos_fh);
        //echo "tamano: ".$tamano."<br>";
        $url_arr_campos_fh="";
        $contador = 1;
  
        foreach ($array_campos_fh as $value) {

            //echo $value->campo_adicional."2";
            $name = $value->campo_adicional."2";
            $v_array_campos_fh[$num]           = $this->input->post($value->campo_adicional."2");
            //echo $name."=".$v_array_campos_fh[$num]."<br>";
            //$arr_campos_fh["$name"] = $v_array_campos_fh;
            if ($contador == 1) {
                $url_arr_campos_fh = "?".$url_arr_campos_fh;
            }



            if ($contador != $tamano) {
                $url_arr_campos_fh = $url_arr_campos_fh.$name."=".$v_array_campos_fh[$num]."&";
            }else{
                $url_arr_campos_fh = $url_arr_campos_fh.$name."=".$v_array_campos_fh[$num];
            }

            
            $contador++;
            $num++;
        }

        if ( count($arr_transacciones) < 1) {
            $this->session->set_flashdata('titulo', "Transacción no encontrada");
            $this->session->set_flashdata('mensaje', "No se encontró una transacción con los datos proporcionados. Intente nuevamente");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
            
            $observaciones = "Id cliente para facturar: ".$id_cliente_para_facturar. " - No se encontró una transacción con los datos proporcionados";
            registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), ERROR_TRANSACCION_FACTURAR, $observaciones);
            
            redirect($url_facturar);
        } else {
            // si existe mas de una coincidencia
            if ( count($arr_transacciones) > 1 ) {
                $this->session->set_flashdata('titulo', "Error en búsqueda de transacción");
                $this->session->set_flashdata('mensaje', "Ocurrió un error al buscar su transacción. Los datos proporcionados generaron más de un caso correcto. El administrador ha sido notificado y se le notificará cuando el ticket pueda facturarse");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
                
                $observaciones = "Id cliente para facturar: ".$id_cliente_para_facturar. " - Los datos proporcionados generaron más de un caso correcto. El administrador ha sido notificado y se le notificará cuando el ticket pueda facturarse";
                registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), ERROR_TRANSACCION_FACTURAR, $observaciones);

                
                redirect($url_facturar);                
            } else {
                // transaccion encontrada. Se obtienen los datos de la transaccion
                $transaccion_buscada = $arr_transacciones[1];
                $transaccion = Model\Emi_trx33_r::find($transaccion_buscada["id_trx33"]);
                
                /* Se valída si la transacción está dentro del rango límite para
                 * proceder a facturar, sino no se valida se muestra mensaje.    
                 *                  */
                if(!$this->valida_limite_facturacion($transaccion)){
                    $this->session->set_flashdata('titulo', "Fecha límite de facturación");
                    $this->session->set_flashdata('mensaje', "Discúlpe las molestias, su transacción ha superado la fecha límite permitida para poder realizar su factura.");
                    $this->session->set_flashdata('tipo_mensaje', 'alert alert-warning alert-dismissible');
                    /* Se registra evento en bitácora */
                    $observaciones = "Límite de facturación. La transacción con idtrx33 =  $transaccion->id_trx33_r excedió la fecha límite de facturación"." Id cliente para facturar: ".$id_cliente_para_facturar;
                    registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), ERROR_TRANSACCION_FACTURAR, $observaciones);
                    redirect($url_facturar);                     
                }
                
                // si ya esta facturada (el id cliente ha sido asignado al receptor correcto)
                if ( $transaccion->id_receptor != $config_portal->id_cliente_autofactura ) {
                   $this->session->set_flashdata('titulo', "Estimado Cliente: la transacción que busca ha sido facturada con anterioridad.");
                   $this->session->set_flashdata('mensaje', "Puede consultar y obtener el XML y PDF de la factura desde el menú Mis comprobantes");
                   $this->session->set_flashdata('tipo_mensaje', 'alert alert-warning alert-dismissible');
                   
                   $observaciones = "Id cliente para facturar: ".$id_cliente_para_facturar. " - idtrx33obtenida: ".$transaccion->id_trx33_r." - Transaccion facturada con anterioridad";
                   registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), ERROR_TRANSACCION_FACTURAR, $observaciones);
                   
                   // si la transaccion se asigno al mismo cliente, entonces se envia a la cosulta de comprobantes, si no, a la vista de consulta
                   if ( $transaccion->id_receptor == $id_cliente_para_facturar ) {
                       $url_mis_comprobantes = base_url()."index.php/mis_comprobantes_pss";
                   } else {
                       $url_mis_comprobantes = base_url()."index.php/facturar/captura_datos_facturacion/".$id_cliente_para_facturar;
                   }
                   
                   redirect($url_mis_comprobantes);                
                    
                } else {

				
                    // =========================================== VERIFICACION DE TICKET EN FACTURA GLOBAL ==========================================
		            // se verifica si la transaccion esta en una factura global
                    $global_id = Model\Emi_trx33_global::find_by_idtrx33($transaccion->id_trx33_r, false);
		            
		            // si la transaccion esta en la global
		            if ( $global_id != null ) {
		            	// si el portal permite facturar ticket en global
		            	if ( $config_portal->facturar_ticket_en_global != 1 ) {
		            		// no se permite facturar ticket en global
                            $observaciones = "idtrx33obtenida: ".$transaccion->id_trx33_r." - El ticket esta en factura global y el portal está configurado a no permitir facturar ticket en global";
                            registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), ERROR_TRANSACCION_FACTURAR, $observaciones);
                            
                            $this->session->set_flashdata('titulo', "Error al procesar transacción");
                            $this->session->set_flashdata('mensaje', "La transacción buscada ha sido considerada para una factura de venta de mostrador. Por favor comuníquese con su proveedor para realizar la aclaración.");
                            $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
                            
                            $url_facturar = base_url()."index.php/".URL_PANTALLA_PRINCIPAL;
                            redirect($url_facturar);
		            	} else {
							$observaciones = "idtrx33obtenida: ".$transaccion->id_trx33_r." - El ticket esta en factura global. Se permite facturacion";
							// transaccion en global pero permite continuar
							registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), BUSCAR_TRANSACCION_FACTURAR, $observaciones);
						}
  		                // ===================================== TERMINA VERIFICACION DE TICKET EN FACTURA GLOBAL ======================================
						
                    
                    }
				    
                     // procede a facturarse
                    $this->session->set_flashdata('titulo', "Transacción disponible");
                    $this->session->set_flashdata('mensaje', "Confirme los datos para proceder a facturar");
                    $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');

                    $url_confirmar_facturacion = base_url()."index.php/facturar/confirmar_facturacion/".$id_cliente_para_facturar."/".$transaccion->id_trx33_r."/".$uso_cfdi.$url_arr_campos_fh;
                    redirect($url_confirmar_facturacion);    
  
                }
            }
        }
        
    }
    
    
    
    
    // funcion que mostrara los datos para confirmar la facturacion
    public function confirmar_facturacion($id_cliente_para_facturar = null, $id_trx33_r = null, $uso_cfdi = null) {
        $this->load->model("model_buscar_transaccion");
        
        $array_campos_fh = $this->model_buscar_transaccion->obtener_estatus();

        //$array_campos_fh = $this->db->query("SELECT * FROM emi_c_info_adicionales WHERE estatus = 1;");
        //echo "string";
        
        //$array_campos_fh = Model\Emi_c_info_adicionales::find_by_estatus('1', FALSE);
        $num=0;
        $tamano = count($array_campos_fh);
        $url_arr_campos_fh="";
        $contador = 1;
        foreach ($array_campos_fh as $value) {

            $name = $value->campo_adicional."2";
            $arry_name[$name] = $_GET[$name];
            
            if ($contador == 1) {
                $url_arr_campos_fh = "?".$url_arr_campos_fh;
            }



            if ($contador != $tamano) {
                $url_arr_campos_fh = $url_arr_campos_fh.$name."=".$arry_name[$name]."&";
            }else{
                $url_arr_campos_fh = $url_arr_campos_fh.$name."=".$arry_name[$name];
            }

            $contador++;
            $num++;


        }
        //die($id_trx33_r);
        // si no llegan los parametros se regresa a la pantalla principal
        if ( $id_cliente_para_facturar == null || $id_trx33_r == null ) {
            
            $this->session->set_flashdata('titulo', "Error al procesar transacción");
            $this->session->set_flashdata('mensaje', "No fue posible obtener la información de la transacción para confirmar la operación. Intente de nuevo por favor.");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
            
            $url_facturar = base_url()."index.php/".URL_PANTALLA_PRINCIPAL;
            redirect($url_facturar);
        }
        
        // se registra evento en la bitacora
        $observaciones = "Id cliente para facturar: ".$id_cliente_para_facturar. " - idtrx33obtenida: ".$id_trx33_r;
        registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), CONFIRMAR_TRANSACCION_FACTURAR, $observaciones);
        
        // se transfieren los datos a la vista
        $data = array();
        $data["id_cliente_para_facturar"] = $id_cliente_para_facturar;
        $data["id_trx33_r"] = $id_trx33_r;
        $config_portal = Model\Pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;
        
        // se obtienen los datos de la transaccion
        $transaccion = Model\Emi_trx33_r::find($id_trx33_r);
        
        // se envia el uso de cfdi elegido anteriormente, se asigna a la transaccion
        if ( $uso_cfdi != null ) {
            $transaccion->uso_cfdi = $uso_cfdi;
        }
        $data["transaccion"] = $transaccion;
        
        // conceptos de la transaccion
        $array_conceptos = $this->model_buscar_transaccion->obtener_conceptos_transaccion($id_trx33_r);

        //$conceptos = $this->db->query("SELECT * FROM emi_trx33_concepto_r WHERE id_trx33_r =".$id_trx33_r);
        //echo print_r($array_conceptos);
        $data["conceptos"] = $array_conceptos;
        
        // catalogo uso de cfdi
        $arr_uso_cfdi = Model\Emi_c_uso_cfdi::all();
        $data["arr_uso_cfdi"] = $arr_uso_cfdi;
        
        $arr_forma_pago = Model\Emi_c_forma_pago::all();
        $data["arr_forma_pago"] = $arr_forma_pago;
        
        $arr_metodo_pago = Model\Emi_c_metodo_pago::all();
        $data["arr_metodo_pago"] = $arr_metodo_pago;

        // se verifica el modo de facturacion
        if ( $config_portal->modo_facturacion == 0 ) {
            // modo por lanzador
            $url_facturar_transaccion = base_url()."index.php/facturar/facturar_transaccion"."/".$id_trx33_r.$url_arr_campos_fh;
        } else {
            // modo por web service
            $url_facturar_transaccion = base_url()."index.php/facturar/facturar_transaccion_en_linea"."/".$id_trx33_r.$url_arr_campos_fh;
        }
        
        $data["url_facturar_transaccion"] = $url_facturar_transaccion;

        $url_anterior = base_url()."index.php/facturar/index";
        $data["url_anterior"] = $url_anterior;


        cargar_interfaz_grafica($this, $data, 'facturar_ticket/view_content_wrapper_confirmar_facturacion', 
                'facturar_ticket/view_content_wrapper_confirmar_facturacion_script');


    }



    
    // funcion que asigna los datos del cliente a la transaccion y continua el proceso de emision que quedo pendiente
    public function facturar_transaccion($id_trx33_r = null) {
        //echo "hola";
        //die();
        $this->load->model("model_buscar_transaccion");

        //$array_campos_fh = $this->db->query("SELECT * FROM emi_c_info_adicionales WHERE estatus = 1;");
        $array_campos_fh = $this->model_buscar_transaccion->obtener_estatus();

        // foreach ($array_campos_fh as $value) {
        //     echo $value->id_info_adicional;
        // }

        $num=0;

        foreach ($array_campos_fh as $value) {

            $name = $value->campo_adicional."2";

            $name_fh = substr($name, 0, -1);

            $arry_name[$name] = $_GET[$name];
            echo $name." = ".$arry_name[$name]."--";
            $q_query_inf_adic = $this->model_buscar_transaccion->insertar_info_flexheader($name_fh, $id_trx33_r, $arry_name[$name]);

            //$q_query_inf_adic = "INSERT INTO emi_trx33_inf_adic VALUES ('$name_fh', $id_trx33_r, '$arry_name[$name]');";
            
            $q_query_inf_adic_r = $this->model_buscar_transaccion->insertar_info_r_flexheader($name_fh, $id_trx33_r, $arry_name[$name]);

            //$q_query_inf_adic_r = "INSERT INTO emi_trx33_inf_adic_r VALUES ('$name_fh', $id_trx33_r, '$arry_name[$name]');";

            $num++;
        }

        //die();
        date_default_timezone_set('America/Mexico_City');
        ////$this->output->enable_profiler(TRUE);
        
        // se obtienen los datos de la transaccion
        $id_cliente_para_facturar    = $this->input->post("id_cliente_para_facturar");
        $id_trx33_r                  = $this->input->post("id_trx33_r");
        
        // se registra evento en la bitacora
        $observaciones = "idtrx33obtenida: ".$id_trx33_r." - Enviada a facturar por lanzador";
        registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), ENVIAR_TRANSACCION_FACTURAR, $observaciones);
        
        // si no llegan los parametros se regresa a la pantalla principal
        if ( $id_cliente_para_facturar == null || $id_trx33_r == null ) {
            
            $this->session->set_flashdata('titulo', "Error al procesar transacción");
            $this->session->set_flashdata('mensaje', "No fue posible obtener la información de la transacción para confirmar la operación. Intente de nuevo por favor.");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
            
            $url_facturar = base_url()."index.php/".URL_PANTALLA_PRINCIPAL;
            redirect($url_facturar);
        }
        

        // datos fiscales del cliente
        $cliente = Model\C_clientes::find($id_cliente_para_facturar);
        
        // se obtienen los datos de la transaccion
        $transaccion_r = Model\Emi_trx33_r::find($id_trx33_r);
        $transaccion   = Model\Emi_trx33::find($id_trx33_r);
        
        $id_ejecucion = $transaccion_r->id_ejecucion;
        $id_sucursal  = $transaccion_r->id_sucursal;
        
        // CONTINUACION DEL PROCESO DE FACTURACION
        // 1) Se obtienen las etapas del proceso por id de proceso
        $this->db->where("secuencia = 2");
        $control_proceso = Model\Control_etapas_procesos::find_by_id_ejecucion($id_ejecucion, false);
        
        // si se encontro
        if ( $control_proceso != null ) {
            // se cambia el estatus de la etapa a en proceso
            $ctrl_etapa_proceso = Model\R_ctrl_eta_lanzador::find_by_id_control_etapa_proceso($control_proceso->id_control_etapa_proceso, false);
            
            // si no se encontro la etapa
            if ( $ctrl_etapa_proceso == null ) {
                
                // se indica el error en pantalla
                $this->session->set_flashdata('titulo', "Error al procesar transacción");
                $this->session->set_flashdata('mensaje', "No fue posible obtener la información de la etapa y del proceso con el que se procesó su ticket. Favor de notificarlo a la sucursal donde fue emitido.");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
                
                $url_facturar = base_url()."index.php/".URL_PANTALLA_PRINCIPAL;
                redirect($url_facturar);
            }
            
            // se arranca el proceso
            $ctrl_etapa_proceso->id_estatus_lanzador = 6; // etapa iniciada
            $ctrl_etapa_proceso->save();
            
            // se asignan los datos
            // se obtiene la serie que corresponde a la sucursal emisora
            $id_entidad = $transaccion_r->id_emisor;
            $aux_id_entidad=$id_entidad;
            if ($id_sucursal!=''||$id_sucursal!=null) {
                $aux_id_entidad = $id_sucursal;
            }
            $this->db->where("tipo = 1");
            $sql_serie_entidad = Model\Pss_series_entidades::find_by_id_entidad($aux_id_entidad);
             
            foreach ($sql_serie_entidad as $serie_entidad) {
               // se obtiene el folio de la serie
               $this->load->model("model_pss_series_entidades");
               $folio = $this->model_pss_series_entidades->obtener_folio($aux_id_entidad, $serie_entidad->serie);
            }
            //die('serie: '.$serie_entidad->serie.'<br>folio: '.$folio);
            
            // si el folio es nulo entonces se busca con la serie de la entidad
            if ( $folio == "" || $folio == null) {
                $sql_serie_entidad = Model\Pss_series_entidades::find_by_id_entidad($id_entidad);
                 
                foreach ($sql_serie_entidad as $serie_entidad) {
                   // se obtiene el folio de la serie
                   $this->load->model("model_pss_series_entidades");
                   $folio = $this->model_pss_series_entidades->obtener_folio($id_entidad, $serie_entidad->serie);
                }
                
            }
            
            //$cliente = Model\C_clientes::find($id_cliente);

            $transaccion_r->serie = $serie_entidad->serie;
            $transaccion_r->folio = $folio;
            
            $transaccion_r->id_receptor    = $cliente->id_cliente;
            $transaccion_r->envia_xml      = 1;
            $transaccion_r->envia_pdf      = 1;
            $transaccion_r->email_envio    = $cliente->email;
            $transaccion_r->save();        
                                           
            $transaccion->rfc_receptor     = $cliente->rfc;
            $transaccion->nombre_receptor  = $cliente->cliente;
            $transaccion->serie            = $serie_entidad->serie;
            $transaccion->folio            = $folio;
			
			// se copia el lugar de expedicion de la tabla r
			$transaccion->lugar_expedicion = $transaccion_r->id_lugar_expedicion;
			
            $transaccion->fecha            = substr( date('c'), 0, 19);
            //Cargar cambios
            $metodo_pago                   = explode(" ", $this->input->post("metodo_pago"));
            $transaccion->metodo_pago      = $metodo_pago[0];
            $forma_pago                    = explode(" ", $this->input->post("forma_pago"));
            $transaccion->forma_pago       = $forma_pago[0];
            $uso_cfdi                      = explode(" ", $this->input->post("uso_cfdi"));
            $transaccion->uso_cfdi         = $uso_cfdi[0];

            $transaccion->save();
            
            // se finaliza la etapa
            $ctrl_etapa_proceso->id_estatus_lanzador = 3; // etapa iniciada
            $ctrl_etapa_proceso->save();
            
            // se inserta la etapa nueva para el lanzador (generacion de XML)
            // se obtiene la etapa 3
            //echo "<br>buscando etapa 3";
            $this->db->where("secuencia", 3);
            $this->db->where("id_ejecucion", $id_ejecucion);
            $control_proceso3 = Model\Control_etapas_procesos::all();
            
            $etapa3 = null;
            foreach ( $control_proceso3 as $proceso) {
                $etapa3 = $proceso;
                break;
            }
            
            //print_r($control_proceso3);
            
            $nueva_etapa = new Model\R_ctrl_eta_lanzador();
            
            // se asignan los datos de la etapa nueva
            $nueva_etapa->id_ctrl_eta_lanzador      = 0;
            $nueva_etapa->id_control_etapa_proceso  = $etapa3->id_control_etapa_proceso;
            $nueva_etapa->id_lote_proceso           = $ctrl_etapa_proceso->id_lote_proceso;
            $nueva_etapa->id_estatus_lanzador       = 1; // pendiente de ser lanzado
            $nueva_etapa->id_proceso                = $etapa3->id_proceso;
            $nueva_etapa->id_ejecucion              = $etapa3->id_ejecucion;
            $nueva_etapa->id_programa               = $etapa3->id_programa;
            $nueva_etapa->lote_actual               = $ctrl_etapa_proceso->lote_actual;
            $nueva_etapa->lote_fin                  = $ctrl_etapa_proceso->lote_fin;
            $nueva_etapa->save();
        }
		
		
		// =========================================== VERIFICACION DE TICKET EN FACTURA GLOBAL ==========================================
		// se invoca la funcion para generar nota de credito automatica, en caso de tener un ticket en global con generacion de nota de credito automatica
		$this->valida_nota_credito_global($id_trx33_r);
		
		// ===================================== TERMINA VERIFICACION DE TICKET EN FACTURA GLOBAL ======================================
            
        $observaciones = "idtrx33obtenida: ".$id_trx33_r." - Transaccion ha sido enviada a facturar por Lanzador. Favor de verificar en Neon.";
        registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), CORRECTO_TRANSACCION_FACTURAR, $observaciones);
            
        // se envia a la pantalla para descargar el PDF y el XML
        $url_descargar_pdf_xml = base_url()."index.php/facturar/descargar_xml_pdf/".$id_trx33_r;
        redirect($url_descargar_pdf_xml);

        
    }
    

	
    // funcion que asigna los datos del cliente a la transaccion y continua el proceso de emision que quedo pendiente
    public function facturar_transaccion_en_linea($id_trx33_r = null) {
        $this->load->model("model_buscar_transaccion");

        //$array_campos_fh = $this->db->query("SELECT * FROM emi_c_info_adicionales WHERE estatus = 1;");
        $array_campos_fh = $this->model_buscar_transaccion->obtener_estatus();

        $num=0;

        foreach ($array_campos_fh as $value) {

            $name = $value->campo_adicional."2";

            $name_fh = substr($name, 0, -1);

            $arry_name[$name] = $_GET[$name];
            //echo $name." = ".$arry_name[$name]."--";




            $q_query_inf_adic = $this->model_buscar_transaccion->insertar_info_flexheader($name_fh, $id_trx33_r, $arry_name[$name]);
            //$q_query_inf_adic = "INSERT INTO emi_trx33_inf_adic VALUES ('$name_fh', $id_trx33_r, '$arry_name[$name]');";

            
            $q_query_inf_adic_r = $this->model_buscar_transaccion->insertar_info_r_flexheader($name_fh, $id_trx33_r, $arry_name[$name]);
            //$q_query_inf_adic_r = "INSERT INTO emi_trx33_inf_adic_r VALUES ('$name_fh', $id_trx33_r, '$arry_name[$name]');";

        }
        date_default_timezone_set('America/Mexico_City');
        ////$this->output->enable_profiler(TRUE);
        
        // se obtienen los datos de la transaccion
        $id_cliente_para_facturar    = $this->input->post("id_cliente_para_facturar");
        $id_trx33_r                  = $this->input->post("id_trx33_r");
        
        // se registra evento en la bitacora
        $observaciones = "idtrx33obtenida: ".$id_trx33_r." - Enviada a facturar por web service";
        registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), ENVIAR_TRANSACCION_FACTURAR, $observaciones);

        
        // si no llegan los parametros se regresa a la pantalla principal
        if ( $id_cliente_para_facturar == null || $id_trx33_r == null ) {
            
            $observaciones = "idtrx33obtenida: ".$id_trx33_r." - No fue posible obtener la información de la transacción para confirmar la operación. Intente de nuevo por favor";
            registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), ERROR_TRANSACCION_FACTURAR, $observaciones);
            
            $this->session->set_flashdata('titulo', "Error al procesar transacción");
            $this->session->set_flashdata('mensaje', "No fue posible obtener la información de la transacción para confirmar la operación. Intente de nuevo por favor.");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
            
            $url_facturar = base_url()."index.php/".URL_PANTALLA_PRINCIPAL;
            redirect($url_facturar);
        }
        

        // datos fiscales del cliente
        $cliente = Model\C_clientes::find($id_cliente_para_facturar);
        
        // se obtienen los datos de la transaccion
        $transaccion_r = Model\Emi_trx33_r::find($id_trx33_r);
        $transaccion   = Model\Emi_trx33::find($id_trx33_r);
        
		// =========================================== VERIFICACION DE TICKET EN FACTURA GLOBAL ==========================================
		// se invoca la funcion para generar nota de credito automatica, en caso de tener un ticket en global con generacion de nota de credito automatica
		$this->valida_nota_credito_global($id_trx33_r);
		
		// ===================================== TERMINA VERIFICACION DE TICKET EN FACTURA GLOBAL ======================================
		
        $id_ejecucion = $transaccion_r->id_ejecucion;
        $id_sucursal  = $transaccion_r->id_sucursal;
        
        // CONTINUACION DEL PROCESO DE FACTURACION
        // se asignan los datos
        // se obtiene la serie que corresponde a la sucursal emisora
        $id_entidad = $transaccion_r->id_emisor;
        $aux_id_entidad=$id_entidad;
        if ($id_sucursal!=''||$id_sucursal!=null) {
            $aux_id_entidad = $id_sucursal;
        }
        $this->db->where("tipo = 1");
        $serie_entidad = Model\Pss_series_entidades::find_by_id_entidad($aux_id_entidad, false);
         
         //foreach ($sql_serie_entidad as $serie_entidad) {
             // se obtiene el folio de la serie
            $this->load->model("model_pss_series_entidades");
            $folio = $this->model_pss_series_entidades->obtener_folio($aux_id_entidad, $serie_entidad->serie);
         //}
        //die('serie: '.$serie_entidad->serie.'<br>folio: '.$folio);
        
        //$cliente = Model\C_clientes::find($id_cliente);

        $transaccion_r->serie = $serie_entidad->serie;
        $transaccion_r->folio = $folio;
        
        $transaccion_r->id_receptor   = $cliente->id_cliente;
        $transaccion_r->envia_xml     = 1;
        $transaccion_r->envia_pdf     = 1;
        $transaccion_r->email_envio   = $cliente->email;
        $transaccion_r->save();
        
        $transaccion->rfc_receptor    = $cliente->rfc;
        $transaccion->nombre_receptor = $cliente->cliente;
        $transaccion->serie           = $serie_entidad->serie;
        $transaccion->folio           = $folio;
        $transaccion->fecha           = substr( date('c'), 0, 19);
        //Cargar cambios
        $metodo_pago                  = explode(" ", $this->input->post("metodo_pago"));
        $transaccion->metodo_pago     = $metodo_pago[0];
        $forma_pago                   = explode(" ", $this->input->post("forma_pago"));
        $transaccion->forma_pago      = $forma_pago[0];
        $uso_cfdi                     = explode(" ", $this->input->post("uso_cfdi"));
        $transaccion->uso_cfdi        = $uso_cfdi[0];

        $transaccion->save();
            
        // se invoca el ws de facturacion
        $this->facturar_por_ws($id_trx33_r);
        
        $observaciones = "idtrx33obtenida: ".$id_trx33_r." - Transaccion facturada correctamente";
        registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), CORRECTO_TRANSACCION_FACTURAR, $observaciones);
    
        // se envia a la pantalla para descargar el PDF y el XML
        $url_descargar_pdf_xml = base_url()."index.php/facturar/descargar_xml_pdf/".$id_trx33_r;
        redirect($url_descargar_pdf_xml);

        
    }
    
	// funcion que busca si un ticket esta en factura global, y si esta, verifica si se debe generar una nota de credito automatica
	public function valida_nota_credito_global($id_trx33_r) {
		//$this->output->enable_profiler(TRUE);
		



		// se verifica si la transaccion esta en una factura global
        $global_id = Model\Emi_trx33_global::find_by_idtrx33($id_trx33_r, false);
		
		// se obtiene la configuracion del portal
        $config_portal = Model\Pss_config_portal::find(1);
        
		// si la transaccion esta en la global
		if ( $global_id != null ) {
			// si el portal permite facturar ticket en global
			if ( $config_portal->facturar_ticket_en_global == 1 ) {
			    // si el portal esta configurado a que genere nota de credito a la global
			    if ( $config_portal->generar_nota_credito_global == 1 ) {
					
                    $observaciones = "idtrx33obtenida: ".$id_trx33_r." - Se procede a generar nota de credito automatica";
                    // transaccion en global pero permite continuar
                    registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), BUSCAR_TRANSACCION_FACTURAR, $observaciones);
			    	
			    	// se obtiene la transaccion original
                    $transaccion_r = Model\Emi_trx33_r::find($id_trx33_r);
			    	
                    $id_entidad = $transaccion_r->id_emisor;
                    $this->db->where("tipo = 2");
			    	
                    $series_entidades = Model\Pss_series_entidades::find_by_id_entidad($id_entidad);
			    	
                    foreach ($series_entidades as $serie_entidad) {
                        $this->load->model("model_pss_series_entidades");
                        $folio_nota_c = $this->model_pss_series_entidades->obtener_folio($id_entidad, $serie_entidad->serie);
                    }
                    
                    // se obtiene el id de la factura global
					$trx33_global = Model\Emi_trx33_r::find_by_id_trx_erp($global_id->id_global, false);
					//echo "<br>IDTRX33 DE LA GLOBAL: ".$trx33_global->id_trx33_r;
					
                    $this->generar_nota_credito_pss($id_trx33_r,$trx33_global->id_trx33_r, $serie_entidad->serie, $folio_nota_c);
			    }
			}
		}
		
	}
	
    public function descargar_xml_pdf($id_trx33) {
        $data = array();
        
        $url_nueva_factura = base_url()."index.php/facturar";
        $data["url_nueva_factura"] = $url_nueva_factura;
        
        $data["id_trx33"] = $id_trx33;
        
        $url_descargar_xml = base_url()."index.php/mis_comprobantes_pss/descargar_xml/".$id_trx33;
        $data["url_descargar_xml"] = $url_descargar_xml;

        $url_descargar_pdf = base_url()."index.php/mis_comprobantes_pss/descargar_pdf/".$id_trx33;
        $data["url_descargar_pdf"] = $url_descargar_pdf;
        /*$pdf_ok = 0;
        while ($pdf_ok < 1) {
            sleep(5)
            $docto_pdf = Model\Emi_trx33_pdf::find_by($id_trx33);
            if ($docto_pdf->pdf !='' && $docto_pdf->pdf!=null) {
                $pdf_ok++;
            }
        }
*/      $url_ajax_busca_fact = base_url()."index.php/Factura/busca_fact/".$id_trx33;
        $data["url_ajax_busca_fact"] = $url_ajax_busca_fact;
        cargar_interfaz_grafica($this, $data, 'facturar_ticket/view_content_wrapper_descarga_xml_pdf', "facturar_ticket/view_content_wrapper_descarga_xml_pdf_script");
        //cargar_interfaz_grafica($this, $data, 'facturar_ticket/view_content_wrapper_descarga_xml_pdf', null);
    }

    function busca_fact($id_trx33){
        
        $emi_pdf = Model\Emi_trx33_pdf::find_by_id_trx33($id_trx33);
        //$db->setQuery('SELECT creado FROM emi_trx33_pdf WHERE id_trx33 ='.$id_trx33);
        $result = "";
        foreach ($emi_pdf as $emi_pdf) {
            $result = $emi_pdf->creado;
        }
         echo json_encode($emi_pdf);
    }

    public function editar_datos_cuenta() {
        // se creal el arreglo para paso de parametros
        $data = array();
        
        // se obtienen los datos del usuario
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;
        
        // se obtienen los datos fiscales del usuario
        $cliente = Model\C_clientes::find($pss_usuario->id_cliente);
        $data["cliente"] = $cliente;
       
        // url para el controlador de validacion de inicio de sesion
        $url_registra_datos_fiscales = base_url("index.php/mi_perfil/registrar_datos_cuenta");
        
        // url para recuperar la contrasena
        $url_anterior = base_url("index.php/mi_perfil/index");

        // se obtienen las preguntas de recuperacion
        $this->db->where("estatus = 1");
        $arr_preguntas_recuperacion = Model\C_preguntas_recuperacion::all();
        $data["arr_preguntas_recuperacion"] = $arr_preguntas_recuperacion;
        
        // se transfieren los parametros al arreglo
        $data["url_registra_datos_fiscales"] = $url_registra_datos_fiscales;
        $data["url_anterior"]                = $url_anterior;
        
        // datos del codigo postal
        $arr_cod_postales = $this->obtener_cat_cod_postal($cliente->codigo_postal);
        $data["arr_cod_postales"] = $arr_cod_postales;
        
        // url de la funcion ajax para consulta de codigos postales
        $url_ajax_cat_cp = base_url()."index.php/mi_perfil/ajax_obtener_cat_cod_postal/";
        $data["url_ajax_cat_cp"] = $url_ajax_cat_cp;
        
        cargar_interfaz_grafica($this, $data, 'mi_perfil/view_content_wrapper_captura_datos_fiscales', "mi_perfil/view_script_captura_datos_fiscales");
        
    }
    
    public function registrar_datos_cuenta() {
        // se obtienen los datos del formulario de captura
        $email_contacto                  = $this->input->post("email_contacto");
        $id_pista_recuperar_contrasena   = $this->input->post("id_pista_recuperar_contrasena");
        $respuesta_recuperar_contrasena  = $this->input->post("respuesta_recuperar_contrasena");
        $rfc                             = $this->input->post("rfc");
        $num_reg_id_trib                 = $this->input->post("num_reg_id_trib");
        $razon_social                    = $this->input->post("cliente");
        $email                           = $this->input->post("email");
        $email_confirma                  = $this->input->post("email_confirma");
        $calle                           = $this->input->post("calle");
        $num_exterior                    = $this->input->post("num_exterior");
        $num_interior                    = $this->input->post("num_interior");
        $cp                              = $this->input->post("cp");
        $colonia                         = $this->input->post("colonia");
        $municipio                       = $this->input->post("municipio");
        $localidad                       = $this->input->post("localidad");
        $estado                          = $this->input->post("estado");
        $pais                            = $this->input->post("pais");
        
        // si las validaciones son correctas
        if ( true ) {
            
            // se busca el registro de usuario
            $usuario_pss = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
            // se reasignan los datos
            $usuario_pss->email                            = $email_contacto;
            $usuario_pss->id_pregunta_recuperacion         = $id_pista_recuperar_contrasena;
            $usuario_pss->respuesta_recuperar_contrasena   = $respuesta_recuperar_contrasena;
            $usuario_pss->fecha_alta                       = date("y-m-d");
            $usuario_pss->dir_ip                           = $this->input->ip_address();
            $usuario_pss->save();
            
            // se obtiene los datos fiscales
            $cliente = Model\C_clientes::find($usuario_pss->id_cliente);
            
            $cliente->cliente           = $razon_social;
            $cliente->rfc               = $rfc;
            $cliente->numero_exterior   = $num_exterior;
            $cliente->numero_interior   = $num_interior;
            $cliente->calle             = $calle;
            $cliente->colonia           = $colonia;
            $cliente->localidad         = $localidad;
            $cliente->referencia        = "";
            $cliente->municipio         = $municipio;
            $cliente->estado            = $estado;
            $cliente->pais              = $pais;
            $cliente->codigo_postal     = $cp;
            $cliente->email             = $email;
            $cliente->estatus           = 1;
            $cliente->num_reg_id_trib   = $num_reg_id_trib;
            $cliente->save();
        
            $this->session->set_flashdata('titulo', "Datos de cuenta y fiscales");
            $this->session->set_flashdata('mensaje', "Sus datos han sido actualizados correctamente");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
            $url_login = base_url()."index.php/mi_perfil/index";
            redirect($url_login);
            
        } else {
            $this->session->set_flashdata('titulo', "Datos de cuenta y fiscales");
            $this->session->set_flashdata('mensaje', "Ocurrión un error al actualizar sus datos. Intente más tarde nuevamente.");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
            $url_login = base_url()."index.php/mi_perfil/index";
            redirect($url_login);
        }
        


        
    }

    /*
        Funcion que invoca el WS de emision de Neon para generar la factura a partir del id_trx33 de la transaccion
    */
    public function facturar_por_ws($id_trx33) {
        // se obtiene la configuracion del portal
        $config_portal = Model\pss_config_portal::find(1);
        
        // url de pruebas
        //$url_ws_facturacion = "http://localhost:8080/NeonEmisionWS_20180324/NeonEmisionWS?wsdl";
        ini_set('default_socket_timeout', 5000);
        try {
            // se verifica si el servicio esta arriba y funcionando
            $url_ws_responde = false;
            $handle = curl_init($config_portal->url_ws_facturacion);
            curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
            
            $response = curl_exec($handle);
            $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
            if ( $httpCode == 200 ) {
                /* la peticion fue correcta */
                $url_ws_responde = true;
            }
            
            curl_close($handle);
            
        } catch (Exception $e) {
            // ocurrio un error al esperar la respuesta del servicio
            $url_ws_responde = false;
        }
        
        $resultado = null;
        // si el ws si esta respondiendo
        if ( $url_ws_responde) {
           // se crea el cliente del web service de facturacion
           $neon_emisionws = new SoapClient($config_portal->url_ws_facturacion);
           
           // se genera el array de parametros
           $parametros = array("trxID" => $id_trx33);
           
           // se invoca la operacion de facturacion
           $resultado =  $neon_emisionws->emitirConTrxID($parametros);
           
           // print_r($resultado);
           // die();
        }
        
        // si se tiene un UUID, se facturo correctamente
        //if ( true && $url_ws_responde ){ 
	    $error_en_timbrado      = false;
	    $mensaje_error_timbrado = "";
		 
        if ( $resultado->return->codigoRespuestaPac != null && $resultado->return->codigoRespuestaPac != "" && $url_ws_responde) {
		    log_message('debug', "PSS con web service y web service responde");

			if ( $resultado->return->codigoRespuestaPac != "10" ) {
				log_message('debug', "PSS con web service. Hubo error en timbrado".$resultado->return->codigoRespuestaPac);
				// hubo error por datos en timbrado
				$error_en_timbrado      = true;
				$mensaje_error_timbrado = $resultado->return->descripcionRespuestaPac;
			} else {
				// facturado correctamente
				log_message('debug', "PSS con web service. Facturado correctamente");
                $observaciones = "idtrx33obtenida: ".$id_trx33_r." - El WS de emision ejecuto correctamente";
                registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), CORRECTO_TRANSACCION_FACTURAR, $observaciones);
                return;

			}
        }
        
        // no se pudo facturar, se devuelve el id_receptor al usado para autofactura a la transaccion
        $transaccion_r = Model\Emi_trx33_r::find($id_trx33);
        $transaccion_r->id_receptor = $config_portal->id_cliente_autofactura;
        $transaccion_r->save();
        
        // ocurrio un error al facturar el documento. Se envia mensaje a los administradores, operadores y usuarios
        $remitente    = Model\Envio_correo_remitente::find_by_es_default(1, FALSE);
        $destinatario = new Model\Envio_correo_destinatario();
        $envio        = new Model\Envio_correo();
        
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
                  
        // se llenan los datos para el envio de correo
        $envio->id_envio_correo  = 0;
        $envio->id_transaccion   = null;
        $envio->id_proceso       = null;
        $envio->id_remitente     = $remitente->id_remitente;
        $envio->procesado        = -1; // pendiente
        $envio->fecha_registro   = date("Y-m-d");
        $envio->fecha_proceso    = null;
        $envio->enviar_adjuntos  = 0;
        $envio->asunto           = "SERVICIO DE AUTOFACTURACION: Error al facturar ticket";
        
        if ( $url_ws_responde ) {
			log_message('debug', "PSS con web service. Web service responde pero hubo error");
            $mensaje_error_correo = $resultado->return->mensaje;
            $url_facturar = base_url()."index.php/".URL_PANTALLA_PRINCIPAL;
			
			// si hubo error al facturar por timbrado
			if ( $error_en_timbrado ) {
				
				$observaciones = "idtrx33obtenida: ".$id_trx33_r." - ".$mensaje_error_timbrado;
				$mensaje_error_correo = "Error al timbrar: ".$mensaje_error_timbrado;
			}
        } else {
			log_message('debug', "PSS con web service. Web service no esta disponible");
			$observaciones = "idtrx33obtenida: ".$id_trx33_r." - Servicio de facturación no está disponible. Error 404.";
			$mensaje_error_correo = "Servicio de facturación no está disponible. Error 404.";

            registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), ERROR_TRANSACCION_FACTURAR, $observaciones);
            $url_facturar = base_url()."index.php/".URL_PANTALLA_PRINCIPAL;

        }
        
        $cuerpo_correo = "MENSAJE ENVIADO POR EL SISTEMA DE FACTURACIÓN<br><br>El usuario [".$pss_usuario->login."-".$pss_usuario->nombre." ".$pss_usuario->apellido_paterno."] (login/nombre) intentó realizar la facturación del ticket [CLAVE INTERNA: ".$id_trx33."] cuya operación no se completó correctamente debido a:<br><br><b>".$mensaje_error_correo."</b><br><br>Favor de atender incidente.<br><br>No es necesario responder a este correo";


        $envio->cuerpo = $cuerpo_correo;
        $envio->save();
        
        // se obtiene el id de envio
        $id_envio = Model\Envio_correo::last_created()->id_envio_correo;
        // se asigna al registro
        $envio = Model\Envio_correo::find($id_envio);
        
        // se genera el registro del destinatario (administrador STO)
        $pss_usuario_administrador_sto = Model\Pss_usuario::find(1, false);
        $destinatario->id_correo_destinatario = 0;
        $destinatario->id_envio_correo        = $id_envio;
        $destinatario->destinatario           = $pss_usuario_administrador_sto->email;
        $destinatario->fecha_proceso          = null;
        $destinatario->estatus_envio          = 1;
        $destinatario->cod_error              = null;
        $destinatario->d_error                = null;
        $destinatario->num_intentos           = 0;
        $destinatario->save();

        // se genera el registro del destinatario (administrador del portal y operadores)
        $this->db->where("tipo_usuario in (1)");
        $pss_usuario_administrador_operador = Model\Pss_usuario::all();
        
        foreach ($pss_usuario_administrador_operador as $unadministrador) {
            $destinatario->id_correo_destinatario = 0;
            $destinatario->id_envio_correo        = $id_envio;
            $destinatario->destinatario           = $unadministrador->email;
            $destinatario->fecha_proceso          = null;
            $destinatario->estatus_envio          = 1;
            $destinatario->cod_error              = null;
            $destinatario->d_error                = null;
            $destinatario->num_intentos           = 0;
            $destinatario->save();
        }

        
        // se genera el registro del destinatario (usuario)
        $destinatario->id_correo_destinatario = 0;
        $destinatario->id_envio_correo        = $id_envio;
        $destinatario->destinatario           = $pss_usuario->email;
        $destinatario->fecha_proceso          = null;
        $destinatario->estatus_envio          = 1;
        $destinatario->cod_error              = null;
        $destinatario->d_error                = null;
        $destinatario->num_intentos           = 0;
        $destinatario->save();

        
        // se actualiza el registro de envio para que el ejecutor de envio lo considere
        $envio->procesado = 1; // listo para enviar
        $envio->save();

        // se cambia el estatus del correo para que se pueda enviar
        $envio->procesado        = 0; // pendiente
        $envio->save();
        
        
        
        $this->session->set_flashdata('titulo', "Error al procesar transacción");
        
        $mensaje_error = $mensaje_error_correo."<br><br>Por favor intente nuevamente más tarde.";
		
		
		
		// si hubo error al timbrar
		if ( $error_en_timbrado ) {
			$mensaje_error_correo .= "<br><br><h2>Error al timbrar: ".$mensaje_error_timbrado."</h2>";
		}
		
		log_message('debug', "PSS con web service. Se regresa error a pantalla de facturacion. ".$mensaje_error_correo);
		
        $this->session->set_flashdata('mensaje', $mensaje_error);
        $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
        
        // la url para facturar se define en el tipo de error
        redirect($url_facturar);
    }
    
public function generar_nota_credito_pss($id_trx33, $id_trx33_global, $serie_nota_credito, $folio_nota_credito){
		
		// se obtienen los datos de la transaccion original
		$trx33 = Model\Emi_trx33::find_by_id_trx33($id_trx33);
		$trx33_r = Model\Emi_trx33_r::find_by_id_trx33_r($id_trx33);
		
		// se obtiene el uuid de la factura global
		$xml_global = Model\Emi_trx33_xml::find($id_trx33_global);
		        
        // se obtiene la ruta de importacion del proceso de importacion xml
        $this->db->where("id_proceso = 8");
        $this->db->where("secuencia = 1");
        $etapas_procesos = Model\C_etapas_procesos::all();
        
        foreach ( $etapas_procesos as $etapa ) {
            $directorio_carga_tickets = $etapa->ruta_entrada;
            break;
        }
        
		//EMISOR
		foreach ($trx33 as $trx33) {
				$emisor_rfc                      = $trx33->rfc_emisor;  
				$emisor_nombre                   = $trx33->nombre_emisor;
				$emisor_regimen_fiscal           = $trx33->regimen_fiscal;
				$total_impuestos_retenidos 		 = $trx33->total_impuestos_retenidos;
				$total_impuestos_trasladados 	 = $trx33->total_impuestos_trasladados;
		}
		 
		 //DOCUMENTO
		 foreach ($trx33_r as $trx33_r) {
			//$fecha                  = $trx33_r->fecha;
			$fecha                  = date('Y-m-d H:i:s');
			$forma_de_pago          = $trx33_r->id_forma_pago;
			$condiciones_de_pago    = $trx33_r->condiciones_de_pago;
			$tipo_cambio            = $trx33_r->tipo_cambio; 
			$moneda                 = $trx33_r->id_moneda;
			$metodo_pago            = 'PUE';//$trx33_r->id_metodo_pago;
			$lugar_expedicion       = $trx33_r->id_lugar_expedicion; 
			$tipo_comprobante       = "E"; // es nota de credito
			$subtotal               = $trx33_r->subtotal;
			$descuento              = $trx33_r->descuento;
			if ( $descuento == '' || $descuento == null ) {
				$descuento=0;
			}
			$enviar_xml                 = $trx33_r->envia_xml;
			$enviar_pdf                 = $trx33_r->envia_pdf;
			$enviar_zip                 = $trx33_r->envia_zip;
			$email_envio                = $trx33_r->email_envio;
			$total                      = $trx33_r->total;
			$confirmacion               = $trx33_r->confirmacion;
			$tipo_documento             = $trx33_r->id_tipo_documento;
			$id_trx_erp                 = $trx33_r->id_trx_erp;
			$totalImpuestosRetenidos    = $trx33_r->totalImpuestosRetenidos;
			$totalImpuestosTrasladados  = $trx33_r->totalImpuestosTrasladados;
			$serie_original             = $trx33_r->serie;
			$folio_original             = $trx33_r->folio;
			$emisor_id_emisor_sto       = $trx33_r->id_emisor;
			$serie                      = null;
			$folio                      = null;
			$id_sucursal                = $trx33_r->id_sucursal;
		 }
		  
		 // si se tiene sucursal
         if ( $trx33_r->id_sucursal != null ) {
             $entidades = $this->db->query("SELECT * FROM c_entidades WHERE id_entidad = ".$id_sucursal. ";");
         } else {
             // se hizo emision desde la matriz
             $entidades = $this->db->query("SELECT * FROM c_entidades WHERE id_entidad = ".$emisor_id_emisor_sto. ";");
         }

		 foreach($entidades->result() as $entidad){
				$rfc_sucursal = $entidad->rfc;
				$entidad_sucursal = $entidad->entidad;
				$id_regimen_sucursal = $entidad->id_regimen;
				$numero_interior_sucursal = $entidad->numero_interior;
				$numero_exterior_sucursal = $entidad->numero_exterior;
				$calle_sucursal = $entidad->calle;
				$colonia_sucrusal = $entidad->colonia;
				$localidad_sucursal = $entidad->localidad;
				$referencia_sucursal = $entidad->referencia;
				$municipio_sucursal = $entidad->municipio;
				$estado_sucursal = $entidad->estado;
				$pais_sucursal = $entidad->pais;
				$codigo_postal_sucursal = $entidad->codigo_postal;
				$email_sucursal = $entidad->email;
				$id_tipo_emisor_sucursal = $entidad->id_tipo_entidad;
				$id_tipo_entidad_padre_sucursal = $entidad->id_entidad_padre;
				$estatus_sucursal = $entidad->estatus;
			}
			
		//Recuperar id_cliente de la tabla de confoguracion del portal, para agregar datos del receptor.
		$pss_config = Model\Pss_config_portal::find(1, false);
		$id_cliente_receptor = $pss_config->id_cliente_autofactura;
		
		//RECEPTOR
		$receptor = Model\C_clientes::find_by_id_cliente($id_cliente_receptor);
		foreach($receptor as $receptorD){
			$rfc_receptor               = $receptorD->rfc;
			$estatus                    = $receptorD->estatus;
		}
		
		//FLEX_HEADERS
		$flex_headers = "";
		$flex_header = Model\Emi_trx33_inf_adic::find_by_id_trx33($id_trx33);

		foreach($flex_header as $flex_headerV){
			$flex_headers .=<<<XML
			<FLEX_HEADER clave="$flex_headerV->id_flex_header" nombre="$flex_headerV->id_flex_header" valor="$flex_headerV->valor"/>				
XML;
		}

		
		//CONCEPTOS
		$c = 1;
		$conceptos = "";
		$concepto_r = Model\Emi_trx33_concepto_r::find_by_id_trx33_r($id_trx33);
		foreach($concepto_r as $concepto){
			$conceptos_impuestos = false;
			
			// ----------------------------------------- REVISAR ESTE COMENTARIO COMPLEMENTARIO CON DEFINICION PARA LA GENEREACION DE NOTA DE CREDITO AUTOMATICA -----------
			// todos los conceptos llevan clave de producto y servicio y de unidad de medida fijos a los requeridos para nota de credito
			
			$conceptos .=<<<XML
		<CONCEPTO numero_linea="$c" clave_prod_serv="84111506" cantidad="$concepto->cantidad" clave_unidad="ACT" unidad="ACTIVIDAD" num_identificacion="$concepto->numero_identificacion" descripcion=" $concepto->descripcion" valor_unitario="$concepto->valor_unitario" importe="$concepto->importe" descuento="$concepto->descuento" >
XML;
			$concepto_impuesto_r = Model\Emi_trx33_con_impuestos_r::find_by_id_trx33_concepto_r($concepto->id_trx33_concepto_r);
			
			foreach($concepto_impuesto_r as $concepto_i){
				if($conceptos_impuestos == false){
					$conceptos .="\n".<<<XML
				<IMPUESTOS>
XML;
				}
				//trasladado
				if($concepto_i->tipo_impuesto == 1){
					$conceptos_impuestos = true;
					$conceptos .="\n".<<<XML
					<TRASLADADOS>
						<IMPUESTO base="$concepto_i->base" impuesto="$concepto_i->impuesto" tipo_factor="$concepto_i->tipo_factor" tasa_o_cuota="$concepto_i->tasa_cuota" importe="$concepto_i->importe"/>
					</TRASLADADOS>				
XML;
				}else
					if($concepto_i->tipo_impuesto == 2){
						//retenido
						$conceptos_impuestos = true;
						$conceptos .="\n".<<<XML
					<RETENIDOS>
						<IMPUESTO base="$concepto_i->base" impuesto="$concepto_i->impuesto" tipo_factor="$concepto_i->tipo_factor" tasa_o_cuota="$concepto_i->tasa_cuota" importe="$concepto_i->importe" />
					</RETENIDOS>
XML;
					}
					
					if($conceptos_impuestos == true){
					$conceptos .="\n".<<<XML
				</IMPUESTOS>
XML;
					}
			}

			$conceptos .="\n".<<<XML
			</CONCEPTO>
XML;
			$c++;
		}
		
		//IMPUESTOS
		$impuestos = Model\Emi_trx33_impuestos_r::find_by_id_trx33_r($id_trx33);
		$trasladados = "";
		$retenidos = "";
		foreach($impuestos as $impuesto){
			//trasladado
			if($impuesto->tipo_impuesto == 1){
				$trasladados =<<<XML
		<IMPUESTO impuesto="$impuesto->impuesto" tipo_factor="$impuesto->tipo_factor" tasa_o_cuota="$impuesto->tasa_o_cuota" importe="$impuesto->importe"/>
XML;
			}else
				if($impuestos->tipo_impuesto == 2){
					//retenido
					$retenidos =<<<XML
		<IMPUESTO impuesto="$impuesto->impuesto" tipo_factor="$impuesto->tipo_factor" tasa_o_cuota="$impuesto->tasa_o_cuota" importe="$impuesto->importe"/>		
XML;
				}
		}
        
        // cadena del emisor. Si se tiene sucursal
        if ( $trx33_r->id_sucursal != null ) {
            $cadena_emisor = <<<XML
			<EMISOR rfc="$emisor_rfc" nombre="$emisor_nombre" regimen_fiscal="$emisor_regimen_fiscal" id_emisor_sto="$emisor_id_emisor_sto" id_emisor_erp="$emisor_id_emisor_sto">
				<SUCURSAL rfc="$rfc_sucursal" nombre="$entidad_sucursal" regimen_fiscal="$id_regimen_sucursal" id_emisor_sto="$id_sucursal" id_emisor_erp="$emisor_id_emisor_sto" numero_interior="$numero_interior_sucursal" numero_exterior="$numero_exterior_sucursal" calle="$calle_sucursal"
					colonia="$colonia_sucrusal" localidad="$localidad_sucursal" referencia="$referencia_sucursal" municipio="$municipio_sucursal" estado="$estado_sucursal" pais="$pais_sucursal" codigo_postal="$codigo_postal_sucursal" email="$email_sucursal" id_tipo_emisor="$id_tipo_emisor_sucursal" id_emisor_padre="$id_tipo_entidad_padre_sucursal" estatus_registro="$estatus_sucursal"/>
			</EMISOR>
XML;
        } else {
            // solo se tiene emisor
            $cadena_emisor = <<<XML
			<EMISOR rfc="$emisor_rfc" nombre="$emisor_nombre" regimen_fiscal="$emisor_regimen_fiscal" id_emisor_sto="$emisor_id_emisor_sto" id_emisor_erp="$emisor_id_emisor_sto">
			</EMISOR>
XML;
        }
			
		 $archivo_importacion= <<<XML
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<DOCUMENTOS>
	<DOCUMENTO serie="$serie_nota_credito" folio="$folio_nota_credito" fecha="$fecha" forma_de_pago="$forma_de_pago" tipo_cambio="$tipo_cambio" moneda="$moneda" metodo_pago="$metodo_pago" lugar_expedicion="$lugar_expedicion" tipo_comprobante="$tipo_comprobante" 
				subtotal="$subtotal" descuento="$descuento" total="$total" tipo_documento="$tipo_documento">
		<DOCUMENTO_ERP id_trx_erp="$id_trx_erp"/>
        <CFDIRELS>
            <CFDIREL tipo_relacion="01" uuid_relacionado="$xml_global->uuid" id_trx_erp_original="$id_trx33_global"/>
        </CFDIRELS>
        $cadena_emisor 
		<RECEPTOR rfc="XAXX010101000" nombre="" residencia_fiscal="" num_reg_id_trib="" uso_cfdi="G01" id_receptor_sto="$id_cliente_receptor" id_receptor_erp="$id_cliente_receptor" numero_exterior="" calle="" numero_interior="" colonia="" localidad="" referencia="" municipio="" estado="" pais="" codigo_postal="" email="" id_tipo_receptor="" id_receptor_padre="" estatus_registro="1" >
		</RECEPTOR>
XML;
		if($flex_headers != ""){
		$archivo_importacion .= <<<XML
		<FLEX_HEADERS>
				$flex_headers
		</FLEX_HEADERS>
XML;
		}
		if($conceptos != ""){
		$archivo_importacion .= "\n". <<<XML
		<CONCEPTOS>
	$conceptos
		</CONCEPTOS>
XML;
		}
		$archivo_importacion .="\n".<<<XML
		<IMPUESTOS
XML;
		if($total_impuestos_retenidos != "" && $total_impuestos_retenidos != null){
	$archivo_importacion .=<<<XML
	totalImpuestosRetenidos="$total_impuestos_retenidos"
XML;
		}
		if($total_impuestos_trasladados != "" && $total_impuestos_trasladados != null){
	$archivo_importacion .=<<<XML
	totalImpuestosTrasladados="$total_impuestos_trasladados"
XML;
		}
		
		$archivo_importacion .=<<<XML
>
XML;
		if($trasladados != ""){
			$archivo_importacion .="\n".<<<XML
			<TRASLADADOS>
		$trasladados
			</TRASLADADOS>
XML;
		}
		
		if($retenidos != ""){
			$archivo_importacion .="\n".<<<XML
			<RETENIDOS>
				$retenidos
			</RETENIDOS>
XML;
		}
		
		$archivo_importacion .="\n".<<<XML
		</IMPUESTOS>
	</DOCUMENTO>
</DOCUMENTOS>
XML;

//echo $archivo_importacion;
//die();
// ------------- crear una tabla de relacion de nota de credito generada automaticamente desde portal ss
		
		//Guardarlos en la ruta para mandar el xml generado a Importacion XML
		$folder = $directorio_carga_tickets;
                $name='pss_nctrxorig_'.$id_trx33."_trxglobal_".$id_trx33_global.".xml";
                $file_name = $folder.$name;
                $control = fopen($file_name,"w+");
                if(file_exists($file_name)){
                    fwrite($control, $archivo_importacion);
                }
                fclose($control);
				
        $observaciones = "idtrx33obtenida: ".$transaccion->id_trx33_r." - Archivo nota de credito creado: ".$file_name;
        // transaccion en global pero permite continuar
        registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), BUSCAR_TRANSACCION_FACTURAR, $observaciones);
				
	}
    /*  Función que dependiendo de la configuración de la frecuencia de límite
     *  de facturación hace las correspondietes validaciones.
     * 
     *  @param  transacción que se desea validar si aún se puede facturar.
     * 
     *  @return verdadero si la transacción se puede facturar
     * 
     *  @author  Iván Mondragón
     *      */
    public function valida_limite_facturacion($transaccion) {
        /* Se obtiene la configuración del portal */
        $id_frecuencia = Model\Pss_config_portal::find(1)->id_frecuencia_max_fac;
        $frecuencia = Model\Pss_frecuencia_max_fac::find_by_id_frecuencia_max_fac($id_frecuencia,false);
        date_default_timezone_set('America/Mexico_City'); // Zona horaria
        
        /* Formatos para fechas */
        $formato_fecha          = "Y-m-d H:i:s";
        $formato_fecha_fin_mes  = "Y-m-t 23:59:59";
        
        /* Se obtiene la fecha de la transacción y la fecha de facturación */
        $fecha_transaccion      = $transaccion->fecha;
        $dias_posteriores       = $frecuencia->dias_posteriores;
        $cambio_anio            = $frecuencia->cambio_anio;
        $fecha_facturacion      = date('Y-m-d H:i:s');

        
        switch ($id_frecuencia) {
            /* Fecha transacción */
            case 1:
                /* Se obtiene la fecha límite respecto a los días en la configuración */
                $valida_anio    = true; // De inicio la validacion de año es correcta
                $dias           = $frecuencia->dias;
                $fecha_limite   = strtotime($fecha_transaccion . " +" . $dias . " day");
                $fecha_limite   = date($formato_fecha, $fecha_limite);

                /* Si no se permite cambio de año se hace la validación */
                if (!$cambio_anio) {

                    $valida_anio = $this->valida_cambio_anio($fecha_transaccion, $fecha_facturacion);
                }
                /* Se valida si la fecha de facturación no revasa la fecha límite */
                $valida_limite = $this->valida_fecha_limite($fecha_limite, $fecha_facturacion, $dias_posteriores);
                return $valida_anio && $valida_limite;
            /* Fecha calendario */
            case 2:
                /* Se obtiene la fecha de fin de mes, respecto de la transacción */
                $fecha_limite = date($formato_fecha_fin_mes, strtotime($fecha_transaccion));
                $mes = (int) date('m', strtotime($fecha_transaccion)); // se obtiene el mes
                
                /* Si no se permite cambio de año y el mes de la transacción es 
                 * diciembre, los días porteriores no se toman en cuenta */
                if (!$cambio_anio && $mes === 12) {
                    return $this->valida_fecha_limite($fecha_limite, $fecha_facturacion);
                }
                /* Si se permite cambio de año se hace solo la validación normal */
                return $this->valida_fecha_limite($fecha_limite, $fecha_facturacion, $dias_posteriores);

            default:
                /* Si no se tuviera configurado ninguna frecuencia */
                return true;
        }
    }
    /*  Función que valida si una transacción está dentro de la fecha límite,
     *  la cuál es configurada por el administrador del sistema.
     * 
     *  @param  fecha límite sin dias posteriores.
     *  @param  fecha del sistema (en el momento en que se desea facturar)
     *  @param  Días posteriores de la fecha límite de facturación
     * 
     *  @return verdadero si la fecha de facturación es <= a la fecha límite
     * 
     *  @author  Iván Mondragón 
     *      */
    public function valida_fecha_limite($fecha_límite,$fecha_facturacion,$dias_posteriores = 0){
                
        $formato_fecha = "Y-m-d H:i:s";
           
        /* Se obtiene la fecha límite con los días posteriores */
        $fecha_limite = strtotime($fecha_límite." +".$dias_posteriores." day" );
        $fecha_limite = date($formato_fecha, $fecha_limite);
              
        /* Se hace cast de la fecha de la transacción a tipo datetime para poder         
         * hacer la comparación de las fechas fácilmente
         *          */
        $fecha_limite       = new DateTime($fecha_limite);
        $fecha_facturacion  = new DateTime($fecha_facturacion);
        
        /* Si la fecha de facturación es mayor a la fecha límite, no pasa validación */
        if($fecha_facturacion > $fecha_limite){
            return false;
        }
        return true;
    }
    /*  Función que valida si la fecha de facturación se encuentra dentro del
     *  mismo año que la fecha de facturación.
     * 
     *  @param  fecha en la que se registró la transacción
     *  @param  fecha en la que se está intentando facturar.
     * 
     *  @return verdadero si la fecha de facturación no se encuentra en el mismo 
     *  año que la fecha de la transacción
     * 
     *  @author  Iván Mondragón 
     *      */
    public function valida_cambio_anio($fecha_transaccion, $fecha_facturacion) {

        /* Se hace cast de la fecha de la transacción a tipo datetime para poder         
         * hacer la comparación de las fechas fácilmente
         *          */
        $fecha_transaccion = new DateTime($fecha_transaccion);
        $fecha_facturacion = new DateTime($fecha_facturacion);
        /* Se saca la diferencia entre fechas */
        $diff = date_diff($fecha_transaccion, $fecha_facturacion);

        /* Si existe cambio de año entre las dos fechas no pasa validación */
        if ($diff->y > 0) {
            return false; // Fecha de facturación revasa año de fecha de transacción
        }
        return true;
    }

}
function nota_credito($id_global,$serie,$folio){
    date_default_timezone_set('America/Mexico_City');
    //$id_global = $transaccion_buscada['id_trx33'];
    $trx33_r                = Model\Emi_trx33_r::find_by_id_trx33_r($id_global);
    $trx_xml                = Model\Emi_trx33_xml::find_by_id_trx33($id_global);
    $trx33                  = Model\Emi_trx33::find_by_id_trx33($id_global);
    $trx33_concepto_r       = Model\Emi_trx33_concepto_r::find_by_id_trx33_concepto_r($id_global);
    $trx33_impuestos_r  = Model\Emi_trx33_con_impuestos_r::find_by_id_trx33_con_impuesto($id_global);
    $k=0;
foreach ($trx33_r as $trx33_r) {

    $fecha                  = $trx33_r->fecha;
    //$fecha                  = date('Y-m-d H:i:s');
    $forma_de_pago          = $trx33_r->id_forma_pago;
    $condiciones_de_pago    = $trx33_r->condiciones_de_pago;
    $tipo_cambio            = $trx33_r->tipo_cambio; 
    $moneda                 = $trx33_r->id_moneda;
    $metodo_pago            = 'PUE';//$trx33_r->id_metodo_pago;
    $lugar_expedicion       = $trx33_r->id_lugar_expedicion; 
    $tipo_comprobante       = $trx33_r->id_tipo_de_comprobante;
    $subtotal               = $trx33_r->subtotal;
    $descuento              = $trx33_r->descuento;
    if ($descuento==''||$descuento==null) {
        $descuento=0;
    }
    $total                  = $trx33_r->total;
    $confirmacion           = $trx33_r->confirmacion;
    $tipo_documento         = $trx33_r->id_tipo_documento;
    $id_trx_erp             = $trx33_r->id_trx_erp;
    $totalImpuestosRetenidos= $trx33_r->totalImpuestosRetenidos;
    $totalImpuestosTrasladados=$trx33_r->totalImpuestosTrasladados;
    $serie_original         = $trx33_r->serie;
    $folio_original         = $trx33_r->folio;
    $emisor_id_emisor_sto   = $trx33_r->id_emisor;
}
foreach ($trx_xml as $trx_xml) {
    //$tipo_relacion          = $trx_xml->
    $uuid_relacionado         = $trx_xml->uuid;
    //$id_trx_erp_original    = $trx_xml->
    
}
    //$enviar_xml             =
    //$enviar_pdf             =
    //$enviar_zip             =
    //$email                  =
foreach ($trx33 as $trx33) {
    $emisor_rfc                      = $trx33->rfc_emisor;  
    $emisor_nombre                   = $trx33->nombre_emisor;
    $emisor_regimen_fiscal           = $trx33->regimen_fiscal;
    //$emisor_id_emisor_sto            =
    //$emisor_id_emisor_erp            =
    //$sucursal_rfc                    =
    //$sucursal_nombre                 =
    //$sucursal_regimen_fiscal         =
    //$sucursal_id_emisor_sto          = $trx33_r->id_emisor;
    //$sucursal_id_emisor_erp          =
    //$sucursal_numero_interior        =
    //$sucursal_numero_exterior        =
    //$sucursal_calle                  =
    //$sucursal_colonia                =
    //$sucursal_localidad              =
    //$sucursal_referencia             =
    //$sucursal_municipio              =
    //$sucursal_estado                 =
    //$sucursal_pais                   =
    //$sucursal_codigo_postal          =
    //$sucursal_email                  =
    //$sucursal_id_tipo_emisor         =
    //$sucursal_id_emisor_padre        =
    //$sucursal_estatus_registro       =
    $receptor_rfc                    = $trx33->rfc_receptor;
    $receptor_nombre                 = $trx33->nombre_receptor;
    $receptor_residencia_fiscal      = $trx33->residencia_fiscal;
    $receptor_num_reg_id_trib        = $trx33->num_reg_id_trib;
    $receptor_uso_cfdi               = $trx33->uso_cfdi;
    //$receptor_id_receptor_sto        = $trx33_r->idreceptor;
    //$flex_l_clave                    =
    //$flex_l_nombre                   =
    //$flex_l_valor                    =
    //$totalImpuestosRetenidos         =
    //$totalImpuestosTrasladados       =
    $traslados_t_impuesto            = $trx33->total_impuestos_trasladados;
    //$traslados_t_tipo_factor         =
    //$traslados_t_tasa_o_cuota        =
    //$traslados_t_importe             =
    $retenidos_t_impuesto            = $trx33->total_impuestos_retenidos;
    //$retenidos_t_importe             =
}
    //$receptor_id_receptor_erp        =
    //$receptor_numero_exterior        =
    //$receptor_calle                  =
    //$receptor_numero_interior        =
    //$receptor_colonia                =
    //$receptor_localidad              =
    //$receptor_referencia             =
    //$receptor_municipio              =
    //$receptor_estado                 =
    //$receptor_pais                   =
    //$receptor_codigo_postal          =
    //$receptor_email                  =
    //$receptor_id_tipo_receptor       =
    //$receptor_id_receptor_padre      =
    //$receptor_estatus_registro       =
    //$destinatario_rfc                =
    //$destinatario_nombre             =
    //$destinatario_residencia_fiscal  =
    //$destinatario_num_reg_id_trib    =
    //$destinatario_uso_cfdi           =
    //$destinatario_id_receptor_sto    =
    //$destinatario_id_receptor_erp    =
    //$destinatario_numero_exterior    =
    //$destinatario_calle              =
    //$destinatario_numero_interior    =
    //$destinatario_colonia            =
    //$destinatario_localidad          =
    //$destinatario_referencia         =
    //$destinatario_municipio          =
    //$destinatario_estado             =
    //$destinatario_pais               =
    //$destinatario_codigo_postal      =
    //$destinatario_email              =
    //$destinatario_id_tipo_receptor   =
    //$destinatario_id_receptor_padre  =
    //$destinatario_estatus_registro   =
    //flexh_clave                      =
    //flexh_nombre                     =
    //flexh_valor                      =
    //$conceptos_numero_linea          =

$trx33_con_parte_r = Model\Emi_trx33_con_parte_r::find_by_id_trx33_con_parte($id_global);
$pss_config        = Model\Pss_config_portal::all();
foreach ($pss_config as $pss_config) {
    $id_cliente_autofactura_config = $pss_config->id_cliente_autofactura;
}
foreach ($trx33_concepto_r as $trx33_concepto_r) {
    $conceptos_clave_prod_serv      = '84111506';//$trx33_concepto_r->id_claveprodserv;
    $conceptos_cantidad             = $trx33_concepto_r->cantidad;
    $conceptos_clave_unidad         = 'ACT';//$trx33_concepto_r->id_clave_unidad;
    $conceptos_unidad               = $trx33_concepto_r->unidad;
    $conceptos_num_identificacion   = $trx33_concepto_r->numero_identificacion;
    $conceptos_descripcion          = $trx33_concepto_r->descripcion;
    $conceptos_valor_unitario       = $trx33_concepto_r->valor_unitario;
    $conceptos_importe              = $trx33_concepto_r->importe;
    $conceptos_descuento            = $trx33_concepto_r->descuento;
    if ($conceptos_descuento==''||$conceptos_descuento==null) {
        $conceptos_descuento = 0;
    }
    $numero_cuenta_predial          = $trx33_concepto_r->cuenta_predial;
    $numero_pedimento               = $trx33_concepto_r->info_aduanera_num_ped; 
}
   
    $traslados_impuesto         = '';
    $traslados_tipo_factor      = '';
    $traslados_tasa_o_cuota     = '';
    $traslados_importe          = '';

    $retenidos_base             = '';
    $retenidos_impuesto         = '';
    $retenidos_tipo_factor      = '';
    $retenidos_tasa_o_cuota     = '';
    $retenidos_importe          = '';
    $traslados_impuestos        = '';
    $retenidos_impuestos        = '';
    $traslados_totales          = '';
    $retenidos_totales          = '';
foreach ($trx33_impuestos_r as $trx33_impuestos_r) {
    if($trx33_impuestos_r->tipo_impuesto=='1'){
        
        $traslados_base             = $trx33_impuestos_r->base;
        $traslados_impuesto         = $trx33_impuestos_r->impuesto; 
        $traslados_tipo_factor      = $trx33_impuestos_r->tipo_factor; 
        $traslados_tasa_o_cuota     = $trx33_impuestos_r->tasa_cuota; 
        $traslados_importe          = $trx33_impuestos_r->importe; 
$traslados_impuestos = <<<XML
                    <TRASLADADOS>
                        <IMPUESTO base="$traslados_base" impuesto="$traslados_impuesto" tipo_factor="$traslados_tipo_factor" tasa_o_cuota="$traslados_tasa_o_cuota" importe="$traslados_importe"/>
                    </TRASLADADOS>
                </IMPUESTOS>
            </CONCEPTO>
        </CONCEPTOS>
        <IMPUESTOS totalImpuestosTrasladados="$totalImpuestosTrasladados">
            <TRASLADADOS>
                <IMPUESTO impuesto="$traslados_impuesto" tipo_factor="$traslados_tipo_factor" tasa_o_cuota="$traslados_tasa_o_cuota" importe="$traslados_importe"/>
            </TRASLADADOS>
XML;
    }
    if($trx33_impuestos_r->tipo_impuesto=='2'){
        $retenidos_base             = $trx33_impuestos_r->base; 
        $retenidos_impuesto         = $trx33_impuestos_r->impuesto; 
        $retenidos_tipo_factor      = $trx33_impuestos_r->tipo_factor; 
        $retenidos_tasa_o_cuota     = $trx33_impuestos_r->tasa_cuota; 
        $retenidos_importe          = $trx33_impuestos_r->importe; 
$retenidos_impuestos=<<<XML
                    <RETENIDOS>
                        <IMPUESTO base="$retenidos_base" impuesto="$retenidos_impuesto" tipo_factor="$retenidos_tipo_factor" tasa_o_cuota="$retenidos_tasa_o_cuota" importe="$retenidos_importe" />
                    </RETENIDOS>
                </IMPUESTOS>
            </CONCEPTO>
        </CONCEPTOS>
        <IMPUESTOS totalImpuestosRetenidos="$totalImpuestosRetenidos">
            <RETENIDOS>
                <IMPUESTO impuesto="$traslados_impuesto" importe="retenidos_importe" />
            </RETENIDOS>
XML;
    }
}
foreach ($trx33_con_parte_r as $trx33_con_parte_r) {
    $parte_num_pedimento            = $trx33_con_parte_r->num_pedimento; 
    $parte_clave_prod_serv          = $trx33_con_parte_r->clave_prod_serv; 
    $parte_cantidad                 = $trx33_con_parte_r->cantidad; 
    $parte_unidad                   = $trx33_con_parte_r->unidad;  
    $parte_num_identificacion       = $trx33_con_parte_r->num_identificacion; 
    $parte_descripcion              = $trx33_con_parte_r->descripcion; 
    $parte_valor_unitario           = $trx33_con_parte_r->valor_unitario; 
    $parte_importe                  = $trx33_con_parte_r->importe; 
}

        
$nota_de_credito =<<<XML
<?xml version='1.0' encoding='UTF-8'?>
<DOCUMENTOS>
    <DOCUMENTO serie="$serie" folio="$folio" fecha="$fecha" forma_de_pago="$forma_de_pago" tipo_cambio="$tipo_cambio" moneda="$moneda" metodo_pago="$metodo_pago" lugar_expedicion="$lugar_expedicion" tipo_comprobante="E" 
    subtotal="$subtotal" descuento="$descuento" total="$total" tipo_documento="2">
        <DOCUMENTO_ERP id_trx_erp="$id_trx_erp"/>
        <CFDIRELS>
            <CFDIREL tipo_relacion="01" uuid_relacionado="$uuid_relacionado" id_trx_erp_original="$id_global" serie_original="$serie_original" folio_original="$folio_original"/>
        </CFDIRELS>
        <EMISOR rfc="$emisor_rfc" nombre="$emisor_nombre" regimen_fiscal="$emisor_regimen_fiscal" id_emisor_sto="$emisor_id_emisor_sto" id_emisor_erp="$emisor_id_emisor_sto">
        </EMISOR>
        <RECEPTOR rfc="$receptor_rfc" nombre="$receptor_nombre" uso_cfdi="$receptor_uso_cfdi" id_receptor_sto="$id_cliente_autofactura_config" id_receptor_erp="$id_cliente_autofactura_config" id_tipo_receptor="1" estatus_registro="1" >
        </RECEPTOR>
        <CONCEPTOS>
            <CONCEPTO clave_prod_serv="$conceptos_clave_prod_serv" cantidad="$conceptos_cantidad" clave_unidad="$conceptos_clave_unidad" unidad="$conceptos_unidad" num_identificacion="$conceptos_num_identificacion" descripcion="Nota de Crédito" valor_unitario="$conceptos_valor_unitario" importe="$conceptos_importe" descuento="$conceptos_descuento" >
                <IMPUESTOS>

XML;
$nota_de_credito .= $traslados_impuestos;
$nota_de_credito .= $retenidos_impuestos;
$nota_de_credito .= <<<XML
        
        </IMPUESTOS>
    </DOCUMENTO>
</DOCUMENTOS>
XML;

return $nota_de_credito;
}
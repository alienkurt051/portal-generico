<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bitacora extends CI_Controller {

 function __construct() {
        parent::__construct();

        // si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
        $existe_sesion = $this->session->userdata("id_usuario");
        if ( empty( $existe_sesion ) ) {
            $this->session->set_flashdata("sesion_caduca","Su sesión ha caducado. Haga inicio de sesión nuevamente");
            redirect(site_url(),'refresh');
        }

    }

  /* controlador para gestionar las cuentas de usuario */
  public function index($usuario = null, $email = null, $fecha_inicio = null, $fecha_fin = null, $accion = null)
  {
    // clave del usuario
    $id_usuario = $this->session->userdata("id_usuario");

    $data = array();
    $data["id_usuario"] = $id_usuario;

    // la url para ver los detalles del usuario
    $url_buscar_bitacora = base_url()."index.php/bitacora/buscar_bitacora_usuario";
    $data["url_buscar_bitacora"] = $url_buscar_bitacora;

    // si se recibio el mensaje de confirmacion se agrega a los datos pasados a la vista
    if ( $this->session->flashdata('titulo') != null ) {
      $data["titulo"]       = $this->session->flashdata('titulo');
      $data["mensaje"]      = $this->session->flashdata('mensaje');
      $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
    }
	
	  $valorvacio = "NaV";
	  
	  // si no se recibio usuario
	  if ( strcmp($usuario, $valorvacio) === 0 ) {
		  $usuario = null;
	  }

	  if ( strcmp($email, $valorvacio) === 0 ) {
		  $email = null;
	  }
    
	if ($usuario != null ) {
		$data["usuario"] = $usuario;
	}
	
	if ($email != null ) {
		$data["email"] = $email;
	}

    // se arma la vista de listadao de eventos en bitacora
    cargar_interfaz_grafica($this, $data, "bitacora/view_content_wrapper_bitacora", "bitacora/view_content_wrapper_bitacora_script");
  }
  
  public function ajax_consultar_bitacora($usuario = null, $email = null, $fecha_inicio = null, $fecha_fin = null, $accion = null) {
	  
      $draw    = $this->input->post("draw");
      $start   = $this->input->post("start");
	  $length  = $this->input->post("length");
	  $search  = $_POST['search']['value'];

	  $valorvacio = "NaV";
	  
	  // si no se recibio usuario
	  if ( strcmp($usuario, $valorvacio) === 0 ) {
		  $usuario = null;
	  }

	  if ( strcmp($email, $valorvacio) === 0 ) {
		  $email = null;
	  }
	  
	  // todos los parametros
      if ( $usuario != null && $email != null ) {
		  $usuario = urldecode($usuario);
		  $email = urldecode($email);
		  $this->db->like("login", $usuario);  
		  $this->db->or_like("email", $email);
	  }

      if ( $usuario != null && $email == null ) {
		  $usuario = urldecode($usuario);
		  $this->db->like("login", $usuario);  
	  }
	  
      if ( $usuario == null && $email != null ) {

		  $email = urldecode($email);
		  $this->db->like("email", $email);
	  }

	  
	  $this->db->limit($length, $start);
      $arrbitacora = Model\v_pss_bitacora::all();
	  
	  $data = array();
      $conteo = 1;
	  $recordsTotal = 0;
	  foreach ($arrbitacora as $bitacora) {
		  $subdata    = array();
		  //$subdata[0] = $conteo;
		  $subdata[0] = ""; //$bitacora->id_bitacora;
		  $subdata[1] = $bitacora->login;
		  $subdata[2] = $bitacora->nombre;
		  $subdata[3] = $bitacora->email;
		  $subdata[4] = $bitacora->fecha_hora;
		  $subdata[5] = $bitacora->dir_ip;
		  $subdata[6] = $bitacora->d_accion;
		  $subdata[7] = $bitacora->observaciones;
		  
		  $conteo++;
		  $data[] = $subdata;
		  $recordsTotal++;
					
	  }
	  
	  // se obtiene el conteo total
      $sql = "SELECT count(*) as conteo FROM v_pss_bitacora";
	  
      if ( $usuario != null && $email != null ) {
		  $sql .= " WHERE login like '%".$usuario."%' or email like '%".$email."%'";
	  }
	  
      if ( $usuario != null && $email == null ) {
		  $sql .= " WHERE login like '%".$usuario."%'";
	  }
	  
      if ( $usuario == null && $email != null ) {
		  $sql .= " WHERE email like '%".$email."%'";
	  }
	  
	  /*
	  if ( $search != "" && $search != null ) {
		  $sql .= " WHERE login like '%".$search."%' or nombre like '%".$search."%' or email like '%".$search."%'";
	  }
      */
	  
      // se ejecuta el conteo
      $resultado = $this->db->query($sql);
      $rowconteo = $resultado->row();
	  
	  $recordsTotal = $rowconteo->conteo;

	  
	  $json_data = array(
	  "draw"             => $draw,
	  "recordsTotal"     => intval($recordsTotal),
	  "recordsFiltered"  => intval($recordsTotal),
	  "data"             => $data
	  );
	  
	  echo json_encode($json_data);
 }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Factura_global extends CI_Controller {

	function __construct() {
		parent::__construct();

		// si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
		$existe_sesion = $this->session->userdata("id_usuario");
		if ( empty( $existe_sesion ) ) {
			redirect(site_url(),'refresh');
		}

	}

	/* controlador para gestionar las cuentas de usuario */
	public function index()
	{
        $data = array();

		// se obtienen los datos del usuario
		$pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
		$data["pss_usuario"] = $pss_usuario;
        
        /*
        // se verifica si existe la tabla de configuracion de factura global
        if ( !$this->db->table_exists('emi_config_global'))
        {
            // tabla no existe, se asume que no esta instalado el modulo, se pone vista para elegir instalar
            $url_config_modulo_global = base_url()."index.php/factura_global/config_modulo_global";
            $data["url_config_modulo_global"] = $url_config_modulo_global;
            
        }
        */

        // si la tabla existe, entonces se obtienen las ultimas 100 configuraciones
        
		// registra evento en bitacora
		//registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), CONSULTAR_MIS_COMPROBANTES);
		
		$url_ver_resultados_ejecucion = base_url()."index.php/factura_global/obtener_listado_tickets_global";
		$data["url_ver_resultados_ejecucion"] = $url_ver_resultados_ejecucion;
        
		$url_ver_cifras_control = base_url()."index.php/factura_global/obtener_cifras_control_global";
		$data["url_ver_cifras_control"] = $url_ver_cifras_control;

		$url_agregar_ejecucion_global = base_url()."index.php/factura_global/agregar_ejecucion_global";
		$data["url_agregar_ejecucion_global"] = $url_agregar_ejecucion_global;
        
		$url_consultar_ejecucion = base_url()."index.php/factura_global/consultar_ejecucion";
		$data["url_consultar_ejecucion"] = $url_consultar_ejecucion;
		
    $url_consultar_detalles = base_url()."index.php/factura_global/consultar_detalles";
		$data["url_consultar_detalles"] = $url_consultar_detalles;
    
    $url_descargar_reporte = base_url()."index.php/factura_global/descargar_reporte/";
		$data["url_descargar_reporte"] = $url_descargar_reporte;
        
        $url_cancelar_ejecucion = base_url()."index.php/factura_global/cancelar_ejecucion";
        $data["url_cancelar_ejecucion"] = $url_cancelar_ejecucion;
        
		if ( $this->session->flashdata('titulo') != null ) {
			$data["titulo"]       = $this->session->flashdata('titulo');
			$data["mensaje"]      = $this->session->flashdata('mensaje');
			$data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
		}
		
		// se obtiene la lista de RFCs emisores
		$arr_emisores = Model\C_entidades::all();
		$data["arr_emisores"] = $arr_emisores;
    $this->db->order_by('descripcion');
    // Arreglo de formas de pago disponibles
		$arr_formas_pago = Model\Emi_c_forma_pago::all();
		$data["arr_formas_pago"] = $arr_formas_pago;

		// se obtienen las ultimas 100 solicitudes con respuesta
    $this->load->Model('model_factura_global');
		$arr_ejecucion_global = $this->model_factura_global->buscar_ejecuciones();
		$data["arr_ejecucion_global"] = $arr_ejecucion_global;
		
		// se arma la vista de asignatura
		cargar_interfaz_grafica($this, $data, "factura_global/view_content_wrapper_principal_factura_global",
            "factura_global/view_content_wrapper_principal_factura_global_script");
	}
    
    
    // ventana para agregar configuracion de ejecucion de global
    public function agregar_ejecucion_global() {
		$data = array();
		// se obtienen los datos del usuario
		$pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
		$data["pss_usuario"] = $pss_usuario;

        $config_portal = Model\pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;
		
		$url_alta_ejecucion = base_url()."index.php/factura_global/alta_ejecucion";
		$data["url_alta_ejecucion"] = $url_alta_ejecucion;

		$url_anterior = base_url()."index.php/factura_global/index";
		$data["url_anterior"] = $url_anterior;

		$url_detalle_ejecucion = base_url()."index.php/factura_global/agregar_detalle_ejecucion";
		$data["url_detalle_ejecucion"] = $url_detalle_ejecucion;


		if ( $this->session->flashdata('titulo') != null ) {
			$data["titulo"]       = $this->session->flashdata('titulo');
			$data["mensaje"]      = $this->session->flashdata('mensaje');
			$data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
		}
        
        // se obtienen los flex headers
        $this->db->where("nivel",1);
        $this->db->order_by("campo_adicional");
        $arr_flex_headers = Model\Emi_c_info_adicionales::all();
        $data["arr_flex_headers"] = $arr_flex_headers;
		
		// se obtiene la lista de RFCs emisores
		$arr_emisores = Model\C_entidades::all();
		$data["arr_emisores"] = $arr_emisores;
		
		// se arma la vista de asignatura
		cargar_interfaz_grafica($this, $data, "factura_global/view_content_wrapper_agregar_ejecucion_global", "factura_global/view_content_wrapper_agregar_ejecucion_global_script");
    }
	
    // ventana para agregar configuracion de ejecucion de global
    public function agregar_detalle_ejecucion() {
        
        $id_entidad     = $this->input->post("id_entidad");
        $tipo_ejecucion = $this->input->post("tipo_ejecucion");
        $hora_ejecucion = $this->input->post("hora_ejecucion");
        $dia_ejecucion  = $this->input->post("dia_ejecucion");
        $meses_elegidos = $this->input->post("mes");
        
		$data = array();
        
        // se envian los meses elegidos a la vista
        $data["meses_elegidos"] = $meses_elegidos;
        
        $entidad = Model\C_entidades::find($id_entidad, false);
        $data["entidad"] = $entidad;
        
        $arr_series_entidad = Model\C_series_entidad::find_by_id_entidad($id_entidad);
        $data["arr_series_entidad"] = $arr_series_entidad;
        
        $data["tipo_ejecucion"] = $tipo_ejecucion;
        
		// se obtienen los datos del usuario
		$pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
		$data["pss_usuario"] = $pss_usuario;

        $config_portal = Model\pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;
		
		$url_alta_ejecucion = base_url()."index.php/factura_global/alta_ejecucion";
		$data["url_alta_ejecucion"] = $url_alta_ejecucion;

		$url_anterior = base_url()."index.php/factura_global/agregar_ejecucion_global";
		$data["url_anterior"] = $url_anterior;

		if ( $this->session->flashdata('titulo') != null ) {
			$data["titulo"]       = $this->session->flashdata('titulo');
			$data["mensaje"]      = $this->session->flashdata('mensaje');
			$data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
		}
        
        $this->load->model("model_factura_global");
        $arr_ejecucion = $this->model_factura_global->obtener_arreglo_programacion($tipo_ejecucion, $meses_elegidos, $hora_ejecucion, $dia_ejecucion);
        $data["arr_ejecucion"] = $arr_ejecucion;
        
        // se obtienen los flex headers
        $this->db->where("nivel",1);
        $this->db->order_by("campo_adicional");
        $arr_flex_headers = Model\Emi_c_info_adicionales::all();
        $data["arr_flex_headers"] = $arr_flex_headers;
		
		// se obtiene la lista de RFCs emisores
		$arr_emisores = Model\C_entidades::all();
		$data["arr_emisores"] = $arr_emisores;
		
		// se arma la vista de asignatura
		cargar_interfaz_grafica($this, $data, "factura_global/view_content_wrapper_agregar_ejecucion_global_detalle", "factura_global/view_content_wrapper_agregar_ejecucion_global_script");
    }

  /**
   * Busca ejecuciones a partir de filtros de búsqueda recibidos vía post.
   */  
	public function consultar_ejecucion() {
		$this->validar_form_consulta();
    // se obtienen los parametros de busqueda
    $datos_filtro = [
        'id_entidad'      => trim($this->input->post("id_entidad")),
        'tipo_ejecucion'  => trim($this->input->post("tipo_ejecucion")),
        'id_forma_pago'   => trim($this->input->post("id_forma_pago")),
        'fecha_ejecucion' => trim($this->input->post("fecha_ejecucion")),
    ];

    $data = array();
    // Se hace la consulta a la BD a partir de los filtros que se reciben
    $this->load->model('model_factura_global');
    $data["arr_ejecucion_global"] = $this->model_factura_global->buscar_ejecuciones($datos_filtro);

    // si mandaron
    $arr_emisores = Model\C_entidades::all();
    $data["arr_emisores"] = $arr_emisores;
    // Arreglo de formas de pago disponibles
    $arr_formas_pago = Model\Emi_c_forma_pago::all();
    $data["arr_formas_pago"] = $arr_formas_pago;

    if ($this->session->flashdata('titulo') != null) {
      $data["titulo"]       = $this->session->flashdata('titulo');
      $data["mensaje"]      = $this->session->flashdata('mensaje');
      $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
    }
    $url_fact = "index.php/factura_global/";
    $url_ver_resultados_ejecucion = base_url($url_fact . "ver_resultados_ejecucion");
    $data["url_ver_resultados_ejecucion"] = $url_ver_resultados_ejecucion;

    $url_agregar_ejecucion_global = base_url($url_fact . "agregar_ejecucion_global");
    $data["url_agregar_ejecucion_global"] = $url_agregar_ejecucion_global;

    $url_consultar_ejecucion = base_url($url_fact . "consultar_ejecucion");
    $data["url_consultar_ejecucion"] = $url_consultar_ejecucion;

    $url_ver_cifras_control = base_url($url_fact . "obtener_cifras_control_global");
    $data["url_ver_cifras_control"] = $url_ver_cifras_control;

    $url_cancelar_ejecucion = base_url($url_fact . "cancelar_ejecucion");
    $data["url_cancelar_ejecucion"] = $url_cancelar_ejecucion;
    
    $url_consultar_detalles = base_url()."index.php/factura_global/consultar_detalles";
		$data["url_consultar_detalles"] = $url_consultar_detalles;
    
    $url_descargar_reporte = base_url()."index.php/factura_global/descargar_reporte/";
		$data["url_descargar_reporte"] = $url_descargar_reporte;
    // se arma la vista
    cargar_interfaz_grafica($this, $data, "factura_global/view_content_wrapper_principal_factura_global", 
            "factura_global/view_content_wrapper_principal_factura_global_script");
  }

  public function cancelar_ejecucion($id_emi_config_global) {
        $ejecucion = Model\Emi_config_global::find($id_emi_config_global);
        
        // se cancela
        $ejecucion->estatus = 100;
        $ejecucion->save();
        
        $this->session->set_flashdata('titulo', "Cancelación de ejecución de factura global");
        $this->session->set_flashdata('mensaje', "Se ha cancelado la ejecución de factura global correctamente");
        $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
		
		$url_login = base_url()."index.php/factura_global/index";
        redirect($url_login);
        
    }
    
    public function ver_resultados_ejecucion($id_emi_config_global, $offset = null) {
        $data = array();
        
		$url_anterior = base_url()."index.php/factura_global/index";
		$data["url_anterior"] = $url_anterior;

        $offset_anterior = 0;
        
        if ( $offset == null ) {
            $offset = 0;
        } else {
            if ( $offset > 0 ) {
                $offset_anterior = $offset - 1000;
            }
            
        }
        
        $offset_siguiente = $offset + 1000;
        
		$url_pagina_anterior = base_url()."index.php/factura_global/ver_resultados_ejecucion/".$id_emi_config_global."/".$offset_anterior;
		$data["url_pagina_anterior"] = $url_pagina_anterior;
        
		$url_pagina_siguiente = base_url()."index.php/factura_global/ver_resultados_ejecucion/".$id_emi_config_global."/".$offset_siguiente;
		$data["url_pagina_siguiente"] = $url_pagina_siguiente;
        
        // se obtienen los datos
        $this->load->model("model_factura_global");
        
        $arr_resultados = $this->model_factura_global->resultados_factura_global($id_emi_config_global, false, 2000, $offset);
        $data["arr_resultados"] = $arr_resultados;
        
		// se arma la vista
		cargar_interfaz_grafica($this, $data, "factura_global/view_content_wrapper_resultados_ejecucion", null);
    }
	
	public function alta_ejecucion() {
        // se obtienen los datos del formulario
        $id_entidad            = $this->input->post("id_entidad");
        $serie                 = $this->input->post("serie");
        $tipo_ejecucion        = $this->input->post("tipo_ejecucion");
        $rango_dia_ejecucion   = $this->input->post("rango_dia_ejecucion");
        $id_mes_ejecucion      = $this->input->post("id_mes_ejecucion");
        $fecha_inicio          = $this->input->post("fecha_inicio");
        $hora_inicio           = $this->input->post("hora_inicio");
        $fecha_fin             = $this->input->post("fecha_fin");
        $hora_fin              = $this->input->post("hora_fin");
        $hora_ejecucion        = $this->input->post("hora_ejecucion");
        $cond_ejecucion        = $this->input->post("cond_ejecucion");
        $dia_ejecucion         = $this->input->post("dia_ejecucion");
        $origen_num_ticket     = $this->input->post("origen_num_ticket");
        $origen_num_ticket_aux = $this->input->post("origen_num_ticket_aux");
        $tipo_global           = $this->input->post("tipo_global");
        $fecha_ejecucion       = $this->input->post("fecha_ejecucion");
        
        
		$data = array();

        // se asume que no hay errores
        $hubo_error = false;
        $mensaje_error = "";
        
        // el receptor es el configurado para la facturacion
        $config_portal = Model\pss_config_portal::find(1);
        $id_receptor = $config_portal->id_cliente_autofactura;
        
        foreach( $id_mes_ejecucion as $mes ) {
            
		    $nueva_ejecucion = new Model\Emi_config_global();
            
            $fecha_hora_inicio = $fecha_inicio[$mes]." ".$hora_inicio[$mes];
            $fecha_hora_fin    = $fecha_fin[$mes]." ".$hora_fin[$mes];
            
		    // se genera el registro de ejecucion
            $nueva_ejecucion->id_entidad            = $id_entidad           ;
            $nueva_ejecucion->serie                 = $serie                ;
            $nueva_ejecucion->id_receptor           = $id_receptor          ;
            $nueva_ejecucion->tipo_ejecucion        = $tipo_ejecucion       ;
            $nueva_ejecucion->rango_dia_ejecucion   = null                  ;
            $nueva_ejecucion->hora_inicio           = $fecha_hora_inicio    ;
            $nueva_ejecucion->hora_fin              = $fecha_hora_fin       ;
            $nueva_ejecucion->hora_ejecucion        = $hora_ejecucion[$mes] ;
            $nueva_ejecucion->cond_ejecucion        = 0                  ;
            $nueva_ejecucion->dia_ejecucion         = $dia_ejecucion[$mes]  ;
            $nueva_ejecucion->origen_num_ticket     = $origen_num_ticket    ;
            $nueva_ejecucion->origen_num_ticket_aux = $origen_num_ticket_aux;
            $nueva_ejecucion->tipo_global           = $tipo_global          ;
            $nueva_ejecucion->fecha_ejecucion       = $fecha_ejecucion[$mes];
            $nueva_ejecucion->estatus               = 0                     ;
            
            $insercion_correcta = false;
            
            $insercion_correcta = $nueva_ejecucion->save();
            
        }
            /*
            // si es para todos los emisores y sucursales
            if ( $id_entidad == "todose" || $id_entidad == "todass" || $id_entidad == "todas") {
                
                // solo emisores
                if ( $id_entidad == "todose" ) {
                    $this->db->where("id_tipo_entidad", 1);
                } else {
                    // solo sucursales
                    if ( $id_entidad == "todass" ) {
                        $this->db->where("id_tipo_entidad", 2);
                    }
                }
		        $arr_emisores = Model\C_entidades::all();
		        
                foreach ($arr_emisores as $emisor) {
                    $nueva_ejecucion->id_entidad = $emisor->id_entidad;
                    $insercion_correcta = $nueva_ejecucion->save();
                }
            } else {
		        $insercion_correcta = $nueva_ejecucion->save();
            }*/

            if ( $insercion_correcta ) {
                $this->session->set_flashdata('titulo', "Ejecución de factura global");
                $this->session->set_flashdata('mensaje', "Se ha registrado la ejecución de factura global correctamente");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
		        
		        $url_login = base_url()."index.php/factura_global/index";
                redirect($url_login);
                
            } else {
                
                $this->session->set_flashdata('titulo', "Ejecución de factura global");
                $this->session->set_flashdata('mensaje', "Ocurrió un error al registrar la ejecución de factura global. Por favor intente más tarde nuevamente.");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
                
		        $url_login = base_url()."index.php/factura_global/agregar_ejecucion_global";
                redirect($url_login);
            }

	}
    
    

	public function alta_ejecucion_back() {
        // se obtienen los datos del formulario
        $id_entidad            = $this->input->post("id_entidad");
        $serie                 = $this->input->post("serie");
        $tipo_ejecucion        = $this->input->post("tipo_ejecucion");
        $rango_dia_ejecucion   = $this->input->post("rango_dia_ejecucion");
        $hora_inicio           = $this->input->post("hora_inicio");
        $hora_fin              = $this->input->post("hora_fin");
        $hora_ejecucion        = $this->input->post("hora_ejecucion");
        $cond_ejecucion        = $this->input->post("cond_ejecucion");
        $dia_ejecucion         = $this->input->post("dia_ejecucion");
        $origen_num_ticket     = $this->input->post("origen_num_ticket");
        $origen_num_ticket_aux = $this->input->post("origen_num_ticket_aux");
        $tipo_global           = $this->input->post("tipo_global");
        $fecha_ejecucion       = $this->input->post("fecha_ejecucion");
        
        
		$data = array();

        // se asume que no hay errores
        $hubo_error = false;
        $mensaje_error = "";
        
        // la hora final no puede ser inferior a la hora inicial
        if ( $hora_fin < $hora_inicio ) {
            
            $mensaje_error .= "La hora final no puede ser menor a la hora inicial";
            $hubo_error = true;
        }
        
        if ( $fecha_ejecucion < date("Y-m-d") ) {
            $mensaje_error .= "<br>La fecha de ejecución no puede ser anterior a la fecha actual";
            $hubo_error = true;
        }
        
        if ( $hubo_error ) {
            $this->session->set_flashdata('titulo', "Solicitud de ejecución");
            $this->session->set_flashdata('mensaje', $mensaje_error);
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
            
		    $url_login = base_url()."index.php/factura_global/agregar_ejecucion_global";
            redirect($url_login);
        } else {
		    $nueva_ejecucion = new Model\Emi_config_global();
            
            // el receptor es el configurado para la facturacion
            $config_portal = Model\pss_config_portal::find(1);
        
            $id_receptor = $config_portal->id_cliente_autofactura;
            
		    // se genera el registro de ejecucion
            $nueva_ejecucion->id_entidad            = $id_entidad           ;
            $nueva_ejecucion->serie                 = $serie                ;
            $nueva_ejecucion->id_receptor           = $id_receptor          ;
            $nueva_ejecucion->tipo_ejecucion        = $tipo_ejecucion       ;
            $nueva_ejecucion->rango_dia_ejecucion   = $rango_dia_ejecucion  ;
            $nueva_ejecucion->hora_inicio           = $hora_inicio          ;
            $nueva_ejecucion->hora_fin              = $hora_fin             ;
            $nueva_ejecucion->hora_ejecucion        = $hora_ejecucion       ;
            $nueva_ejecucion->cond_ejecucion        = $cond_ejecucion       ;
            $nueva_ejecucion->dia_ejecucion         = $dia_ejecucion        ;
            $nueva_ejecucion->origen_num_ticket     = $origen_num_ticket    ;
            $nueva_ejecucion->origen_num_ticket_aux = $origen_num_ticket_aux;
            $nueva_ejecucion->tipo_global           = $tipo_global          ;
            $nueva_ejecucion->fecha_ejecucion       = $fecha_ejecucion      ;
            $nueva_ejecucion->estatus               = 0                     ;
            
            $insercion_correcta = false;
            
            // si es para todos los emisores y sucursales
            if ( $id_entidad == "todose" || $id_entidad == "todass" || $id_entidad == "todas") {
                
                // solo emisores
                if ( $id_entidad == "todose" ) {
                    $this->db->where("id_tipo_entidad", 1);
                } else {
                    // solo sucursales
                    if ( $id_entidad == "todass" ) {
                        $this->db->where("id_tipo_entidad", 2);
                    }
                }
		        $arr_emisores = Model\C_entidades::all();
		        
                foreach ($arr_emisores as $emisor) {
                    $nueva_ejecucion->id_entidad = $emisor->id_entidad;
                    $insercion_correcta = $nueva_ejecucion->save();
                }
            } else {
		        $insercion_correcta = $nueva_ejecucion->save();        
            }

            if ( $insercion_correcta ) {
                $this->session->set_flashdata('titulo', "Ejecución de factura global");
                $this->session->set_flashdata('mensaje', "Se ha registrado la ejecución de factura global correctamente");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
		        
		        $url_login = base_url()."index.php/factura_global/index";
                redirect($url_login);
                
            } else {
                
                $this->session->set_flashdata('titulo', "Ejecución de factura global");
                $this->session->set_flashdata('mensaje', "Ocurrió un error al registrar la ejecución de factura global. Por favor intente más tarde nuevamente.");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
                
		        $url_login = base_url()."index.php/factura_global/agregar_ejecucion_global";
                redirect($url_login);
            }
        }
        


	}
    
    public function obtener_cifras_control_global($id_emi_config_global, $tipo_global) {
        // se carga el modelo de factura global
        $this->load->model("model_factura_global");
        $arr_tickets_global = $this->model_factura_global->tickets_en_factura_global($id_emi_config_global);
        
        $nombre_archivo = FCPATH."/reports/factura_global";
        
        // se genera el archivo con el reporte
        if ( $tipo_global == 0 ) {
            // si es global previa
            $nombre_archivo .= "_PREVIA_";
        } else {
            // es productiva
            $nombre_archivo .= "_PRODUCTIVA_";
        }
        
        $nombre_archivo .= $id_emi_config_global.'.xls';
        
        $numero_fila = 1;
        /* Encabezados html y de la tabla */
        $html = "<!DOCTYPE html> <html> <head> "
                . "<title>Reporte de tickets</title> "
                . "<meta charset='utf-8'>"
                . "</head> <body>";        
        $table = "<table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>id               </th>
                        <th>sucursal         </th>
                        <th>numero_ticket    </th>
                        <th>fecha            </th>
                        <th>subtotal         </th>
                        <th>descuento        </th>
                        <th>iva              </th>
                        <th>total            </th>
                        <th>fecha_num_ticket </th>
                        <th>id_trx33         </th>
                        <th>facturado        </th>
                        <th>estatus          </th>
                        <th>id_global        </th>
                    </tr>
                </thead><tbody>";
        
        /* Se crea y se escribe el contenido en el archivo */
        $archivo = fopen($nombre_archivo, "w");
            fwrite($archivo, $html.$table);
        
            /* Se crea cada fila con sus respectivos datos */
            foreach ($arr_tickets_global as $ticket) {            
                $fila = "<tr>"
                       ."<td>" . $numero_fila                . "</td>"
                       ."<td>" . (string)$ticket["id"]               . "</td>"
                       ."<td>" . (string)$ticket["sucursal"]         . "</td>"
                       ."<td>" . (string)$ticket["numero_ticket"]    . "</td>"
                       ."<td>" . (string)$ticket["fecha"]            . "</td>"
                       ."<td>" . (string)$ticket["subtotal"]         . "</td>"
                       ."<td>" . (string)$ticket["descuento"]        . "</td>"
                       ."<td>" . (string)$ticket["iva"]              . "</td>"
					   ."<td>" . (string)$ticket["total"]            . "</td>"
					   ."<td>" . (string)$ticket["fecha_num_ticket"] . "</td>"
					   ."<td>" . (string)$ticket["id_trx33"]         . "</td>"
                       ."<td>" . (string)$ticket["facturado"]        . "</td>"
                       ."<td>" . (string)$ticket["estatus"]          . "</td>"
                       ."<td>" . (string)$ticket["id_global"]        . "</td>"
                       ."</tr>";
                fwrite($archivo, $fila);
                $numero_fila++;
            }
            $close_tag = "  </table></body> </html>";
            fwrite($archivo, $close_tag);
        fclose($archivo);
        
        /* Cabeceras para descargar el archivo */        
        if (file_exists($nombre_archivo)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($nombre_archivo).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($nombre_archivo));
            readfile($nombre_archivo);
            exit;
        }   
    }

    public function obtener_listado_tickets_global($id_emi_config_global, $tipo_global) {
        // se carga el modelo de factura global
        $this->load->model("model_factura_global");
        $arr_tickets_global = $this->model_factura_global->tickets_en_factura_global($id_emi_config_global);
        
        $nombre_archivo = FCPATH."/reports/factura_global";
        
        // se genera el archivo con el reporte
        if ( $tipo_global == 0 ) {
            // si es global previa
            $nombre_archivo .= "_PREVIA_";
        } else {
            // es productiva
            $nombre_archivo .= "_PRODUCTIVA_";
        }
        
        $nombre_archivo .= $id_emi_config_global.'.xls';
        
        $numero_fila = 1;
        /* Encabezados html y de la tabla */
        $html = "<!DOCTYPE html> <html> <head> "
                . "<title>Reporte de tickets</title> "
                . "<meta charset='utf-8'>"
                . "</head> <body>";        
        $table = "<table>
                <thead>
                    <tr>
                        <th>#                </th>
                        <th>id               </th>
                        <th>serie_r          </th>
                        <th>id_trx_erp       </th>
                        <th>folio_global     </th>
                        <th>trx33rg          </th>
                        <th>uuid             </th>
                        <th>sucursal         </th>
                        <th>numero_ticket    </th>
                        <th>fecha            </th>
                        <th>subtotal         </th>
                        <th>descuento        </th>
                        <th>iva              </th>
                        <th>total            </th>
                        <th>fecha_num_ticket </th>
                        <th>id_trx33         </th>
                        <th>facturado        </th>
                        <th>estatus          </th>
                        <th>id_global        </th>
                        <th>archivo_global   </th>
                        <th>forma_pago       </th>
                    </tr>
                </thead><tbody>";
              
        
        /* Se crea y se escribe el contenido en el archivo */
        $archivo = fopen($nombre_archivo, "w");
            fwrite($archivo, $html.$table);
        
            /* Se crea cada fila con sus respectivos datos */
            foreach ($arr_tickets_global as $ticket) {            
                $fila = "<tr>"
                    ."<td>" . $numero_fila                . "</td>"
                    ."<td>" . (string)$ticket["id"]               . "</td>"
                    ."<td>" . (string)$ticket["serie_r"]          . "</td>"
                    ."<td>" . (string)$ticket["id_trx_erp"]       . "</td>"
                    ."<td>" . (string)$ticket["folio_global"]     . "</td>"
                    ."<td>" . (string)$ticket["trx33rg"]          . "</td>"
                    ."<td>" . (string)$ticket["uuid"]             . "</td>"
                    ."<td>" . (string)$ticket["sucursal"]         . "</td>"
                    ."<td>&nbsp;" . (string)$ticket["numero_ticket"]    . "</td>"
                    ."<td>" . (string)$ticket["fecha"]            . "</td>"
                    ."<td>" . (string)$ticket["subtotal"]         . "</td>"
                    ."<td>" . (string)$ticket["descuento"]        . "</td>"
                    ."<td>" . (string)$ticket["iva"]              . "</td>"
					."<td>" . (string)$ticket["total"]            . "</td>"
					."<td>" . (string)$ticket["fecha_num_ticket"] . "</td>"
					."<td>" . (string)$ticket["id_trx33"]         . "</td>"
                    ."<td>" . (string)$ticket["facturado"]        . "</td>"
                    ."<td>" . (string)$ticket["estatus"]          . "</td>"
                    ."<td>" . (string)$ticket["id_global"]        . "</td>"
                    ."<td>" . (string)$ticket["ruta_archivo"]     . "</td>"
                    ."<td>" . (string)$ticket["id_forma_pago"]    . "</td>"
                    ."</tr>";
                fwrite($archivo, $fila);
                $numero_fila++;
            }
            $close_tag = "  </table></body> </html>";
            fwrite($archivo, $close_tag);
        fclose($archivo);
        
        /* Cabeceras para descargar el archivo */        
        if (file_exists($nombre_archivo)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($nombre_archivo).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($nombre_archivo));
            readfile($nombre_archivo);
            exit;
        }   
    }

    /**
     * Realiza la validación del formulario de búsqueda de consulta de ejecuciones
     * Si existe al algún error dependiendo las reglas, se redigire al index y se
     * envía el mensaje del error.
     */
    private function validar_form_consulta() {
    $this->form_validation->set_rules('id_entidad', 'Emisor', 'required');
    $this->form_validation->set_rules('fecha_ejecucion', 'Fecha ejecución', 'callback_validar_fecha|trim');
    $this->form_validation->set_message('validar_fecha', 'La fecha no coincide con el formato yyyy/mm/dd');
    // Si hay error en la validación se toma el primer error y se manda
    if ($this->form_validation->run() == FALSE) {
      $errors = explode("</p>", validation_errors());
      $this->session->set_flashdata('titulo', "Error al consultar ejecuciones");
      $this->session->set_flashdata('mensaje', "$errors[0]");
      $this->session->set_flashdata('tipo_mensaje', 'alert alert-warning alert-dismissible');

      $url_index = base_url() . "index.php/factura_global/index";
      redirect($url_index);
    }
  }

  /**
   * Valida el formato de la fecha_ejecucion, si es vacía también es valido ya que
   * es opcional. La función la manda llamar el validador de codeigniter
   * 
   * @param string $fecha
   * @return boolean True si es correcto el formato o si está vacía
   */
  public function validar_fecha($fecha) {
    $patron_fecha = "/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/";
    return empty($fecha) || (preg_match($patron_fecha, $fecha) === 1) ? true : false;
  }

  /**
   * Connsulta los detalles de una ejecución de una factura global a partir del
   * id_emi_config_global y el id_entidad, regresa un json si todo es correcto
   * 
   * @param string $fecha
   * @return json Regresa el código, mensaje y arreglo de detalles (si hay)
   */
  public function consultar_detalles() {
    // Si las validaciones de los requisitos son correctas
    if ($this->validar_ids_consulta_detalles()) {
      $ids = explode('|', trim($this->input->post('datos')));
      $this->load->model('model_factura_global');
      $filtros_detalles = [
          'id_emi_config_global' => $ids[0], 'id_entidad' => $ids[1]
      ];
      $arr_detalles = $this->model_factura_global->obtener_detalles($filtros_detalles);
      echo $this->responder_json(200, 'Detalles de facturas globales', $arr_detalles);
    }
  }

  /**
   * Construye una estructura JSON con código, mensaje y datos.
   * 
   * @param int $codigo Código de respuesta del json
   * @param string $mensaje Mensaje de respuesta del json
   * @param mix $datos Datos que se deseen regresar en cualquier formato
   * @return json Formato JSON con los parámetros recibidos 
   */
  private function responder_json($codigo,$mensaje,$datos) {
    return json_encode([
        'code'    => (int)$codigo,
        'message' => $mensaje,
        'data'    => $datos
    ],JSON_UNESCAPED_UNICODE);
  }
  
  /**
   * Valida que los ids para consultar los detalles lleguen correctamente, el 
   * formato en el que deben estar es id_emi_config_global|id_entidad ej '1|2'
   * 
   * @param string datos Se recibe mediante post ['datos' => '1|2']
   * @return bool True si se valida correctamente, false de lo contrario
   */
  private function validar_ids_consulta_detalles() {
    $datos = trim($this->input->post('datos'));
    $patron = '/^[0-9]*[|{1}?][0-9]*/';
    if (preg_match($patron, $datos) !== 1) {
      echo $this->responder_json('404', 'No se recibió correctamente los datos de la petición', null);
      return false;
    }
    return true;
  }

  /**
   * Descarga un reporte de factura global a partir de un id_global el cual se le
   * genera a cada XML que se genera cuando se ejecuta un proceso de factura global
   * 
   * @param int $id_global Identificador del xml de la factura global
   */
  public function descargar_reporte($id_global) {
    // se carga el modelo de factura global
    $this->load->model("model_factura_global");
    $arr_tickets_global = $this->model_factura_global->consultar_reporte((int)$id_global);
    $nombre_archivo = FCPATH . "reports/"
            . $this->model_factura_global->consultar_nombre_reporte($id_global) . '.xls';
    $numero_fila = 1;
    /* Encabezados html y de la tabla */
    $html = "<!DOCTYPE html> <html> <head> "
            . "<title>Reporte factura global</title> "
            . "<meta charset='utf-8'>"
            . "</head> <body>";
    $table = "<table><thead><tr><th>#</th>";
    // Coloca dinámicamente los headers de la tabla
    foreach ($arr_tickets_global[0] as $key => $value) {
      $table .= "<th>$key</th>";
    }
    $table .= "</tr></thead><tbody>";

    /* Se crea y se escribe el contenido en el archivo */
    $archivo = fopen($nombre_archivo, "w");
    fwrite($archivo, $html . $table);

    /* Se crea cada fila con sus respectivos datos */
    foreach ($arr_tickets_global as $ticket) {
      $fila = "<tr>" . "<td>" . $numero_fila . "</td>";
      foreach ($ticket as $value) {
        $fila .= "<td>" . (string) $value . "</td>";
      }
      $fila .= "</tr>";
      fwrite($archivo, $fila);
      $numero_fila++;
    }
    $close_tag = "  </table></body> </html>";
    fwrite($archivo, $close_tag);
    fclose($archivo);
    $this->colocar_headers_descarga_xls($nombre_archivo);
  }

  /**
   * Coloca los header necesarios para iniciar la descargar de un archivo excel
   * con extensión XLS.
   * @param string $nombre_archivo Full path del archivo con extensión xls.
   */
  private function colocar_headers_descarga_xls($nombre_archivo){
    /* Cabeceras para descargar el archivo xls */
    if (file_exists($nombre_archivo)) {
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename="' . basename($nombre_archivo) . '"');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($nombre_archivo));
      readfile($nombre_archivo);
      exit;
    }
  }

}
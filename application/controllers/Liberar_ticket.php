<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Liberar_ticket extends CI_Controller {

    function __construct() {
        parent::__construct();

        // si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
        $existe_sesion = $this->session->userdata("id_usuario");
        if (empty($existe_sesion)) {
            redirect(site_url(), 'refresh');
        }
    }
    
    /* Dirige a vista para buscar transacción que se intentará liberar.
     * 
     *      */

    public function index() {
        // se crea el arreglo para paso de parametros
        $data = array();
        // url de la vista siguiente, si es correcta la informacion
        $url_vista_previa           = base_url("index.php/liberar_ticket/busqueda_ticket");
        $data["url_vista_previa"]   = $url_vista_previa;
        $url_anterior               = base_url("index.php/" . URL_PANTALLA_PRINCIPAL);
        $data["url_anterior"]       = $url_anterior;
        // se obtiene la lista de entidades que si tienen serie definida para facturar
        $series_entidades = Model\Pss_series_entidades::all();

        // si no hay sucursales con series definidas
        if (empty($series_entidades)) {            
            $mensaje_error          = "Ninguna de las entidades emisoras han sido configuradas con una serie para facturar. No es posible continuar con el proceso de liberación de ticket.";
            $data["mensaje_error"]  = $mensaje_error;
            cargar_interfaz_grafica($this, $data, 'facturar_ticket/view_content_wrapper_error_config_facturacion', null);
            return;
        }
        // entidades con series para facturar
        $this->load->model("model_buscar_transaccion");
        $data["arr_entidades"] = $this->model_buscar_transaccion->obtener_entidades_con_serie();

        // campos para busqueda de transaccion
        $arr_campos_transaccion         = Model\V_pss_campos_transaccion::all();
        $data["arr_campos_transaccion"] = $arr_campos_transaccion;
        if ( $this->session->flashdata('titulo') != null ) {
          $data["titulo"]       = $this->session->flashdata('titulo');
          $data["mensaje"]      = $this->session->flashdata('mensaje');
          $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
        }        
        cargar_interfaz_grafica($this, $data, 'liberar_ticket/view_content_wrapper_captura_busqueda_ticket', 'liberar_ticket/view_script_captura_facturacion');
    }

    /* Verifica si la transacción existe y está cancelada/timbrada/notimbrada, si
     * es así dirige a método confirmar_liberar_modificar_ticket, si no existe 
     * dirige a vista de búsqueda con mensaje de warning.
     * 
     * @param int sucursal Identificador de sucursal (vía post)
     * @param string Identificador del concepto, pueden ser más de uno (vía post)
     *      */

    public function busqueda_ticket() {

        // se crea el arreglo para paso de parametros
        $data                           = array();
        $id_entidad                     = trim($this->input->post("sucursal"));
        $arr_campos                     = array();

        // se obtiene la configuracion del portal
        $config_portal                  = Model\Pss_config_portal::find(1);
        $data["config_portal"]          = $config_portal;

        // se obtiene la lista de campos flex
        $arr_campos_transaccion         = Model\V_pss_campos_transaccion::all();
        $data["arr_campos_transaccion"] = $arr_campos_transaccion;

        $i = 1;
        foreach ($arr_campos_transaccion as $campo) {
            $arr_campos[$i]["id_flex_header"]   = $campo->id_flex_header;
            $arr_campos[$i]["campo_adicional"]  = $campo->campo_adicional;
            $arr_campos[$i]["valor"]            = trim($this->input->post($campo->campo_adicional));
            $i++;
        }

        // valor de campos para transaccion a liberar
        $data["arr_valor"] = $arr_campos;

        $this->load->model("model_buscar_transaccion");
        $arr_transacciones = $this->model_buscar_transaccion->obtener_idtrx33_transaccion($arr_campos, $id_entidad);

        // se definen las urls
        $url_transaccion_no_encontrada = base_url("index.php/liberar_ticket");

        // si no se encontro la transaccion
        if (count($arr_transacciones) < 1) {
            $this->session->set_flashdata('titulo', "Transacción no encontrada");
            $this->session->set_flashdata('mensaje', "No se encontraron transacciones con los datos utilizados para la búsqueda. Por favor, verifique sus datos e intente nuevamente.");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-warning alert-dismissible');
            redirect($url_transaccion_no_encontrada);
        } else {
            // si existe mas de una coincidencia
            if (count($arr_transacciones) > 1) {
                $this->session->set_flashdata('titulo', "Error en búsqueda de transacción");
                $this->session->set_flashdata('mensaje', "Ocurrió un error al buscar su transacción. Los datos proporcionados generaron más de un caso correcto. El administrador ha sido notificado.");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
                redirect($url_transaccion_no_encontrada);
            } else {
                // transaccion encontrada. Se obtienen los datos de la transaccion a liberar
                // recuperar datos del ticket, serie, folio, subtotal y total
                $transaccion_buscada = $arr_transacciones[1];
                $transaccion = Model\Emi_trx33_r::find($transaccion_buscada["id_trx33"], false);
                
                // Si no se puede liberar se redirige con mensaje de warning
                $this->validar_estatus_transaccion($transaccion->id_trx33_r);

                $data["transaccion"] = $transaccion;

                $config_portal = Model\Pss_config_portal::find(1);
                $data["config_portal"] = $config_portal;

                // conceptos de la transaccion
                $conceptos = $this->db->query("SELECT * FROM emi_trx33_concepto_r WHERE id_trx33_r =" . $transaccion->id_trx33_r);
                $data["conceptos"] = $conceptos->result();                
                // Si se puede liberar se manda a la confirmación
                $url_liberar_modificar_ticket = base_url() . "index.php/liberar_ticket/confirmar_liberar_modificar_ticket/" . $transaccion_buscada["id_trx33"];
                redirect($url_liberar_modificar_ticket);
            }
        }
    }

    /* Si la transacción existe se ejecuta esta función, se recupera su 
     * información de la BD para presentar en una vista previa de confirmación,
     * si se confirma la liberación se redirige al controller plugins/liberar_ticket_decathlon
     * en el método liberar_ticket, el cual será el que realice la liberación del
     * ticket.
     * 
     * @param int $id_trx33 Identificador de la transacción a liberar     
     *      */
    
    public function confirmar_liberar_modificar_ticket($id_trx33) {

        // se crea el arreglo para paso de parametros
        $data                   = array();
        // se obtiene la configuracion del portal
        $config_portal          = Model\Pss_config_portal::find(1);
        $data["config_portal"]  = $config_portal;
        $transaccion            = Model\Emi_trx33_r::find($id_trx33, false);
        $data["transaccion"]    = $transaccion;

        // conceptos de la transaccion
        $conceptos = $this->db->query("SELECT * FROM emi_trx33_concepto_r WHERE id_trx33_r = " . $id_trx33);
        $data["conceptos"]          = $conceptos->result();
        $data["url_liberar_ticket"] = base_url("index.php/plugins/liberar_ticket_decathlon/liberar_ticket");
        $data["url_anterior"]       = base_url("index.php/liberar_ticket/index");

        // se abre la ventana para confirmar la liberacion, edicion de ticket
        cargar_interfaz_grafica($this, $data, 'liberar_ticket/view_content_wrapper_confirmar_liberar_modificar', false);
    }

    /* Valida si es posible liberar la transacción a partir de su id_trx33, si no
     * está facturado a pendiente se procede con la liberación.
     * 
     * @param int $id_trx33 Identificador de la transacción a validar     
     *      */
    
    private function validar_estatus_transaccion($id_trx33) {
        // Se obtiene estatus de factura; pendiente, facturado, cancelado, c/error
        $this->load->model("model_buscar_transaccion");
        $codigo = $this->model_buscar_transaccion->buscar_estatus_transaccion($id_trx33);
        $url_transaccion_no_valida = base_url("index.php/liberar_ticket");
        // Si está pendiente
        if ($codigo === 0) {
            $this->session->set_flashdata('titulo', "Transacción pendiente");
            $this->session->set_flashdata('mensaje', "La transacción se encuentra pendiente de facturar por lo que no se permite liberar, por favor intente con otra.");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-warning alert-dismissible');
            redirect($url_transaccion_no_valida);
        } // Si ya se facturó
        if ($codigo === 1) {
            $this->session->set_flashdata('titulo', "Transacción facturada");
            $this->session->set_flashdata('mensaje', "La transacción se encuentra facturada por lo que no se permite liberar, por favor intente con otra.");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-warning alert-dismissible');
            redirect($url_transaccion_no_valida);
        }
    }
        
}

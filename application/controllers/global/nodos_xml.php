<?php
/* Archivo que almacena las constantes que se ocupan para formar un XML de 
 * factura global. Básicamente son los nodos que puede llevar un doc XML
 * 
 * @author  Iván Mondragón
 *  */
/* Datos generales de XML */
const VERSION   = '1.0';
const ENCODING  = 'UTF-8';
const FRECUENCIA = 'ultimo';
/* Primeros nodos */
const DOCUMENTOS        = 'DOCUMENTOS';
const DOCUMENTO         = 'DOCUMENTO';
const DOCUMENTO_ERP     = 'DOCUMENTO_ERP';
const EMISOR            = 'EMISOR';
const SUCURSAL          = 'SUCURSAL';
const RECEPTOR          = 'RECEPTOR';

/* Nodo conceptos */
const CONCEPTOS     = 'CONCEPTOS';
const CONCEPTO      = 'CONCEPTO';
const IMPUESTOS     = 'IMPUESTOS';
const TRASLADADOS   = 'TRASLADADOS';
const IMPUESTO      = 'IMPUESTO';
const FLEX_LINES    = 'FLEX_LINES';
const FLEX_LINE     = 'FLEX_LINE';

/* Atributos de nodos */
$DOCUMENTO_ATTR = [
    'serie'             => null, 
    'folio'             => null, 
    'fecha'             => null, 
    'forma_de_pago'     => "01", 
    'metodo_pago'       => "PUE", 
    'moneda'            => "MXN", 
    'tipo_cambio'       => 1,
    'lugar_expedicion'  => null, 
    'subtotal'          => 0, 
    'descuento'         => 0, 
    'total'             => 0, 
    'tipo_documento'    => 1, 
    'tipo_comprobante'  => "I"
   ];
$DOCUMENTO_ERP_ATTR = [
    'id_trx_erp'        => 1
    ];
$EMISOR_ATTR = [
    'id_emisor_erp'     => "1001", /* VALOR CAMBIADO DEPENDIENDO DEL CLIENTE */
    'id_emisor_sto'     => "1001", /* VALOR CAMBIADO DEPENDIENDO DEL CLIENTE */
    'nombre'            => "Aeropuerto Internacional de la Ciudad de México, S.A. de C.V.", /* VALOR CAMBIADO DEPENDIENDO DEL CLIENTE */
    'regimen_fiscal'    => "601", 
    'rfc'               => "AIC980601988" /* VALOR CAMBIADO DEPENDIENDO DEL CLIENTE */
    ];
$SUCURSAL_ATTR = [
    'calle'             => null, 
    'codigo_postal'     => null, 
    'colonia'           => null, 
    'estado'            => null, 
    'estatus_registro'  => 1, 
    'id_emisor_erp'     => null,// id de sucursal
    'id_emisor_padre'   => "1001", /* VALOR CAMBIADO DEPENDIENDO DEL CLIENTE */
    'id_emisor_sto'     => null, // id de sucursal
    'id_tipo_emisor'    => "2", 
    'localidad'         => null, 
    'municipio'         => null, 
    'nombre'            => null,
    'numero_exterior'   => null, 
    'numero_interior'   => null, 
    'pais'              => null, 
    'referencia'        => null, 
    'regimen_fiscal'    => null,
    'rfc'               => null
   ];
$RECEPTOR_ATTR = [
    'rfc'               => "XAXX010101000", 
    'nombre'            => "Ventas al publico en general", 
    'residencia_fiscal' => null, 
    'num_reg_id_trib'   => null, 
    'uso_cfdi'          => "P01", 
    'id_receptor_sto'   => "2", /* VALOR CAMBIADO DEPENDIENDO DEL CLIENTE */
    'numero_exterior'   => null, 
    'calle'             => null, 
    'numero_interior'   => null, 
    'colonia'           => null, 
    'localidad'         => null, 
    'municipio'         => null,
    'estado'            => null, 
    'pais'              => null, 
    'codigo_postal'     => null, 
    'email'             => null, 
    'estatus_registro'  => "3"
   ];
/* Atributos de nodo conceptos y subnodos */
$CONCEPTO_ATTR = [
    'numero_linea'          => null, 
    'clave_prod_serv'       => "01010101", 
    'cantidad'              => 1,
    'clave_unidad'          => "ACT",
    'num_identificacion'    => null, // clave del ticket
    'descripcion'           => "Venta", 
    'valor_unitario'        => null, //ejemplo (subtotal)
    'importe'               => null, // ejemplo misma cantidad de valor unitario(subtotal)
    'descuento'             => 0.00 // se toma de tabla, sino tiene es cero
   ];
$IMPUESTO_ATTR = [
    'base'          => null, // Si el subtotal * 0.16 = iva real, se coloca el subtotal en la base
    'impuesto'      => "002", 
    'tipo_factor'   => "Tasa", 
    'importe'       => null, // iva real 
    'tasa_o_cuota'  => "0.160000"
    ];
/* Atributos constantes en el concepto que está exento de iva */
$IMPUESTO_ATTR_EXCENTO = [
    'base'          => null,
    'impuesto'      => "002", 
    'tipo_factor'   => "Exento"
    ];
$FLEX_LINE_ATTR = [
    'nombre'    => "trx_33", // solicitar 
    'valor'     => null, // valor de id_trx33 que se consulta a partit de num_identificacion
    'clave'     => "trx_33" // solicitar 
    ];
/* Atributos de impuestos totales */
$IMPUESTOS_TOTALES_ATTR = [
    'totalImpuestosTrasladados'     => "0.00", 
    'totalImpuestosRetenidos'       => "0.00" // siempre cero con anforama
    ];
$IMPUESTO_TOTAL_ATTR = [
    'impuesto'      => "002", 
    'tipo_factor'   => "Tasa", 
    'importe'       => "0.00", // iva real 
    'tasa_o_cuota'  => "0.160000"
    ];
/* Atributos constantes cuando todos los conceptos están exentos de iva */
$IMPUESTO_TOTAL_EXENTO_ATTR = [
    'impuesto'      => "002", 
    'tipo_factor'   => "Exento", 
    'importe'       => "0.00", // iva real 
    'tasa_o_cuota'  => "0.000000"
    ];
// si hay descuento
// verificar:
// si el descuento real menos el subtotal) * 0.16 = al iva real (redondeando a 2 cifras) entonces se coloca en la base 
// la base es igual al subtotal - descuento (redondeado a dos cifras) 
// y el importe es subtotal - descuento real * 0.16 (que debe ser igual a iva real)
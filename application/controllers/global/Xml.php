<?php
/** Script que se ejecuta desde consola y genera una factura global, se ejecuta
 *  de la siguiente manera ('C:\xampp2\php\php.exe' es la ruta de php): 
 * 
 *      C:\xampp2\php\php.exe index.php global xml index 1006 201801 C%3A%5Cglobales 01 1000
 * 
 * @param (1006)      -> identificador de la sucursal
 * @param (201801)    -> mes en que se desea generar la factura ( aaaamm )
 * @param (C%3A%5Cglobales%5C)-> ruta donde se almacenará el xml (con permisos 
 *                       lectura/escritura) se deberá codificar como url.
 * @param (01)        -> forma de pago más utilizada en los tickets del xml.
 * @param (1000)      -> número máximo de tickets (conceptos) que entrarán en la global
 *                      este parámetro es el único que es opcional, por default entran
 *                      1200 conceptos
 * 
 * Author: Iván Mondragón Sotres
 *  */
defined('BASEPATH') OR exit('No direct script access allowed');
require('nodos_xml.php');
class Xml extends CI_Controller {
    
    private $valor_unitario = 0;
    private $iva_calculado  = 0;
    private $descuento      = 0;
    private $iva            = 0.16;
    private $id_procesados  = [];
    private $global = null;

    
    function __construct() {
        parent::__construct();
        $this->load->model('global/model_factura_global');
        $this->load->library('cli');
    }

    public function index($sucursal = false, $fecha = false, $ruta_archivo = false, $forma_pago = false,$cantidad_tickets = 999) {

        if (!$sucursal || !$fecha || !$cantidad_tickets || !$ruta_archivo || !$forma_pago){
            $mensaje = "         Parametros incompletos          ";
            $this->model_factura_global->registrar_bitacora(1,$mensaje);
            $this->cli->coloar_mensaje_error_cli($mensaje);
        }        
//        die("Parametros completos $sucursal $fecha $cantidad_tickets");
        /* Obtiene un id_global consecutivo */
        $this->global = $this->obtener_maximo_id_global();
        
        /* Se construye ruta absoluta/nombre del archivo xml */
        $nombre_archivo = date("Y-m-d_"). date("H_i_s")."_$sucursal";
        $ruta_archivo   = urldecode($ruta_archivo).$nombre_archivo."_"."$this->global.xml";
        $this->validar_escritura_archivo($ruta_archivo);
        
        /* Se construye el layout del xml y al final se colocan los valores totales */
        $this->crear($ruta_archivo,$sucursal,$fecha,$cantidad_tickets);
        $this->guardar_ruta_archivo($ruta_archivo,$forma_pago);
        $this->modificar_attr_documento($ruta_archivo,$sucursal,$forma_pago);        

    }
    /*  Se crea el layout del XML con XMLWriter, nodo a nodo y colocando sus atributos
     * 
     *  @param  ruta absoluta del archivo que se creará
     *  @param  sucursal de la que se generará la factura global
     *  @param  fecha (año y mes) en la que se buscarán los tickets para la global
     *  @param  número máximo de tickets (conceptos) que tendrá el xml 
     *          (no entra los que tienen error de cálculo de IVA)
     *      */
    function crear($nombre_archivo,$sucursal,$fecha,$cantidad_tickets) {
        global $DOCUMENTO_ATTR;
        
        $this->validar_existe_clase('XMLWriter');
        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->setIndent(true);
        $xml->startDocument(VERSION, ENCODING);

        $xml->startElement(DOCUMENTOS);
            $xml->startElement(DOCUMENTO);
            foreach ($DOCUMENTO_ATTR as $nombre_attr => $valor_attr) {
                $xml->writeAttribute($nombre_attr,$valor_attr);
            }       
                
                /* ERP */
                $this->nodo_erp($xml);                
                /* Emisor */
                $this->cargar_info_emisor_sucursal($sucursal);
                $this->nodo_emisor($xml,$sucursal); 
                /* Receptor */
                $this->cargar_info_receptor($sucursal); 
                $this->nodo_receptor($xml);               
                
                
                /* Conceptos */
                $xml->startElement(CONCEPTOS);
                    $tickets = $this->obtener_tickets($sucursal,$fecha,$cantidad_tickets);
                    /* Recorrer conceptos (n) */
                    $numero_linea = 1;
                    foreach ($tickets->result() as $ticket) {                        
                        /* Concepto */
                        $this->cargar_attr_concepto($ticket,$numero_linea);
                        $this->nodo_concepto($xml,$ticket);
                        $this->crear_registro_global($ticket,$sucursal);
                        $numero_linea++;
                    }
                    $this->actualiza_estatus_ticket($sucursal);
                    
                $xml->endElement();
                /* Fin conceptos */
                
                /* Impuesto totales */
                $this->nodo_impuestos_totales($xml); 
                
            $xml->endElement();                    
            /* Fin documento */
        $xml->endElement();
        /* Fin documentos */
        file_put_contents($nombre_archivo, $xml->outputMemory(true));
    }

    /*  Funciones que cargan cada nodo y sus atributos */    
    function nodo_erp($xml){
        global $DOCUMENTO_ERP_ATTR;
        $DOCUMENTO_ERP_ATTR['id_trx_erp'] = $this->global;
                
        $xml->startElement(DOCUMENTO_ERP);
        foreach ($DOCUMENTO_ERP_ATTR as $nombre_attr => $valor_attr) {
            $xml->writeAttribute($nombre_attr,$valor_attr);
        }        
        $xml->endElement();
        /* Fin ERP */        
    }
    function nodo_emisor($xml,$sucursal){
        global $EMISOR_ATTR,$SUCURSAL_ATTR;
        
        $xml->startElement(EMISOR);
        foreach ($EMISOR_ATTR as $nombre_attr => $valor_attr) {
            $xml->writeAttribute($nombre_attr,$valor_attr);
        }   // Si no es organización coloca nodo de sucursal
            if(!$this->model_factura_global->validar_entidad($sucursal)){
                $xml->startElement(SUCURSAL);
                    foreach ($SUCURSAL_ATTR as $nombre_attr => $valor_attr) {
                        $xml->writeAttribute($nombre_attr,$valor_attr);
                    }            
                $xml->endElement();
            }
        $xml->endElement();
        /* Fin Emisor */
        
    }    
    function nodo_receptor($xml){
        global $RECEPTOR_ATTR;
        
        $xml->startElement(RECEPTOR);
        foreach ($RECEPTOR_ATTR as $nombre_attr => $valor_attr) {
            $xml->writeAttribute($nombre_attr,$valor_attr);
        }
        $xml->endElement();
        /* Fin Receptor */         
    }
    function nodo_concepto($xml,$ticket){
        global $CONCEPTO_ATTR;
        
        $xml->startElement(CONCEPTO);
        foreach ($CONCEPTO_ATTR as $nombre_attr => $valor_attr) {
            $xml->writeAttribute($nombre_attr,$valor_attr);
        }  
            /* Si tiene iva coloca nodo de impuesto o si es nulo (impuesto exento) */
            if ($ticket->iva > 0 || is_null($ticket->iva)) {
                /* Comienzan impuestos */      
                $this->nodo_impuestos_conceptos($xml,$ticket);
            } /* Sino no lleva nodo de impuestos */
            /* Comienzan Flex lines */
            $this->nodo_flex_lines($xml,$ticket);
        $xml->endElement();
        /* Guarda el id del ticket para actualizar su estatus al final */
        array_push($this->id_procesados, "$ticket->id");               
        /* Fin concepto */        
    }
    function nodo_impuestos_conceptos($xml,$ticket){
        global $IMPUESTO_ATTR,$IMPUESTO_ATTR_EXCENTO;
        
        $xml->startElement(IMPUESTOS);
            $xml->startElement(TRASLADADOS);
                $xml->startElement(IMPUESTO);
                /* Si no está exento de iva se cargan los valores del ticket */
                if(!is_null($ticket->iva)){
                    $this->cargar_attr_impuesto($ticket);
                    foreach ($IMPUESTO_ATTR as $nombre_attr => $valor_attr) {
                        $xml->writeAttribute($nombre_attr,$valor_attr);
                    }
                }else{
                    $this->cargar_attr_impuesto_exento($ticket);
                /* Si está exento llevará dos atributos constantes el nodo*/
                    foreach ($IMPUESTO_ATTR_EXCENTO as $nombre_attr => $valor_attr) {
                        $xml->writeAttribute($nombre_attr,$valor_attr);
                    }                    
                }
                $xml->endElement();
            $xml->endElement();
        $xml->endElement();
        /* Fin impuestos */        
    }
    function nodo_flex_lines($xml,$ticket){
        global $FLEX_LINE_ATTR;
        
        $xml->startElement(FLEX_LINES);
            $xml->startElement(FLEX_LINE);  
            $this->cargar_attr_flex($ticket);
            foreach ($FLEX_LINE_ATTR as $nombre_attr => $valor_attr) {
                $xml->writeAttribute($nombre_attr,$valor_attr);
            }
            $xml->endElement();
        $xml->endElement();
        /* Fin Flex lines */        
    }
    function nodo_impuestos_totales($xml){
        global $IMPUESTOS_TOTALES_ATTR,$IMPUESTO_TOTAL_ATTR,$IMPUESTO_TOTAL_EXENTO_ATTR;
        /* Total de impuestos es igual al importe total acumulado */
        $IMPUESTOS_TOTALES_ATTR['totalImpuestosTrasladados'] = $IMPUESTO_TOTAL_ATTR['importe'];
        
        $xml->startElement(IMPUESTOS);
        foreach ($IMPUESTOS_TOTALES_ATTR as $nombre_attr => $valor_attr) {
            $xml->writeAttribute($nombre_attr,$valor_attr);
        }
            $xml->startElement(TRASLADADOS);
                $xml->startElement(IMPUESTO);
                if($IMPUESTOS_TOTALES_ATTR['totalImpuestosTrasladados'] > 0){
                    foreach ($IMPUESTO_TOTAL_ATTR as $nombre_attr => $valor_attr) {
                        $xml->writeAttribute($nombre_attr,$valor_attr);
                    }
                }else{
                    /* Si no hay impuestos transladados se colocan los atributos constantes */
                    foreach ($IMPUESTO_TOTAL_EXENTO_ATTR as $nombre_attr => $valor_attr) {
                        $xml->writeAttribute($nombre_attr,$valor_attr);
                    }
                }
                $xml->endElement(); 
            $xml->endElement();
        $xml->endElement();
        /* Fin impuesto totales */        
    }
    function cargar_info_receptor($sucursal){
        global $RECEPTOR_ATTR;
        $receptor   = Model\C_entidades::find_by_id_entidad($sucursal,false);
        $config     = Model\Pss_config_portal::find(1);
        if (!$receptor){
            $mensaje = "Datos de sucursal($sucursal), no encontrados en c_entidades";
            $this->model_factura_global->registrar_bitacora(1,$mensaje);
            $this->cli->coloar_mensaje_error_cli($mensaje);
        }
        if (!$config->id_cliente_factura_global){
            $mensaje = "No se encuentra id_cliente_factura_global en pss_config_portal";
            $this->model_factura_global->registrar_bitacora(1,$mensaje);
            $this->cli->coloar_mensaje_error_cli($mensaje);
        }
        $RECEPTOR_ATTR['id_receptor_sto']   =  $config->id_cliente_factura_global;
        $RECEPTOR_ATTR['numero_exterior']   =  $receptor->numero_exterior;
        $RECEPTOR_ATTR['calle']             =  $receptor->calle;
        $RECEPTOR_ATTR['numero_interior']   =  $receptor->numero_interior;
        $RECEPTOR_ATTR['colonia']           =  $receptor->colonia;
        $RECEPTOR_ATTR['localidad']         =  $receptor->localidad;
        $RECEPTOR_ATTR['municipio']         =  $receptor->municipio;
        $RECEPTOR_ATTR['estado']            =  $receptor->estado;
        $RECEPTOR_ATTR['pais']              =  $receptor->pais;
        $RECEPTOR_ATTR['codigo_postal']     =  $receptor->codigo_postal;
        $RECEPTOR_ATTR['email']             =  $receptor->email;
               
    }    
    function cargar_info_emisor_sucursal($sucursal){
        global $EMISOR_ATTR,$SUCURSAL_ATTR;
        /* Si es una organización únicamente se obtienen datos del emisor */
        if($this->model_factura_global->validar_entidad($sucursal)){
            $emisor = Model\C_entidades::find($sucursal, false);
        }else{ 
        /* Si es sucursal se obtienen los datos del emisor (su padre) y se llenan
         * los datos del nodo sucursal */
            $sucursal_obj   = Model\C_entidades::find($sucursal, false);
            $emisor         = Model\C_entidades::find($sucursal_obj->id_entidad_padre, false);
            $SUCURSAL_ATTR['id_emisor_sto']     = $sucursal_obj->id_entidad;
            $SUCURSAL_ATTR['id_emisor_erp']     = $sucursal_obj->numero_entidad;
            $SUCURSAL_ATTR['id_emisor_padre']   = $sucursal_obj->id_entidad_padre;
        } // Se llenan los datos del emisor
        $EMISOR_ATTR = [
            'id_emisor_erp'     => $emisor->numero_entidad,
            'id_emisor_sto'     => $emisor->id_entidad,
            'nombre'            => $emisor->entidad, 
            'regimen_fiscal'    => $emisor->id_regimen,
            'rfc'               => $emisor->rfc 
            ];        
    }    
    function cargar_attr_concepto($ticket,$numero_linea){
        global $CONCEPTO_ATTR,$DOCUMENTO_ATTR;
        /* Cierra valor unitario a dos decimales sin redondear */
        $this->validar_existe_funcion('bcdiv');
        $this->valor_unitario = bcdiv($ticket->subtotal, '1', 2);
        $this->descuento      = bcdiv($ticket->descuento, '1', 2);
        /* Si el atributo existe en nodos_xml.php carga la info para colocarla */
        if(array_key_exists('num_identificacion', $CONCEPTO_ATTR)){
            $CONCEPTO_ATTR['num_identificacion']=  $ticket->numero_ticket;
        }
        $CONCEPTO_ATTR['numero_linea']          =  $numero_linea;
        $CONCEPTO_ATTR['valor_unitario']        =  $this->valor_unitario;
        $CONCEPTO_ATTR['importe']               =  $this->valor_unitario;
        /* Acumulado total */
        $DOCUMENTO_ATTR['subtotal']            += $this->valor_unitario;
        $DOCUMENTO_ATTR['descuento']           += $this->descuento;        
        /* Si hay descuento se toma hasta dos decimales sin redondear */
        if ($ticket->descuento > 0) {
            $CONCEPTO_ATTR['descuento'] = $this->descuento;
            return;
        }
        $CONCEPTO_ATTR['descuento'] = "0.00";
    }
    function cargar_attr_impuesto($ticket) {
        global $IMPUESTO_ATTR,$IMPUESTO_TOTAL_ATTR;

		$IMPUESTO_ATTR['base']       =  round($ticket->iva / $this->iva,6);
        $IMPUESTO_ATTR['importe']   = $ticket->iva;
        /* Se obtiene el importe acumulado */
        $IMPUESTO_TOTAL_ATTR['importe'] +=  $ticket->iva;
    }
    function cargar_attr_impuesto_exento($ticket) {
        global $IMPUESTO_ATTR_EXCENTO;
        $IMPUESTO_ATTR_EXCENTO['base']      = $ticket->subtotal - $ticket->descuento;
    }    
    function cargar_attr_flex($ticket) {
        global $FLEX_LINE_ATTR;

        $FLEX_LINE_ATTR['valor']      = $ticket->id_trx33;
    }
    
    /*  Obtiene una lista de tickets para generar la factura global
     * 
     * @param   identificador de la sucursal (necesaria para buscar en su tabla)
     * @param   fecha que indica el mes del cual se desea generar la factura global,
     *          con formato aaaamm (año a cuatro digitos y mes a dos digitos)
     * @param   número de tickets que se desea que entren en la factura global
     *          (no todos entran en la factura, debido a que posteriormente se 
     *          descartan los que tienen error de IVA)
     * 
     * @return  si se encontraron tickets se regresa un array, de lo contrario 
     *          termina la ejecurción del programa
     *      */
    function obtener_tickets($sucursal, $fecha, $cantidad_tickets) {
        /* Se obtienen los tickets dependiendo sucursal y fecha*/
        $tickets = $this->model_factura_global->obtener_tickets($sucursal, $fecha, $cantidad_tickets);
        if(!$tickets->result()){
            $mensaje = "Sin pendientes en sucursal: $sucursal y fecha: $fecha";
            $this->cli->coloar_mensaje_error_cli($mensaje,1);
        }
        return $tickets;
    }
    /*  Una vez que se construyó el xml se colocan los montos total que se acumularon
     *  de dicho ticket, (atributos del nodo DOCUMENTO)
     * 
     * @param   ruta del archivo que se modificará
     * @param   identificador de la sucursal para buscar la serie correspondiente
     * @param   forma de pago que llevará el documento XML.
     *      */
    function modificar_attr_documento($archivo,$sucursal,$forma_pago) {
        global $DOCUMENTO_ATTR, $IMPUESTOS_TOTALES_ATTR,$RECEPTOR_ATTR;
        /* Coloca atributos al final por los totales acumulados */
        $fecha = date("Y-m-d") . "T" . date("h:i:s");
        /* Abre el archivo recién creado, coloca los totales y lo guarda */
        $this->validar_existe_funcion('simplexml_load_file');
        $xml = simplexml_load_file($archivo);
        /* Si no se puede leer el archivo creado para modificarlo */
        if($xml === false){
            $mensaje = "No se puede leer el archivo recién creado: $archivo";
            $this->model_factura_global->registrar_bitacora(1,$mensaje);
            $this->cli->coloar_mensaje_error_cli($mensaje);
        }        

        $xml->DOCUMENTO['fecha'] = $fecha;
        $xml->DOCUMENTO['subtotal'] = $DOCUMENTO_ATTR['subtotal'];
        $xml->DOCUMENTO['descuento']= $DOCUMENTO_ATTR['descuento'];
        $xml->DOCUMENTO['forma_de_pago']= $forma_pago;
        $xml->DOCUMENTO['serie'] = $this->model_factura_global->obtener_serie($sucursal);

        $DOCUMENTO_ATTR['total'] = ($DOCUMENTO_ATTR['subtotal'] - $DOCUMENTO_ATTR['descuento']) + $IMPUESTOS_TOTALES_ATTR['totalImpuestosTrasladados'] - $IMPUESTOS_TOTALES_ATTR['totalImpuestosRetenidos'];

        $xml->DOCUMENTO['total'] = $DOCUMENTO_ATTR['total'];
        $xml->DOCUMENTO['lugar_expedicion'] = $RECEPTOR_ATTR['codigo_postal'];
        $xml->saveXML($archivo);
    }
    /*  Hace un registro en la tabla "Emi_trx33_global" para guardar los ids de las
     *  facturas globales que se han generado.
     * 
     * @param   ticket del cual se creará el registro (ticket que entra en global)
     * @param   sucursal para colocar el identificador del cliente
     * 
     *      */
    function crear_registro_global($ticket,$sucursal){
        $factura_global = new Model\Emi_trx33_global();
        $fecha = date("Y-m-d h:i:s");

        $factura_global->idtrx33                    = $ticket->id_trx33;
        $factura_global->id_global                  = $this->global;
        $factura_global->id_emisor                  = $sucursal;
//        $factura_global->fecha_emision              = $datos_global['fecha_emision'];
//        $factura_global->fecha_cfdi_global          = $datos_global['fecha_cfdi_global'];
//        $factura_global->fecha_ejecucion            = $datos_global['fecha_ejecucion'];
//        $factura_global->fecha_extraccion_inicial   = $datos_global['fecha_extraccion_inicial'];
        $factura_global->fecha_extraccion_final     = $fecha;
        $factura_global->frecuencia                 = FRECUENCIA;   
        
        $factura_global->save();
    }
    /*  Obtiene un id_global único para colocar a cada xml, simulando una secuencia

     * @return  id global generado
     *      */
    function obtener_maximo_id_global() {
            $seq_global = new Model\Seq_global();
            $seq_global->id_global = 0;
            $seq_global->save();
            
            $id = Model\Seq_global::last_created()->id_global;
        return $id;
    }

    /*  Guarda la relación del archivo xml que se generó para la factura global
     *  la forma de pago que le corresponde a cada XML.
     * 
     * @param   ruta del archivo donde se guardó el xml
     * @param   forma de pago que se guardará en la tabla archivos globales
     *      */
    function guardar_ruta_archivo($ruta_archivo,$forma_pago){
        $archivo_global = new Model\Archivos_globales();

        $archivo_global->id_global      = $this->global;
        $archivo_global->ruta_archivo   = $ruta_archivo;
        $archivo_global->id_forma_pago  = $forma_pago;
        
        $archivo_global->save();        
    }
    /*  Actualiza los estatus de todos los tickets que ingresaron en factura global 
     *  (estatus correcto) y coloca su id_global
     * 
     *  @param  número de la sucursal para identificar en qué tabla hará la actualización
     *      */
    function actualiza_estatus_ticket($sucursal){
        /* Se actualiza el estatus nodo correcto y su id_global */
        $arr = implode(",", $this->id_procesados);
     
        $update = "update control_global_$sucursal set estatus=1,id_global=$this->global where id in($arr)";
        $this->db->query($update);        
        
    }
    /*  Valida si existe la carpeta donde se creará el archivo y si tiene permisos
     *  de escritura, si algo de esto no es posible termina la ejecución código 
     *  2 y escribe en pss_bitácora.
     * 
     *  @param string $ruta_archivo Filepath del nuevo archivo
     *      */
    function validar_escritura_archivo($ruta_archivo) {
        if(!is_dir(dirname($ruta_archivo))){
            $mensaje =  "Imposible acceder al directorio: ".dirname($ruta_archivo);
            $this->model_factura_global->registrar_bitacora(1,$mensaje);
            $this->cli->coloar_mensaje_error_cli($mensaje);
        }
        if (!is_writable(dirname($ruta_archivo))) {
            $mensaje = "Imposible escribir archivo $ruta_archivo";
            $this->model_factura_global->registrar_bitacora(1,$mensaje);
            $this->cli->coloar_mensaje_error_cli($mensaje);
        }
    }
    /*  Valida si existe alguna función que se requiera, esto por si no se carga
     *  alguna librería previa, si algo de esto no es posible termina la 
     *  ejecución código 2 y escribe en pss_bitácora.
     * 
     *  @param string $nombre_funcion Nombre de la función a validar
     *      */
    function validar_existe_funcion($nombre_funcion) {
        if (!function_exists($nombre_funcion)) {
            $mensaje = "Verificar librería que carga la función: $nombre_funcion";
            $this->model_factura_global->registrar_bitacora(1,$mensaje);
            $this->cli->coloar_mensaje_error_cli($mensaje);            
        }
    }
    /*  Valida si existe alguna clase que se requiera, esto por si no se carga
     *  alguna librería previa, si algo de esto no es posible termina la 
     *  ejecución código 2 y escribe en pss_bitácora.
     * 
     *  @param string $nombre_clase Nombre de la clase a validar si existe
     *      */
    function validar_existe_clase($nombre_clase) {
        if (!class_exists($nombre_clase)) {
            $mensaje = "Verificar librería que carga la función: $nombre_clase";
            $this->model_factura_global->registrar_bitacora(1,$mensaje);
            $this->cli->coloar_mensaje_error_cli($mensaje);            
        }
    }

}

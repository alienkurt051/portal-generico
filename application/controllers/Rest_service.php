<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/* Valores con los que se identificara para poder acceder al WS */
define('USUARIO', 'quickParkAICM');
define('CONTRASENIA', 'qu1ckp4rK');

/**
 * Interfaz web service expuesto en modo REST para realizar la busqueda y
 * facturación de una transacción del servicio de autofacturación del AICM

 * Endpoint de la peticion: index.php
 * Resource: /rest_service/facturar
 *
 * La peticion se hacer con el metodo POST y dentro de la peticion se deben
 * incluir los datos de identificacion, datos fiscales del receptor y los
 * complementos esperados
 *
 * @author     Ivan Mondragon
 *  */

class Rest_service extends CI_Controller {
    /* Nombres de los campos con los que se identificara para poder acceder al WS */

    private $datos_identificacion = array('usuario', 'contrasenia');
    /* Datos fiscales de un receptor de validacion que espera el WS */
    private $datos_esperados = array('id_sistema_movil',
        'rfc',
        'nombre_usuario',
        'nombre_receptor',
        'domicilio',
        'uso_cfdi',
        'email',
        'sucursal',
        'campos_adicionales'
    );

    function __construct() {
        parent::__construct();
    }

    /**
     * Metodo que se ejecuta cuando se manda llamar el WS
     *
     * @return regresa un mensaje con error u ok en tipo json
     *      */

    public function facturar() {
        /* Si la peticion no es post */
        if ($this->input->method() != "post") {
            $this->responder("Error", "Método no admitido.", 405);
            return;
        }
        $datos_recibidos = $this->input->post();
        /* Si no se autentica correctamente */
        if (!$this->autenticar($datos_recibidos)) {
            $this->responder("Not Authorized", "Falló la autenticación", 401);
            return;
        }
        /* Si los datos recibidos del usuarios son correctos */
        if ($this->validar_datos($this->datos_esperados, $datos_recibidos)) {
            /* Datos que vienen como query string */
            $complementos_qs = $datos_recibidos['campos_adicionales'];
            $domicilio_qs = $datos_recibidos['domicilio'];
            /* Se extraen de los datos enviados, los campos de la transaccion y el domicilio */
            $arr_campos_transaccion = $this->extraer_query_string($complementos_qs);
            $arr_campos_domicilio = $this->extraer_query_string($domicilio_qs);
            /* Complementos necesarios dependiendo de la configuracion */
            $complementos_esperados = $this->recuperar_flex();
            /* Si los complementos son correctos, ya se cuentran en la variable $_POST */
            if ($this->validar_datos($complementos_esperados, $arr_campos_transaccion)) {
//                    $this->responder("Ok", "Datos y Complementos correctos",200);
                /* Se verifica si el cliente esta registrado en el portal */
                $id_sistema_movil = $this->input->post('id_sistema_movil');
                /* Se verifica si el cliente esta registrado en el portal */
                $cliente = Model\Pss_usuario::find_by_id_sistema_movil($id_sistema_movil, false);                
                if (is_null($cliente)) {
                    /* Si no existe se crea y se regresa su id */
                    $id_usuario = $this->f_agregar_usuario();
                } else {
                    /* Si ya esta registrado se toma su id */
                    $id_usuario = $cliente->id_usuario_pss;
                }
                /* Se obtiene el id del cliente al que se va a facturar */
                $id_cliente = $this->verificar_rfc_registrado($id_usuario);
//                    $this->responder("Ok", "Usuario con id= $id_usuario, Cliente con id=$id_cliente", 200);
                /* Ya vaidados los datos se busca la transaccion */
                $this->busca_transaccion($id_cliente, $id_usuario);
                return;
            }
            $this->responder("Bad Request", "Complementos incorrectos", 400);
            return;
        }
        $this->responder("Bad Request", "Datos de usuario incompletos", 400);
    }

    /**
     * Respuesta del ws de un estatus y un mesanje en formato json

     * @param estatus de la respuesta
     * @param mensaje dependiendo del estatus de la respuesta
     * @param codigo del estatus
     * @param archivo pdf en base64
     * @param archivo xml en base64
     * @return devuelve la respuesta en formato json
     *      */

    private function responder($estatus, $mensaje, $codigo, $pdf = null, $xml = null) {
        if ($pdf == null || $xml == null) {
            $response = array('status' => $estatus,
                'code' => $codigo,
                'message' => $mensaje
            );
        } else {
            $response = array('status' => $estatus,
                'code' => $codigo,
                'message' => $mensaje,
                "pdfBase64" => $pdf,
                'xmlBase64' => $xml
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response,JSON_UNESCAPED_UNICODE);
    }

    /**
     * Valida si los datos esperados son los mismos que se reciben y si no
     * estan vacios
     *
     * @param arreglo de datos esperados
     * @param arreglo de datos recibidos
     * @return regresa true si los datos son validos, false si no lo son
     *      */

    private function validar_datos($datos_esperados, $array_recibidos) {

        foreach ($datos_esperados as $dato) {
            /* Si existe cada campo y no esta vacio continua */
            if (array_key_exists($dato, $array_recibidos) && !empty($array_recibidos[$dato])) {
                continue;
            }
            return false;
        }
        return true;
    }

    /**
     * Extrae los datos del campo campos_adicionales y el domicilio, los cuales
     * vienen como query string y los coloca en un array y en la variable post
     *
     * @param arreglo de datos recibidos via post
     * @return arreglo de campos adicionales como clave => valor
     *      */

    public function extraer_query_string($query_string) {
        $array_flex = explode('&', $query_string);

        foreach ($array_flex as $value) {
            list($key, $val) = explode('=', $value);
            $result[$key] = $val;
            $_POST[$key] = $val;
        }
        return $result;
    }

    /**
     * Valida los datos de autenticacion que vienen dentro del cuerpo del request

     * @param arreglo de datos recibidos
     * @return regresa true si los datos son correctos o false de lo contrario
     *      */

    public function autenticar($array_recibidos) {
        /* Si los datos tienen la estructura que se espera */
        if ($this->validar_datos($this->datos_identificacion, $array_recibidos)) {
            /* Si alguno de los valores no conciden */
            if ($array_recibidos['usuario'] != USUARIO || $array_recibidos['contrasenia'] != CONTRASENIA) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Recupera los campos flex que se solicitan dependiendo de la configuracion
     * del portal.

     * @return arreglo de campos flex
     *      */

    public function recuperar_flex() {
        $arr_campos_transaccion = Model\V_pss_campos_transaccion::all();
        $arr_campos = array();
        foreach ($arr_campos_transaccion as $campo) {
            array_push($arr_campos, $campo->campo_adicional);
        }
        return $arr_campos;
    }

    // funcion que verifica si la transaccion existe
    public function busca_transaccion($id_cliente_para_facturar, $id_usuario) {

        $id_entidad = $this->input->post("sucursal");
        $uso_cfdi = $this->input->post("uso_cfdi");
        $arr_campos = array();

        // se registra evento en la bitacora
        $observaciones = "Id cliente para facturar: " . $id_cliente_para_facturar;
        registrar_evento_bitacora($this, $id_usuario, BUSCAR_TRANSACCION_FACTURAR, $observaciones);
        // se obtiene la configuracion del portal
        $config_portal = Model\Pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;
        // se obtiene la lista de campos flex
        $arr_campos_transaccion = Model\V_pss_campos_transaccion::all();

        $i = 1;
        foreach ($arr_campos_transaccion as $campo) {
            $arr_campos[$i]["id_flex_header"] = $campo->id_flex_header;
            $arr_campos[$i]["campo_adicional"] = $campo->campo_adicional;
            $arr_campos[$i]["valor"] = $this->input->post($campo->campo_adicional);
            $i++;
        }

        $this->load->model("model_buscar_transaccion");
        $arr_transacciones = $this->model_buscar_transaccion->obtener_idtrx33_transaccion($arr_campos, $id_entidad);

        /* Si no se encontraron transacciones */

        if (count($arr_transacciones) < 1) {

            $this->responder("Conflict", "Transacción no encontrada. No se encontró una transacción con los datos proporcionados. Intente nuevamente", 409);

            $observaciones = "Id cliente para facturar: " . $id_cliente_para_facturar . " - No se encontró una transacción con los datos proporcionados";
            registrar_evento_bitacora($this, $id_usuario, ERROR_TRANSACCION_FACTURAR, $observaciones);

            return;
        } else {
            // si existe mas de una coincidencia
            if (count($arr_transacciones) > 1) {
                $this->responder("Conflict", "Error en búsqueda de transacción. Ocurrió un error al buscar su transacción. Los datos proporcionados generaron más de un caso correcto.", 409);

                $observaciones = "Id cliente para facturar: " . $id_cliente_para_facturar . " - Los datos proporcionados generaron más de un caso correcto. El administrador ha sido notificado y se le notificará cuando el ticket pueda facturarse";
                registrar_evento_bitacora($this, $id_usuario, ERROR_TRANSACCION_FACTURAR, $observaciones);
                return;
            } else {
                // transaccion encontrada. Se obtienen los datos de la transaccion
                $transaccion_buscada = $arr_transacciones[1];
                $transaccion = Model\Emi_trx33_r::find($transaccion_buscada["id_trx33"]);
//                die($transaccion->id_receptor);
                // si ya esta facturada (el id cliente ha sido asignado al receptor correcto)
                if ($transaccion->id_receptor != $config_portal->id_cliente_autofactura) {
                    $this->responder("Conflict", "Transacción no disponible. La transacción ha sido facturada con anterioridad", 409);

                    $observaciones = "Id cliente para facturar: " . $id_cliente_para_facturar . " - idtrx33obtenida: " . $transaccion->id_trx33_r . " - Transaccion facturada con anterioridad";
                    registrar_evento_bitacora($this, $id_usuario, ERROR_TRANSACCION_FACTURAR, $observaciones);
                    return;
                } else {
                    // =========================================== VERIFICACION DE TICKET EN FACTURA GLOBAL ==========================================
                    // se verifica si la transaccion esta en una factura global
                    $global_id = Model\Emi_trx33_global::find_by_idtrx33($transaccion->id_trx33_r, false);

                    // si la transaccion esta en la global
                    if ($global_id != null) {
                        // si el portal permite facturar ticket en global
                        if ($config_portal->facturar_ticket_en_global != 1) {
                            // no se permite facturar ticket en global
                            $observaciones = "idtrx33obtenida: " . $transaccion->id_trx33_r . " - El ticket esta en factura global y el portal está configurado a no permitir facturar ticket en global";
                            registrar_evento_bitacora($this, $id_usuario, ERROR_TRANSACCION_FACTURAR, $observaciones);
                            $this->responder("Conflict", "Error al procesar transacción. La transacción buscada ha sido considerada para una factura de venta de mostrador. Por favor comuníquese con su proveedor para realizar la aclaración.", 409);
                            return;
                        } else {
                            $observaciones = "idtrx33obtenida: " . $transaccion->id_trx33_r . " - El ticket esta en factura global. Se permite facturacion";
                            // transaccion en global pero permite continuar
                            registrar_evento_bitacora($this, $id_usuario, BUSCAR_TRANSACCION_FACTURAR, $observaciones);
                        }
                        // ===================================== TERMINA VERIFICACION DE TICKET EN FACTURA GLOBAL ======================================
                    }
                    /* Se procede a facturar */
                    $id_trx33_r = $transaccion->id_trx33_r;
                    $this->facturar_transaccion_en_linea($id_cliente_para_facturar, $id_trx33_r, $id_usuario);
                }
            }
        }
    }

    /**
     * Crea el usuario con los datos recibidos por post y envia un correo de su
     * verificacion con su usuario y contrasena para loggearse, en caso de que
     * no se pueda crear finaliza la ejecucion del programa y regresa el error.

     * @return id_usuario_pss del usuario creado
     *      */

    public function f_agregar_usuario() {
        // datos del usuario
        $id_sistema_movil = $this->input->post('id_sistema_movil');
        $login = $this->input->post("email");
        $email = $this->input->post("email");
        $contrasena = $this->input->post("rfc");
        $id_pregunta_recuperacion = 1;
        $nombre = $this->input->post("nombre_usuario");
        $id_tipo_usuario = 4; /* Tipo de usuario cliente */

        $data = array();

        // ---------------------- validacion de los datos --------------------------
        // se verifican los datos recibidos, validando unicamente el email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->responder("Bad Request", "Datos incorrectos. Email no válido", 400);
            die();
        } else {

            // datos correctos, se procede a generar el registro nuevo
            // se carga el modelo de institucion para trabajar con la nueva asignatura
            $nuevo_usuario = new Model\Pss_usuario();
            $insercion_correcta = true;

            // se asignan los datos

            $nuevo_usuario->id_usuario_pss              = 0;
            $nuevo_usuario->login                       = $login;
            $nuevo_usuario->contrasena                  = MD5($contrasena);
            $nuevo_usuario->id_pregunta_recuperacion    = $id_pregunta_recuperacion;
            $nuevo_usuario->nombre                      = $nombre;
            $nuevo_usuario->email                       = $email;
            $nuevo_usuario->fecha_alta                  = date("Y-m-d H:i:s");
            $nuevo_usuario->tipo_usuario                = $id_tipo_usuario;
            $nuevo_usuario->id_estatus                  = 1; // activo
            $nuevo_usuario->dir_ip                      = $this->input->ip_address();
            $nuevo_usuario->id_sistema_movil            = $id_sistema_movil; // activo

            /* Se guarda el registro y si hay error al crear el usuario */
            if(!$nuevo_usuario->save()){
                $insercion_correcta = false;
            } 
            $id_usuario = Model\Pss_usuario::last_created()->id_usuario_pss;
//            $insercion_correcta = true;

            // si fue exitosa, se invoca el listado de asignaturas
            if ($insercion_correcta) {

                // 20180313 se agrega envio de correo al crear cuenta
                $remitente = Model\Envio_correo_remitente::find_by_es_default(1, FALSE);
                $destinatario = new Model\Envio_correo_destinatario();
                $envio = new Model\Envio_correo();
                $adjunto = new Model\Envio_correo_adjuntos();

                // se obtiene la configuracion del portal
                $config_portal = Model\pss_config_portal::find(1);

                // se llenan los datos para el envio de correo
                $envio->id_envio_correo = 0;
                $envio->id_transaccion = null;
                $envio->id_proceso = null;
                $envio->id_remitente = $remitente->id_remitente;
                $envio->procesado = -1; // pendiente
                $envio->fecha_registro = date("Y-m-d");
                $envio->fecha_proceso = null;
                $envio->enviar_adjuntos = 0;
                $envio->asunto = "Acceso para " . $config_portal->titulo_pantalla_principal;

                $cuerpo_correo = "ESTIMADO(A) " . $nombre . ":<br><br>Se ha creado correctamente su cuenta de usuario, para acceder al portal de facturación electrónica.<br><br>Sus datos de acceso son:<br>Usuario: " . $login . "<br>Contraseña: " . $contrasena . "<br><br><br>Sitio WEB: " . base_url() . "<br><br><br><br>El usuario se responsabilizará del uso correcto de las claves de acceso concedidas, las cuales son intransferibles y pueden ser revocadas o canceladas ante un uso inadecuado.<br><br><br>Nota: No es necesario responder a esta cuenta de correo.";


                $envio->cuerpo = $cuerpo_correo;
                $envio->save();

                // se obtiene el id de envio
                $id_envio = Model\Envio_correo::last_created()->id_envio_correo;
                // se asigna al registro
                $envio = Model\Envio_correo::find($id_envio);

                // se genera el registro del destinatario
                $destinatario->id_correo_destinatario = 0;
                $destinatario->id_envio_correo = $id_envio;
                $destinatario->destinatario = $email;
                $destinatario->fecha_proceso = null;
                $destinatario->estatus_envio = 1;
                $destinatario->cod_error = null;
                $destinatario->d_error = null;
                $destinatario->num_intentos = 0;
                $destinatario->save();

                // se actualiza el registro de envio para que el ejecutor de envio lo considere
                $envio->procesado = 1; // listo para enviar
                $envio->save();

                // se cambia el estatus del correo para que se pueda enviar
                $envio->procesado = 0; // pendiente
                $envio->save();

                // Regresa el id del usuario que se creo
//                $this->responder("Ok", "Nuevo usuario creado. La cuenta de usuario [  $login  ] ha sido creada correctamente y una notificación por correo electrónico será enviada al usuario en breve.", 201);
                return $id_usuario;
            } else {
                /* Error al tratar de crear el usuario */
                $this->responder("Internal Server Error", "Error al crear la cuenta de usuario. Ocurrió un error al tratar de generar la cuenta de usuario [' . $login . ']. Por favor intente más tarde.", 500);
                die();
            }
        }
    }

    /**
     * Checa si el rfc enviado se encuentra registrado y asociado a un usuario
     * sino se  encuentra registra al cliente y hace la asociacion con el usuario

     * @param id del usuario al cual se hara la asociacion con el cliente
     * @return regresa el id del cliente con el que se encuentra registrado el rfc
     *      */

    function verificar_rfc_registrado($id_usuario) {
        $rfc = $this->input->post('rfc');
        $email = $this->input->post('email');
        $nombre_receptor = $this->input->post('nombre_receptor');
        /* Si rfc no esta registrado */
        $cliente = Model\C_clientes::find_by_rfc($rfc, false);
        $id_cliente = null;

        /* Se verifica la relacion con cliente */
        $this->db->WHERE("id_usuario = $id_usuario "
                . "and id_cliente IN "
                . "(SELECT id_cliente  FROM c_clientes where rfc='$rfc')");
        /* Aunque el resultado puede traer muchos datos se toma el primero */
        $this->db->LIMIT(1);
        $r_usuario_cliente = Model\Pss_r_usuario_cliente::all();

        /* Si no hay relacion de usuario y cliente */
        if (empty($r_usuario_cliente)) {
            /* Registra cliente en c_clientes */
//            echo "No hay relación. REGISTRANDO cliente...";
            $cliente = new Model\C_clientes();

            // se asignan los datos

            $cliente->id_cliente                  = 0;
            $cliente->cliente                     = $nombre_receptor;
            $cliente->rfc                         = $rfc;
            $cliente->email                       = $email;
            $cliente->calle                       = $this->input->post('calle');
            $cliente->numero_exterior             = $this->input->post('numeroExterior');
            $cliente->numero_interior             = $this->input->post('numeroInterior');
            $cliente->codigo_postal               = $this->input->post('codigoPostal');
            $cliente->colonia                     = $this->input->post('colonia');
            $cliente->municipio                   = $this->input->post('municipio');
            $cliente->localidad                   = $this->input->post('localidad');
            $cliente->estado                      = $this->input->post('estado');
            $cliente->pais                        = $this->input->post('pais');

            // se guarda el registro
            $cliente->save();

            $id_cliente = Model\C_clientes::last_created()->id_cliente;
//            echo "Cliente nuevo registrado con id = $id_cliente";


            /* Registrar relacion de cliente con usuario */
//            echo "Relacionando..";
            $r_cliente_usuario = new Model\Pss_r_usuario_cliente();

            // se asignan los datos
            $r_cliente_usuario->id_r_usuario_cliente = 0;
            $r_cliente_usuario->id_usuario = $id_usuario;
            $r_cliente_usuario->id_cliente = $id_cliente;
            $r_cliente_usuario->fecha_alta = date("Y-m-d H:i:s");

            // se guarda el registro
            $r_cliente_usuario->save();
//            echo "Relación creada ";
        } else {
            /* Si hay relacion por lo tanto el cliente esta registrado en
             * c_clientes y se obtiene el primer id del cliente RELACIONADO */
            $id_cliente = $r_usuario_cliente[0]->id_cliente;
//            echo "La relacion del cliente con el usuario y el rfc esta registrado con el id_cliente = $id_cliente";
        }
        return $id_cliente;
    }


    public function facturar_transaccion_en_linea($id_cliente_para_facturar, $id_trx33_r, $id_usuario) {
        date_default_timezone_set('America/Mexico_City');

        // se registra evento en la bitacora
        $observaciones = "idtrx33obtenida: " . $id_trx33_r . " - Enviada a facturar por web service";
        registrar_evento_bitacora($this, $id_usuario, ENVIAR_TRANSACCION_FACTURAR, $observaciones);


        // si no llegan los parametros se regresa a la pantalla principal
        if ($id_cliente_para_facturar == null || $id_trx33_r == null) {

            $observaciones = "idtrx33obtenida: " . $id_trx33_r . " - No fue posible obtener la información de la transacción para confirmar la operación. Intente de nuevo por favor";
            registrar_evento_bitacora($this, $id_usuario, ERROR_TRANSACCION_FACTURAR, $observaciones);

            $this->responder("Internal Server Error", "Error al procesar transacción. No fue posible obtener la información de la transacción para confirmar la operación. Intente de nuevo por favor.", 500);
            die();
        }


        // datos fiscales del cliente
        $cliente = Model\C_clientes::find($id_cliente_para_facturar);

        // se obtienen los datos de la transaccion
        $transaccion_r = Model\Emi_trx33_r::find($id_trx33_r);
        $transaccion = Model\Emi_trx33::find($id_trx33_r);

        // =========================================== VERIFICACION DE TICKET EN FACTURA GLOBAL ==========================================
        // se invoca la funcion para generar nota de credito automatica, en caso de tener un ticket en global con generacion de nota de credito automatica
        $this->valida_nota_credito_global($id_trx33_r, $id_usuario);

        // ===================================== TERMINA VERIFICACION DE TICKET EN FACTURA GLOBAL ======================================

        $id_ejecucion = $transaccion_r->id_ejecucion;
        $id_sucursal = $transaccion_r->id_sucursal;
        /* Se asignara el metodo y forma de pago de la transaccion origen
            porque por ws no se puede elegir         */
        $metodo_pago = $transaccion_r->id_metodo_pago;
        $forma_pago = $transaccion_r->id_forma_pago;

        // CONTINUACION DEL PROCESO DE FACTURACION
        // se asignan los datos
        // se obtiene la serie que corresponde a la sucursal emisora
        $id_entidad = $transaccion_r->id_emisor;
        $aux_id_entidad = $id_entidad;
        if ($id_sucursal != '' || $id_sucursal != null) {
            $aux_id_entidad = $id_sucursal;
        }
        $this->db->where("tipo = 1");
        $sql_serie_entidad = Model\Pss_series_entidades::find_by_id_entidad($aux_id_entidad);

        foreach ($sql_serie_entidad as $serie_entidad) {
            // se obtiene el folio de la serie
            $this->load->model("model_pss_series_entidades");
            $folio = $this->model_pss_series_entidades->obtener_folio($aux_id_entidad, $serie_entidad->serie);
        }
        //die('serie: '.$serie_entidad->serie.'<br>folio: '.$folio);
        //$cliente = Model\C_clientes::find($id_cliente);

        $transaccion_r->serie = $serie_entidad->serie;
        $transaccion_r->folio = $folio;

        $transaccion_r->id_receptor = $cliente->id_cliente;
        $transaccion_r->envia_xml = 1;
        $transaccion_r->envia_pdf = 1;
        $transaccion_r->email_envio = $cliente->email;
        $transaccion_r->save();

        $transaccion->rfc_receptor = $cliente->rfc;
        $transaccion->nombre_receptor = $cliente->cliente;
        $transaccion->serie = $serie_entidad->serie;
        $transaccion->folio = $folio;
        $transaccion->fecha = substr(date('c'), 0, 19);
        //Cargar cambios
        $transaccion->metodo_pago = $metodo_pago;
        $transaccion->forma_pago = $forma_pago;
        $transaccion->uso_cfdi = $this->input->post("uso_cfdi");

        $transaccion->save();

        // se invoca el ws de facturacion
        $this->facturar_por_ws($id_trx33_r, $id_usuario);

        $observaciones = "idtrx33obtenida: " . $id_trx33_r . " - Transaccion facturada correctamente";
        registrar_evento_bitacora($this, $id_usuario, CORRECTO_TRANSACCION_FACTURAR, $observaciones);
        /* Termina facturacion, se envia la respuesta junto con el pdf y el xml */
        $this->enviar_xml_pdf($id_trx33_r);
    }

    public function valida_nota_credito_global($id_trx33_r, $id_usuario) {
        //$this->output->enable_profiler(TRUE);
        // se verifica si la transaccion esta en una factura global
        $global_id = Model\Emi_trx33_global::find_by_idtrx33($id_trx33_r, false);

        // se obtiene la configuracion del portal
        $config_portal = Model\Pss_config_portal::find(1);

        // si la transaccion esta en la global
        if ($global_id != null) {
            // si el portal permite facturar ticket en global
            if ($config_portal->facturar_ticket_en_global == 1) {
                // si el portal esta configurado a que genere nota de credito a la global
                if ($config_portal->generar_nota_credito_global == 1) {

                    $observaciones = "idtrx33obtenida: " . $id_trx33_r . " - Se procede a generar nota de credito automatica";
                    // transaccion en global pero permite continuar
                    registrar_evento_bitacora($this, $id_usuario, BUSCAR_TRANSACCION_FACTURAR, $observaciones);

                    // se obtiene la transaccion original
                    $transaccion_r = Model\Emi_trx33_r::find($id_trx33_r);

                    $id_entidad = $transaccion_r->id_emisor;
                    $this->db->where("tipo = 2");

                    $series_entidades = Model\Pss_series_entidades::find_by_id_entidad($id_entidad);

                    foreach ($series_entidades as $serie_entidad) {
                        $this->load->model("model_pss_series_entidades");
                        $folio_nota_c = $this->model_pss_series_entidades->obtener_folio($id_entidad, $serie_entidad->serie);
                    }

                    // se obtiene el id de la factura global
                    $trx33_global = Model\Emi_trx33_r::find_by_id_trx_erp($global_id->id_global, false);
                    //echo "<br>IDTRX33 DE LA GLOBAL: ".$trx33_global->id_trx33_r;

                    $this->generar_nota_credito_pss($id_trx33_r, $trx33_global->id_trx33_r, $serie_entidad->serie, $folio_nota_c, $id_usuario);
                }
            }
        }
    }

    public function generar_nota_credito_pss($id_trx33, $id_trx33_global, $serie_nota_credito, $folio_nota_credito, $id_usuario) {

        // se obtienen los datos de la transaccion original
        $trx33 = Model\Emi_trx33::find_by_id_trx33($id_trx33);
        $trx33_r = Model\Emi_trx33_r::find_by_id_trx33_r($id_trx33);

        // se obtiene el uuid de la factura global
        $xml_global = Model\Emi_trx33_xml::find($id_trx33_global);

        // se obtiene la ruta de importacion del proceso de importacion xml
        $this->db->where("id_proceso = 8");
        $this->db->where("secuencia = 1");
        $etapas_procesos = Model\C_etapas_procesos::all();

        foreach ($etapas_procesos as $etapa) {
            $directorio_carga_tickets = $etapa->ruta_entrada;
            break;
        }

        //EMISOR
        foreach ($trx33 as $trx33) {
            $emisor_rfc = $trx33->rfc_emisor;
            $emisor_nombre = $trx33->nombre_emisor;
            $emisor_regimen_fiscal = $trx33->regimen_fiscal;
            $total_impuestos_retenidos = $trx33->total_impuestos_retenidos;
            $total_impuestos_trasladados = $trx33->total_impuestos_trasladados;
        }

        //DOCUMENTO
        foreach ($trx33_r as $trx33_r) {
            //$fecha                  = $trx33_r->fecha;
            $fecha = date('Y-m-d H:i:s');
            $forma_de_pago = $trx33_r->id_forma_pago;
            $condiciones_de_pago = $trx33_r->condiciones_de_pago;
            $tipo_cambio = $trx33_r->tipo_cambio;
            $moneda = $trx33_r->id_moneda;
            $metodo_pago = 'PUE'; //$trx33_r->id_metodo_pago;
            $lugar_expedicion = $trx33_r->id_lugar_expedicion;
            $tipo_comprobante = "E"; // es nota de credito
            $subtotal = $trx33_r->subtotal;
            $descuento = $trx33_r->descuento;
            if ($descuento == '' || $descuento == null) {
                $descuento = 0;
            }
            $enviar_xml = $trx33_r->envia_xml;
            $enviar_pdf = $trx33_r->envia_pdf;
            $enviar_zip = $trx33_r->envia_zip;
            $email_envio = $trx33_r->email_envio;
            $total = $trx33_r->total;
            $confirmacion = $trx33_r->confirmacion;
            $tipo_documento = $trx33_r->id_tipo_documento;
            $id_trx_erp = $trx33_r->id_trx_erp;
            $totalImpuestosRetenidos = $trx33_r->totalImpuestosRetenidos;
            $totalImpuestosTrasladados = $trx33_r->totalImpuestosTrasladados;
            $serie_original = $trx33_r->serie;
            $folio_original = $trx33_r->folio;
            $emisor_id_emisor_sto = $trx33_r->id_emisor;
            $serie = null;
            $folio = null;
            $id_sucursal = $trx33_r->id_sucursal;
        }

        // si se tiene sucursal
        if ($trx33_r->id_sucursal != null) {
            $entidades = $this->db->query("SELECT * FROM c_entidades WHERE id_entidad = " . $id_sucursal . ";");
        } else {
            // se hizo emision desde la matriz
            $entidades = $this->db->query("SELECT * FROM c_entidades WHERE id_entidad = " . $emisor_id_emisor_sto . ";");
        }

        foreach ($entidades->result() as $entidad) {
            $rfc_sucursal = $entidad->rfc;
            $entidad_sucursal = $entidad->entidad;
            $id_regimen_sucursal = $entidad->id_regimen;
            $numero_interior_sucursal = $entidad->numero_interior;
            $numero_exterior_sucursal = $entidad->numero_exterior;
            $calle_sucursal = $entidad->calle;
            $colonia_sucrusal = $entidad->colonia;
            $localidad_sucursal = $entidad->localidad;
            $referencia_sucursal = $entidad->referencia;
            $municipio_sucursal = $entidad->municipio;
            $estado_sucursal = $entidad->estado;
            $pais_sucursal = $entidad->pais;
            $codigo_postal_sucursal = $entidad->codigo_postal;
            $email_sucursal = $entidad->email;
            $id_tipo_emisor_sucursal = $entidad->id_tipo_entidad;
            $id_tipo_entidad_padre_sucursal = $entidad->id_entidad_padre;
            $estatus_sucursal = $entidad->estatus;
        }

        //Recuperar id_cliente de la tabla de confoguracion del portal, para agregar datos del receptor.
        $pss_config = Model\Pss_config_portal::find(1, false);
        $id_cliente_receptor = $pss_config->id_cliente_autofactura;

        //RECEPTOR
        $receptor = Model\C_clientes::find_by_id_cliente($id_cliente_receptor);
        foreach ($receptor as $receptorD) {
            $rfc_receptor = $receptorD->rfc;
            $estatus = $receptorD->estatus;
        }

        //FLEX_HEADERS
        $flex_headers = "";
        $flex_header = Model\Emi_trx33_inf_adic::find_by_id_trx33($id_trx33);

        foreach ($flex_header as $flex_headerV) {
            $flex_headers .= <<<XML
			<FLEX_HEADER clave="$flex_headerV->id_flex_header" nombre="$flex_headerV->id_flex_header" valor="$flex_headerV->valor"/>
XML;
        }


        //CONCEPTOS
        $c = 1;
        $conceptos = "";
        $concepto_r = Model\Emi_trx33_concepto_r::find_by_id_trx33_r($id_trx33);
        foreach ($concepto_r as $concepto) {
            $conceptos_impuestos = false;

            // ----------------------------------------- REVISAR ESTE COMENTARIO COMPLEMENTARIO CON DEFINICION PARA LA GENEREACION DE NOTA DE CREDITO AUTOMATICA -----------
            // todos los conceptos llevan clave de producto y servicio y de unidad de medida fijos a los requeridos para nota de credito

            $conceptos .= <<<XML
		<CONCEPTO numero_linea="$c" clave_prod_serv="84111506" cantidad="$concepto->cantidad" clave_unidad="ACT" unidad="ACTIVIDAD" num_identificacion="$concepto->numero_identificacion" descripcion=" $concepto->descripcion" valor_unitario="$concepto->valor_unitario" importe="$concepto->importe" descuento="$concepto->descuento" >
XML;
            $concepto_impuesto_r = Model\Emi_trx33_con_impuestos_r::find_by_id_trx33_concepto_r($concepto->id_trx33_concepto_r);

            foreach ($concepto_impuesto_r as $concepto_i) {
                if ($conceptos_impuestos == false) {
                    $conceptos .= "\n" . <<<XML
				<IMPUESTOS>
XML;
                }
                //trasladado
                if ($concepto_i->tipo_impuesto == 1) {
                    $conceptos_impuestos = true;
                    $conceptos .= "\n" . <<<XML
					<TRASLADADOS>
						<IMPUESTO base="$concepto_i->base" impuesto="$concepto_i->impuesto" tipo_factor="$concepto_i->tipo_factor" tasa_o_cuota="$concepto_i->tasa_cuota" importe="$concepto_i->importe"/>
					</TRASLADADOS>
XML;
                } else
                if ($concepto_i->tipo_impuesto == 2) {
                    //retenido
                    $conceptos_impuestos = true;
                    $conceptos .= "\n" . <<<XML
					<RETENIDOS>
						<IMPUESTO base="$concepto_i->base" impuesto="$concepto_i->impuesto" tipo_factor="$concepto_i->tipo_factor" tasa_o_cuota="$concepto_i->tasa_cuota" importe="$concepto_i->importe" />
					</RETENIDOS>
XML;
                }

                if ($conceptos_impuestos == true) {
                    $conceptos .= "\n" . <<<XML
				</IMPUESTOS>
XML;
                }
            }

            $conceptos .= "\n" . <<<XML
			</CONCEPTO>
XML;
            $c++;
        }

        //IMPUESTOS
        $impuestos = Model\Emi_trx33_impuestos_r::find_by_id_trx33_r($id_trx33);
        $trasladados = "";
        $retenidos = "";
        foreach ($impuestos as $impuesto) {
            //trasladado
            if ($impuesto->tipo_impuesto == 1) {
                $trasladados = <<<XML
		<IMPUESTO impuesto="$impuesto->impuesto" tipo_factor="$impuesto->tipo_factor" tasa_o_cuota="$impuesto->tasa_o_cuota" importe="$impuesto->importe"/>
XML;
            } else
            if ($impuestos->tipo_impuesto == 2) {
                //retenido
                $retenidos = <<<XML
		<IMPUESTO impuesto="$impuesto->impuesto" tipo_factor="$impuesto->tipo_factor" tasa_o_cuota="$impuesto->tasa_o_cuota" importe="$impuesto->importe"/>
XML;
            }
        }

        // cadena del emisor. Si se tiene sucursal
        if ($trx33_r->id_sucursal != null) {
            $cadena_emisor = <<<XML
			<EMISOR rfc="$emisor_rfc" nombre="$emisor_nombre" regimen_fiscal="$emisor_regimen_fiscal" id_emisor_sto="$emisor_id_emisor_sto" id_emisor_erp="$emisor_id_emisor_sto">
				<SUCURSAL rfc="$rfc_sucursal" nombre="$entidad_sucursal" regimen_fiscal="$id_regimen_sucursal" id_emisor_sto="$id_sucursal" id_emisor_erp="$emisor_id_emisor_sto" numero_interior="$numero_interior_sucursal" numero_exterior="$numero_exterior_sucursal" calle="$calle_sucursal"
					colonia="$colonia_sucrusal" localidad="$localidad_sucursal" referencia="$referencia_sucursal" municipio="$municipio_sucursal" estado="$estado_sucursal" pais="$pais_sucursal" codigo_postal="$codigo_postal_sucursal" email="$email_sucursal" id_tipo_emisor="$id_tipo_emisor_sucursal" id_emisor_padre="$id_tipo_entidad_padre_sucursal" estatus_registro="$estatus_sucursal"/>
			</EMISOR>
XML;
        } else {
            // solo se tiene emisor
            $cadena_emisor = <<<XML
			<EMISOR rfc="$emisor_rfc" nombre="$emisor_nombre" regimen_fiscal="$emisor_regimen_fiscal" id_emisor_sto="$emisor_id_emisor_sto" id_emisor_erp="$emisor_id_emisor_sto">
			</EMISOR>
XML;
        }

        $archivo_importacion = <<<XML
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<DOCUMENTOS>
	<DOCUMENTO serie="$serie_nota_credito" folio="$folio_nota_credito" fecha="$fecha" forma_de_pago="$forma_de_pago" tipo_cambio="$tipo_cambio" moneda="$moneda" metodo_pago="$metodo_pago" lugar_expedicion="$lugar_expedicion" tipo_comprobante="$tipo_comprobante"
				subtotal="$subtotal" descuento="$descuento" total="$total" tipo_documento="$tipo_documento">
		<DOCUMENTO_ERP id_trx_erp="$id_trx_erp"/>
        <CFDIRELS>
            <CFDIREL tipo_relacion="01" uuid_relacionado="$xml_global->uuid" id_trx_erp_original="$id_trx33_global"/>
        </CFDIRELS>
        $cadena_emisor
		<RECEPTOR rfc="XAXX010101000" nombre="" residencia_fiscal="" num_reg_id_trib="" uso_cfdi="G01" id_receptor_sto="$id_cliente_receptor" id_receptor_erp="$id_cliente_receptor" numero_exterior="" calle="" numero_interior="" colonia="" localidad="" referencia="" municipio="" estado="" pais="" codigo_postal="" email="" id_tipo_receptor="" id_receptor_padre="" estatus_registro="1" >
		</RECEPTOR>
XML;
        if ($flex_headers != "") {
            $archivo_importacion .= <<<XML
		<FLEX_HEADERS>
				$flex_headers
		</FLEX_HEADERS>
XML;
        }
        if ($conceptos != "") {
            $archivo_importacion .= "\n" . <<<XML
		<CONCEPTOS>
	$conceptos
		</CONCEPTOS>
XML;
        }
        $archivo_importacion .= "\n" . <<<XML
		<IMPUESTOS
XML;
        if ($total_impuestos_retenidos != "" && $total_impuestos_retenidos != null) {
            $archivo_importacion .= <<<XML
	totalImpuestosRetenidos="$total_impuestos_retenidos"
XML;
        }
        if ($total_impuestos_trasladados != "" && $total_impuestos_trasladados != null) {
            $archivo_importacion .= <<<XML
	totalImpuestosTrasladados="$total_impuestos_trasladados"
XML;
        }

        $archivo_importacion .= <<<XML
>
XML;
        if ($trasladados != "") {
            $archivo_importacion .= "\n" . <<<XML
			<TRASLADADOS>
		$trasladados
			</TRASLADADOS>
XML;
        }

        if ($retenidos != "") {
            $archivo_importacion .= "\n" . <<<XML
			<RETENIDOS>
				$retenidos
			</RETENIDOS>
XML;
        }

        $archivo_importacion .= "\n" . <<<XML
		</IMPUESTOS>
	</DOCUMENTO>
</DOCUMENTOS>
XML;

//echo $archivo_importacion;
//die();
// ------------- crear una tabla de relacion de nota de credito generada automaticamente desde portal ss
        //Guardarlos en la ruta para mandar el xml generado a Importacion XML
        $folder = $directorio_carga_tickets;
        $name = 'pss_nctrxorig_' . $id_trx33 . "_trxglobal_" . $id_trx33_global . ".xml";
        $file_name = $folder . $name;
        $control = fopen($file_name, "w+");
        if (file_exists($file_name)) {
            fwrite($control, $archivo_importacion);
        }
        fclose($control);

        $observaciones = "idtrx33obtenida: " . $transaccion->id_trx33_r . " - Archivo nota de credito creado: " . $file_name;
        // transaccion en global pero permite continuar
        registrar_evento_bitacora($this, $id_usuario, BUSCAR_TRANSACCION_FACTURAR, $observaciones);
    }

    /*
      Funcion que invoca el WS de emision de Neon para generar la factura a partir del id_trx33 de la transaccion
     */

    public function facturar_por_ws($id_trx33, $id_usuario) {
        // se obtiene la configuracion del portal
        $config_portal = Model\pss_config_portal::find(1);

        // url de pruebas
        //$url_ws_facturacion = "http://localhost:8080/NeonEmisionWS_20180324/NeonEmisionWS?wsdl";
        ini_set('default_socket_timeout', 5000);
        try {
            // se verifica si el servicio esta arriba y funcionando
        $url_ws_responde = false;
            $handle = curl_init($config_portal->url_ws_facturacion);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);

            $response = curl_exec($handle);
            $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
            if ($httpCode == 200) {
                /* la peticion fue correcta */
                $url_ws_responde = true;
            }

            curl_close($handle);
        } catch (Exception $e) {
            // ocurrio un error al esperar la respuesta del servicio
            $url_ws_responde = false;
        }


        // si el ws si esta respondiendo
        if ($url_ws_responde) {
            // se crea el cliente del web service de facturacion
            $neon_emisionws = new SoapClient($config_portal->url_ws_facturacion);

            // se genera el array de parametros
            $parametros = array("trxID" => $id_trx33);

            // se invoca la operacion de facturacion
            $resultado = $neon_emisionws->emitirConTrxID($parametros);

            //print_r($resultado);
            //die();
            // si se timbró
            //if ( true && $url_ws_responde ){
            if ( $resultado->return->codigoRespuestaPac == 10 && $url_ws_responde) {
                $observaciones = "idtrx33obtenida: " . $id_trx33 . " - El WS de emision ejecuto correctamente, repuesta PAC: ".$resultado->return->codigoRespuestaPac;
                registrar_evento_bitacora($this, $id_usuario, CORRECTO_TRANSACCION_FACTURAR, $observaciones);               
                return;
            }
        }


        // no se pudo facturar, se devuelve el id_receptor al usado para autofactura a la transaccion
        $transaccion_r = Model\Emi_trx33_r::find($id_trx33);
        $transaccion_r->id_receptor = $config_portal->id_cliente_autofactura;
        $transaccion_r->save();

        // ocurrio un error al facturar el documento. Se envia mensaje a los administradores, operadores y usuarios
        $remitente = Model\Envio_correo_remitente::find_by_es_default(1, FALSE);
        $destinatario = new Model\Envio_correo_destinatario();
        $envio = new Model\Envio_correo();

        $pss_usuario = Model\Pss_usuario::find($id_usuario, false);

        // se llenan los datos para el envio de correo
        $envio->id_envio_correo = 0;
        $envio->id_transaccion = null;
        $envio->id_proceso = null;
        $envio->id_remitente = $remitente->id_remitente;
        $envio->procesado = -1; // pendiente
        $envio->fecha_registro = date("Y-m-d");
        $envio->fecha_proceso = null;
        $envio->enviar_adjuntos = 0;
        $envio->asunto = "SERVICIO DE AUTOFACTURACION: Error al facturar ticket";

        if ($url_ws_responde) {
            $mensaje_error_correo = $resultado->return->descripcionRespuestaPac;
        } else {
            $observaciones = "idtrx33obtenida: " . $id_trx33_r . " - Servicio de facturación no está disponible. Error 404.";
            registrar_evento_bitacora($this, $id_usuario, ERROR_TRANSACCION_FACTURAR, $observaciones);

            $mensaje_error_correo = "Servicio de facturación no está disponible. Error 404.";
        }

        $cuerpo_correo = "MENSAJE ENVIADO POR EL SISTEMA DE FACTURACIÓN<br><br>El usuario [" . $pss_usuario->login . "-" . $pss_usuario->nombre . " " . $pss_usuario->apellido_paterno . "] (login/nombre) intentó realizar la facturación del ticket [CLAVE INTERNA: " . $id_trx33 . "] cuya operación no se completó correctamente debido a:<br><br><b>" . $mensaje_error_correo . "</b><br><br>Favor de atender incidente.<br><br>No es necesario responder a este correo";


        $envio->cuerpo = $cuerpo_correo;
        $envio->save();

        // se obtiene el id de envio
        $id_envio = Model\Envio_correo::last_created()->id_envio_correo;
        // se asigna al registro
        $envio = Model\Envio_correo::find($id_envio);

        // se genera el registro del destinatario (administrador STO)
        $pss_usuario_administrador_sto = Model\Pss_usuario::find(1, false);
        $destinatario->id_correo_destinatario = 0;
        $destinatario->id_envio_correo = $id_envio;
        $destinatario->destinatario = $pss_usuario_administrador_sto->email;
        $destinatario->fecha_proceso = null;
        $destinatario->estatus_envio = 1;
        $destinatario->cod_error = null;
        $destinatario->d_error = null;
        $destinatario->num_intentos = 0;
        $destinatario->save();

        // se genera el registro del destinatario (administrador del portal y operadores)
        $this->db->where("tipo_usuario in (1)");
        $pss_usuario_administrador_operador = Model\Pss_usuario::all();

        foreach ($pss_usuario_administrador_operador as $unadministrador) {
            $destinatario->id_correo_destinatario = 0;
            $destinatario->id_envio_correo = $id_envio;
            $destinatario->destinatario = $unadministrador->email;
            $destinatario->fecha_proceso = null;
            $destinatario->estatus_envio = 1;
            $destinatario->cod_error = null;
            $destinatario->d_error = null;
            $destinatario->num_intentos = 0;
            $destinatario->save();
        }


        // se genera el registro del destinatario (usuario)
        $destinatario->id_correo_destinatario = 0;
        $destinatario->id_envio_correo = $id_envio;
        $destinatario->destinatario = $pss_usuario->email;
        $destinatario->fecha_proceso = null;
        $destinatario->estatus_envio = 1;
        $destinatario->cod_error = null;
        $destinatario->d_error = null;
        $destinatario->num_intentos = 0;
        $destinatario->save();


        // se actualiza el registro de envio para que el ejecutor de envio lo considere
        $envio->procesado = 1; // listo para enviar
        $envio->save();

        // se cambia el estatus del correo para que se pueda enviar
        $envio->procesado = 0; // pendiente
        $envio->save();
        
        /* Se envia el mensaje de error de facturacion y termina la ejecucion */
        $mensaje_error = $mensaje_error_correo . " Por favor intente nuevamente más tarde. El administrador del servicio ha sido notificado del error; una copia del correo electrónico se enviará a su buzón.";
        $this->responder("Internal Server Error", "Error al procesar transacción. $mensaje_error", 500);
        die();
    }

    /**
     * Obtiene el xml de la BD, el pdf a partir del servicio birt y  los 
     * convierte en base64, indica el mensaje exitoso de la transaccion e 
     * incluye los archivos en la respuesta.

     * @param id de la factura de la cual se convertira el pdf y xml
     *      */

    public function enviar_xml_pdf($id_trx33) {
        /* Se obtiene el xml de la BD y el pdf directo del servicio Birt que lo
         * genera para no esperar hasta que el servicio lo genere.
         *      */
        $emi_pdf = $this->obtener_pdf($id_trx33);
        $emi_xml = Model\Emi_trx33_xml::find_by_id_trx33($id_trx33, false);
        $pdf = null;
        $xml = null;

        /* Si se encuentra el pdf y el xml */
        if (!empty($emi_pdf) && !empty($emi_xml)) {
            $pdf = $emi_pdf;
            $xml = $emi_xml->xml_timbrado;
        }
        /* Se convierten en base 64 para enviar en el cuerpo de la respuesta json */
        $pdf_base64 = base64_encode($pdf);
        $xml_base64 = base64_encode($xml);
        
        $this->responder("Ok", "Transaccion facturada correctamente.", 201, $pdf_base64, $xml_base64);
    }
    
    /**
     * A partir del id_trx33 se obtiene el pdf invicando directamente al servicio
     * birt para no esperar a qué lo genere y lo coloque en la BD.
     
     * @param id de la factura de la cual se obtendra el pdf
     * @return regresa el binario del pdf, sino es correcta la peticion regresa false
     *      */
    public function obtener_pdf($id_trx33){
        /* Url de servicio Birt el cual genera el pdf a partir del id_trx33 
         * se obtiene de la configuracion del portal */
        $config_portal = Model\pss_config_portal::find(1);        
        $url_ws_birt = $config_portal->url_generador_pdf.$id_trx33;
        ini_set('default_socket_timeout', 5000);
        try {
            // se verifica si el servicio esta arriba y funcionando
            $handle = curl_init($url_ws_birt);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);

            $response = curl_exec($handle);
            $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

            if ($httpCode == 200) {
                /* la peticion fue correcta */
                return $response;
            }                        
            curl_close($handle);
            return false;
        } catch (Exception $e) {
            // ocurrio un error al esperar la respuesta del servicio
            return false;
        }        
    }
}

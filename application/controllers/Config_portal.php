<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_portal extends CI_Controller {

 function __construct() {
        parent::__construct();
        
        // si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
        $existe_sesion = $this->session->userdata("id_usuario");
        if ( empty( $existe_sesion ) ) {
            redirect(site_url(),'refresh');
        }

    }
    
	/**
	 * Configuracion de portal
	 *
	 */
	public function index() {

	
        // se creal el arreglo para paso de parametros
        $data = array();
        
        // se obtienen los datos del usuario
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;
        
        // se obtienen los parametros de configuracion del portal
        $config_portal = Model\pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;

        // Posibles Identificadores de cliente para realizar factura global
        $identificador_global           = Model\C_clientes::like('rfc', 'XAXX010101000')->all();
        $data["identificador_global"]   = $identificador_global;
        
        // Se obtiene el catalogo de frecuencia maxima para facturar
        $frecuencias = Model\Pss_frecuencia_max_fac::all();
        $data["frecuencias"] = $frecuencias;     
                        
        // url para editar frecuencia maxima de facturacion
        $url_frecuencia_fac = base_url("index.php/config_portal/".URL_FRECUENCIA_FACTURACION);
        $data["url_frecuencia_fac"] = $url_frecuencia_fac;  
                
        // url para el controlador de validacion de inicio de sesion
        $url_actualiza_config_portal = base_url("index.php/config_portal/actualiza_config_portal");
        
        // pregunta de recuperacion usada
        $pregunta_recuperacion = Model\C_preguntas_recuperacion::find($pss_usuario->id_pregunta_recuperacion, false);
        $data["pregunta_recuperacion"] = $pregunta_recuperacion->pregunta;
        
        // url para recuperar la contrasena
        $url_anterior = base_url("index.php/".URL_PANTALLA_PRINCIPAL);
        
        // url para configurar flex headers
        $url_config_fh = base_url("index.php/config_portal/configurar_flex_headers_facturacion");
        // url para agregar flex headers (campos adicionales a los de busqueda)
        $url_agregar_fh = base_url("index.php/config_portal/agregar_flex_headers");										
        
        // url para eliminar flex header
        $url_eliminar_campo_flex_autofactura = base_url("index.php/config_portal/eliminar_campo_flex_autofactura");
        
        // se transfieren los parametros al arreglo
        $data["url_actualiza_config_portal"]         = $url_actualiza_config_portal;
        $data["url_anterior"]                        = $url_anterior;
        $data["url_config_fh"]                       = $url_config_fh;
        $data["url_agregar_fh"]                      = $url_agregar_fh;
        $data["url_eliminar_campo_flex_autofactura"] = $url_eliminar_campo_flex_autofactura;
        
        
        // series por entidad configuradas
        $arr_series_entidades = Model\V_pss_series_entidades::all();
        $data["arr_series_entidades"] = $arr_series_entidades;
        
        // procesos
        $this->db->where("id_proceso = 8");
        $arr_procesos = Model\C_procesos::all();
        $data["arr_procesos"] = $arr_procesos;
        
        // urls para crear y eliminar series de facturacion
        $url_config_series = base_url()."index.php/config_portal/configurar_series";
        $data["url_config_series"] = $url_config_series;
        
        $url_eliminar_entidad_serie = base_url()."index.php/config_portal/eliminar_serie_entidad";
        $data["url_eliminar_entidad_serie"] = $url_eliminar_entidad_serie;
        
        // unidad de medida y clave de producto o servicio
        if ( $config_portal->clave_prod_serv_generico != null && $config_portal->clave_prod_serv_generico != "" ) {
            $clave_prod_serv = $this->obtener_cat_clave_prod_serv($config_portal->clave_prod_serv_generico);
            $des_clave_prod_serv_generico = $clave_prod_serv->descripcion;
        } else {
            $des_clave_prod_serv_generico = "";
        }
        
        if ( $config_portal->unidad_medida_generico != null && $config_portal->unidad_medida_generico != "" ) {
            $clave_unidad_medida = $this->obtener_cat_unidad_medida($config_portal->unidad_medida_generico);
            $des_unidad_medida_generico = $clave_unidad_medida->nombre;
        } else {
            $des_unidad_medida_generico = "";
        }

        $data["des_clave_prod_serv_generico"] = $des_clave_prod_serv_generico;
        $data["des_unidad_medida_generico"]   = $des_unidad_medida_generico;
        
        // si se recibio el mensaje de confirmacion se agrega a los datos pasados a la vista
        if ( $this->session->flashdata('titulo') != null ) {
          $data["titulo"]       = $this->session->flashdata('titulo');
          $data["mensaje"]      = $this->session->flashdata('mensaje');
          $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
        }
        
        // campos para busqueda de transaccion
        $arr_campos_transaccion = Model\V_pss_campos_transaccion::all();
        $data["arr_campos_transaccion"] = $arr_campos_transaccion;
        
        // configuracion de correo de envio
        $config_correo = Model\Envio_correo_remitente::find(1, false);
        // si no hay configuracion se envia nulo
        if ( $config_correo == null ) {
            $config_correo = new Model\Envio_correo_remitente();
        }
        $data["config_correo"] = $config_correo;
        
        cargar_interfaz_grafica($this, $data, 'config/view_content_wrapper_config_portal', null);

	}
    
    public function plugins() {
	
        // se creal el arreglo para paso de parametros
        $data = array();
        
        // url de configuracion
        $url_anterior = base_url("index.php/config_portal");
        $data["url_anterior"] = $url_anterior;
        
        // se leen los plugins
        $arr_plugins = parse_ini_file("./application/controllers/plugins/plugins.ini", true);
        $data["arr_plugins"] = $arr_plugins;
        
        cargar_interfaz_grafica($this, $data, 'config/view_content_wrapper_config_portal_plugins', null);
    }
    
    public function obtener_cat_clave_prod_serv($clave_prod_serv) {
        // se asigna uso de utf en los nombres
        $this->db->simple_query("SET NAMES \'utf8\'");
        
        // se obtienen los datos del codigo postal
        $cat_clave_prod_serv = Model\Emi_c_claveprodserv::find_by_id_claveprodserv($clave_prod_serv, false);

        return $cat_clave_prod_serv;
    }
    
    public function obtener_cat_unidad_medida($clave_unidad_medida) {
        // se asigna uso de utf en los nombres
        $this->db->simple_query("SET NAMES \'utf8\'");
        
        // se obtienen los datos del codigo postal
        $cat_clave_unidad = Model\Emi_c_clave_unidad::find_by_id_clave_unidad($clave_unidad_medida, false);

        return $cat_clave_unidad;
    }
    
    public function configurar_series() {
        $data = array();
		
        // si se recibio el mensaje de confirmacion se agrega a los datos pasados a la vista
        if ( $this->session->flashdata('titulo') != null ) {
          $data["titulo"]       = $this->session->flashdata('titulo');
          $data["mensaje"]      = $this->session->flashdata('mensaje');
          $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
        }
		
        $arr_entidades = Model\V_pss_c_entidades::all();
        $data["arr_entidades"] = $arr_entidades;
        
        $url_agregar_serie_entidad = base_url("index.php/Config_portal/agregar_serie_entidad");
        $data["url_agregar_serie_entidad"] = $url_agregar_serie_entidad;
        
        $url_anterior = base_url("index.php/Config_portal");
        $data["url_anterior"] = $url_anterior;
        
        $url_ajax_series_entidad = base_url()."index.php/config_portal/ajax_obtener_series_entidad/";
        $data["url_ajax_series_entidad"] = $url_ajax_series_entidad;
        
        cargar_interfaz_grafica($this, $data, 'config/view_content_wrapper_config_portal_serie_entidad_autofactura', 'config/view_content_wrapper_config_portal_serie_entidad_autofactura_script');
    }
    
    public function ajax_obtener_series_entidad($id_entidad) {
        // se asigna uso de utf en los nombres
        $this->db->simple_query("SET NAMES \'utf8\'");
		
        // se obtienen los datos del codigo postal
        $this->db->where("id_entidad = ", $id_entidad);
        $c_series = Model\C_series_entidad::all();
        
        // se devuelve el arreglo en formato json
        $arr_series_entidad = array();
        foreach ($c_series as $serie) {
            $serie_entidad["serie"] = $serie->serie;
            $serie_entidad["secuencia"] = $serie->secuencia;
            array_push($arr_series_entidad, $serie_entidad);
        }

        echo json_encode($arr_series_entidad);
        
    }
    
    public function agregar_serie_entidad() {
        //$this->output->enable_profiler(TRUE);
        
        // se obtienen los datos
        $id_entidad     = $this->input->post("id_entidad");
        $serie          = $this->input->post("serie");
        $tipo_factura   = $this->input->post("tipo_factura");
        
        // se inserta l serie con entidad
        $nuevo_se = new Model\pss_series_entidades();
        
        if ($tipo_factura=='1') {
            $nuevo_se->id_serie_entidad  = 0;
            $nuevo_se->serie             = $serie;
            $nuevo_se->id_entidad        = $id_entidad;
            $nuevo_se->tipo              = $tipo_factura;
            $nuevo_se->save();
            
        }
        if ($tipo_factura=='2') {
            $nuevo_se->id_serie_entidad  = 0;
            $nuevo_se->serie             = $serie;
            $nuevo_se->id_entidad        = $id_entidad;
            $nuevo_se->tipo              = $tipo_factura;
            $nuevo_se->save();
            
        }
        if ($tipo_factura=='3') {
			// se asigna la serie como factura
            $nuevo_se->id_serie_entidad  = 0;
            $nuevo_se->serie             = $serie;
            $nuevo_se->id_entidad        = $id_entidad;
            $nuevo_se->tipo              = "1";
            $nuevo_se->save();
			
			// se asigna la serie como nota de credito
            $nuevo_se->id_serie_entidad  = 0;
            $nuevo_se->serie             = $serie;
            $nuevo_se->id_entidad        = $id_entidad;
            $nuevo_se->tipo              = "2";
            $nuevo_se->save();
            
        }
		
		// verifica si hay error
		$error = $this->db->error();
		//print_r($error);
		//die();
		
		// si ocurrio un error
		if ( $error != null && $error["code"] > 0 ) {
			$codigo_error = $error["code"];
			
			// si es duplicado
			if ( $codigo_error = 1062 ) {
                // se redirige a la ventana de configuracion
                $this->session->set_flashdata('titulo', "Nueva serie para autofacturación");
                $this->session->set_flashdata('mensaje', "Ya existe una serie definida para la entidad elegida y el tipo de documento seleccionado. Intente otra combinación o elimine la existente.");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
		        $url_login = base_url()."index.php/config_portal/configurar_series";
                redirect($url_login);
			} else  {
				// otro error
                // se redirige a la ventana de configuracion
                $this->session->set_flashdata('titulo', "Nueva serie para autofacturación");
                $this->session->set_flashdata('mensaje', "Ocurrió un error en la definición de la serie y el tipo de documento elegido. Por favor comuníquese a soporte para notificar el incidente");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
		        $url_login = base_url()."index.php/config_portal/configurar_series";
                redirect($url_login);
			}
		}
		
        // se redirige a la ventana de configuracion
        $this->session->set_flashdata('titulo', "Nueva serie para autofacturación");
        $this->session->set_flashdata('mensaje', "La asociación de la serie elegida para autofacturación ha sido realizada con éxito");
        $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
		$url_login = base_url()."index.php/config_portal/index";
        redirect($url_login);
    }
    
    public function eliminar_serie_entidad($id_serie_entidad) {
        // se elimina el campo
        $serie_entidad = Model\Pss_series_entidades::find($id_serie_entidad);
        $serie_entidad->delete();
        
        // se redirige a la ventana de configuracion
        $this->session->set_flashdata('titulo', "Eliminar asociación de Entidad-Serie para autofacturación");
        $this->session->set_flashdata('mensaje', "La serie-Entidad elegidas para autofacturación ha sido desasociada del portal de autofactura. Esto no afecta la configuración de Neon.");
        $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
		$url_login = base_url()."index.php/config_portal/index";
        redirect($url_login);
    }
    
    public function configurar_flex_headers_facturacion() {
        $data = array();
        
        // se obtiene la lista de tipos de dato
        $arr_tipodato = Model\Pss_c_tipo_dato::all();
        $data["arr_tipodato"] = $arr_tipodato;
        
        // listado de flex headers
        $i=1;
        //echo "string";
        $this->load->model("model_agregar_flexheader");
        $array_id_fh = $this->model_agregar_flexheader->obtener_id_flexheader();
                //echo "string";

        //echo $array_id_fh;
        //print_r($array_id_fh);

								  
			 
        $arr_id_fh = null;
						 
        foreach ($array_id_fh as $q_id_fh) {
            if ( $q_id_fh != null ) {
                $arr_id_fh[$i] = $q_id_fh;
                $i++;
            }
        }

        //echo print_r($arr_id_fh);
        //echo "hola";
        //die();
        //$arr_id_fh = null;
        if ($arr_id_fh != null) {
        
            $array_ids = '';
            foreach ($arr_id_fh as $id_fh) {
                if ($array_ids=='') {
                    $array_ids = $id_fh->id_flex_header;
                }else{
                    $array_ids = $array_ids.','.$id_fh->id_flex_header;
                }
            }
            //echo $array_ids;

            if($array_ids!=null){

                $i=1;
                $array_info_fh = $this->model_agregar_flexheader->obtener_info_flexheader($array_ids);
							  
                //$query_flex_header = $this->db->query($query);

                
                $arr_flex_headers=null;
                foreach ($array_info_fh as $c_flex_header) {
                    if ( $c_flex_header != null ) {
                        // se agrega el cliente al arreglo de clientes
                        $arr_flex_headers[$i] = $c_flex_header;
                        $i++;
                    }
                }
                $data["arr_flex_headers"] = $arr_flex_headers;
                //die();
            }
        }else{

            $array_all_info_fh = $this->model_agregar_flexheader->obtener_all_info_flexheader();

			 
			  
															
						  
														  
            $arr_flex_headers=null;
            foreach ($array_all_info_fh as $c_flex_header) {
                if ( $c_flex_header != null ) {
                    // se agrega el cliente al arreglo de clientes
                    $arr_flex_headers[$i] = $c_flex_header;
                    $i++;
                }
            }
            $data["arr_flex_headers"] = $arr_flex_headers;
        }
        //die();
        $url_agregar_campo_flex = base_url("index.php/Config_portal/agregar_campo_flex_autofactura");
        $data["url_agregar_campo_flex"] = $url_agregar_campo_flex;
        
        $url_anterior = base_url("index.php/Config_portal");
        $data["url_anterior"] = $url_anterior;
        
        cargar_interfaz_grafica($this, $data, 'config/view_content_wrapper_config_portal_fh_autofactura', null);
    }

    public function agregar_flex_headers() {
        $this->load->model("model_agregar_flexheader");



        $data = array();

        // la url para cambiar el estado del registro
        $url_cambiar_estado_fh = base_url()."index.php/config_portal/cambiar_estado_fh";
        $data["url_cambiar_estado_fh"] = $url_cambiar_estado_fh;



        $url_agregar_fh = base_url("index.php/config_portal/agregar_flex_headers");
        $data["url_agregar_fh"]                      = $url_agregar_fh;


        // campos para busqueda de transaccion
        //$arr_c_info_adicionales = Model\Emi_c_info_adicionales::all();

        $arr_c_info_adicionales = $this->model_agregar_flexheader->obtener_flexheaders();
        $data["arr_c_info_adicionales"] = $arr_c_info_adicionales;
        //echo $arr_c_info_adicionales;
        
        // se obtiene la lista de tipos de dato
        $arr_tipodato = Model\Pss_c_tipo_dato::all();
        $data["arr_tipodato"] = $arr_tipodato;
        
        // listado de flex headers
        $i=1;
        $this->load->model("model_agregar_flexheader");
        $query_id_fh = $this->model_agregar_flexheader->obtener_id_flexheader();

        //die();
        $arr_id_fh =null;
        foreach ($query_id_fh as $q_id_fh) {
            if ( $q_id_fh != null ) {
                $arr_id_fh[$i] = $q_id_fh;
                $i++;
            }
        }
        ///edit
        
        if ($arr_id_fh != null) {
        
            $array_ids = '';
            foreach ($arr_id_fh as $id_fh) {
                if ($array_ids=='') {
                    $array_ids = $id_fh->id_flex_header;
                }else{
                    $array_ids = $array_ids.','.$id_fh->id_flex_header;
                }
            }
            //echo $array_ids;
//die();
            if($array_ids!=null){

                $i=1;
                $array_info_fh = $this->model_agregar_flexheader->obtener_info_flexheader($array_ids);

                //echo $query;
															  
                $arr_flex_headers=null;
                foreach ($array_info_fh as $c_flex_header) {
                    if ( $c_flex_header != null ) {
                        // se agrega el cliente al arreglo de clientes
                        $arr_flex_headers[$i] = $c_flex_header;
                        $i++;
                    }
                }
                $data["arr_flex_headers"] = $arr_flex_headers;
            }else{


            }
        }else{
            $array_all_info_fh = $this->model_agregar_flexheader->obtener_all_info_flexheader();

														  
            $arr_flex_headers=null;
            foreach ($array_all_info_fh as $c_flex_header) {
                if ( $c_flex_header != null ) {
                    // se agrega el cliente al arreglo de clientes
                    $arr_flex_headers[$i] = $c_flex_header;
                    $i++;
                }
            }
            $data["arr_flex_headers"] = $arr_flex_headers;
        }


        $url_agregar_campo_flex = base_url("index.php/Config_portal/agregar_campo_flex_autofactura");
        $data["url_agregar_campo_flex"] = $url_agregar_campo_flex;
        


        $url_agregar_nombre_flex          = base_url()."index.php/Config_portal/agregar_nombre_flex";

        $data["url_agregar_nombre_flex"]       = $url_agregar_nombre_flex; 



        $url_anterior = base_url("index.php/Config_portal");
        $data["url_anterior"] = $url_anterior;
        
        cargar_interfaz_grafica($this, $data, 'config/view_content_wrapper_agregar_portal_fh_autofactura.php', null);
    }

    public function cambiar_estado_fh($id_fh_editado, $id_estatus) {
        // se obtiene el campo que se bloqueara

        $this->load->model("model_agregar_flexheader");
        $campo_inf_adic = $this->model_agregar_flexheader->update_estatus_fh($id_fh_editado, $id_estatus);
        //$campo_inf_adic = Model\Emi_c_info_adicionales::find($id_fh_editado);

        // se cambia el estatus
        //$campo_inf_adic->estatus = $id_estatus;
        // se guarda el registro
        //$campo_inf_adic->save();
        echo "id_estatus ".$id_estatus;

        

        $this->session->set_flashdata('titulo', "Edición de configuracion");
        //die();
        // si se activo
        if ( $id_estatus == 1 ) {
            $this->session->set_flashdata('mensaje', "El flexheader ha sido agregada correctamente");
        } else {
        // se bloqueo
            $this->session->set_flashdata("mensaje", "El flexheader ha sido eliminada correctamente");
        }
          
        $this->session->set_flashdata("tipo_mensaje", 'alert alert-success alert-dismissible');
          
        $url_principal_fh = base_url()."index.php/config_portal/agregar_flex_headers";
        redirect($url_principal_fh);
        
    }


    public function agregar_nombre_flex() {
        //$this->output->enable_profiler(TRUE);

        //inserta los flexheader en las tablas
        $this->load->model("model_agregar_flexheader");
        //$array_campos_fh = $this->db->query("");
        $array_campos_fh = $this->model_agregar_flexheader->obtener_estatus_flexheaders();
        echo print_r($array_campos_fh);
        $num=0;
        $url_arr_campos_fh="";
        foreach ($array_campos_fh as $value) {

            $name = $value->campo_adicional;
            $id_info = $value->id_info_adicional;
            $valor        = $this->input->post($name);

									
            $q_query_inf_adic = $this->model_agregar_flexheader->actualizar_nombre_fh($valor,$id_info);
			
										  

															

            $num++;


        }

				

        // se redirige a la ventana de configuracion
        $this->session->set_flashdata('titulo', "Campo de identificacion de Flex Header");
        $this->session->set_flashdata('mensaje', "Se agregó el campo de identificación de Flex Header existosamente");
        $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
        $url_login = base_url()."index.php/config_portal/index";
        redirect($url_login);    
    }

    
    public function agregar_campo_flex_autofactura() {
        //$this->output->enable_profiler(TRUE);
        
        // se obtienen los datos
        $id_flex_header        = $this->input->post("id_flex_header");
        $etiqueta_flex_header  = $this->input->post("etiqueta_flex_header");
        $id_tipo_dato          = $this->input->post("id_tipo_dato");
        $placeholder           = $this->input->post("placeholder");
        $simbol                = $this->input->post("simbol");
        
        // se inserta el campo
        $nuevo_fh = new Model\Pss_fh_transaccion();
        
        $nuevo_fh->id_fh_transaccion     = 0;
        $nuevo_fh->id_flex_header        = $id_flex_header;
        $nuevo_fh->etiqueta_flex_header  = $etiqueta_flex_header;
        $nuevo_fh->id_tipo_dato          = $id_tipo_dato;
        $nuevo_fh->placeholder           = $placeholder;
		$nuevo_fh->simbol                = $simbol;
        $nuevo_fh->save();
        
        // se redirige a la ventana de configuracion
        $this->session->set_flashdata('titulo', "Campo de identificacion de transacción");
        $this->session->set_flashdata('mensaje', "Se agregó el campo de identificación de transacción existosamente");
        $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
		$url_login = base_url()."index.php/config_portal/index";
        redirect($url_login);
    }
    
    public function eliminar_campo_flex_autofactura($id_fh_transaccion) {
        // se elimina el campo
        $fh = Model\Pss_fh_transaccion::find($id_fh_transaccion);
        $fh->delete();
        
        // se redirige a la ventana de configuracion
        $this->session->set_flashdata('titulo', "Eliminación de Campo de identificacion de transacción");
        $this->session->set_flashdata('mensaje', "Se eliminó el campo de identificación de transacción existosamente");
        $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
		$url_login = base_url()."index.php/config_portal/index";
        redirect($url_login);
    }

	public function eliminar_campo_fh($id_fh_transaccion) {
        // se elimina el campo
        $fh = Model\Pss_fh_transaccion::find($id_fh_transaccion);
        $fh->delete();
        
        // se redirige a la ventana de configuracion
        $this->session->set_flashdata('titulo', "Eliminación de Campo de identificacion de transacción");
        $this->session->set_flashdata('mensaje', "Se eliminó el campo de identificación de transacción existosamente");
        $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
        $url_login = base_url()."index.php/config_portal/index";
        redirect($url_login);
    }
    public function actualiza_config_portal() {
        // se obtienen los datos del formulario de captura
        $plantilla_portal                  = $this->input->post("plantilla_portal");
        $titulo_pantalla_principal         = $this->input->post("titulo_pantalla_principal");
        $titulo_menu                       = $this->input->post("titulo_menu");
        $activar_captcha                   = $this->input->post("activar_captcha");
        $activar_autoregistro              = $this->input->post("activar_autoregistro");
        $activar_autodesbloqueo            = $this->input->post("activar_autodesbloqueo");
        $notif_trx_no_encontrada           = $this->input->post("notif_trx_no_encontrada");
        $usar_email_como_login             = $this->input->post("usar_email_como_login");
        $usar_contrasena                   = $this->input->post("usar_contrasena");
        $activar_fecha_max_facturar        = $this->input->post("activar_fecha_max_facturar");
        $fecha_max_para_facturar           = $this->input->post("fecha_max_para_facturar");
        $aviso_login                       = $this->input->post("aviso_login");
        $aviso_principal                   = $this->input->post("aviso_principal");
        $facturar_ticket_en_global         = $this->input->post("facturar_ticket_en_global");
		$generar_nota_credito_global       = $this->input->post("generar_nota_credito_global");
        $usar_concepto_generico            = $this->input->post("usar_concepto_generico");
        $clave_prod_serv_generico          = $this->input->post("clave_prod_serv_generico");
        $unidad_medida_generico            = $this->input->post("unidad_medida_generico");
        $descripcion_generico              = $this->input->post("descripcion_generico");
        $activar_elegir_uso_cfdi           = $this->input->post("activar_elegir_uso_cfdi");
        $activar_elegir_metodo_pago        = $this->input->post("activar_elegir_metodo_pago");
        $activar_elegir_forma_pago         = $this->input->post("activar_elegir_forma_pago");
        $id_cliente_autofactura            = $this->input->post("id_cliente_autofactura");
        $usuario_imap                      = $this->input->post("usuario_imap");
        $contrasena_imap                   = $this->input->post("contrasena_imap");
        $servidor_imap                     = $this->input->post("servidor_imap");
        $puerto_imap                       = $this->input->post("puerto_imap");
        $protocolo                         = $this->input->post("protocolo");
        $fecha_config                      = date("Y-m-d H:i:s");
        $ip_config                         = $this->input->ip_address();
        $url_ws_facturacion                = $this->input->post("url_ws_facturacion");
        $url_generador_pdf                 = $this->input->post("url_generador_pdf");
        $modo_facturacion                  = $this->input->post("modo_facturacion");
        $activar_modo_kiosko               = $this->input->post("activar_modo_kiosko");
        $modo_kiosko_tiempo                = $this->input->post("modo_kiosko_tiempo");
        $id_proceso_liberacion_ticket      = $this->input->post("id_proceso_liberacion_ticket");
        $leyenda_info_soporte              = $this->input->post("leyenda_info_soporte");
        $instrucciones_soporte             = $this->input->post("instrucciones_soporte");
        $email_soporte                     = $this->input->post("email_soporte");
        $telefono_soporte                  = $this->input->post("telefono_soporte");
		$uso_cfdi_default                  = $this->input->post("uso_cfdi_default");
		$metodo_pago_default               = $this->input->post("metodo_pago_default");
		$forma_pago_default                = $this->input->post("forma_pago_default");
		$recuperar_contrasena_con_pregunta = $this->input->post("recuperar_contrasena_con_pregunta");
        $id_cliente_factura_global         = $this->input->post("id_cliente_factura_global");
	if ($id_cliente_factura_global == $id_cliente_autofactura) {
            $this->session->set_flashdata('titulo', "Configuración del portal");
            $this->session->set_flashdata('mensaje', "El identificador del cliente "
                    . "para factura global no puede ser el mismo que el identificador"
                    . " del cliente en transacción pendiente de facturar");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
            $url_config = base_url()."index.php/config_portal/index";
            redirect($url_config);
        }	
        
		// si la configuracion de factura global viene nula, se asume cero
		if ( $facturar_ticket_en_global == null ) {
			$facturar_ticket_en_global = 0;
			
			// se desactiva la generacion de nota de credito, pues no puede estar el uno sin el otro
			$generar_nota_credito_global = 0;
		}
		if ( $generar_nota_credito_global == null ) {
			$generar_nota_credito_global = 0;
		}
		
        // si las validaciones son correctas
        if ( true ) {
            
            // se busca el registro de config
            $config_portal = Model\Pss_config_portal::find(1);
            
            // si viene vacio el id para autofctura se usa 1
            if ( $id_cliente_autofactura == null || $id_cliente_autofactura == "" ) {
                $id_cliente_autofactura = 1;
            }
            
            $config_portal->plantilla_portal                  = $plantilla_portal                 ;
            $config_portal->titulo_pantalla_principal         = $titulo_pantalla_principal        ;
            $config_portal->titulo_menu                       = $titulo_menu                      ;
            $config_portal->activar_captcha                   = $activar_captcha                  ;
            $config_portal->activar_autoregistro              = $activar_autoregistro             ;
            $config_portal->activar_autodesbloqueo            = $activar_autodesbloqueo           ;
            $config_portal->notif_trx_no_encontrada           = $notif_trx_no_encontrada          ;
            $config_portal->usar_email_como_login             = $usar_email_como_login            ;
            $config_portal->usar_contrasena                   = $usar_contrasena                  ;
            $config_portal->activar_fecha_max_facturar        = $activar_fecha_max_facturar       ;
            $config_portal->fecha_max_para_facturar           = $fecha_max_para_facturar          ;
            $config_portal->facturar_ticket_en_global         = $facturar_ticket_en_global        ;
			$config_portal->generar_nota_credito_global       = $generar_nota_credito_global      ;
            $config_portal->usar_concepto_generico            = $usar_concepto_generico           ;
            $config_portal->clave_prod_serv_generico          = $clave_prod_serv_generico         ;
            $config_portal->unidad_medida_generico            = $unidad_medida_generico           ;
            $config_portal->descripcion_generico              = $descripcion_generico             ;
            $config_portal->activar_elegir_uso_cfdi           = $activar_elegir_uso_cfdi          ;
            $config_portal->activar_elegir_metodo_pago        = $activar_elegir_metodo_pago       ;
            $config_portal->activar_elegir_forma_pago         = $activar_elegir_forma_pago        ;
            $config_portal->id_cliente_autofactura            = $id_cliente_autofactura           ;
            $config_portal->fecha_config                      = $fecha_config                     ;
            $config_portal->ip_config                         = $ip_config                        ;
            $config_portal->aviso_login                       = $aviso_login                      ;
            $config_portal->aviso_principal                   = $aviso_principal                  ;
            $config_portal->url_ws_facturacion                = $url_ws_facturacion               ;
            $config_portal->url_generador_pdf                 = $url_generador_pdf                ;            
            $config_portal->modo_facturacion                  = $modo_facturacion                 ;
            $config_portal->activar_modo_kiosko               = $activar_modo_kiosko              ;
            $config_portal->modo_kiosko_tiempo                = $modo_kiosko_tiempo * 1000        ; // el tiempo se guarda en milisegundos
            $config_portal->id_proceso_liberacion_ticket      = $id_proceso_liberacion_ticket     ;
            $config_portal->leyenda_info_soporte              = $leyenda_info_soporte             ;
            $config_portal->instrucciones_soporte             = $instrucciones_soporte            ;
            $config_portal->email_soporte                     = $email_soporte                    ;
            $config_portal->telefono_soporte                  = $telefono_soporte                 ;
		    $config_portal->uso_cfdi_default                  = $uso_cfdi_default                 ;
		    $config_portal->metodo_pago_default               = $metodo_pago_default              ;
		    $config_portal->forma_pago_default                = $forma_pago_default               ;
		    $config_portal->recuperar_contrasena_con_pregunta = $recuperar_contrasena_con_pregunta;
            $config_portal->id_cliente_factura_global         = $id_cliente_factura_global        ;
            
            $config_portal->save();
        
            // se guarda la configuracion de correo electronico
            $config_correo = Model\Envio_correo_remitente::find(1, false);
            // si no hay configuracion se envia nulo
            if ( $config_correo == null ) {
                $config_correo = new Model\Envio_correo_remitente();
            }
            
            $config_correo->id_remitente     = 1;
            $config_correo->d_remitente      = $titulo_pantalla_principal;
            $config_correo->usuario_imap     = $usuario_imap;
            $config_correo->contrasena_imap  = $contrasena_imap;
            $config_correo->servidor_imap    = $servidor_imap;
            $config_correo->puerto_imap      = $puerto_imap;
            $config_correo->root_folder      = "inbox";
            $config_correo->success_folder   = "success";
            $config_correo->failed_folder    = "failed";
            $config_correo->protocolo        = $protocolo;
            $config_correo->es_default       = 1;
            $config_correo->save();
                    
            $this->session->set_flashdata('titulo', "Configuración del portal");
            $this->session->set_flashdata('mensaje', "Los datos de configuración del portal fueron actualizados correctamente. Esta configuración es válida a partir del siguiente inicio de sesión.");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
		    $url_login = base_url()."index.php/config_portal/index";
            redirect($url_login);
            
        } else {
            $this->session->set_flashdata('titulo', "Configuración del portal");
            $this->session->set_flashdata('mensaje', "Ocurrió un error al actualizar los datos de configuración del portal");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
		    $url_login = base_url()."index.php/config_portal/index";
            redirect($url_login);
        }

    }
    /*  Función que se manda llamar cuando se quiere editar la frecuencia del 
     *  límite para poder facturar una transacción.
     *      */
    public function configurar_frecuencia_facturacion(){
        // Se obtiene el catálogo de frecuencia máxima para facturar
        $frecuencias = Model\Pss_frecuencia_max_fac::all();
        $data["frecuencias"] = $frecuencias; 
        
        $url_editar_frecuencia = base_url("index.php/Config_portal/editar_frecuencia_facturacion");
        $data["url_editar_frecuencia"] = $url_editar_frecuencia;
        
        $url_anterior = base_url("index.php/Config_portal");
        $data["url_anterior"] = $url_anterior;

        // se obtienen los parametros de configuracion del portal
        $config_portal = Model\pss_config_portal::find(1);
        $data["config_portal"] = $config_portal;
        
        // Se obtienen datos de la frecuencia que está configurada
        $frecuencia_max_fac = Model\Pss_frecuencia_max_fac::find_by_id_frecuencia_max_fac($config_portal->id_frecuencia_max_fac, false);
        $data["frecuencia_max_fac"] = $frecuencia_max_fac;        
        
        cargar_interfaz_grafica($this, $data, 'config/view_content_wrapper_config_portal_frecuencia_fracturacion', 'config/view_content_wrapper_config_portal_script');
    }
    /*  Función que permite la actualización de los valores de la frecuencia
     *  límite para poder facturar una transacción. Actualiza la tabla 
     *  pss_config_portal y pss_frecuencia_max_fac.
     * 
     *      */
    public function editar_frecuencia_facturacion(){
    
        /* Se reciben los datos del formulario */
        $id_frecuencia_max_fac  = $this->input->post("id_frecuencia_max_fac");
        $dias                   = $this->input->post("dias");
        $dias_posteriores       = $this->input->post("dias_posteriores");
        $cambio_anio            = $this->input->post("cambio_anio");
		
		// cambio anio no puede ser nulo
		if ( $cambio_anio == null || $cambio_anio == "" ) {
			$cambio_anio = 0;
		}
        
        /* Validar datos recibidos */
        
        /* Se crea cada entidad para ser actualizada */
        $config_portal = Model\Pss_config_portal::find(1);        
        $config_portal->id_frecuencia_max_fac = $id_frecuencia_max_fac;
        $config_portal->save();

        $frecuencia_max_fac = Model\Pss_frecuencia_max_fac::find_by_id_frecuencia_max_fac($id_frecuencia_max_fac,false);

        $frecuencia_max_fac->dias               = $dias;
        $frecuencia_max_fac->dias_posteriores   = $dias_posteriores ;
        $frecuencia_max_fac->cambio_anio        = $cambio_anio ;

        /* Se crean los títulos y la url para el mensaje */
        $this->session->set_flashdata('titulo', "Configuración del portal");        
        $url_config = base_url()."index.php/config_portal/index";
        
        /* Si no hay error se manda mensaje existoso */
        if ($frecuencia_max_fac->save(true))
        {
            $this->session->set_flashdata('mensaje', "Se actualizó correctamente la fecha límite de facturación");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
         
            redirect($url_config);            
        }  
        /* Si existe algún error en la actualización de los datos se redirecciona */
		$error_bd = $this->db->error();
		$mensaje_error = "Ocurrió un error al actualizar los datos de configuración del portal: <br>".$error_bd["code"]."-".$error_bd["message"];
        $this->session->set_flashdata('mensaje', $mensaje_error);
        $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
        redirect($url_config);

    }
    /* Función que obtiene los datos de la frecuencia límite de facturación a 
     * partir de un identificador.
     * 
     * @return regresa un arreglo de tipo json con los datos de la frecuencia.
     *      */
    public function obtener_datos_frecuencia() {
        /* Se obtienen los datos dependiendo del id de la frecuencia */
        $id_frecuencia_max_fac = $this->input->post('id_frecuencia_max_fac');
        $frecuencia_max_fac = Model\Pss_frecuencia_max_fac::find_by_id_frecuencia_max_fac($id_frecuencia_max_fac,false);
        
        /* Se regresa la respuesta de tipo json */
        echo json_encode(
                array(
                    'dias' => $frecuencia_max_fac->dias,
                    'dias_posteriores' => $frecuencia_max_fac->dias_posteriores,
                    'cambio_anio' => $frecuencia_max_fac->cambio_anio
                )
        );
    }

}

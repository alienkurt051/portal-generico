<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Peticiones_pendientes_sat extends CI_Controller {

	function __construct() {
		parent::__construct();


				// si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
		$existe_sesion = $this->session->userdata("id_usuario");
		if ( empty( $existe_sesion ) ) {
			redirect(site_url(),'refresh');
		}

	}

	/* controlador para gestionar las cuentas de usuario */
	public function index()
	{

		// registra evento en bitacora
		//registrar_evento_bitacora($this, $this->session->userdata("id_usuario"), CONSULTAR_MIS_COMPROBANTES);

		$data = array();
		// se obtienen los datos del usuario
		$pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
		$data["pss_usuario"] = $pss_usuario;
		
		$url_consultar_solicitudes = base_url()."index.php/peticiones_pendientes_sat/consultar_solicitudes";
		$data["url_consultar_solicitudes"] = $url_consultar_solicitudes;

		$url_agregar_solicitud_descarga = base_url()."index.php/peticiones_pendientes_sat/agregar_solicitud_descarga";
		$data["url_agregar_solicitud_descarga"] = $url_agregar_solicitud_descarga;

		if ( $this->session->flashdata('titulo') != null ) {
			$data["titulo"]       = $this->session->flashdata('titulo');
			$data["mensaje"]      = $this->session->flashdata('mensaje');
			$data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
		}
		
		// se obtiene la lista de RFCs emisores
		$arr_emisores = Model\C_entidades::all();
		$data["arr_emisores"] = $arr_emisores;
		
		$url_anterior = base_url()."index.php/".URL_PANTALLA_PRINCIPAL;
		$data["url_anterior"] = $url_anterior;

		$url_consultar_solicitudes = base_url()."index.php/peticiones_pendientes_sat/consultar_solicitudes";
		$data["url_consultar_solicitudes"] = $url_consultar_solicitudes;

		// se obtienen las ultimas 100 solicitudes con respuesta
        $this->db->order_by("id desc");
        $this->db->limit(100);
		$this->db->where("codigo is not null and codigo <>''");
		$arr_solicitudes = Model\Peticiones_pendientes_sat::all();
		$data["arr_solicitudes"] = $arr_solicitudes;
		
		// se arma la vista de asignatura
		cargar_interfaz_grafica($this, $data, "peticiones_pendientes_sat/view_content_wrapper_peticiones_pendientes_sat", null);
		
	}
    
    
    // ventana para generar solicitudes de descarga del SAT
    public function agregar_solicitud_descarga() {
		$data = array();
		// se obtienen los datos del usuario
		$pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
		$data["pss_usuario"] = $pss_usuario;
		
		$url_alta_solicitud = base_url()."index.php/peticiones_pendientes_sat/alta_solicitud";
		$data["url_alta_solicitud"] = $url_alta_solicitud;

		$url_anterior = base_url()."index.php/peticiones_pendientes_sat/index";
		$data["url_anterior"] = $url_anterior;

		if ( $this->session->flashdata('titulo') != null ) {
			$data["titulo"]       = $this->session->flashdata('titulo');
			$data["mensaje"]      = $this->session->flashdata('mensaje');
			$data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
		}
		
		// se obtiene la lista de RFCs emisores
		$arr_emisores = Model\C_entidades::all();
		$data["arr_emisores"] = $arr_emisores;
		
		// se arma la vista de asignatura
		cargar_interfaz_grafica($this, $data, "peticiones_pendientes_sat/view_content_wrapper_agregar_solicitud_descarga", null);
    }
	
	public function consultar_solicitudes() 
	{
		//$this->output->enable_profiler(TRUE);
		// se obtienen los parametros de busqueda
		$emisor_rfc       = $this->input->post("emisor_rfc");
		$receptor_rfc     = $this->input->post("receptor_rfc");
		$fecha_inicial    = $this->input->post("fecha_inicial");
		$fecha_final      = $this->input->post("fecha_final");
        //$fecha_solicitud  = $this->input->post("fecha_solicitud");
		$uuid             = $this->input->post("uuid");
        $estatus_solicitud = $this->input->post("estatus_solicitud");
		
		// boton que se presiono
		$boton_presionado = $this->input->post("btn_procesar_solicitud");
		
		$data = array();
		
		$url_consultar_solicitudes = base_url()."index.php/peticiones_pendientes_sat/consultar_solicitudes";
		$data["url_consultar_solicitudes"] = $url_consultar_solicitudes;

		$url_agregar_solicitud_descarga = base_url()."index.php/peticiones_pendientes_sat/agregar_solicitud_descarga";
		$data["url_agregar_solicitud_descarga"] = $url_agregar_solicitud_descarga;
        

        // es consulta filtrada
        $this->db->where("emisor_rfc",$emisor_rfc);
        
        // si mandaron rfc receptor
        if ( $receptor_rfc != null && $receptor_rfc != "" ) {
            $this->db->where("receptor_rfc",$receptor_rfc);
        }
        
        // fechas
        /*
        $this->db->where("fecha_inicial>=",$fecha_inicial);
        $this->db->where("fecha_final<=",$fecha_final);
        */
        
        // si se obtuvo estatus solicitud
        if ( $estatus_solicitud != null && $estatus_solicitud != "" ) {
            if ( $estatus_solicitud == 1 ) {
                $this->db->where("codigo", $estatus_solicitud);
            } else {
                $this->db->where("(codigo <> 1 or codigo is null)");
            }
            
        }
        
        $this->db->order_by("id desc");
        $this->db->limit(100);


		$arr_solicitudes = Model\Peticiones_pendientes_sat::all();
		$data["arr_solicitudes"] = $arr_solicitudes;
				
		// si mandaron 
		
		$arr_emisores = Model\C_entidades::all();
		$data["arr_emisores"] = $arr_emisores;

		
		if ( $this->session->flashdata('titulo') != null ) {
			$data["titulo"]       = $this->session->flashdata('titulo');
			$data["mensaje"]      = $this->session->flashdata('mensaje');
			$data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
		}
		
		// se arma la vista de asignatura
		cargar_interfaz_grafica($this, $data, "peticiones_pendientes_sat/view_content_wrapper_peticiones_pendientes_sat", null);

	}

	
	public function alta_solicitud() {
        
		$emisor_rfc     = $this->input->post("emisor_rfc");
		$receptor_rfc   = $this->input->post("receptor_rfc");
		$fecha_inicial  = $this->input->post("fecha_inicial");
		$fecha_final    = $this->input->post("fecha_final");
		$hora_inicial   = $this->input->post("hora_inicial");
		$hora_final     = $this->input->post("hora_final");

        
		$data = array();
		
		// se inserta la solicitud
		if ( $fecha_inicial == "" ) {
			$fecha_inicial = null;
		}
        
		if ( $fecha_final == "" ) {
			$fecha_final = null;
		}
		
        // se asume que no hay errores
        $hubo_error = false;
        
        // la fecha final no puede ser inferior a la fecha inicial
        if ( $fecha_final < $fecha_inicial ) {
            $this->session->set_flashdata('titulo', "Solicitud de descarga");
            $this->session->set_flashdata('mensaje', "La fecha final no puede ser menor a la fecha inicial");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
            
            $hubo_error = true;
        }
        
        if ( $hubo_error ) {
		    $url_login = base_url()."index.php/peticiones_pendientes_sat/agregar_solicitud_descarga";
            redirect($url_login);
        } else {
		    $nueva_solicitud = new Model\Peticiones_pendientes_sat();
		    //llama al modelo y su funcion
		    $nueva_solicitud->emisor_rfc      = $emisor_rfc   ;
		    $nueva_solicitud->receptor_rfc    = $receptor_rfc ;
		    $nueva_solicitud->fecha_inicial   = $fecha_inicial." ".$hora_inicial;
		    $nueva_solicitud->fecha_final     = $fecha_final." ".$hora_final;
		    $nueva_solicitud->uuid            = null; // el uuid es la que enrega el SAT como acuse de recepcion de solicitud. No se guarda
            //$nueva_solicitud->fecha_solicitud = date("Y-m-d H:i:s");
		    $insercion_correcta = $nueva_solicitud->save();
		    
            if ( $insercion_correcta ) {
                $this->session->set_flashdata('titulo', "Solicitud de descarga");
                $this->session->set_flashdata('mensaje', "Se ha registrado una solicitud de descarga correctamente");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
		        
		        $url_login = base_url()."index.php/peticiones_pendientes_sat/index";
                redirect($url_login);
                
            } else {
                
                $this->session->set_flashdata('titulo', "Solicitud de descarga");
                $this->session->set_flashdata('mensaje', "Ocurrió un error al registrar la solicitud de descarga. Por favor intente más tarde nuevamente.");
                $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
                
		        $url_login = base_url()."index.php/peticiones_pendientes_sat/agregar_solicitud_descarga";
                redirect($url_login);
            }
        }
        


	}

	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conteo extends CI_Controller {

 function __construct() {
        parent::__construct();
        
        // si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
        $existe_sesion = $this->session->userdata("id_usuario");
        if ( empty( $existe_sesion ) ) {
            redirect(site_url(),'refresh');
        }
    }
    /**
     * Inicio de sesion
     *
     */
    public function index() {
        
        //$this->output->enable_profiler(TRUE);
        // se creal el arreglo para paso de parametros
        $data = array();
        
        // se obtienen los datos del usuario
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;

        $pss_con_consulta_conteo = Model\Pss_con_consulta_conteo::all();
        $data["pss_con_consulta_conteo"] = $pss_con_consulta_conteo;

        $config_portal = Model\Pss_config_portal::find(1, false);
        $data["config_portal"] = $config_portal;
        
        // se obtiene la relacion de clientes asociados al usuario
        $arr_r_usuario_cliente = Model\Pss_r_usuario_cliente::find_by_id_usuario($pss_usuario->id_usuario_pss);
       
        // url para el controlador de validacion de inicio de sesion
        $url_editar_cuenta = base_url("index.php/mi_perfil/editar_datos_cuenta");
        $url_editar_datos_fiscales = base_url("index.php/mi_perfil/editar_datos_fiscales");
        
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;
        
        $config_portal = Model\Pss_config_portal::find(1, false);
        $data["config_portal"] = $config_portal;
        
        // si el portal no esta configurado para usar el RFC como login
        if ( $config_portal->usar_contrasena != CONTROL_ACCESO_SOLO_LOGIN_RFC ) {
            // pregunta de recuperacion usada
            $pregunta_recuperacion = Model\C_preguntas_recuperacion::find($pss_usuario->id_pregunta_recuperacion, false);
            $data["pregunta_recuperacion"] = $pregunta_recuperacion->pregunta;
        }

        $url_busqueda_razon = base_url("index.php/Conteo/busqueda_familia");
        $data["url_busqueda_razon"] = $url_busqueda_razon;   
        // url para recuperar la contrasena
        $url_anterior = base_url("index.php/".URL_PANTALLA_PRINCIPAL);
        $url_upload_file = base_url("index.php/Conteo/upload_file");
        
        // se transfieren los parametros al arreglo
        $data["url_editar_cuenta"]           = $url_editar_cuenta;
        $data["url_editar_datos_fiscales"]   = $url_editar_datos_fiscales;
        $data["url_anterior"]                = $url_anterior;
        $data["url_upload_file"]             = $url_upload_file;       
       
        $familias_array = Model\pss_cont_familias::find_by_id_cliente_ebs($pss_usuario->id_usuario_pss);

        $e=1;
        $familia_query="";
        if($familias_array!=null){
        foreach($familias_array as $familia){
            if($familia!=null){
                /*$arr_familas[$e]=$familia->rfc;
                $e++;*/
                if($e==1){
                    $familia_query .="'".$familia->rfc."'";
                }else{
                    $familia_query .=",'".$familia->rfc."'";
                }
                $e++;
            }
        } 

        $i=1;
        $conteo = $this->db->query("SELECT * FROM pss_cont_detalle WHERE rfc in (".$familia_query.")");
        //$conteo = Model\Pss_cont_detalle::find_by_rfc($familia_query);
        $arr_conteo=null;
        
        foreach ($conteo->result() as $contador) {
            if ( $contador != null ) {
                // se agrega el cliente al arreglo de clientes
                $arr_conteo[$i] = $contador;
                $i++;
            }
        }
        $data["datos_contador"] = $arr_conteo;
        }else{
            $data["datos_contador"] = null;
        }


        if ( $this->session->flashdata('titulo') != null ) {
          $data["titulo"]       = $this->session->flashdata('titulo');
          $data["mensaje"]      = $this->session->flashdata('mensaje');
          $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
        }
        cargar_interfaz_grafica($this, $data, 'conteo/view_content_wrapper_datos_conteo', null);

    }
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Archivos extends CI_Controller {

 function __construct() {
        parent::__construct();
        
        // si la sesion ya expiro, entonces se envia a la pagina de inicio para hacer login nuevamente
        $existe_sesion = $this->session->userdata("id_usuario");
        if ( empty( $existe_sesion ) ) {
            redirect(site_url(),'refresh');
        }
    }
    /**
     * Inicio de sesion
     *
     */
    public function index() {
        
        //$this->output->enable_profiler(TRUE);
        // se creal el arreglo para paso de parametros
        $data = array();
        
        // se obtienen los datos del usuario
        $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;
        
        $config_portal = Model\Pss_config_portal::find(1, false);
        $data["config_portal"] = $config_portal;
        
        // se obtiene la relacion de clientes asociados al usuario
        $arr_r_usuario_cliente = Model\Pss_r_usuario_cliente::find_by_id_usuario($pss_usuario->id_usuario_pss);
       
        // url para el controlador de validacion de inicio de sesion
        $url_editar_cuenta = base_url("index.php/mi_perfil/editar_datos_cuenta");
        $url_editar_datos_fiscales = base_url("index.php/mi_perfil/editar_datos_fiscales");
         $pss_usuario = Model\Pss_usuario::find($this->session->userdata("id_usuario"), false);
        $data["pss_usuario"] = $pss_usuario;
        
        $config_portal = Model\Pss_config_portal::find(1, false);
        $data["config_portal"] = $config_portal;
        
        // si el portal no esta configurado para usar el RFC como login
        if ( $config_portal->usar_contrasena != CONTROL_ACCESO_SOLO_LOGIN_RFC ) {
            // pregunta de recuperacion usada
            $pregunta_recuperacion = Model\C_preguntas_recuperacion::find($pss_usuario->id_pregunta_recuperacion, false);
            $data["pregunta_recuperacion"] = $pregunta_recuperacion->pregunta;
        }

        $url_busqueda_razon = base_url("index.php/Conteo/busqueda_familia");
        $data["url_busqueda_razon"] = $url_busqueda_razon;   
        // url para recuperar la contrasena
        $url_anterior = base_url("index.php/".URL_PANTALLA_PRINCIPAL);
        $url_upload_file = base_url("index.php/Conteo/upload_file");
        
        // se transfieren los parametros al arreglo
        $data["url_editar_cuenta"]           = $url_editar_cuenta;
        $data["url_editar_datos_fiscales"]   = $url_editar_datos_fiscales;
        $data["url_anterior"]                = $url_anterior;
        $data["url_upload_file"]             = $url_upload_file;

       
        //$familias_array = $this->db->query("SELECT * FROM pss_cont_familias WHERE id_cliente_ebs=".$pss_usuario->id_usuario_pss);
        $familias_array = Model\pss_cont_familias::find_by_id_cliente_ebs($pss_usuario->id_usuario_pss);
        //$data["familias_array"]  = $familias_array->row();
        $e=1;
        $familia_query="";
        foreach($familias_array as $familia){
            if($familia!=null){
                /*$arr_familas[$e]=$familia->rfc;
                $e++;*/
                if($e==1){
                    $familia_query .="'".$familia->rfc."'";
                }else{
                    $familia_query .=",'".$familia->rfc."'";
                }
                $e++;
            }
        } 
        $folder = FCPATH."uploads/";
        $directorio = opendir($folder); //ruta actual
        $e=1;
        $arr_archivos=null;
        while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
        {
            $arr_archivos[$e]=$archivo;
            $e++;
           
        }
        $data["arr_archivos"]=$arr_archivos;

        if ( $this->session->flashdata('titulo') != null ) {
          $data["titulo"]       = $this->session->flashdata('titulo');
          $data["mensaje"]      = $this->session->flashdata('mensaje');
          $data["tipo_mensaje"] = $this->session->flashdata('tipo_mensaje');
        }
        cargar_interfaz_grafica($this, $data, 'archivo/view_content_wrapper_datos_archivos', null);

    }
    
    public function do_download($file){
        $path = FCPATH."uploads/";
        header("Content-disposition: attachment; filename=".$file);
        header("Content-type: application/octet-stream");
        readfile($path.$file);
        $url_archivo = base_url()."index.php/".URL_ARCHIVOS;
        redirect($url_archivo);
    }

    public function do_upload(){
        /************************************/
        $path = FCPATH."uploads/";
        $config['upload_path']    = $path;
        if ( !file_exists($path) ) {
            // se crea si no existe
            mkdir($path, 0755, true);
        }
        $config['allowed_types']  = 'txt';
        $config['remove_spaces']  = TRUE;
        $config['max_size']       = '1000000';
        $this->load->library('upload', $config);
       
        if (!$this->upload->do_upload()){
            /*Por si no lo guarda bien*/
            $this->session->set_flashdata('titulo', "Error al subir el archivo");
            $this->session->set_flashdata('mensaje', "Por favor intente volver a subir el archivo");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-danger alert-dismissible');
            $url_archivo = base_url()."index.php/".URL_ARCHIVOS;
            redirect($url_archivo);
        }else{
            //Extrae el nombre del archivo subido
            $archivoDato = $this->upload->data();
            //Se abre el archivo con la ruta especifica
            $file = fopen($path.$archivoDato['file_name'],"r");
            //Inicio de bucle para extraer los datos del txt
            $datos = "INSERT INTO pss_cont_detalle VALUES ";
            $contador=1;
            while(!feof($file)){
                $cadena = fgets($file);
                $array = explode("|", $cadena);
                if($contador==1){
                    $datos .= "(null,1301, '".
                    $array[1]."', '".
                    $array[2]."', '".
                    $array[3]."', '".
                    $array[4]."', '".
                    $array[5]."')";
                }else{
                    $datos .= ",(null,1301, '".
                    $array[1]."', '".
                    $array[2]."', '".
                    $array[3]."', '".
                    $array[4]."', '".
                    $array[5]."')";
                }
                $contador++;
            }   
            
            $this->db->query($datos);
            //Fin del bucle
            //se cierra el archivo
            fclose($file);
            $this->session->set_flashdata('titulo', "Se cargo el archivo ");
            $this->session->set_flashdata('mensaje', "Se han cargado un total de ".$contador." de datos correctamente");
            $this->session->set_flashdata('tipo_mensaje', 'alert alert-success alert-dismissible');
            $url_archivo = base_url()."index.php/".URL_ARCHIVOS;
            redirect($url_archivo);
        }
    }
}
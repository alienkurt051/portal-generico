<?php

$lang['db_connection_error']       = 'No se puede conectar a la base de datos con este grupo de conexion o cadena dsn : %s';
$lang['models_not_found']          = 'No se pueden encontrar los modelos en la ruta especificada: %s';
$lang['models_found_no_relations'] = 'Modelo %s localizado, pero no se encuentran las propiedades de relacion.';
$lang['extensions_not_found']      = 'No se pueden encontrar las extensiones en la ruta especificada: %s';
$lang['empty_arguments']           = 'No se puede continuar ejecutando %s sin pasar algun parametro.';
$lang['cannot_create_model']       = 'Gas ORM no puede crear el fichero para el/los modelo/s: %s';
$lang['cannot_create_migration']   = 'Gas ORM no se puede crear un fichero para la migracion: %s';
$lang['migration_no_setting']      = 'Gas ORM auto-migracion fue detenida porque no se ha encontrado la configuracion.';
$lang['migration_no_dir']          = 'Gas ORM auto-migracion fue detenida porque no se ha encontrado un directorio valido..';
$lang['migration_no_initial']      = 'Gas ORM auto-migracion fue detenida porque la version de migracion es'
                                    .' superior a \'0\'.';
$lang['migration_disabled']        = 'Gas ORM auto-migracion fue detenida porque la biblioteca de migracion esta'
                                    .' deshabilitada.';
$lang['both_auto_error']           = 'Gas ORM no puede ejecutar la auto creacion de tablas y modelos al mismo tiempo,'
                                    .' desactiva alguna de las opciones.';

$lang['auto_check']                = 'El campo %s no es valido para el auto-incremento.';
$lang['char_check']                = 'El campo %s no es valido para tipos char.';
$lang['date_check']                = 'El campo %s no es valido para tipos datetime.';



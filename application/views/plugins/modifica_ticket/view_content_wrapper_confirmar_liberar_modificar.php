  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edición/liberación de tickets
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Edición/liberación de tickets</a></li>
        <li class="active">Confirmar operación</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
 
      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Confirme los datos de su transacción</h3>
          
        </div>
		<?php 
		// liberar ticket
		if ( $operacion == 1 ) {
		    echo '<form id="form_editar_liberar_ticket" method="post" action="'.$url_liberar_ticket.'">';
		} else {
			// editar ticket
			echo '<form id="form_editar_liberar_ticket" method="post" action="'.$url_modificar_ticket.'">';
		}
		?>
		<input type="hidden" name="conteo_lineas" id="conteo_lineas" value="<?php echo count($conceptos); ?>">
		<input type="hidden" name="lineas_activas" id="lineas_activas" value="<?php echo count($conceptos); ?>">
        <input type="hidden" name="id_trx33" id="id_trx33" value="<?php echo $transaccion->id_trx33_r; ?>">
        <div class="box-body">
        
                <div class="row">
                  <div class="col-md-12">
                        <?php
                        if ( isset($titulo) ) {
                        ?>
                          <div class="<?php echo $tipo_mensaje; ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                            <?php echo $mensaje; ?>
                          </div>
                        <?php
                        }
                        ?>
                  </div>
                </div>
        
          <div class="row">
            <div class="col-md-12">          

             <div class="form-group">
                <div class="row">
                  <div class="col-md-12">
                    <h4>Condiciones de pago y datos del encabezado</h4>
                  </div>
                </div>
              </div>

             <div class="form-group">
                <div class="row">

                      <div class="col-md-2">
                        <label class="control-label">Método de pago</label>
                      </div>
                      <div class="col-md-4">
                        <?php
                          
                          echo '<input style="width:260px" type="text" name="metodo_pago" id="metodo_pago" class="form-control" value="';
                          
                            echo $transaccion->id_metodo_pago;
                          echo '" readonly>';
                        ?>
                      </div>
                    

                      <div class="col-md-2">
                        <label class="control-label">Forma de pago</label>
                      </div>
                      <div class="col-md-4">
                        <input style="width: 260px" type="text" name="forma_pago" id="forma_pago" class="form-control" <?php
                        
                                echo 'value ="'.$transaccion->id_forma_pago.'"';
                            
                          ?> readonly>
                      </div>
                    </div>
                  </div>

             <div class="form-group">
                <div class="row">
                      <div class="col-md-2">

                        
                        <label class="control-label">Uso de CFDi</label>
                      </div>
                      <div class="col-md-10">
                        <?php echo $transaccion->uso_cfdi; ?>
                      </div>
                    


                </div>
              </div>
              
             <div class="form-group">
                <div class="row">
                   <div class="col-md-2">
                     <label class="control-label">Moneda</label>
                   </div>
                   <div class="col-sm-2">
                     <input type="text" value="<?php echo $transaccion->id_moneda; ?>" class="form-control" readonly>
                   </div>
                   <div class="col-md-2">
                     <label class="control-label">Tipo cambio</label>
                   </div>
                   <div class="col-sm-2">
                     <input type="text" value="<?php echo $transaccion->tipo_cambio; ?>" class="form-control" readonly>
                   </div>
                </div>
              </div>
              
             <div class="form-group">
                <div class="row">
                  <div class="col-md-12">
                    <h4>Conceptos</h4>
                  </div>
                </div>
              </div>
              
             <table class="table table-hover table-stripped table-condensed">
               <tr>
                 <th>#</th>
                 <th>CveProdServ<br>Num.Ident.</th>
                 <th width="5%">Cantidad</th>
				 <th width="5%">Cantidad<br>nueva</th>
                 <th>Unidad</th>
                 <th>Descripción</th>
                 <th width="8%">P.U.</th>
                 <th width="8%">Importe</th>
				 <th width="8%">Importe<br>nuevo</th>
                 <th>Descuento</th>
                 <?php
                 // si se puede modificar ticket se agregan botones
                 if ( $operacion == 2) {
                     echo '<th>Opciones<br>conceptos</th>';
                     echo '<th>Borrar línea</th>';
                 }
                 ?>
               </tr>
               <?php
               $cont = 1;
               foreach ($conceptos as $concepto) {
                   $id_linea = "linea".$cont;
                   echo '<tr id="'.$id_linea.'">';
                   $numero_linea = "numero_linea".$cont;
                   echo '<td>'.$cont.'<input type="hidden" name="'.$numero_linea.'" id="'.$numero_linea.'" value="'.$concepto->id_trx33_concepto_r.'"></td>';
                   echo '<td>'.$concepto->id_claveprodserv.'<br>'.$concepto->numero_identificacion.'</td>';
                   
                   $id_cantidad = "cantidad".$cont;
                   echo '<td class="text-center">'.$concepto->cantidad.'</td>';
				   echo '<td><input type="text" id="'.$id_cantidad.'" name="'.$id_cantidad.'" value="'.$concepto->cantidad.'" class="form-control text-center" readonly></td>';
                   echo '<td>'.$concepto->id_clave_unidad.'<br>'.$concepto->unidad.'</td>';
                   echo '<td>'.$concepto->descripcion.'</td>';
                   
                   $id_precio_unitario = "precio_unitario".$cont;
                   //echo '<td><span class="pull-right">'.$concepto->valor_unitario.'</span></td>';
                   echo '<td><input type="text" id="'.$id_precio_unitario.'" name="'.$id_precio_unitario.'" value="'.$concepto->valor_unitario.'" class="form-control text-right" readonly></td>';
                   //echo '<td>'.number_format($concepto->valor_unitario,2).'</td>';
                   
                   $id_importe = "importe".$cont;
				   echo '<td class="text-right">'.$concepto->importe.'</td>';
                   echo '<td><input type="text" id="'.$id_importe.'" name="'.$id_importe.'" value="'.$concepto->importe.'" class="form-control text-right" readonly></td>';
                   //echo '<td>'.number_format($concepto->importe,2).'</td>';
                   echo '<td>'.number_format($concepto->descuento,2).'</td>';
                   
                   // si se puede modificar ticket se agregan botones
                   if ( $operacion == 2) {
                       $on_click_cantidad_mas   = "agregar_eliminar_uno('".$id_cantidad."','".$id_precio_unitario."','".$id_importe."',  1,".$concepto->cantidad.",".$cont.");";
                       $on_click_cantidad_menos = "agregar_eliminar_uno('".$id_cantidad."','".$id_precio_unitario."','".$id_importe."', -1,".$concepto->cantidad.",".$cont.");";
                       echo '<td><a href="javascript:void(0)" onclick="'.$on_click_cantidad_mas.'" class="btn btn-default" title="Agregar uno">+1</a> <a href="javascript:void(0)" onclick="'.$on_click_cantidad_menos.'" class="btn btn-default" title="Reducir uno">-1</a></td>';
                       //echo '<td><a href="javascript:void(0)" onclick="'.$on_click_cantidad_menos.'" class="btn btn-default" title="Reducir uno">-1</a></td>';
                       
                       $id_estatus_linea = "id_estatus_linea".$cont;
					   $id_boton_borrar = "id_boton_borrar".$cont;
					   $eliminar_linea = "eliminar_linea('".$id_estatus_linea."','".$id_boton_borrar."',".$concepto->importe.",'".$id_cantidad."',".$concepto->cantidad.")";
                       echo '<td><input type="hidden" id="'.$id_estatus_linea.'" name='.$id_estatus_linea.' value="1"><button id="'.$id_boton_borrar.'" onclick="'.$eliminar_linea.'" class="btn btn-danger" type="button">Borrar línea</button></td>';
                   }
                   
                   echo '</tr>';
                   
                   $cont++;
               }
               ?>

             </table>
             <hr>
             <div class="form-group">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="col-sm-6 control-label">Subtotal</label>
                      <div class="col-sm-6">
                        <input type="text" id="id_subtotal" value="<?php echo $transaccion->subtotal; ?>" class="form-control text-right" readonly>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
             <div class="form-group">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="col-sm-6 control-label">Descuento</label>
                      <div class="col-sm-6">
                        <input type="text" value="<?php echo number_format($transaccion->descuento,2); ?>" class="form-control text-right" readonly>
                      </div>
                    
                    </div>
                  </div>
                </div>
              </div>
             <div class="form-group">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="col-sm-6 control-label">Impuestos retenidos</label>
                      <div class="col-sm-6">
                        <input type="text" id="id_impuestos_retenidos" value="<?php echo $transaccion->totalImpuestosRetenidos; ?>" class="form-control text-right" readonly>
                      </div>
                    
                    </div>
                  </div>
                </div>
              </div>
             <div class="form-group">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="col-sm-6 control-label">Impuestos trasladados</label>
                      <div class="col-sm-6">
                        <input type="text" id="id_impuestos_trasladados" value="<?php echo $transaccion->totalImpuestosTrasladados; ?>" class="form-control text-right" readonly>
                      </div>
                    
                    </div>
                  </div>
                </div>
              </div>
             <div class="form-group">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="col-sm-6 control-label">Total</label>
                      <div class="col-sm-6">
                        <input type="text" id="id_total" value="<?php echo $transaccion->total; ?>" class="form-control text-right" readonly>
                      </div>
                    
                    </div>
                  </div>
                </div>
              </div>
            
            </div>
           

          </div>
        </div>
		
        <!-- /.box-body -->
        <div class="box-footer">
		  <?php 
		  // si el ticket fue cancelado con antelacion
		  if ( $transaccion->id_receptor == -1 ) {
		  ?>
		  <div class="row">
		    <div class="col-md-12">
			  <span class="bg-red">ATENCIÓN: -- El ticket ha sido cancelado con anterioridad. Puede usar el botón liberar para poder usarlo nuevamente --</span>
			</div>
		  </div>
		  <?php
		  }
		  ?>
		  <div class="row">
		    <div class="col-md-12">
          <?php
          if ( $operacion == 1) {
              // si se puede liberar
              $url_liberar_ticket .= "/".$transaccion->id_trx33_r;
              echo '<input class="btn btn-success" type="submit" value="Liberar ticket"/>';
          } else {
			  // url para cancelar ticket
			  
			  
              // si se puede modificar
              $url_modificar_ticket .= "/".$transaccion->id_trx33_r;
			  echo '<input class="btn btn-success" type="submit" value="Modificar ticket"/>';
			  echo '<span class="pull-right"> Si desea cancelar el ticket haga clic en el siguiente botón <a href="'.$url_cancelar_ticket.'" class="btn btn-danger"><i class="fa fa-eject"></i> Cancelar ticket</a></span> ';
          }
          ?>
              <a href="<?php echo $url_anterior; ?>" class="btn btn-warning">Regresar</a>
			</div>
		  </div>

        </div>
        <!-- /.box-footer-->
		</form>
		
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
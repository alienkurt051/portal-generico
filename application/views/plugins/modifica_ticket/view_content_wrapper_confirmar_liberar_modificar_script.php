<script>

$(document).ready(function() {


});

function agregar_eliminar_uno(id_cantidad, id_precio_unitario, id_importe, cantidad, cantidad_original, contador) {
    var id_cantidad_elegido       = "#" + id_cantidad;
    var id_preciounitario_elegido = "#" + id_precio_unitario;
    var id_importe_elegido        = "#" + id_importe;
	var id_estatus_linea          = "#id_estatus_linea" + contador;
    
    var cantidad_obtenida        = parseInt($(id_cantidad_elegido).val());
    var precio_unitario_obtenido = parseFloat($(id_preciounitario_elegido).val());
    var importe_obtenido         = $(id_importe_elegido).val();
	var estatus_linea            = $(id_estatus_linea).val();
    
	// si esta eliminada no se puede modificar la cantidad
	if ( estatus_linea == 0 ) {
        alert("La línea ha sido marcada para ser eliminada. No se puede modificar las cantidades.");
        return;		
	}
	
    // no se puede restar cuando es 1, se debe eliminar la linea
    if ( cantidad_obtenida == "1" && cantidad == -1 ) {
        alert("Se ha alcanzado la cantidad mínima. Para continuar elimina la línea");
        return;
    } 
	
	// no se pueden agregar mas de los que habia
    if ( cantidad_obtenida == cantidad_original && cantidad == 1 ) {
        alert("No se puede incrementar la cantidad del concepto por encima de la cantidad original");
        return;			
    }
    
    cantidad_obtenida = parseInt(cantidad_obtenida) + parseInt(cantidad);
    importe_obtenido  = cantidad_obtenida * precio_unitario_obtenido;
    
    // actualiza cantidad e importe
    $(id_cantidad_elegido).val(cantidad_obtenida);
    $(id_importe_elegido).val(importe_obtenido);
	
	// se recalcula subtotal, impuestos y totales
	var subtotal = parseFloat($("#id_subtotal").val());
	var iva      = parseFloat($("#id_impuestos_trasladados").val());
	var total    = parseFloat($("#id_total").val());
	
    // se resta	
	if ( cantidad == -1 ) {
		subtotal = subtotal - precio_unitario_obtenido;
	} else {
		// se suma
		subtotal = subtotal + precio_unitario_obtenido;
	}		
    
	iva = subtotal * 0.16;
	total = subtotal + iva;

	
	$("#id_subtotal").val(subtotal);
	$("#id_impuestos_trasladados").val(iva);
	$("#id_total").val(total);

}

function eliminar_linea(id_estatus_linea, id_boton_borrar, importe, id_cantidad, cantidad_original) {
	
	// si solo hay una linea no se puede eliminar
	var conteo_lineas = $("#conteo_lineas").val();
	var lineas_activas = $("#lineas_activas").val();
	
    var id_estatus_linea    = "#" + id_estatus_linea;
	var estatus             = $(id_estatus_linea).val();
	var id_cantidad_elegido = "#" + id_cantidad;

    var boton_borrar = "#" + id_boton_borrar;
	var cantidad_obtenida = $(id_cantidad_elegido).val();
	
	// si cantidad y cantidad original no son iguales, no se puede borrar
	if ( ( cantidad_obtenida != cantidad_original ) && estatus == 1 ) {
		alert("No se puede borrar una línea editada. Para poder eliminar la  línea es necesario regresar la cantidad editada al número original");
		return;
	}
	
	// si se desea eliminar
	if ( estatus == 1 ) {
	    if ( conteo_lineas < 2 ) {
	    	alert("El ticket sólo tiene un concepto. Debe utilizar el botón Cancelar Ticket para eliminarlo");
	    	return;
	    }
	    
	    // si es la ultima linea y las demas estan borradas, tampoco se puede eliminar
	    if ( lineas_activas < 2 ) {
	    	alert("Sólo resta una línea de concepto activa, para eliminar todo el ticket, utilice el botón Cancelar Ticket");
	    	return;		
	    }
	}
	
	// se recalcula subtotal, impuestos y totales
	var subtotal = parseFloat($("#id_subtotal").val());
	var iva      = parseFloat($("#id_impuestos_trasladados").val());
	var total    = parseFloat($("#id_total").val());
	
	// si esta activa
	if ( estatus == 1 ) {
		subtotal = subtotal - parseFloat(importe);
		$(id_estatus_linea).val(0);
		$(boton_borrar).html('Activar');
		
		// se resta una linea activa
		lineas_activas--;
		
		$("#lineas_activas").val(lineas_activas);
	} else {
		subtotal = subtotal + parseFloat(importe);
		$(id_estatus_linea).val(1);
		$(boton_borrar).html('Borrar línea');
		
		// se agrega una linea activa
		lineas_activas++;
		
		$("#lineas_activas").val(lineas_activas);
	}
	
	iva = subtotal * 0.16;
	total = subtotal + iva;
	
	$("#id_subtotal").val(subtotal);
	$("#id_impuestos_trasladados").val(iva);
	$("#id_total").val(total);
}


</script>
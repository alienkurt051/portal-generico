<link rel="stylesheet" href="<?= base_url() . "assets/dist/css/busqueda.css" ?>">
<div id="loading">
    <div style="position: relative; left: -50%;font-size:80px;">
        <i class="fa fa-spinner fa-pulse"></i>
    </div>
</div>
<div class="box-body" id="resultados" style="display: none">        
    <div class="row" >
        <div class="col-md-4 col-lg-offset-8"> <strong>Total de registros encontrados: </strong><span id="total"></span></div>
    </div>
    <div class="row" >
    </div>  
    <div class="table-responsive">
        <table class="table table-hover table-condensed" width='100%' id='listaFactutas'>
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">RFC Emisor</th>
                    <th scope="col">Emisor</th>
                    <th scope="col">RFC Receptor</th>
                    <th scope="col">Receptor</th>
                    <th scope="col">Serie</th>
                    <th scope="col">Folio</th>
                    <th scope="col">UUID</th>                
                    <th scope="col">Fecha<br />timbrado</th>                
                    <th scope="col">Moneda</th>                
                    <th scope="col">Total</th>
                    <th scope="col">Acciones</th>

                </tr>
            </thead>
            <tbody></tbody>
        </table> 
    </div>
    <div class="text-center" style='margin-top: 10px;' id='pagination'></div>    
</div>

<!-- Modal para desplegar alerts -->
<div class="modal fade" id="no_resultados" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="alert-dismissible" style="display: block;">
            <div style="display: flow-root;">
                <button type="button" class="close" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remove"></span>
                </button>
            </div>
            <span class="fa fa-info-circle text-info" ></span>                                    
            <strong>No se obtuvieron resultados</strong><br/><p>No se encontraron resultados con los parámetros dados</p>
        </div>
    </div>
</div>   
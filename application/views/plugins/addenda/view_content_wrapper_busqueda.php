<!--Se reutilizan estilos que ya se cuentan para una búsqueda y alerts -->
<link rel="stylesheet" href="<?= base_url() . "assets/dist/css/busqueda.css" ?>">
<link rel="stylesheet" href="<?= base_url() . "assets/dist/css/plugins/addenda/edicion.css" ?>">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agregar addenda Postimbrado
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Addenda</a></li>
            <li class="active">Agregar addenda Postimbrado</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">                    
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Filtro de consulta</h3>
            </div>            
            <button id="searchIcon"type="submit" data-toggle="busqueda" data-placement="left" title="Nueva búsqueda" style="padding: 0px;"><i class="fa fa-search" ></i></button>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (isset($titulo)) {
                            ?>
                            <div class="<?php echo $tipo_mensaje; ?>">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                                <?php echo $mensaje; ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div> 
                <div class="container" style="width:auto;">
                    <form id="buscar_comprobantes" style="margin: 0px 10%;">
                        <div class="panel-heading">Emisor</div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="rfc_emisor">RFC</label>
                                <input class="form-control" name="rfc_emisor" id="rfc_emisor" type="text" placeholder="RFC">
                            </div> 
                            <div class="form-group col-md-8">
                                <label for="nombre_emisor">Nombre</label>
                                <input class="form-control" name="nombre_emisor" id="nombre_emisor" type="text" placeholder="Nombre">
                            </div>     
                        </div> 
                        <div class="panel-heading">Receptor</div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="rfc_receptor">RFC</label>
                                <input class="form-control" name="rfc_receptor" id="rfc_receptor" type="text" placeholder="RFC">
                            </div> 
                            <div class="form-group col-md-8">
                                <label for="nombre_receptor">Nombre</label>
                                <input class="form-control" name="nombre_receptor" id="nombre_receptor" type="text" placeholder="Nombre">
                            </div>     
                        </div> 
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="serie">Serie</label>
                                <input class="form-control" name="serie" id="serie" type="text" placeholder="Serie">
                            </div> 
                            <div class="form-group col-md-4">
                                <label for="folio_inicio">Folio inicio</label>
                                <input class="form-control" name="folio_inicio" id="folio_inicio" type="text" placeholder="Folio inicio">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="folio_fin">Folio fin</label>
                                <input class="form-control" name="folio_fin" id="folio_fin" type="text" placeholder="Folio fin">
                            </div>                             
                        </div>                         
                        <div class="form-row">                    
                            <div class="form-group col-md-2 col-md-offset-8">
                                <input id="clear"  type="button" value="Limpiar" class="btn btn-default" >
                            </div>                                     
                            <div class="form-group col-md-2">
                                <input id="search" type="submit" value="Buscar" class="btn btn-success pull-right" >
                            </div>                  
                        </div>
                    </form>
                </div>
                <div id="results">
                </div> 
                <!-- Modal para desplegar alerts -->
                <div class="modal fade" id="alert" role="dialog">
                    <div class="modal-dialog modal-sm">
                        <div class="alert-dismissible" style="display: block;">
                            <div style="display: flow-root;">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </div>
                            <span class="fa fa-warning text-warning" ></span>                                    
                            <strong>¡Atención!</strong><br/><p>Ingresar mínimo un campo de búsqueda</p>
                        </div>
                    </div>
                </div>                
                <!-- /.box-body -->
            </div>
        </div>       
    </section>
    <!-- /.content -->    
</div>
<!-- /.content-wrapper -->


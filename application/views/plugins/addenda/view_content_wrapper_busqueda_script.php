<script>

    $(document).ready(function () {
        
        $('[data-toggle="busqueda"]').tooltip();        
        
        let contenido       = $(".content");
        let searchIcon      = $("#searchIcon");
        let bodySearch      = $(".box-body");
        let form            = $('form');
        let clear           = $('#clear');
        let inputsForm      = $("form :input[type=text]");
        let folioInicio     = $("#folio_inicio");
        let folioFin        = $("#folio_fin");
        let urlConsulta     = '<?= base_url() ?>index.php/plugins/Addenda/cargar_resultados';
        
        /* Limpia el formulario cuando se presiona el botón */
        clear.click(function () {
            form.trigger("reset");
        });
        /* Valida folios */
        validaFolio();
        /* Evento cuando se requiere hacer una búsqueda */
        form.submit(function (e) {
            e.preventDefault();
            /* Si no se ha ingresado ningún dato en el formulario */
            if(!validaInputsVacios()){
                $('#alert').modal('show'); 
                return;
            }
            /* Se oculta el formulario y modifica algunos estilos */
            form.hide(500, function () {
                searchIcon.show(300);
                searchIcon.slideDown("slow");
                contenido.css("min-height", "auto");
                bodySearch.css("padding", "0px");
            });
            cargarBusqueda();
        });
        /* Evento que muestra nuevamente el formulario para hacer una búsqueda */
        searchIcon.click(function () {
            form.show(500, function () {
                /*Regresa los estilos a sus propiedades originales */
                searchIcon.hide(300);
                searchIcon.slideUp("slow");
                contenido.css("min-height", "250px");
                bodySearch.css("padding", "10px");

            });
        });
        /* Se obtienen todos los inputs del formulario y se verifican si 
         * al menos uno contiene un valor, de lo contrario se manda un false
         *   */
        function validaInputsVacios(){
            var estatus = false;
            inputsForm.each(function(){
                /* Si cada input contiene algún dato regresa true*/
                if($.trim($(this).val()).length !== 0 )
                    estatus = true;
            });   
            return estatus;
        }
        /* Valida que en el rango sea correcto */
        function validaFolio(){
            folioFin.prop('disabled', true);
            folioInicio.keyup(function(){                
                if($.trim($(this).val()).length !== 0){
                /* Sólo si el folio inicio contiene valor se activa el folio fin */                    
                    folioFin.prop('disabled', false);
                    return;
                }
                folioFin.prop('disabled', true);
                folioFin.val('');
            });
        }

        /* Función que manda llamar vía ajax la consulta */
        function cargarBusqueda() {
            let resultados = $("#results");
            let dataForm = recuperarDatosForm();
            $.ajax({
                url: urlConsulta,
                type: 'post',
                data : dataForm,
                dataType: 'html',
                success: function (response) {
                    resultados.html(response);                    
                }
            });
        }
        /* Recupera los datos del form y se quitan los espacios en blanco */
        function recuperarDatosForm(){        
            return  {
                rfc_emisor          : $.trim($("#rfc_emisor").val()),
                nombre_emisor       : $.trim($("#nombre_emisor").val()),
                rfc_receptor        : $.trim($("#rfc_receptor").val()),
                nombre_receptor     : $.trim($("#nombre_receptor").val()),
                serie               : $.trim($("#serie").val()),
                folio_inicio        : $.trim($("#folio_inicio").val()),
                folio_fin           : $.trim($("#folio_fin").val())      
            };
        }

});
</script>
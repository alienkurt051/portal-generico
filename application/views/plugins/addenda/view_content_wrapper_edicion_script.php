<script>

    $(document).ready(function () {
        /*Se obtiene url y id de documento a editar */
        let urlInfoDocumento    = '<?= base_url() ?>index.php/plugins/Addenda/informacion_documento/';
        let urlDireSucursal     = '<?= base_url() ?>index.php/plugins/Addenda/direccion_sucursal/';
        let urlInfoEncabezado   = '<?= base_url() ?>index.php/plugins/Addenda/informacion_encabezado_addenda/';
        let urlConceptos        = '<?= base_url() ?>index.php/plugins/Addenda/conceptos_documento/';
        let urlGenerarAddenda   = '<?= base_url() ?>index.php/plugins/Addenda/generar_addenda';
        let urlValidaAddenda    = '<?= base_url() ?>index.php/plugins/Addenda/validar_addenda';
        let idTrx33             = parseInt('<?= $id_trx33 ?>');
        let form                = $('form');
        let spinnerInfo         = $('#buscar_comprobantes ul:first-child');
        let spinnerEncabezado   = $('#buscar_comprobantes ul:eq(1)');
        let spinnerConceptos    = $('#buscar_comprobantes ul:eq(2)');
        let tablas              = $('.table-responsive');
        let encabezado          = $('#encabezado');        
        let search              = $('#search');        
        let mensajeAlert        = $('.alert-dismissible').first();        
        let loading             = $('#loading');        
        let alert               = $('#alert');        

        /* Servicios que se cargan cuando renderiza la vista */
        cargarDatos();
        /* Evento que se dispara cuando se intenta generar nodo addenda */
        form.submit(function (e) {
            e.preventDefault();
            /* Permite poder hacer dismiss al modal */
            alert.modal({ backdrop: 'static',keyboard: false, show: true});
            mensajeAlert.hide();
            loading.show();
            /* Recupera los datos y los envía en una petición ajax a validar */
            let dataForm = recuperarDatosForm(); 
            validarAddenda(dataForm.encabezado,dataForm);
        });        

        /* Función que manda llamar vía ajax la consulta de la información */
        function cargarInformacionDocumento() {
            /* Se obtienen las celdas donde se colocará la información */
            let celdas = $('#informacion_documento td');
            return $.ajax({
                url: urlInfoDocumento + idTrx33,
                type: 'post',
                dataType: 'json',
                success: function (response) {
                    /* Si la respuesta contiene valores */
                    if (response.length) {
                        var informacion = [];
                        /* Se obtienen los valores de la respuesta a un array */
                        $.each(response[0], function (key, value) {
                            informacion.push(value);
                        });
                        /* Se agregan los valores de la respuesta a la vista  */
                        $.each(celdas, function (key) {
                            $(this).append(informacion[key]);
                        });                          
                    }
                    /* Si no se muestra mensaje */
                }
            });
        }
        /* Función que manda llamar vía ajax la dirección de la sucursal */
        function cargarDireccionSucursal() {
            return $.ajax({
                url: urlDireSucursal + idTrx33,
                type: 'post',
                dataType: 'json',
                success: function (response) {
                    /* Si la respuesta contiene valores */
                    if (response) {
                        /* Se recorren los valores y se colocan en su respectivo 
                         * input
                         * */
                        $.each(response, function (key, value) {
                            $("#"+key).val(value);
                        });                         
                    }
                    /* Si no se muestra mensaje */
                }
            });
        }
        /* Función que manda llamar vía ajax los datos del encabezado de la addenda */
        function cargarInformacionEncabezado() {
            return $.ajax({
                url: urlInfoEncabezado + idTrx33,
                type: 'post',
                dataType: 'json',
                success: function (response) {
                    /* Si la respuesta contiene valores */
                    if (response) {
                        /* Se recorren los valores y se colocan en su respectivo 
                         * input
                         * */
                        $.each(response[0], function (key, value) {
                            $("#"+key).val(value);
                        });                        
                    }
                    /* Si no se muestra mensaje */
                }
            });
        }         
        /* Función que manda llamar vía ajax los datos del encabezado de la addenda */
        function cargarConceptos() {
            var tbody = $('#conceptos tbody');
            /* Se obtienen reglas de validación para campos editables en conceptos */
            let validacionesOrdenCom    = <?= $reglasOrdenCom ?>;
            let validacionesNumeroIde   = <?= $reglasNumeroIde ?>;
            return $.ajax({
                url: urlConceptos + idTrx33,
                type: 'post',
                dataType: 'json',
                success: function (response) {
                    /* Si la respuesta contiene valores */                  
                    if (response) {
                        /* Se recorren los valores y se colocan en su respectivo 
                         * input
                         * */
                        $.each(response, function (key, value) {
                            /* Se crea cada renglón */
                            let tr = document.createElement("tr");
                            tr.setAttribute("id",response[key].id_trx33_conceptos);
                            /* Se obtiene cada valor y se crea su celda en la tabla */
                            let numeroConcepto = crearCelda(key+1);
                            let claveProdServ = crearCelda(response[key].clave_prod_serv);
                            let numeroIdentificacion = crearNodoInput(response[key].numero_identificacion,validacionesNumeroIde.class,validacionesNumeroIde);
                            let unidad = crearCelda(response[key].unidad);
                            let cantidad = crearCelda(response[key].cantidad);
                            let descripcion = crearCelda(response[key].descripcion);
                            let valorUnitario = crearCelda(response[key].valor_unitario);
                            let importe = crearCelda(response[key].importe);                            
                            let ordenCompra = crearNodoInput(response[key].orden_compra,validacionesOrdenCom.class,validacionesOrdenCom);
                            let idTrx33Con = crearNodoInput(response[key].id_trx33_conceptos,"id_trx33_conceptos",{});
                            $(idTrx33Con).hide();
                            /* Se agrega cada celda al renglón */
                            tr.appendChild(numeroConcepto);
                            tr.appendChild(claveProdServ);
                            tr.appendChild(numeroIdentificacion);
                            tr.appendChild(unidad);
                            tr.appendChild(cantidad);
                            tr.appendChild(descripcion);
                            tr.appendChild(valorUnitario);
                            tr.appendChild(importe);
                            tr.appendChild(ordenCompra);
                            tr.appendChild(idTrx33Con);                            
                            /* Se agrega cada renglón al cuerpo de la tabla */
                            tbody.append(tr);
                        });                        
                    }
                    /* Si no se muestra mensaje */
                }
            });
        }
        /* Función que manda llamar vía ajax el servicio que genera la addenda */
        function agregarAddenda(dataForm) {
            $.ajax({
                url: urlGenerarAddenda,
                type: 'post',
                data : dataForm,
                dataType: 'html',
                success: function (response) {
                    /* Si se generó correctamente la addenda */
                    let clases = 'fa fa-check-circle-o text-success';    
                    let exito = '¡Agregado correctamente!';
                    let mensajeExito = 'Se ha editado correctamente la addenda del documento'; 
                    $('.modal-footer > a').show(); 
                    $('.modal-footer > button').hide(); 
                    colocarMensajeAlerta(exito,mensajeExito,clases);
                },
                error: function(xhr, textStatus){
                    /* Si existe algún error al generar la addenda */
                    let clases = 'fa fa-warning text-danger';
                    let fallo = '¡Ha ocurrido un error!';
                    let mensajeFallo = 'No se pudo generar correctamente la addenda del documento con ID: '+idTrx33;                    
                    colocarMensajeAlerta(fallo,mensajeFallo,clases);
                },                
                complete: function(){
                    /* Cuando termina la petición se oculta el gifs de loading */
                    loading.fadeOut("slow",function(){
                        mensajeAlert.show("fast");    
                    });                    
                }
            });
        }        
        function validarAddenda(encabezado,dataForm) {                                                             
            $.ajax({
                url: urlValidaAddenda,
                type: 'post',
                data : encabezado,
                dataType: 'json',
                success: function (response) {
                    if (!$.isEmptyObject(response.error)) {
                        /* Si existe algún error de VALIDACIÓN */
                        let clases = 'fa fa-warning text-danger';
                        let fallo = '¡Error de validación!';
                        let mensajeFallo = response.error;                    
                        colocarMensajeAlerta(fallo,mensajeFallo,clases);
                        /* Oculta loading, coloca mensaje y la coloca modal con la 
                         * propiedad que impide hacer dismiss */
                        loading.fadeOut("slow",function(){
                            mensajeAlert.show("fast");                          
                        });                         
                        return;
                    }
                    /* Si no existe nungún error de validacion se procede a 
                     * tratar de generar la addenda (vía ajax)
                     * */
                    agregarAddenda(dataForm);
                },
                error: function(xhr, textStatus){
                    /* Si existe algún error al generar la addenda */
                    let clases = 'fa fa-warning text-danger';
                    let fallo = '¡Ha ocurrido un error!';
                    let mensajeFallo = 'No se pudo generar correctamente la addenda del documento con ID: '+idTrx33;                    
                    colocarMensajeAlerta(fallo,mensajeFallo,clases);
                },                
                complete: function(){
                   
                }
            });                
        }        
        /* Crea elemento inputs que se agregará en la tabla de conceptos */
        function crearNodoInput(valor, clase, validaciones) {

            var td = document.createElement("td");
            var input = document.createElement("input");
            
            /* AGREGA VALIDACIONES PARA CAMPOS EDITABLES PARA CADA CONCEPTO */
            /* Atributos como patter y title*/
            $.each(validaciones.attributes,function(key, value){
                input.setAttribute(key, value);
            });
            /* Propiedades como required */
            $.each(validaciones.props,function(key, value){
                $(input).prop(key,value);
            });            
            
            input.setAttribute("class", "form-control "+clase);
            input.value = valor;
            td.appendChild(input);
            return td;
        }
        /* Función que crea cada celda para insertar en la tabla */
        function crearCelda(valor) {
            let td = document.createElement("td");
            td.innerHTML = valor;
            return td;
        }
        /* Obtiene los datos del formulario y los coloca en un json */
        function recuperarDatosForm(){
            var datosEnviar = {
                encabezado:{},
                conceptos:[],
                id_trx33:idTrx33
            };
            /* Se recorre cada input que se encuentra en los conceptos 
             * y se obtiene su identificador (id_trx33_conceptos) y su valor
             * */
            $("#conceptos tbody tr").each(function(){
                var concepto = {};
                /* Se obtienen los inputs de cada renglón */
                $(this).find(":input").each(function(){
                    /* La segunda clase se toma como la clave para cada concepto */
                    let key = $(this).attr("class").split(' ')[1];
                    let value = $(this).val();
                    concepto[key] = $.trim(value);
                });
                /* Se agrega cada concepto al array de conceptos de json */
                datosEnviar.conceptos.push(concepto);
            });
            /* Se recorre cada input que se encuentra en los datos de encabezado 
             * y se obtiene su identificador correspondiente y su valor
             * */            
            $("#encabezado :input").each(function(){
                var input   = $(this); 
                let id      = input.attr('id');
                let valor   = input.val();
                datosEnviar.encabezado[$.trim(id)] = $.trim(valor);               
            });    
            return datosEnviar;        
        }
        /* Oculta el spinner de carga y muestra el elemento que se le pasa*/
        function mostrarInformacionCargada(spinner,elemento){
            spinner.fadeOut(700,function() {
                elemento.show(800);                            
              });            
        }
        /* Precarga los datos separados en cuatro peticiones ajax */
        function cargarDatos(){
            search.prop('disabled', true);
            /* When recibe las peticiones asíncronas que se harán */
            $.when(
                    /* Se intenta preecargar datos relacionados con la addenda */
                    cargarInformacionDocumento(), 
                    cargarDireccionSucursal(),
                    cargarInformacionEncabezado(),
                    cargarConceptos()
            )/* Then se ejecuta si todas las peticiones ajax fueron exitosas */
            .then(function(){
                /* Se ocultan los spinners de carga y se muestran los contenidos */
                mostrarInformacionCargada(spinnerInfo, tablas.first());
                mostrarInformacionCargada(spinnerEncabezado, encabezado);
                mostrarInformacionCargada(spinnerConceptos, tablas.eq(1));
                search.prop('disabled', false);
            })/* Si algo sale mal con alguna petición se ejecuta fail */
            .fail( function(xhr, textStatus) {
                /* Muestra un mensaje de alerta y redirige */
                let clases = 'fa fa-warning text-warning';
                let fallo = '¡Ha ocurrido un error al precargar los datos!';
                let mensajeFallo = 'Por favor, intentarlo nuevamente, documento con ID: ' + idTrx33;                    
                colocarMensajeAlerta(fallo,mensajeFallo,clases);
                loading.hide();
                mensajeAlert.show("fast");
            });             
        }
        /* Configura el modal de alerta y coloca mensajes respectivos */
        function colocarMensajeAlerta(encabezado, mensaje, clases){
            /* Coloca mensajes y coloca clases para colores e iconos */
            mensajeAlert.children('strong').text(encabezado);
            mensajeAlert.children('p').empty()
                    .append(mensaje);                    
            mensajeAlert.children('span').removeClass()
                    .addClass(clases);        
        }


    });
</script>
<script>
    $(document).ready(function () {

        let loading         = $("#loading");
        let noResultados    = $("#no_resultados");
        let total           = $("#total");
        let tabla           = $("#resultados");
        let pagina          = $("#pagination");
        let body            = $("html, body");
        let regPorPagina    = '<?= $rows ?>';
        let datosBusqueda   = {
                                rfc_emisor          : '<?= $rfc_emisor      ?>',
                                nombre_emisor       : '<?= $nombre_emisor   ?>',
                                rfc_receptor        : '<?= $rfc_receptor    ?>',
                                nombre_receptor     : '<?= $nombre_receptor ?>',
                                serie               : '<?= $serie           ?>',
                                folio_inicio        : '<?= $folio_inicio    ?>',
                                folio_fin           : '<?= $folio_fin       ?>'
                            };

        /* Se ocultan los elementos */
        loading.hide();
        pagina.hide();
        //        tabla.hide();
        $('[data-toggle="reporte"]').tooltip();


        /* Evento que escucha la paginación */
        $('#pagination').on('click', 'a', function (e) {
            e.preventDefault();
            body.animate({ scrollTop: 0 }, "fast");
            var pageno = $(this).attr('data-ci-pagination-page');
            loadPagination(pageno, regPorPagina);
        });

        loadPagination(0, regPorPagina);
        /* Se carga una página */
        function loadPagination(pagno, rows) {
            loading.show();
            $.ajax({
                url: '<?= base_url() ?>index.php/plugins/Addenda/cargar_pagina/' + pagno + "/" + rows,
                type: 'post',
                data: datosBusqueda,
                dataType: 'json',
                success: function (response) {        
                    /* Si no hay resultados se despliega mensaje en modal */
                    if (response.total == 0) {
                        noResultados.modal('show');
                        tabla.hide();
                        $("#searchIcon").trigger( "click" );                        
                        return;
                    }
                    $('#pagination').html(response.pagination);
                    createTable(response.result, response.row);
                    total.text(response.total);
                    /* Aparece primero la tabla y después la paginación */
                    tabla.show(800, function () {
                        pagina.show(500);
                    });                        

                },
                complete: function () {
                    loading.hide();
                },
                error: function (xhr) {
                    alert("Error: " + xhr.status + " Inténtelo más tarde");
                }
            });
        }

        /* Se construye la tabla */
        function createTable(result, sno) {
        let vacio = "-";
        let urlEditar = '<?= base_url() ?>index.php/plugins/Addenda/editar/';
        sno = Number(sno);
        $('#listaFactutas tbody').empty();
        /* Se evaluan si los datos están vacíos se sustituye por un guión */
        for (index in result) {
            let rfc_emisor          = !result[index].rfc_emisor         ? vacio : result[index].rfc_emisor;
            let nombre_emisor       = !result[index].nombre_emisor      ? vacio : result[index].nombre_emisor;
            let rfc_receptor        = !result[index].rfc_receptor       ? vacio : result[index].rfc_receptor;
            let nombre_receptor     = !result[index].nombre_receptor    ? vacio : result[index].nombre_receptor;
            let serie               = !result[index].serie              ? vacio : result[index].serie;
            let folio               = !result[index].folio              ? vacio : result[index].folio;
            let uuid                = !result[index].uuid               ? vacio : result[index].uuid;
            let fecha_timbrado      = !result[index].fecha_timbrado     ? vacio : result[index].fecha_timbrado;
            let moneda              = !result[index].moneda             ? vacio : result[index].moneda;
            let total               = !result[index].total              ? vacio : result[index].total;
            let id_trx33            = !result[index].id_trx33           ? vacio : result[index].id_trx33;

            sno += 1;
            /* Se construye cada fila */
            var tr = "<tr>";
            tr += "<td>" + sno              + "</td>";
            tr += "<td>" + rfc_emisor       + "</td>";
            tr += "<td>" + nombre_emisor    + "</td>";
            tr += "<td>" + rfc_receptor     + "</td>";
            tr += "<td>" + nombre_receptor  + "</td>";
            tr += "<td>" + serie            + "</td>";
            tr += "<td>" + folio            + "</td>";
            tr += "<td>" + uuid             + "</td>";
            tr += "<td>" + fecha_timbrado   + "</td>";
            tr += "<td>" + moneda           + "</td>";
            tr += "<td>" + total            + "</td>";
            tr += "<td>" + "<a href='" + urlEditar + id_trx33 +"' title='Agregar addenda'> <i class='fa fa-pencil'></i></a>" + "</td>";
            tr += "</tr>";
            $('#listaFactutas tbody').append(tr);

        }
    }
});
</script>
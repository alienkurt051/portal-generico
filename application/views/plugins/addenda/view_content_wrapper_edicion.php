<!--Se cargan estilos-->
<link rel="stylesheet" href="<?= base_url() . "assets/dist/css/plugins/addenda/edicion.css" ?>">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Editar addenda Postimbrado
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Addenda</a></li>
            <li class="active">Editar addenda Postimbrado</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">                    
        <div class="box">    
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (isset($titulo)) {
                            ?>
                            <div class="<?php echo $tipo_mensaje; ?>">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                                <?php echo $mensaje; ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div> 
                <div class="container" style="width:auto;">
                    <!--<form id="buscar_comprobantes" class="form-horizontal" >-->
                    <?php
                        $attributes = array('class' => 'form-horizontal', 'id' => 'buscar_comprobantes');
                        echo form_open('addenda_form',$attributes)
                    ?>
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                        Información del documento
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body ">   
                                        <ul class="fa-ul">
                                            <i class="fa fa-spinner fa-pulse" ></i>
                                        </ul>
                                        <div class="table-responsive">          
                                            <table id="informacion_documento"class="table">
                                                <tbody>
                                                    <tr class="active">
                                                        <th >Emisor: </th>
                                                        <td></td>
                                                        <th>Serie: </th>
                                                        <td></td>
                                                        <th>Folio: </th>
                                                        <td></td>
                                                    </tr>
                                                    <tr class="active">
                                                        <th>Receptor: </th>
                                                        <td></td>
                                                        <th>Moneda: </th>
                                                        <td></td>
                                                        <th>Monto: </th>
                                                        <td>$ </td>                                                        
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                        Encabezado de la addenda
                                    </h4>
                                </div>
                                <ul class="fa-ul">
                                    <i class="fa fa-spinner fa-pulse" ></i>
                                </ul>                                
                                <div id="collapse2" class="panel-collapse collapse in">
                                    <div id="encabezado" class="panel-body">
                                        <!-- Datos a ingresar para encabezado de addenda-->
                                        <div class="form-group">
                                            <label class="col-md-2" for="orden_compra">Orden de compra</label>
                                            <div class="col-md-4">
                                                <input class="form-control" name="orden_compra" id="orden_compra" placeholder="Orden de compra">
                                            </div>    
                                            <label class="col-md-2" for="gln_comprador">GLN TCM</label>
                                            <div class="col-md-4">
                                                <input class="form-control" name="gln_comprador" id="gln_comprador" placeholder="GLN TCM">
                                            </div>                                                    
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-md-2" for="seccion_departamento">Sección o Depto.</label>
                                            <div class="col-md-4">
                                                <input class="form-control" name="seccion_departamento" id="seccion_departamento" placeholder="Sección o Depto.">
                                            </div>    
                                            <label class="col-md-2" for="gln_proveedor">GLN Proveedor</label>
                                            <div class="col-md-4">
                                                <input class="form-control" name="gln_proveedor" id="gln_proveedor" placeholder="GLN Proveedor">
                                            </div>                                                    
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-md-2" for="numero_proveedor">Número de Proveedor</label>
                                            <div class="col-md-4">
                                                <input class="form-control" name="numero_proveedor" id="numero_proveedor" placeholder="Número de Proveedor">
                                            </div>    
                                            <label class="col-md-2" for="gln_entrega">GLN de Sucursal de Entrega</label>
                                            <div class="col-md-4">
                                                <input class="form-control" name="gln_entrega" id="gln_entrega" placeholder="GLN de Sucursal de Entrega">
                                            </div>                                                    
                                        </div>                                        
                                        <div class="form-group">
                                            <label class="col-md-2" for="dias_plazo_pago">Días de Plazo pago</label>
                                            <div class="col-md-4">
                                                <input class="form-control" name="dias_plazo_pago" id="dias_plazo_pago" placeholder="Días de Plazo pago">
                                            </div>                                                        
                                        </div> 
                                        <div id="direccion_sucursal">
                                            <h5>Dirección de sucursal</h5>
                                            <hr>                                            
                                            <div class="form-group">
                                                <label class="col-md-1" for="entidad">Sucursal</label>
                                                <div class="col-md-7">
                                                    <input class="form-control" name="entidad" id="entidad" placeholder="Sucursal">
                                                </div>    
                                                <label class="col-md-1" for="estado">Ciudad</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" name="estado" id="estado" placeholder="Ciudad">
                                                </div>                                                    
                                            </div>     
                                            <div class="form-group">                                            
                                                <label class="col-md-1" for="calle">Calle</label>
                                                <div class="col-md-7">
                                                    <input class="form-control" name="calle" id="calle" placeholder="Calle">
                                                </div>
                                                <label class="col-md-1" for="codigo_postal">Código postal</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" name="codigo_postal" id="codigo_postal" placeholder="Código postal">
                                                </div>
                                            </div> 
                                        </div>    
                                    </div>                                   
                                </div>                                
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                        Detalles de los conceptos
                                    </h4>
                                </div>
                                <ul class="fa-ul">
                                    <i class="fa fa-spinner fa-pulse" ></i>
                                </ul>                                
                                <div id="collapse3" class="panel-collapse collapse in">
                                    <div class="table-responsive">          
                                        <table id="conceptos"class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Clave de producto o servicio</th>
                                                    <th>Número de identificación</th>
                                                    <th>Unidad de medida</th>
                                                    <th>Cantidad</th>
                                                    <th>Descripción</th>
                                                    <th>PU</th>
                                                    <th>Importe</th>
                                                    <th>Orden de compra</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>                        
                        </div>                        
                        <div class="form-row">                                                        
                            <div class="col-md-2 col-md-offset-10">
                                <input id="search" type="submit" value="Generar" class="btn btn-success pull-right" >
                            </div>                  
                        </div>
                    </form>
                </div>
                <!-- Modal para desplegar alerts -->
                <div class="modal fade" id="alert" role="dialog">
                    <div class="modal-dialog modal-sm">
                        <div class="alert-dismissible">                       
                            <span></span>                                    
                            <strong></strong><br/><p></p> 
                            <div class="modal-footer">
                                <a href="<?= $url_home ?>" class="btn" role="button">Ok</a>
                                <button type="button" class="btn" data-dismiss="modal">Ok</button>
                            </div>                            
                        </div>
                        <i id="loading" class="fa fa-spinner fa-pulse"></i>
                    </div>
                </div>                 
                <!-- /.box-body -->
            </div>
        </div>       
    </section>   
    <!-- /.content -->    
</div>
<!-- /.content-wrapper -->


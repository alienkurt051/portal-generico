<script src="http://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<link href="http://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<script>

    $(document).ready(function () {
        
        $('[data-toggle="busqueda"]').tooltip();
        /* Funciones para utilizar los plugins de datepicker y timepicker */
        colocarDate();
        colocarTime();        
        
        let contenido       = $(".content");
        let searchIcon      = $("#searchIcon");
        let bodySearch      = $(".box-body");
        let form            = $('form');
        let clear           = $('#clear');
        var horaFin         = $('#hora_fin');
        var horaInicio      = $('#hora_inicio');
        
        /* Limpia el formulario cuando se presiona el botón */
        clear.click(function () {
            form.trigger("reset");
        });
        /* Evento cuando se requiere hacer una búsqueda */
        form.submit(function (e) {
            e.preventDefault();
            /* Se oculta el formulario y modifica algunos estilos */
            form.hide(500, function () {
                searchIcon.show(300);
                searchIcon.slideDown("slow");
                contenido.css("min-height", "auto");
                bodySearch.css("padding", "0px");
            });
            cargarBusqueda();
        });
        /* Evento que muestra nuevamente el formulario para hacer una búsqueda */
        searchIcon.click(function () {
            form.show(500, function () {
                /*Regresa los estilos a sus propiedades originales */
                searchIcon.hide(300);
                searchIcon.slideUp("slow");
                contenido.css("min-height", "250px");
                bodySearch.css("padding", "10px");

            });
        });
        /* Validaciones de horas de inicio y fin */
        horaFin.change(function(){
            if (!horaInicio.val()){
                horaInicio.val("00:00");
            }           
        });
        horaInicio.change(function(){
            /* Si borran el valor de de hora inicio y hay hora fin */
            if($(this).val() === "" && horaFin.val() !== ""){
                $(this).val("00:00");
            }
        });
        /* Función que trae manda llamar vía ajax la consulta */
        function cargarBusqueda() {
            let resultados = $("#results");
            let dataForm = recuperarDatosForm();
//            console.log("dataForm");
            $.ajax({
                url: '<?= base_url() ?>index.php/plugins/Ticket/cargar_resultados',
                type: 'post',
                data : dataForm,
                dataType: 'html',
                success: function (response) {
                    resultados.html(response);                    
                }
            });
        }
        /* Recupera los datos del form y se quitan los espacios en blanco */
        function recuperarDatosForm(){        
            return  {
                numero_registros    : $.trim($("#numeroRegistros option:selected").text()),
                sucursal            : $.trim($("#sucursal option:selected").val()),
                fecha_inicio        : $.trim($("#fecha_inicio").val()),
                fecha_fin           : $.trim($("#fecha_fin").val()),
                hora_inicio         : $.trim($("#hora_inicio").val()),
                hora_fin            : $.trim($("#hora_fin").val()),
                minutos_inicio      : $.trim($("#minutos_inicio").val()),
                minutos_fin         : $.trim($("#minutos_fin").val()),                
                caja                : $.trim($("#caja").val()),
                transaccion         : $.trim($("#transaccion").val())       
            };
        }
        /* Coloca timepicker para poder seleccionar las horas */
        function colocarTime(){
         
            var horaInicio = new TimePicker(['hora_inicio','hora_fin'], {
              lang: 'pt',
              theme: 'blue-grey'
            });
            horaInicio.on('change', function(evt) {
                var value = (evt.hour || '00') + ':' + (evt.minute || '00');
                evt.element.value = value;
            });
       
        }
        /* Colocar datepicker para seleccionar las fechas en inputs */
        function colocarDate(){
            /* Para cada input del las fechas en rango */
             $('#event_period').datepicker({
                inputs: $('.actual_range'),
                format: "yyyy-mm-dd",
                language: "es",
                todayBtn: "linked"
            });
            /* Para que sólo permita colocar fechas válidas */
            $('#event_period input').each(function() {
                $(this).datepicker('clearDates');
            });            
        }
});
</script>
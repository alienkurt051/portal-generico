<link rel="stylesheet" href="<?= base_url() . "assets/dist/css/busqueda.css" ?>">
<div id="loading">
    <div style="position: relative; left: -50%;">
        <img  src="<?= $loading_img ?>" class="img-responsive" alt="Cinque Terre" > 
    </div>
</div>
<div class="box-body" id="resultados" style="display: none">        
    <div class="row" >
        <div class="col-md-4 col-lg-offset-8"> <strong>Total de registros encontrados: </strong><span id="total"></span></div>
    </div>
    <div class="row" >
        <div class="col-md-2">
            <a id="html"  type="button" data-toggle="reporte" title="Generar reporte html" value="Generar Reporte" class="btn btn-success pull-left" >Generar Reporte</a>
        </div> 
    </div>  
    <div class="table-responsive">
        <table class="table table-hover table-condensed" width='100%' id='listaFactutas'>
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Id STO</th>
                    <th scope="col"><?php echo NOMBRE_ID_ERP; ?></th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Sucursal</th>
                    <th scope="col">Subtotal</th>
                    <th scope="col">Total<br>Impuestos<br>Transaladados</th>
                    <th scope="col">Total</th>
					<th scope="col">Estatus<br>Facturación</th>
					<th scope="col">RFC<br>Receptor</th>
					<th scope="col">Nombre<br>Receptor</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table> 
    </div>
    <div class="text-center" style='margin-top: 10px;' id='pagination'></div>    
</div>

<!-- Modal -->
<div class="modal fade" id="no_resultados" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">No se obtuvieron resultados</h4>
            </div>
        </div>
    </div>
</div>

<!-- Script -->
<script type='text/javascript'>
    $(document).ready(function () {

        let loading         = $("#loading");
        let noResultados    = $("#no_resultados");
        let total           = $("#total");
        let tabla           = $("#resultados");
        let pagina          = $("#pagination");
        let generarHtml    = $("#html");
        let regPorPagina    = '<?= $rows ?>';
        let datosBusqueda   = {
                                sucursal        : '<?= $sucursal        ?>',
                                fecha_inicio    : '<?= $fecha_inicio    ?>',
                                fecha_fin       : '<?= $fecha_fin       ?>',
                                hora_inicio     : '<?= $hora_inicio     ?>',
                                hora_fin        : '<?= $hora_fin        ?>',
                                caja            : '<?= $caja            ?>',
                                transaccion     : '<?= $transaccion     ?>'
                            };

        /* Se convierten los datos de búsqueda como query string para generar html */
        let parametrosDescarga = jQuery.param(datosBusqueda);

        /* Se ocultan los elementos */
        loading.hide();
        pagina.hide();
        //        tabla.hide();
        $('[data-toggle="reporte"]').tooltip();


        /* Evento que escucha la paginación */
        $('#pagination').on('click', 'a', function (e) {
            e.preventDefault();
            var pageno = $(this).attr('data-ci-pagination-page');
            loadPagination(pageno, regPorPagina);
        });

        loadPagination(0, regPorPagina);
        generarHtml.attr("href", '<?= base_url() ?>index.php/plugins/ticket/generar_reporte?' + parametrosDescarga);

        /* Se carga una página */
        function loadPagination(pagno, rows) {
            loading.show();
            $.ajax({
                url: '<?= base_url() ?>index.php/plugins/ticket/cargar_pagina/' + pagno + "/" + rows,
                type: 'post',
                data: datosBusqueda,
                dataType: 'json',
                success: function (response) {
    //                    console.log(datosBusqueda);
                    /* Si no hay resultados se despliega mensaje en modal */
                    if (response.total == 0) {
                        noResultados.modal('show');
                        tabla.hide();
                        $("#searchIcon").trigger( "click" );                        
                        return;
                    }
                    $('#pagination').html(response.pagination);
                    createTable(response.result, response.row);
                    total.text(response.total);
                    /* Aparece primero la tabla y después la paginación */
                    tabla.show(800, function () {
                        pagina.show(500);
                    });                        

                },
                complete: function () {
                    loading.hide();
                },
                error: function (xhr) {
                    alert("Error: " + xhr.status + " Inténtelo más tarde");
                }
            });
        }

        /* Se construye la tabla */
        function createTable(result, sno) {
        let vacio = "-";
        sno = Number(sno);
        $('#listaFactutas tbody').empty();
        /* Se evaluan si los datos están vacíos se sustituye por un guión */
        for (index in result) {
            let id_trx33_r                  = !result[index].id_trx33_r                ? vacio : result[index].id_trx33_r;
            let id_trx_erp                  = !result[index].id_trx_erp                ? vacio : result[index].id_trx_erp;
            let fecha                       = !result[index].fecha                     ? vacio : result[index].fecha;
            let id_sucursal                 = !result[index].id_sucursal               ? vacio : result[index].id_sucursal;
            let subtotal                    = !result[index].subtotal                  ? vacio : result[index].subtotal;
            let totalImpuestosTransladados  = !result[index].totalImpuestosTrasladados ? vacio : result[index].totalImpuestosTrasladados;
            let total                       = !result[index].total                     ? vacio : result[index].total;
			let estatus_facturacion         = !result[index].estatus_facturacion       ? vacio : result[index].estatus_facturacion;
			let rfc_receptor                = !result[index].rfc_receptor              ? vacio : result[index].rfc_receptor;
			let nombre_receptor             = !result[index].nombre_receptor           ? vacio : result[index].nombre_receptor;


            sno += 1;
            /* Se construye cada fila */
            var tr = "<tr>";
            tr += "<td>" + sno                          + "</td>";
            tr += "<td>" + id_trx33_r                   + "</td>";
            tr += "<td>" + id_trx_erp                   + "</td>";
            tr += "<td>" + fecha                        + "</td>";
            tr += "<td>" + id_sucursal                  + "</td>";
            tr += "<td>" + subtotal                     + "</td>";
            tr += "<td>" + totalImpuestosTransladados   + "</td>";
            tr += "<td>" + total                        + "</td>";
			tr += "<td>" + estatus_facturacion          + "</td>";
			tr += "<td>" + rfc_receptor                 + "</td>";
			tr += "<td>" + nombre_receptor              + "</td>";
            tr += "</tr>";
            $('#listaFactutas tbody').append(tr);

        }
    }
});
</script>

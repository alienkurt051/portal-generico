<link rel="stylesheet" href="<?= base_url() . "assets/dist/css/busqueda.css" ?>">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Búsqueda avanzada de tickets
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Tickets</a></li>
            <li class="active">Búsqueda avanzada</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">                    
        <div class="box">    
            <button id="searchIcon"type="submit" data-toggle="busqueda" data-placement="right" title="Nueva búsqueda" style="padding: 0px;"><i class="fa fa-search" ></i></button>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (isset($titulo)) {
                            ?>
                            <div class="<?php echo $tipo_mensaje; ?>">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                                <?php echo $mensaje; ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div> 
                <div class="container" style="width:auto;">
                    <form>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputState">Sucursal</label>
                            <select id="sucursal" name="sucursal" class="form-control" required>
                                <?php
                                foreach ($arr_entidades as $unaEntidad) {
                                    echo '<option value="' . $unaEntidad->id_entidad . '">' . $unaEntidad->entidad . '</option>';
                                }
                                ?>

                            </select>
                        </div> 
                        <div class="form-group col-md-6">
                            <label for="caja">Caja</label>
                            <input class="form-control" name="caja" id="caja" placeholder="Caja">
                        </div>     
                    </div>    
                    <div class="form-row">
                        <div class="form-group col-md-6">

                            <label class="control-label">Fecha</label>
                            <div class="input-group input-daterange col-md-12" id="event_period">
                                <input type="text" class="form-control actual_range " name="fecha_inicio" id="fecha_inicio" value="" placeholder="Inicio">
                                <div class="input-group-addon" style="border:none;">al</div>
                                <input type="text" class="form-control actual_range" name="fecha_fin" id="fecha_fin" value="" placeholder="Fin">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="transaccion">Transacción</label>
                            <input class="form-control" name="transaccion" id="transaccion" placeholder="Transacción">
                        </div>   
                    </div> 
                    <div class="form-row">
                        <div class="form-group col-md-6">

                            <label class="control-label">Hora</label>
                            <div class="input-group col-md-12">
                                <input type="text" class="form-control" name="hora_inicio" id="hora_inicio" value=""  pattern="([01]?[0-9]|2[0-3]):[0-5][0-9]" title="HH:mm (formato 24 horas)" placeholder="HH:mm">
                                <div class="input-group-addon" style="border:none;">a las</div>
                                <input type="text" class="form-control" name="hora_fin" id="hora_fin" value="" pattern="([01]?[0-9]|2[0-3]):[0-5][0-9]" title="HH:mm (formato 24 horas)" min="0" max="23" placeholder="HH:mm">

                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputState">Registros por página</label>
                            <select name="estatus" class="form-control" id="numeroRegistros">
                                <option value="5" selected>5</option>
                                <option value="30">30</option>
                                <option value="1000">1000</option>
                            </select>
                        </div>   
                    </div> 

                    <div class="form-row">                    
                        <div class="col-md-2 col-md-offset-8">
                            <input id="clear"  type="button" value="Limpiar" class="btn btn-default pull-right" >
                        </div>                                     
                        <div class="col-md-2">
                            <input id="search" type="submit" value="Buscar" class="btn btn-success pull-right" >
                        </div>                  
                    </div>
                </form>
                </div>
                <div id="results">
                </div>     
                <!-- /.box-body -->
            </div>
        </div>       
    </section>
    <!-- /.content -->    
</div>
<!-- /.content-wrapper -->


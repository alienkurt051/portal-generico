<script>
$( document ).ready(function() {
	var url_busqueda = "<?php echo base_url()."index.php/bitacora/ajax_consultar_bitacora"; ?>";
	
	var usuario = $("#usuario").val();
	
	var email = $("#email").val();
	
	if ( usuario != null && usuario != "" ) {
		url_busqueda = url_busqueda + "/" + encodeURI(usuario);
	} else {
		url_busqueda = url_busqueda + "/NaV";
	}

	if ( email != null && email != "" ) {
		url_busqueda = url_busqueda + "/" + encodeURI(email);
	} else {
		url_busqueda = url_busqueda + "/NaV";
	}
	//alert(url_busqueda);
	
  $("#listado_bitacora").dataTable({
	  "processing":true,
	  "serverSide": true,
      "ordering": false,
      "searching": true,
	  "ajax": {
		  url : url_busqueda,
		  type: "POST",
		  error: function() {
			  alert("Mayeyó el datatable con busqueda");
		  }
	  },
	  "order"String: [[ 1, 'asc'String ]]
  });

});

function buscar_bitacora() {
	var url_busqueda = "<?php echo base_url()."index.php/bitacora/index"; ?>";
	
	var usuario = $("#usuario").val();
	
	var email = $("#email").val();
	
	if ( usuario != null && usuario != "" ) {
		url_busqueda = url_busqueda + "/" + encodeURI(usuario);
	} else {
		url_busqueda = url_busqueda + "/NaV";
	}

	if ( email != null && email != "" ) {
		url_busqueda = url_busqueda + "/" + encodeURI(email);
	} else {
		url_busqueda = url_busqueda + "/NaV";
	}

	window.location.replace(url_busqueda);
}
</script>



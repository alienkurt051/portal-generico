  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bitácora de  eventos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#">Bitácora</a></li>
        <li class="active"> Consulta</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

         <div class="box">
            <div class="box-header">
            <div class="row">
              <div class="col-md-3">
                  <h3 class="box-title">Consulta de eventos en bitácora</h3><br>
				  
              </div>
			</div>
			<form method="post" action="<?php echo $url_buscar_bitacora; ?>">
			<div class="row">
              <div class="col-md-2">
			     Usuario
			  </div>
              <div class="col-md-4">
                 <input type="text" id="usuario" name="usuario" class="form-control" placeholder="Teclee clave de usuario o RFC" 
				 <?php
				 if ( isset($usuario) ) {
					 echo 'value="'.$usuario.'"';
				 }
			     ?>
				 />
              </div>
              <div class="col-md-2">
			     email
			  </div>
              <div class="col-md-4">
                 <input type="text" id="email" name="email" class="form-control" placeholder="ejemplo@email.com"
                 <?php
				 if ( isset($email) ) {
					 echo 'value="'.$email.'"';
				 }
			     ?>
				 />
              </div>
			</div>

			<div class="row">
              <div class="col-md-1">
                 <a href="javascript:void(0)" class="btn btn-default" onclick="buscar_bitacora();">Buscar</a>
              </div>
            </div>
            </form>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
              <?php
              if ( isset($titulo) ) {
              ?>
                <div class="<?php echo $tipo_mensaje; ?>">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-ban"></i> <?php echo $titulo; ?></h4>
                  <?php echo $mensaje; ?>
                </div>
              <?php
              }
              ?>
            
              <div class="row">
                 <div class="col-md-12">
                     <p>Se muestran los últimos 100 registros de la bitácora</p>
                 </div>
              </div>
              <br>
            
              <div class="table-responsive">
                <table id="listado_bitacora" class="table table-striped table-condensed table-hover dataTable">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Login</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>F.Evento</th>
                    <th>Dir IP</th>
                    <th>Acción</th>
                    <th>Observaciones</th>					
                  </tr>
                  </thead>
				  <tbody>
                  <?php
				  /*
                  $conteo = 0;
                  foreach ($arrbitacora as $bitacora) {
                      $conteo++;
                  echo '<tr>';
                  echo '  <td>'.$conteo.'</td>';
                  echo '  <td>'.$bitacora->login.'</td>';
                  echo '  <td>'.$bitacora->nombre.'</td>';
                  echo '  <td>'.$bitacora->email.'</td>';
                  echo '  <td>'.$bitacora->fecha_hora.'</td>';
                  echo '  <td>'.$bitacora->dir_ip.'</td>';
                  echo '  <td>'.$bitacora->d_accion.'</td>';
                  echo '  <td>'.$bitacora->observaciones.'</td>';					
                  echo '</tr>';
					  
					  
                      
                  }
				  */
                  ?>
                 </tbody>
                </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      <a href="javascript:void(0);" onclick='$("#modal-soporte").modal();'>Contacte a soporte</a>
    </div>

    <!-- Default to the left -->

    <strong>Copyright &copy; 2019 V. 1.7.2 <a href="http://www.stoconsulting.com" target="_blank">STO Consulting</a></strong>
    
    <div class="modal fade" id="modal-soporte" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">¿Requiere ayuda?</h4>
              </div>
              <div class="modal-body">
                <?php
				// se escribe la informacion de soporte
				// leyenda de soporte
				echo '<span>'.$config_portal->leyenda_info_soporte.'</span><br><br>';
				// instrucciones
				echo '<span>'.$config_portal->instrucciones_soporte.'</span><br><br>';
				
				// correo electronico
				echo '<span>Correo electrónico:<br><ul class="list-unstyled">';
				
				// los correos electronicos estan separados por ;
                $correo = strtok($config_portal->email_soporte, ";");
                
                while ($correo !== false)
                {
                echo '<li><i class="fa fa-envelope"></i> '.$correo.'</li>';
                $correo = strtok(";");
                } 
				
				echo '</ul></span><br>';
				
				// telefonos
				echo '<span>Teléfonos:<br><ul class="list-unstyled">';
				
				// los telefonos estan separados por ;
                $correo = strtok($config_portal->telefono_soporte, ";");
                
                while ($correo !== false)
                {
                echo '<li><i class="fa fa-phone"></i> '.$correo.'</li>';
                $correo = strtok(";");
                } 
				
				echo '</ul></span><br>';
				?>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Aceptar</button>
              </div>
            </div>
            <!-- /.modal-content -->
        </div>
          <!-- /.modal-dialog -->
    </div>
	
  </footer>
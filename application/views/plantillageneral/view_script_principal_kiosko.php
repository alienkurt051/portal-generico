
<script src="<?php echo base_url("assets/plugins/jquery_idletimer/dist/idle-timer.1.1.0.js"); ?>"></script>
<script>
    // tiempo definido en configuracion de portal
    $(document).idleTimer( <?php echo $modo_kiosko_tiempo; ?> );
      
    $(document).on( "idle.idleTimer", function(event, elem, obj){
        window.location.replace("<?php echo $url_cierre_sesion; ?>");
    });
    
</script>
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Agregar solicitud de descarga de CFDi
			</h1>
		</section>

		<!-- Main content -->
		<section class="content">

		 <div class="box">
			<div class="box-header">
			    <div class="row">
				   <div class="col-md-10">
				       <h3 class="box-title">En esta ventana puedes solicitar la descarga de CFDi desde el repositorio del SAT.</h3>
				   </div>
				   <div class="col-md-2 ">
				       <a href="<?php echo $url_anterior; ?>" class="btn btn-warning pull-right"> Regresar</a>
				   </div>
				</div>
				
				
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				
                <div class="row">
                  <div class="col-md-12">
                        <?php
                        if ( isset($titulo) ) {
                        ?>
                          <div class="<?php echo $tipo_mensaje; ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                            <?php echo $mensaje; ?>
                          </div>
                        <?php
                        }
                        ?>
                  </div>
                </div>
				
				<br/>
				<form id="formulario_consulta" method="post" action="<?php echo $url_alta_solicitud; ?>">
				<div class="row">
				    <div class="col-md-12">
					    <div class="form-group">
					    	<div class="row">
					    		<div class="col-md-12">
					    			<div class="row">
					    				<div class="col-md-2">
					    					<h4>*Emisor</h4>
					    				</div>
					    				<div class="col-md-4">
					    					<select id="emisor_rfc" name="emisor_rfc" class="form-control" required>
					    					   <option value="" selected>--Elige un RFC emisor--</option>
					    					   <?php
					    					   foreach ($arr_emisores as $emisor) {
					    						   echo '<option value="'.$emisor->rfc.'">'.$emisor->rfc." - ".$emisor->entidad.'</option>';
					    					   }
					    					   
					    					   ?>
					    					</select>
					    				</div>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					    <div class="form-group">
					    	<div class="row">
					    		<div class="col-md-12">
					    			<div class="row">
					    				<div class="col-md-2">
					    					<h4>RFC Receptor</h4>
					    				</div>
					    				<div class="col-md-4">
					    					<input type="text" id="receptor_rfc" name="receptor_rfc" value="" class="form-control">
					    				</div>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					    
					    <div class="form-group">
					    	<div class="row">
					    	    <div class="col-md-2">
					    	    	 <label>*Fecha y hora inicial</label>
					    	    </div>
					    	    <div class="col-md-2">
					    	    	<input type="date" id="fecha_inicial" name="fecha_inicial" class="form-control" placeholder="Fecha inicial" required>
					    	    </div>
                                <div class="col-md-2">
                                    <input type="time" id="hora_inicial" name="hora_inicial" min="00:00:00" max="23:59:59" step="1" required>
                                </div>
					    	    <div class="col-md-2">
					    	    	 <label>*Fecha y hora final</label>
					    	    </div>
					    	    <div class="col-md-2">
					    	    	<input type="date" id="fecha_final" name="fecha_final" class="form-control" placeholder="Fecha final" required>
					    	    </div>
                                <div class="col-md-2">
                                    <input type="time" id="hora_final" name="hora_final" min="00:00:00" max="23:59:59" step="1" required>
                                </div>

					    	</div>
					    </div>
                        
                        
					    <div class="form-group">
					    	<div class="row">
					    			
					    		<div class="col-md-12">
					    			<input type="submit" value="Agregar solicitud" id="Buscar" name="btn_procesar_solicitud" class="btn btn-success pull-right"/>
					    		</div>
					    	</div>
					    </div>
					
					
					
				    </div>
				</div>
				</form>

				 <!-- /.box-body -->
			</div>

		</div>
		</section>
				
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->


	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Descarga de CFDi desde el Sistema de Administración Tributaria (SAT)
			</h1>
		</section>

		<!-- Main content -->
		<section class="content">

		 <div class="box">
			<div class="box-header">
			    <div class="row">
				   <div class="col-md-10">
				       <h3 class="box-title">En esta ventana puedes consultar y solicitar la descarga de CFDi desde el repositorio del SAT y consultar el estado de las solicitudes que has realizado.</h3>
				   </div>
				   <div class="col-md-2 ">
				       <a href="<?php echo $url_agregar_solicitud_descarga; ?>" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Agregar solicitud</a>
				   </div>
				</div>
				
				
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				
                <div class="row">
                  <div class="col-md-12">
                        <?php
                        if ( isset($titulo) ) {
                        ?>
                          <div class="<?php echo $tipo_mensaje; ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                            <?php echo $mensaje; ?>
                          </div>
                        <?php
                        }
                        ?>
                  </div>
                </div>
				
				<br/>
				<form id="formulario_consulta" method="post" action="<?php echo $url_consultar_solicitudes; ?>">
				<div class="row">
				   <div class="col-md-12">
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-2">
										<h4>*Emisor</h4>
									</div>
									<div class="col-md-4">
										<select id="emisor_rfc" name="emisor_rfc" class="form-control" required>
										   <option value="" selected>--Elige un RFC emisor--</option>
										   <?php
										   foreach ($arr_emisores as $emisor) {
											   echo '<option value="'.$emisor->rfc.'">'.$emisor->rfc." - ".$emisor->entidad.'</option>';
										   }
										   
										   ?>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-2">
										<h4>RFC Receptor</h4>
									</div>
									<div class="col-md-4">
										<input type="text" id="receptor_rfc" name="receptor_rfc" value="" class="form-control">
									</div>
								    <div class="col-md-2">
								    	<label>UUID</label>
								    </div>
								    <div class="col-md-4">
								        <input type="text" id="uuid" name="uuid" class="form-control" placeholder="UUID">
								    </div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
						    <div class="col-md-2">
						    	 <label>Fecha inicial</label>
						    </div>
						    <div class="col-md-4">
						    	<input type="date" id="fecha_inicial" name="fecha_inicial" class="form-control" placeholder="Fecha inicial" >
						    </div>
						    <div class="col-md-2">
						    	 <label>Fecha final</label>
						    </div>
						    <div class="col-md-4">
						    	<input type="date" id="fecha_final" name="fecha_final" class="form-control" placeholder="Fecha final" >
						    </div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
						    <div class="col-md-2">
						    	 <label>Estado de la solicitud</label>
						    </div>
						    <div class="col-md-4">
								<select class="form-control" id="estatus_solicitud" name="estatus_solicitud">
								    <option value="" selected>Todas</option>
									<option value="2">Pendientes</option>
									<option value="1">Procesadas</option>
								</select>
						    </div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
								
							<div class="col-md-12">
								<input type="submit" value="Buscar" id="Buscar" name="btn_procesar_solicitud" class="btn btn-primary pull-right"/>
							</div>
						</div>
					</div>
					
					
					
				   </div>
				</div>
				</form>
				
			
				 
				 					<!-- boton para agregar datos a la tabla peticiones_pendientes_sat -->
									 
				 <!-- /.box-body -->

			 <?php
				 // si ya se hizo una busqueda
			 if ( isset($arr_solicitudes) ) {
				 ?>
				 <hr>
				 <h3>Listado de solicitudes realizadas</h3>
				 <div class="row">
				    <div class="col-md-12 table-responsive">
						<table id="listado_comprobantes" class="table table-striped table-hover table-condensed">
							<thead>
								<tr>
									<!-- <th>&nbsp;</th> -->
									<th>#</th>
									<!--<th>ID RFC33</th>-->
									<th>RFC Emisor</th>
									<th>RFC receptor</th>
									<th>Fecha inicial</th>
									<th>Fecha final</th>
									<th>UUID</th>
									<th>Código</th>
									<th>Mensaje</th>
								</tr>
							</thead>
							<tbody>
							<?php
							if ( count($arr_solicitudes) > 0 ) {
								
								$conteo = 0;
								$total_facturas = 0;
								$id_docto = "";
								foreach ($arr_solicitudes as $solicitud) {
									$conteo++;

									echo '<tr>';
									echo '  <td>'.$conteo.'</td>';
									echo '  <td>'.$solicitud->emisor_rfc.'</td>';
									echo '  <td>'.$solicitud->receptor_rfc.'</td>';
									echo '  <td>'.$solicitud->fecha_inicial.'</td>';
									echo '  <td>'.$solicitud->fecha_final.'</td>';
									echo '  <td>'.$solicitud->uuid.'</td>';
									echo '  <td>'.$solicitud->codigo.'</td>';
									echo '  <td>'.$solicitud->mensaje.'</td>';
									
									echo '</tr>';

								}

							} else {
								echo '<tr>';
								echo '<td colspan="8">No se encontraron registros con los parámetros de búsqueda indicados. Intente de nuevo por favor con otros valores.</td>';
								echo '<tr>'; 
							}
			 }
							?>
							</tbody>
						</table>


					</div>
				</div>
						<!-- /.box-body -->
			</div>

		</div>
		</section>
				
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->


<script src="<?php echo base_url()."assets/plugins/input-mask/jquery.inputmask.js"; ?>"></script>
<script src="<?php echo base_url()."assets/plugins/input-mask/jquery.inputmask.extensions.js"; ?>"></script>
<script src="<?php echo base_url()."assets/plugins/input-mask/jquery.inputmask.date.extensions.js"; ?>"></script>
<script src="<?php echo base_url()."assets/plugins/datepicker/bootstrap-datepicker.js"; ?>"></script>
<script>
	
	$(document).ready(function() {

		var url_agregar_campo = "<?php echo $url_agregar_campo; ?>";
		var url_consultar_comprobantes_pss=  "<?php echo $url_consultar_comprobantes_pss; ?>";
		//alert("funcion de botones");
		//console.log(url_agregar_campo,url_consultar_comprobantes_pss);
		
	//Date picker
	$('#Fecha_inicial').datepicker({
		format: 'yyyy-mm-dd',
		language: "es",
		autoclose: true
	});

	$('#Fecha_final').datepicker({
		format: 'yyyy-mm-dd',
		language: "es",
		autoclose: true
	});
	
	//alert("heil");
	$('#Buscar').click(function(){
		$('#formulario_consulta').attr('action', ' <?php echo  $url_consultar_comprobantes_pss; ?>');
	});


	$('#btn_agregar_campo').click(function(){
		$('#formulario_consulta').attr('action', '<?php echo  $url_agregar_campo; ?>');
		alert("Se ha agregado con éxito");
	});

	
	
	$('#box_envio_correo').hide();
	
});


	function envia_xml(id_docto) {
		if (id_docto=='') {
		var checkboxes=document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
		var chkbx_all=document.getElementById("all");
		var aux=0;
		for(i=0;i<checkboxes.length;i++) //recorremos todos los controles
		{
			if(checkboxes[i].type == "checkbox" && checkboxes[i].checked == "1" && checkboxes[i].value!="all" && checkboxes[i].value!="on") //solo si es un checkbox entramos
			{
				if(aux==0){
					aux = checkboxes[i].value;
				}
				else{
					aux+=','+checkboxes[i].value;
				}
			}
		}
		if (aux!=0) {
			document.getElementById('ids_merge').value = aux;
			$('#box_envio_correo').hide();
			$('#id_docto').val(id_docto);
			$('#box_envio_correo').show("slow");
			document.getElementById("id_docto").value = id_docto;
			$('html, body').animate({
				scrollTop: $("#box_envio_correo").offset().top
			}, 1000);
		}
		else{
			alert("Debes seleccionar al menos un comprobante.");
		}
	}
	else{
		$('#box_envio_correo').hide();
		$('#id_docto').val(id_docto);
		$('#box_envio_correo').show("slow");
		document.getElementById("id_docto").value = id_docto;
		$('html, body').animate({
			scrollTop: $("#box_envio_correo").offset().top
		}, 1000);
	}
	
}
	function activos_on(){
		document.getElementById("solo_cancelados").checked = 0;
	}
	function cancelados_on(){
		document.getElementById("solo_activos").checked = 0;
	}
	function seleccionar_todos() {
		$('input[type="checkbox"]').prop("checked", true);
	}

	function deseleccionar_todos() {
		$('input[type="checkbox"]').prop("checked", false);
	}

	function enviar_elegidos() {
		$("#formulario_comprobantes").submit();
	}

	function descargar_reporte(){
		$("#descargar_reporte_excel").submit();
	}
	function descargar_anexo(id_trx){
		$('#id_trx').val(id_trx);
		$("#archivo_anexo"+id_trx).submit();
	}
	function descargar_reportepss(){
		$("#descargar_reporte_excelpss").submit();
	}
function agregar_campo(){
		$("#agregar_campo").submit();
	}

</script>
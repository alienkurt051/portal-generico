  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mi perfil
        <small>Registre y actualice sus datos personales</small>
      </h1>

    </section>
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" class="form" method="post" action="#">
              <div class="box-body">
              
                <div class="row">
                  <div class="col-md-12">
                        <?php
                        if ( isset($titulo) ) {
                        ?>
                          <div class="<?php echo $tipo_mensaje; ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                            <?php echo $mensaje; ?>
                          </div>
                        <?php
                        }
                        ?>
                  </div>
                </div>
                
                <div class="row">
                  <!-- emisor -->
                  <div class="col-md-11">
                    <h4 class="box-title">Datos de la cuenta de usuario</h4>
                  </div>
                  <div class="col-md-1">
                    <a href="<?php echo $url_editar_cuenta; ?>" class="btn btn-warning pull-right"><i class="fa fa-pencil"></i> Editar</a>
                  </div>
                </div>
              
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-2">
                      <label>Nombre</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="nombre" name="nombre" class="form-control" value="<?php echo $pss_usuario->nombre; ?>" readonly>
                    </div>
                    <div class="col-md-3">
                      <input type="text" id="apellido_paterno" name="apellido_paterno" class="form-control" value="<?php echo $pss_usuario->apellido_paterno; ?>" readonly>
                    </div>
                    <div class="col-md-3">
                      <input type="text" id="apellido_materno" name="apellido_materno" class="form-control" value="<?php echo $pss_usuario->apellido_materno; ?>" readonly>
                    </div>

                  </div>
                </div>
              
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-2">
                      <label>Login</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="login" name="login" class="form-control" value="<?php echo $pss_usuario->login; ?>" readonly>
                    </div>
                    <div class="col-md-2">
                      <Label>eMail contacto</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="email_contacto" name="email_contacto" class="form-control" value="<?php echo $pss_usuario->email; ?>" readonly>
                    </div>
                  </div>
                </div>
                
                <?php 
                // no se muestran estos campos cuando el portal esta configurado a usar el RFC como login
                if ( $config_portal->usar_contrasena != CONTROL_ACCESO_SOLO_LOGIN_RFC ) {
                ?>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-2">
                      <label>Pista</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="id_pista_recuperar_contrasena" name="id_pista_recuperar_contrasena" class="form-control" value="<?php echo $pregunta_recuperacion; ?>" readonly>
                    </div>
                    <div class="col-md-2">
                      <Label>Respuesta</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="respuesta_recuperar_contrasena" name="respuesta_recuperar_contrasena" class="form-control" value="<?php echo $pss_usuario->respuesta_recuperar_contrasena; ?>" readonly>
                    </div>
                  </div>
                </div>
                <br>
                <?php
                } // mostrar solo si el portal NO esta configurado para usar el RFC como login
                ?>
                <hr>
                <div class="form-group">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Listado de familias </h4>
                  </div>
                  <div class="col-md-2">
                    <a href="<?php echo $url_editar_datos_familia; ?>" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar familia</a>
                  </div>
                </div>
                <hr>
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>ID familia</th>
                      <th>RFC</th>
                      <th>Número de instancia</th>
                      <th>Contador</th>
                      <th>Fecha de alta</th>
                      <th> </th>
                      <th> </th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php
                     if($arr_familias!=null){
                        for ($i = 1; $i <= count($arr_familias); $i++){                     
                      ?>  
                    <tr>
                      <td><?php echo $arr_familias[$i]->id_familia; ?></td>
                      <td><?php echo $arr_familias[$i]->rfc; ?></td>
                      <td><?php echo $arr_familias[$i]->num_instancia; ?></td>
                      <td><?php echo $arr_familias[$i]->contador; ?></td>
                      <td><?php echo $arr_familias[$i]->fecha_alta; ?></td>
                      <td>
                        <?php
                      $url_editar_familia = $url_editar_datos_familia."/".$arr_familias[$i]->id_familia."/".$arr_familias[$i]->id_cliente_ebs;
                      $url_borrar_familia = $url_borrar_datos_familia."/".$arr_familias[$i]->id_familia;
                      
                      echo '<a href="'.$url_editar_familia.'" class="btn btn-success"><i class="fa fa-edit"></i> Editar este RFC</a>';
                      ?>                      
                      </td>
                      <td>
                        <?php  echo '<a href="'.$url_borrar_familia.'" class="btn btn-danger"><i class="fa fa-close"></i> Eliminar</a>'; ?>                       
                      </td>
                  </tr>
                  <?php
                     } 
                  }else{
                    ?>
                    <tr><td colspan="7" align="center"><h4>Sin datos disponibles</h4></td></tr>
                    <?php
                  }
                  ?>   
                  </tbody>
                </table>
              </div>

              <div class="box-footer">
  
              </div>
              <!-- /.box-footer-->
            </form>
            
          </div>
        </div>
          <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
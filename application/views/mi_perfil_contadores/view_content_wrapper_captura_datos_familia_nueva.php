  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mi perfil
        <small>Actualice datos de familia</small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
              <?php
              if ( isset($titulo) ) {
              ?>
                <div class="<?php echo $tipo_mensaje; ?>">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                  <?php echo $mensaje; ?>
                </div>
              <?php
              }
              ?>
        </div>
      </div>

      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" class="form" method="post" action="<?php echo $url_registra_datos_familia_nueva; ?>">
              <div class="box-body">
                
                <div class="row">
                  <!-- emisor -->
                  <div class="col-md-12">
                    <h4 class="box-title">Datos de la cuenta de usuario</h4>
                  </div>
                </div>
              
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-2">
                      <label>Login</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="login" name="login" class="form-control" value="<?php echo $pss_usuario->login; ?>" readonly>
                    </div>
                    <div class="col-md-2">
                      <Label>Nombre</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="nombre_completo" name="nombre_completo" class="form-control" value="<?php echo $pss_usuario->nombre." ".$pss_usuario->apellido_paterno." ".$pss_usuario->apellido_materno; ?>" readonly>
                    </div>
                  </div>
                </div>
              
                <div class="row">
                  <!-- emisor -->
                  <div class="col-md-12">
                    <h4 class="box-title">Información fiscal</h4>                   
                  </div>
                </div>
                <input type="hidden" name="id_cliente_ebs" id="id_cliente_ebs" value="<?php echo $pss_usuario->id_usuario; ?>">
                <div class="form-group">
                  <div class="row">
                      <div class="col-md-2">
                        <label>Codigo familia</label>
                      </div>
                      <div class="col-md-4">
                        <input  style="text-transform: capitalize" type="text" id="id_familia" name="id_familia" class="form-control" value="" required>
                      </div>                    
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-2">
                      <label>RFC</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="rfc" name="rfc" class="form-control" minlength="12" maxlength="13" value="" style="text-transform:uppercase;" required>
                    </div>
                    <div class="col-md-2">
                      <label>Número de instancia</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="num_instancia" name="num_instancia" class="form-control" value="">
                    </div>
                  </div>
                  <div class="row" id="rfc_utilizado" style="border: solid; display: none;box-shadow: 0px 0px 15px;">
                    
                </div>
                <div class="form-group">
                  <br>                  
                  <div class="row">
                    <div class="col-md-2">
                      <label>Contador</label>
                    </div>
                    <div class="col-md-4">
                      <input  style="text-transform: capitalize" type="text" id="contador" name="contador" class="form-control" value="" required>
                    </div>
                    <div class="col-md-2">
                      <label>Fecha actual</label>
                    </div>
                    <div class="col-md-4">
                      <?php $time = time(); ?>
                      <input  style="text-transform: capitalize" type="text" id="fecha_alta" name="fecha_alta" class="form-control" value="<?php echo date("d-m-Y H:i:s", $time); ?> " readonly="readonly">
                    </div>
                  </div>
                </div>
                
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" value="Guardar" class="btn btn-success"> <a href="<?php echo $url_anterior; ?>" class="btn btn-danger">Cancelar</a>
              </div>
              <!-- /.box-footer-->
            </form>
          </div>
          
          <!-- /.box -->
        </div>



      </div>
      
 

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Configuración del portal
        <small>Control de plugins</small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            
              <div class="box-body">
                
                <div class="row">
                  <div class="col-md-12">
                        <?php
                        if ( isset($titulo) ) {
                        ?>
                          <div class="<?php echo $tipo_mensaje; ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                            <?php echo $mensaje; ?>
                          </div>
                        <?php
                        }
                        ?>
                  </div>
                </div>

                <div class="row">
                  <!-- emisor -->
                  <div class="col-md-12">
                    <h4 class="box-title">Plugins</h4>
                      <br>
                      <small>La siguiente tabla muestra la lista de los plugins configurados en el portal</small>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-12 table-responsive">
                      <table class="table table-condensed table-stripped table-hover">
                        <tr>
                          <th>#</th>
                          <th>Archivo</th>
                          <th>Controller</th>
                          <th>Icono</th>
                          <th>Etiqueta</th>
                          <th>Es visible</th>
                          <th>Es publico</th>
                        </tr>
                        
                <?php
                
                  $cont = 0;
                  for ($cont = 0; $cont < count($arr_plugins); $cont++) {
                      echo '<tr>';
                      echo '<td>'.($cont+1).'</td>';
                      echo '<td>'.$arr_plugins[$cont]["archivo"].'</td>';
                      echo '<td>'.$arr_plugins[$cont]["controller"].'</td>';
                      echo '<td>'.$arr_plugins[$cont]["icono"].'</td>';
                      echo '<td>'.$arr_plugins[$cont]["etiqueta"].'</td>';
                      echo '<td>'.$arr_plugins[$cont]["es_visible"].'</td>';
                      echo '<td>'.$arr_plugins[$cont]["es_publico"].'</td>';
                      echo '</tr>';
                  }// para cada campo
                  ?>
                        
                       </table>
                     </div>
                   </div>
                </div>
                <hr>
                

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" value="Guardar" class="btn btn-success"> <a href="<?php echo $url_anterior; ?>" class="btn btn-danger">Cancelar</a>
              </div>
              <!-- /.box-footer-->
            
          </div>
          
          <!-- /.box -->
        </div>



      </div>
      
 

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
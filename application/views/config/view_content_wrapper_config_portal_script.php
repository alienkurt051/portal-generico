<script>

    $(document).ready(function () {
        /* Funciones */
        valida_frecuencia();

        /* Si la frecuencia es de fecha calendario no se deben ingresar días */
        $("#id_frecuencia_max_fac").change(function () {
            /* Cada que se cambia el select los valores son reseteados */
            $("#dias").val(1);
            $("#dias_posteriores").val(1);

            valida_frecuencia();
        });

        /* Si la frecuencia es de fecha calendario no se deben ingresar días */
        function valida_frecuencia() {
            let dias_div            = $(".dias_hide");
            var dias                = $("#dias");
            var diasPosteriores     = $("#dias_posteriores");
            var cambioAnio          = $("#cambio_anio");
            let frecuencia          = $("#id_frecuencia_max_fac");
            let urlDatos            = '<?= base_url() ?>index.php/Config_portal/obtener_datos_frecuencia/';

            /* Petición ajax para traer los valores de la frecuencia de facruración
             * que se seleccione, se envía como parámetro el id.*/
            $.ajax({
                url: urlDatos,
                type: 'post',
                data: {id_frecuencia_max_fac: frecuencia.val()},
                dataType: "json",
                success: function (response) {
                    let respDias                = response.dias;
                    let respDiasPosteriores     = response.dias_posteriores;
                    let respCambioAnio          = parseInt(response.cambio_anio);
                    /* Se colocan los valores obtenidos dentro de cada input */
                    dias.val(respDias);
                    diasPosteriores.val(respDiasPosteriores);
                    /* Se activa el checkbox si hay cambio de anio*/
                    if (respCambioAnio === 1) {
                        cambioAnio.prop("checked", true);
                        return;
                    }
                    $("#cambio_anio").prop("checked", false);
                }
            });
            /* Si se selecciona por fecha calendario se deshabilita el input de
             * los días ya que en fecha calendario no se ocupa ese valor. */
            if (frecuencia.val() == 2) {
                dias_div.prop("disabled", true);
                return;
            }
            dias_div.prop("disabled", false);
        }
    });
</script>
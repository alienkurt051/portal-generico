<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Configuración del portal
            <small>Editar frecuencia máxima de facturación</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">                    
        <div class="box">    
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (isset($titulo)) {
                            ?>
                            <div class="<?= $tipo_mensaje ?>">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-exclamation-circle"></i> <?= $titulo ?></h4>
                                <?= $mensaje ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div> 
                <form role="form" class="form" method="post" action="<?= $url_editar_frecuencia ?>">
                    <div class="form-group row">
                        <div class="col-md-8">
                            <label for="frecuencia" class="col-form-label">Frecuencia</label>
                            <div class="col-md-12">
                                <small>Se refiere a la periodicidad con la cual 
                                    se evalúa la fecha límite que tiene una 
                                    transacción para ser facturada.</small>
                            </div>
                        </div>    
                        <div class="col-md-4">
                            <select class="form-control" id="id_frecuencia_max_fac" name="id_frecuencia_max_fac">
                                <?php
                                /* Se recorren todas las frecuencias que hay */
                                foreach ($frecuencias as $frecuencia) {
                                    /* Seleccionado el que está en la configuración */
                                    if ($frecuencia->id_frecuencia_max_fac === $config_portal->id_frecuencia_max_fac) {
                                        echo '<option value="' . $frecuencia->id_frecuencia_max_fac . '" selected>' . $frecuencia->frecuencia . '</option>';
                                        continue;
                                    }
                                    /* Si es año se coloca el nombre año ya que en la BD viene como anio */
                                    if ($frecuencia->frecuencia === 'ANIO') {
                                        echo '<option value="' . $frecuencia->id_frecuencia_max_fac . '">AÑO</option>';
                                        continue;
                                    }
                                    echo '<option value="' . $frecuencia->id_frecuencia_max_fac . '">' . $frecuencia->frecuencia . '</option>';
                                }
                                ?>   

                            </select>
                        </div>
                    </div>                    
                    <div class="form-group row dias_hide">
                        <div class="col-md-8">
                            <label for="dias" class="col-form-label">Días</label>
                            <div class="col-md-12">
                                <small>Número de días límite para poder emitir 
                                    factura (Esta sección solo aplica para la 
                                    frecuencia FECHA TRANSACCIÓN). </small>
                            </div>
                        </div>                          
                        <div class="col-md-4">
                            <input class="form-control dias_hide" name="dias" type="number" id="dias" value="<?= $frecuencia_max_fac->dias ?>" min="1" max="365">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-8">
                            <label for="dias_posteriores" class="col-form-label">Días posteriores</label>
                            <div class="col-md-12">
                                <small>Número de días sumados a los días límite 
                                    para facturar transacción.</small>
                            </div>                            
                        </div>      
                        <div class="col-md-4">
                            <input class="form-control" name="dias_posteriores" type="number" id="dias_posteriores" value="<?= $frecuencia_max_fac->dias_posteriores ?>"  min="0" max="365">
                        </div>
                    </div> 
                    <br /><br /><br />
                    <div class="form-group row">
                        <div class="col-md-8">
                            <label for="cambio_anio" class="col-form-label">Permitir facturar de año anterior</label>
                            <div class="col-md-12">
                                <small>Si está activado el switch se permitirá 
                                    facturar transacciones de un año anterior.</small>
                            </div>                            
                        </div>      
                        <div class="col-md-4" style="text-align:end;">
                            <label class="switch">
                                <input id="cambio_anio" name="cambio_anio" type="checkbox" value='1'>
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>                     
                    <div class="box-footer">
                        <input type="submit" value="Guardar" class="btn btn-success"> <a href="<?= $url_anterior ?>" class="btn btn-danger">Cancelar</a>
                    </div>
                </form>
                <!-- /.box-body -->                
            </div>
        </div>
    </section>  
    <!-- /.content -->

</div>
<!-- /.content-wrapper -->
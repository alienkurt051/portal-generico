  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Configuración del portal
        <small>Configurar flex headers</small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" class="form" method="post" action="<?php echo $url_agregar_nombre_flex; ?>">
              <div class="box-body">
                
                <div class="row">
                  <div class="col-md-12">
                        <?php
                        if ( isset($titulo) ) {
                        ?>
                          <div class="<?php echo $tipo_mensaje; ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                            <?php echo $mensaje; ?>
                          </div>
                        <?php
                        }
                        ?>
                  </div>
                </div>

                <div class="row">
                  <!-- emisor -->
                  <div class="col-md-12">
                    <h4 class="box-title">Agregar flex header</h4>
                  </div>
                </div>
              
                <div class="row">
                  <!-- emisor -->
                  <div class="col-md-10">
                    <h4 class="box-title">Campos para búqueda de transacción</h4>
                  </div>
                </div>
              
                <?php
                // si no se tiene configuracion de campos
                if ( count($arr_c_info_adicionales) < 1 ) {
                    ?>
                <div class="form-group">
                  <div class="row">                  
                    <div class="col-md-8">
                      No se tiene configuración de campos de búsqueda de transacción.
                    </div>
                  </div>
                </div>
                    <?php
                } else {
                    // se listan los campos de busqueda
                    ?>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-10 table-responsive">
                      <table class="table table-condensed table-stripped table-hover">
                        <tr>
                          <th class="col-md-1">#</th>
                          <th class="col-md-2">Campo adicional</th>
                          <th class="col-md-3">Descripcion</th>
                          <th class="col-md-1">Nivel</th>
                          <th class="col-md-2">Etiqueta Observaciones</th>
                          <th class="col-md-2">Accion</th>
                        </tr>
                        
                <?php
                  $cont = 1;
                  foreach ($arr_c_info_adicionales as $campo) {
                      echo '<tr>';
                      echo '<td class="col-md-1">'.$cont.'</td>';
                      echo '<td class="col-md-2">'.$campo->campo_adicional.'</td>';
                      echo '<td class="col-md-3">'.$campo->descripcion.'</td>';
                      echo '<td class="col-md-1">'.$campo->nivel.'</td>';



                      // cambio de estatus
                      $url_cambiar_estado_usuario_id = "";
                      $title_cambiar_estatus = "";
                      $icono_cambiar_estatus = "";
                      $bandera ="";

                      // se agrega boton para activar y desactivar cuentas
                      if ( $campo->estatus == 0 || $campo->estatus == null  ) {
                          // desbloquear
                          $url_cambiar_estado_fh_id = $url_cambiar_estado_fh."/".$campo->id_info_adicional."/1";
                          $title_cambiar_estatus = "Activar";
                          $icono_cambiar_estatus = "fa fa-square-o";
                          $icon_color = "text-success";
                          $bandera ="disabled";

                      } else {
                          // bloquear
                          $url_cambiar_estado_fh_id = $url_cambiar_estado_fh."/".$campo->id_info_adicional."/0";
                          $title_cambiar_estatus = "Desactivar";
                          $icono_cambiar_estatus = "fa fa-check-square";
                          $icon_color = "text-success";
                          $bandera ="";

                      }
                      echo '<td class="col-md-1"><input type="text" class="form-control"  name="'.$campo->campo_adicional.'" id="'.$campo->campo_adicional.'"'.$bandera.' value ="'.$campo->flexheader.'" required></td>';
                      echo '<td><a href="'.$url_cambiar_estado_fh_id.'" class="'.$icon_color.'" title="'.$title_cambiar_estatus.'"><i class="'.$icono_cambiar_estatus.'"></i></a><td>';
                      

                      echo '</tr>';
                      $cont++;
                  }// para cada campo
                  ?>
                        
                       </table>
                     </div>
                   </div>
                </div>
                
                
                <?php
                } // si ya se tienen campos de busqueda
                ?>

                

              </div>

              <div class="form-group">
                <div class="row">
                    <div class="col-md-9">
                       <input type="submit" value="Guardar" class="btn btn-success pull-right">
                    </div> 
                    <div class="col-md-1">
                       <a href="<?php echo $url_anterior; ?>" class="btn btn-danger">Cancelar</a>
                    </div>                 
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
<!--                 <a href="<?php echo $url_anterior; ?>" class="btn btn-danger">Cancelar</a>
 -->              </div>
              <!-- /.box-footer-->
            </form>

          </div>
          
          <!-- /.box -->
        </div>



      </div>
      
 

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
    $(document).ready(function () {
        /* Elimina los espacios de cada input (excepto contraseña) cuando se
         * se van a enviar los elementos del formulario.
         * */
        $("form").submit(function () {
            $("form :input").each(function () {
                /* Si no es la contraseña se le quitan los espacios en blanco de inicio y fin */
                if (!$(this).is(":password")) {
                    $(this).val($.trim($(this).val()));
                }

            });
        });
    });
</script>



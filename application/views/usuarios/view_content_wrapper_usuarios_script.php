<script>
$( document ).ready(function() {

      <?php
      // si el login esta marcado como usar RFC, el correo de contacto se usa como correo para facturacion
      if ( $config_portal->usar_contrasena != CONTROL_ACCESO_SOLO_LOGIN_RFC ) {
      ?>
      // validacion de usuario
      $('#login').blur(function(){
	     var loginbuscado = $("#login").val();
	     
         jQuery.ajax({
             type: "POST",
             url: "<?php echo base_url(); ?>" + "index.php/login/verifica_disponibilidad_login",
             dataType: 'json',
             data: {login: loginbuscado},
             success: function(res) {
                if (res) {
                    if ( res.codigo == 0 ) {
                        $("#login").val(res.loginsugerido);
                        $("#mensaje_modal").text(res.mensaje);
                        $('#modal-login').on('hidden.bs.modal', function (e) {
                            $("#login").focus();
                            $("#btn_registrar").attr("disabled", true);
                        })
                        $('#modal-login').modal();
                    } else {
                        $("#btn_registrar").removeAttr("disabled");
                    }

                }
             }
         });
      });
      
          <?php
      }
      ?>
      

});
</script>



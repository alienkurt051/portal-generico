  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Lista
        <small>Lista de archivos contadores</small>
      </h1>

    </section>
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">

            <!-- /.box-header -->
            <!-- form start -->
            
              <div class="box-body">
              
                <div class="row">
                  <div class="col-md-12">
                        <?php
                        if ( isset($titulo) ) {
                        ?>
                          <div class="<?php echo $tipo_mensaje; ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                            <?php echo $mensaje; ?>
                          </div>
                        <?php
                        }
                        ?>
                  </div>
                </div>
                
                <?php echo form_open_multipart(base_url('index.php/archivos/do_upload'));?> 
            
                <form action = "" method = "" enctype="multipart/form-data">
                  <div class="row">
                    <div class="col-md-4">
                        <h4 class="box-title">Subir archivo manualmente</h4>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <input type = "file" name = "userfile" class="btn btn-default" /> 
                    </div>
                    <div class="col-md-2">
                      <input type = "submit" value = "Subir" class="btn btn-success" id="btn-subir" /> 
                    </div>
                  </div>
                   
                </form> <hr>
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="box-title">Lista de archivos en el sistema</h4>
                    </div>
                  </div>
                <hr>
              <script type="text/javascript">
                $(document).ready(function() {
                  $('#tabla_conteo').DataTable();

                });
              </script>
              <table id="tabla_conteo" class="table table-striped table-bordered table-condensed" style="width:100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>NOMBRE DEL ARCHIVO</th>
                      </tr>
                  </thead>
                  <tbody>
              <?php
                for($i=1;$i<=count($arr_archivos);$i++){
                  if($i>=3){
                  echo "<tr><td>";
                  echo "".$i;
                  echo "</td>";
                  echo "<td>";
                  echo "<a href='".base_url('index.php/archivos/do_download/'.$arr_archivos[$i].'')."'>".$arr_archivos[$i]."</a>";
                  echo "</td></tr>";
                  }
                }
               ?>
                  </tbody>
                </table>
          </div>
          
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<script src="<?php echo base_url()."assets/plugins/input-mask/jquery.inputmask.js"; ?>"></script>
<script src="<?php echo base_url()."assets/plugins/input-mask/jquery.inputmask.extensions.js"; ?>"></script>
<script src="<?php echo base_url()."assets/plugins/input-mask/jquery.inputmask.date.extensions.js"; ?>"></script>
<script src="<?php echo base_url()."assets/plugins/datepicker/bootstrap-datepicker.js"; ?>"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>

<script>

$(document).ready(function() {



   <?php
   // para cada campo de captura
   foreach ( $arr_campos_transaccion as $campo ) {
       // si es campo numerico o de texto
       if ( $campo->clave_tipo_dato == "date" ) {
       ?>
    //Date picker
    $('#<?php echo $campo->campo_adicional; ?>').datepicker({
        format: 'yyyy-mm-dd',
        language: "es",
        autoclose: true
    });
   <?php
     } // para campo que es fecha
   }
    ?>
    colocarHora();
});

function obtenerEntero(id_campo) {
	var elidCampo = "#" + id_campo;
    var texto = $(elidCampo).val();

	if ( texto != null && texto != "" ) {
		var texto_ajustado = parseInt(texto);
        
        $(elidCampo).val(texto_ajustado);
	}
}

function validaPatron(id_campo, patron) {
	var elidCampo = "#" + id_campo;
    var texto = $(elidCampo).val();
    var regex1 = RegExp(patron,'g');
    var str1 = texto;
    var array1;
	var texto_ajustado = "";
	
	if ( texto != null && texto != "" ) {
        while ((array1 = regex1.exec(str1)) !== null) {
          texto_ajustado = texto_ajustado + array1[0];
        }
        
        $(elidCampo).val(texto_ajustado);
	}
    
    

}
function colocarHora(){
    if ($("#HORA").length) {
        var timepicker = new TimePicker('HORA', {
          lang: 'pt',
          theme: 'blue-grey'
        });
        timepicker.on('change', function(evt) {
          var hora = evt.hour;
          if(hora<10){
            hora="0"+hora;
          }
          var value = (hora || '00') + ':' + (evt.minute || '00');
          evt.element.value = value;
        });
    }
}

</script>

<script>
  $(document).ready(function () {
    // Únicamente permite envíar el formulario una ocasión, después lo deshabilita
    $('form').submit(function () {
      $(this).find('button[type="submit"],input[type="submit"]').prop('disabled', true);
    });
  });
</script>

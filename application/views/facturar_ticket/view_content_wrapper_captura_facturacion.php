<!-- Content Wrapper. Contains page content 
INSERT INTO pss_c_tipo_dato (id_tipo_dato, clave_tipo_dato, d_tipo_dato) VALUES ('4', 'time', 'Hora');
Query para que funcione el input time
-->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Facturación de tickets
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Facturación</a></li>
      <li class="active">Captura datos del ticket</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

        <!-- general form elements -->
        <div class="box box-primary">
          <!-- /.box-header -->
          <!-- form start -->
            <div class="box-body">

              <div class="row">
                <div class="col-md-12">
                      <?php
                      if ( isset($titulo) ) {
                      ?>
                        <div class="<?php echo $tipo_mensaje; ?>">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                          <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                          <?php echo $mensaje; ?>
                        </div>
                      <?php
                      }
                      ?>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4">
                  <div class="row">
                    <div class="col-md-12">
                      <h4 class="box-title"><strong>Información fiscal</strong></h4>
                    </div>
                  </div>
                  <div class="row">
                    <!-- emisor -->
                    <div class="col-md-3">
                      <label>RFC</label>
                    </div>
                    <div class="col-md-9">
                      <?php echo $cliente->rfc; ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <Label>Nombre o razón social</label>
                    </div>
                    <div class="col-md-9">
                      <?php echo $cliente->cliente; ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <label>Envío de documentos a:</label>
                    </div>
                    <div class="col-md-9">
                      <?php echo $cliente->email; ?>
                    </div>
                  </div>

                </div>
                <div class="col-md-8">
                  <div class="row">
                    <!-- emisor -->
                    <div class="col-md-12">
                      <h4 class="box-title"><strong>Domicilio</strong></h4>
                    </div>
                  </div>


                  <div class="row">
                    <!-- emisor -->
                    <div class="col-md-1">
                      <label>Calle</label>
                    </div>
                    <div class="col-md-11">
                      <?php echo $cliente->calle; ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-1">
                      <label>Núm. exterior</label>
                    </div>
                    <div class="col-md-2">
                      <?php echo $cliente->numero_exterior; ?>
                    </div>
                    <div class="col-md-1">
                      <Label>Núm. interior</label>
                    </div>
                    <div class="col-md-2">
                      <?php echo $cliente->numero_interior; ?>
                    </div>
                    <div class="col-md-1">
                      <label>C. P.</label>
                    </div>
                    <div class="col-md-1">
                      <?php echo $cliente->codigo_postal; ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-1">
                      <label>Colonia</label>
                    </div>
                    <div class="col-md-11">
                      <?php echo $cliente->colonia; ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-1">
                      <label>Municipio</label>
                    </div>
                    <div class="col-md-5">
                       <?php echo $cliente->municipio; ?>
                    </div>
                    <div class="col-md-1">
                      <label>Localidad</label>
                    </div>
                    <div class="col-md-5">
                      <?php echo $cliente->localidad; ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-1">
                      <label>Estado</label>
                    </div>
                    <div class="col-md-2">
                      <?php echo $cliente->estado; ?>
                    </div>
                    <div class="col-md-1">
                      <label>País</label>
                    </div>
                    <div class="col-md-2">
                      <?php echo $cliente->pais; ?>
                    </div>
                  </div>
                </div>

            </div>
            <!-- /.box-body -->

        </div>

        <!-- /.box -->
      </div>


    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Datos de la transacción</h3>

      </div>
      <form class="form-horizonal" role="form" method="POST" action="<?php echo $url_vista_previa; ?>">
      <input type="hidden" name="id_cliente_para_facturar" id="id_cliente_para_facturar" value="<?php echo $cliente->id_cliente; ?>">
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
           <div class="form-group">
            <div class="row">
              <div class="col-md-12">
                Teclee aquí la información de su ticket de compra para obtener la factura
              </div>
            </div>
           </div>

             <div class="form-group">
                <div class="row">
                
                <div class="col-md-12">
                  <div class="form-group">
                      <div class="col-sm-2">

                        
                        <label class="control-label">Uso de CFDi</label>
                      </div>
                      <div class="col-sm-10">
                        <?php
                          if ( $config_portal->activar_elegir_uso_cfdi != 1 ) {
                            echo '<input class="form-control" type="text" name="uso_cfdi_elegido" id="uso_cfdi" readonly value="';
                            
                                foreach ( $arr_uso_cfdi as $uso_cfdi ) {
									
									// si se tiene uso de cfdi default se usa ese
									if ( $config_portal->uso_cfdi_default != null && $config_portal->uso_cfdi_default != "" ) {
                                        if( $uso_cfdi->id_uso_cfdi == $config_portal->uso_cfdi_default ) { 
                                            echo $uso_cfdi->id_uso_cfdi.' - '.$uso_cfdi->descripcion;
										}
                                    } else {
                                        if( $uso_cfdi->id_uso_cfdi == $uso_cfdi_elegido ) { 
                                            echo $uso_cfdi->id_uso_cfdi.' - '.$uso_cfdi->descripcion;
                                        }
									}									
                                } 
                            
                            echo '">';
                          }else{
                        ?>
                        <select name="uso_cfdi" id="uso_cfdi" class="form-control"  >
                        <?php
                        foreach ( $arr_uso_cfdi as $uso_cfdi ) {
							
                            // si se tiene uso de cfdi default se usa ese
                            if ( $config_portal->uso_cfdi_default != null && $config_portal->uso_cfdi_default != "" ) {

                                if ( $uso_cfdi->id_uso_cfdi == $config_portal->uso_cfdi_default ) {
                                    echo '<option value ="'.$uso_cfdi->id_uso_cfdi.'" selected>';
                                    echo $uso_cfdi->id_uso_cfdi.' - '.$uso_cfdi->descripcion;
                                    echo'</option>';
                                } else {
                                    echo '<option value ="'.$uso_cfdi->id_uso_cfdi.'">'.$uso_cfdi->id_uso_cfdi.' - '.$uso_cfdi->descripcion.'</option>';
                                }
								
								
                            } else {
                                // si es la misma
                                if ( $uso_cfdi->id_uso_cfdi == $uso_cfdi_elegido ) {
                                    echo '<option value ="'.$uso_cfdi->id_uso_cfdi.'" selected>';
                                    echo $uso_cfdi->id_uso_cfdi.' - '.$uso_cfdi->descripcion;
                                    echo'</option>';
                                } else {
                                    echo '<option value ="'.$uso_cfdi->id_uso_cfdi.'">'.$uso_cfdi->id_uso_cfdi.' - '.$uso_cfdi->descripcion.'</option>';
                                }
                            }		
							

                        }
                        ?>
                        </select>
                        <?php
                      }
                      ?>
                      </div>

                  </div>
                </div>
                

                    


                </div>
              </div>
           
           <div class="form-group">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Sucursal</label>
                    <div class="col-sm-10">
                      <select id="sucursal" name="sucursal" class="form-control" required>
                        <?php
                        foreach ($arr_entidades as $unaEntidad ) {
                          echo '<option value="'.$unaEntidad->id_entidad.'">'.$unaEntidad->entidad.'</option>';
                        }
                        ?>

                      </select>
                    </div>

                  </div>
                </div>
              </div>
            </div>

            <?php
            // para cada campo de captura
            if ( count($arr_campos_transaccion) > 0 && $arr_campos_transaccion != null ) {


                foreach ( $arr_campos_transaccion as $campo ) {
                    // si es campo numerico o de texto

                    if ($campo->clave_tipo_dato != "date" &&  $campo->clave_tipo_dato != "time") {

                    ?>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-12">

                      <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo $campo->etiqueta_flex_header; ?></label>
                        <div class="col-sm-10">
                          <input 
						  <?php
						  // si es tipo numerico
						  $input_type = "";
						  if ( $campo->clave_tipo_dato == "number" ) {
							  $input_type = "number";
						  } else {
							  $input_type = "text";
						  }
						  
						  echo 'type="'.$input_type.'"';
                          ?>						 
					      class="form-control" name="<?php echo $campo->campo_adicional; ?>" id="<?php echo $campo->campo_adicional; ?>" placeholder="<?php echo $campo->placeholder; ?>" 
						  <?php
						       // si es numerico
							   if ( $campo->clave_tipo_dato == "number" ) {
							       $expresion = "obtenerEntero('".$campo->campo_adicional."')";
							       echo 'onblur="'.$expresion.'"'; 
							   } else {
						           if ($campo->patron != null && $campo->patron != "" ){ 
							           $expresion = "validaPatron('".$campo->campo_adicional."','".$campo->patron."')";
							           echo 'onblur="'.$expresion.'"'; 
							       }								   
							   }
							   
						  
						  
 ?>
						  required>
                        </div>

                      </div>
                      
                    </div>
                  </div>
                </div>

                    <?php
                    }
                  else if($campo->clave_tipo_dato == "time") {
                    // campo de hora
                    //var_dump( $campo->etiqueta_flex_header);
                    ?>
                 <div class="form-group">
                   <div class="row">
                    <div class="col-md-12">

                      <label class="col-sm-2 control-label"><?php echo $campo->etiqueta_flex_header; ?></label>
                      <div class="col-sm-10">

                          <input type="text" class="form-control selector" id="<?php echo $campo->campo_adicional; ?>" name="<?php echo $campo->campo_adicional; ?>" required>

                      </div>

                    </div>
                  </div>
                </div>
                    <?php
                    }else {
                    // campo de fecha
                    ?>
                 <div class="form-group">
                   <div class="row">
                    <div class="col-md-12">

                      <label class="col-sm-2 control-label"><?php echo $campo->etiqueta_flex_header; ?></label>
                      <div class="col-sm-10">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" id="<?php echo $campo->campo_adicional; ?>" name="<?php echo $campo->campo_adicional; ?>" required>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
                    <?php
                    }
                }
            } // si hay campos definidos
            else {
                ?>
                <div class="row">
                  <div class="col-md-12">
                    <span>No se han configurado los campos para buscar transacción.</span>
                  </div>
                </div>
                <?php
            }
            ?>

            <hr>

            <?php
            // para cada campo de captura
            if ( count($arr_campos_fh) > 0 && $arr_campos_fh != null ) {

                foreach ( $arr_campos_fh as $campofh ) {
                    // si es campo numerico o de texto
                    if ($campofh->campo_adicional != null) {
            ?>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-12">

                            <div class="form-group">
                              <label class="col-sm-3 control-label"><?php echo $campofh->flexheader; ?></label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" name="<?php echo $campofh->campo_adicional."2"; ?>" id="<?php echo $campofh->campo_adicional."2"; ?>" placeholder="<?php echo $campofh->flexheader; ?>" >
                              </div>

                            </div>
                            
                          </div>
                        </div>
                      </div>

            <?php
                    }
                }
            }
            ?>
          </div>
          <div class="col-md-6">
            <!-- imagen guia -->
            <div class="row">
              <div class="col-sm-12">
                <img class="img-responsive" src="<?php echo $url_guia_ticket; ?>" alt="Guía de ticket">
                <h4>Ejemplo de comprobante impreso</h4>
              </div>
            </div>
          </div>

        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <input type="submit" class="btn btn-success" value="Continuar"> <a href="<?php echo $url_anterior; ?>" class="btn btn-danger">Cancelar</a>
      </div>
      </form>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->


    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

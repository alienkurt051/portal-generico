<!--Estilos para captcha-->
<link href="<?php echo base_url("assets/dist/css/captcha/captcha.css")?>" rel="stylesheet" >

<div class="box box-default box-solid" id="captcha">
    <div class="box-body">
        <div class="row">
            <div class="col-xs-7" id="captImg">
                <?php echo $captchaImg; ?>
            </div>
            <div class="col-xs-5" >
                <a href="javascript:void(0);" class="btn btn-app refreshCaptcha" data-toggle="tooltip" title="Actualizar captcha">
                    <i class="fa fa-rotate-left"></i> Actualizar Captcha
                </a>                
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <div class="row">
            <div class="col-xs-7">            
                <label for="pwd">Ingresar los caracteres mostrados en la imagen:</label>
            </div>
            <div class="col-xs-5">         
                <input type="text" name="captcha" class="form-control" autocomplete="off" required/>
            </div>
        </div>
    </div>
    <!-- /.box-footer -->
</div>
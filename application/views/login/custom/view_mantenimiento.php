<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><?php echo $config_portal->titulo_pantalla_principal; ?></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    

    <div class="row">
      <div class="col-xs-12">
	    <p class="login-box-msg"><h2>M A N T E N I M I E N T O</h2></p>
        <h3>Estimado Cliente:</h3>
		
		<p>Nos encontramos realizando tareas de mantenimiento y actualización a nuestro portal para brindarte un mejor servicio.</p>
		<br/><br/>
		<p>Agradecemos tu preferencia.</p>
      </div>
      <!-- /.col -->
    </div>


  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


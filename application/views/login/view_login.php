
<body>  
  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100">
        <div class="login100-form-title" style="background-image: url(<?php echo base_url("assets_custom/dist/img/bg-01.jpg"); ?>);">
          <span class="login100-form-title-1">
             <?php echo $config_portal->titulo_pantalla_principal; ?>
          </span>
        </div>
         <form class="login100-form validate-form" id="form_inicio_sesion" action="<?php echo $url_valida_inicio_sesion; ?>" method="post">
          <div style="color:red;"><?php if ( isset($titulo) ) { echo $mensaje; }?></div>
          <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
            <span class="label-input100">Usuario</span>
            <input type="text" id="login" name="login" class="input100" placeholder="Login" required>
            <span class="focus-input100"></span>
          </div>

          <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
            <span class="label-input100">Contraseña</span>
           <input type="password" id="contrasena" name="contrasena" class="input100" placeholder="Contraseña">
            <span class="focus-input100"></span>
          </div>

          <div class="flex-sb-m w-full p-b-30">
            <div class="contact100-form-checkbox">
              <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
              <label class="label-checkbox100" for="ckb1">
                Recordarme
              </label>
            </div>

            <div>
               <?php 
                // si el autodesbloqueo esta activo
                if ( $config_portal->activar_autodesbloqueo == 1 ) {
                    ?>
                <span><a href="<?php echo $url_recuperar_contrasena; ?>">Olvidé mi contraseña</a></span>
                <?php
                }// autodesbloqueo
                ?>
            </div>
          </div>

          <div class="container-login100-form-btn">
            <button type="submit"class="login100-form-btn">Aceptar</button>
            <?php
             // si el autoregistro esta activo
              if ( $config_portal->activar_autoregistro == 1 ) {
            ?>
             <span class="pull-right"><a href="<?php echo $url_crear_nueva_cuenta; ?>" class="text-center">Crear una cuenta nueva</a></span>
            <?php
              }// autoregistro */
            ?>
          </div>

            <?php
              // si se activo el captcha
              if ($config_portal->activar_captcha) {
                  $this->load->view($vista_captcha);
              }
            ?>
          <div style="background-color: white; margin-top: 20px">
            <span id="aviso_login"><?php echo $config_portal->aviso_login; ?></span>
          </div>
        </form>
      </div>
    </div>
  </div>



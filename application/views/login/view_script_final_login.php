<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
        /* Si se tiene activado el captcha en la configuración */
        <?php if ($config_portal->activar_captcha) { ?>
            let urlActualizarCaptcha = '<?php echo $url_actualizar_captcha ?>';
            actualizar_captcha(urlActualizarCaptcha);
            $('#captImg').find("img").addClass("img-responsive");
            $('[data-toggle="tooltip"]').tooltip();
        <?php } ?>

    });
    /* Permite actualizar el captcha si así se tiene configurado */
    function actualizar_captcha(urlActualizarCaptcha) {
        var captchaImagen = $('#captImg');
        $('.refreshCaptcha').on('click', function () {
            /* Hace una petición ajax y recibe una imagen de capcha */
            $.get(urlActualizarCaptcha)
                    .done(function (datosImagen) {  // Si la petición es correcta
                        /* Recibe la imagen de captcha y la coloca */
                        captchaImagen.html(datosImagen);
                        captchaImagen.find("img").addClass("img-responsive");
                    })
                    .fail(function () {
                        /* Si hay errores actualiza toda la página */
                        alert("Error al actualizar captcha");
                        location.reload();
                    });
        });
    }
</script>
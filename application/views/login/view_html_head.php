<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $config_portal->titulo_menu; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets_custom/dist/css/vendor/animate/animate.css"); ?>">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets_custom/dist/css/vendor/css-hamburgers/hamburgers.min.css"); ?>">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets_custom/dist/css/vendor/animsition/css/animsition.min.css"); ?>">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets_custom/dist/css/vendor/select2/select2.min.css"); ?>">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets_custom/dist/css/vendor/daterangepicker/daterangepicker.css"); ?>">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets_custom/dist/css/util.css"); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets_custom/dist/css/main.css"); ?>">
<!--===============================================================================================-->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
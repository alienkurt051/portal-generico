<!--===============================================================================================-->
	<script src="<?php echo base_url("assets_custom/dist/css/vendor/jquery/jquery-3.2.1.min.js"); ?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url("assets_custom/dist/css/vendor/animsition/js/animsition.min.js"); ?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url("assets_custom/dist/css/vendor/bootstrap/js/popper.js"); ?>"></script>
	<script src="<?php echo base_url("assets_custom/dist/css/vendor/bootstrap/js/bootstrap.min.js"); ?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url("assets_custom/dist/css/vendor/select2/select2.min.js"); ?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url("assets_custom/dist/css/vendor/daterangepicker/moment.min.js"); ?>"></script>
	<script src="<?php echo base_url("assets_custom/dist/css/vendor/daterangepicker/daterangepicker.js"); ?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url("assets_custom/dist/css/vendor/countdowntime/countdowntime.js"); ?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url("assets_custom/dist/js/main.js"); ?>"></script>
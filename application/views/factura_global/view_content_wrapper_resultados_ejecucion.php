	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Resultados de ejecucion de factura global
			</h1>
		</section>

		<!-- Main content -->
		<section class="content">

		 <div class="box">
			<div class="box-header">
			    <div class="row">
				   <div class="col-md-10">
				       <h3 class="box-title"></h3>
				   </div>
				   <div class="col-md-2 ">
				       <a href="<?php echo $url_anterior; ?>" class="btn btn-warning pull-right"> Regresar</a>
				   </div>
				</div>
				
				
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				
                <div class="row">
                  <div class="col-md-12">
                        <?php
                        if ( isset($titulo) ) {
                        ?>
                          <div class="<?php echo $tipo_mensaje; ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                            <?php echo $mensaje; ?>
                          </div>
                        <?php
                        }
                        ?>
                  </div>
                </div>
				
				<br/>

				
			
				 
				 					<!-- boton para agregar datos a la tabla peticiones_pendientes_sat -->
									 
				 <!-- /.box-body -->
				 <hr>
				 <h3>Resultados de ejecucion de factura global</h3>
                 <div class="row">
                    <div class="col-md-3">
                        <a href="<?php echo $url_pagina_anterior; ?>">Atras</a> <a href="<?php echo $url_pagina_siguiente; ?>">Siguiente</a>
                    </div>
                 </div>
				 <div class="row">
				    <div class="col-md-12 table-responsive">
						<table id="listado_comprobantes" class="table table-striped table-hover table-condensed">
							<thead>
								<tr>
									<th>#</th>
									<th>Ticket</th>
									<th>Fecha</th>
									<th>Subtotal</th>
									<th>Descuento</th>
									<th>IVA (16)</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
							<?php
							if ( count($arr_resultados) > 0 ) {
								
								$conteo = 0;
								foreach ($arr_resultados as $resultado) {
									$conteo++;

									echo '<tr>';
									echo '  <td>'.$conteo.'</td>';
									echo '  <td>'.$resultado["numero_ticket"].'</td>';
									echo '  <td>'.$resultado["fecha"].'</td>';
									echo '  <td>'.$resultado["subtotal"].'</td>';
									echo '  <td>'.$resultado["descuento"].'</td>';
									echo '  <td>'.$resultado["iva"].'</td>';
									echo '  <td>'.$resultado["total"].'</td>';
									
									echo '</tr>';

								}
							} else {
								echo '<tr>';
								echo '<td colspan="7">No se encontraron registros con los parámetros de búsqueda indicados. Intente de nuevo por favor con otros valores.</td>';
								echo '<tr>'; 
							}
			 
							?>
							</tbody>
						</table>


					</div>
				</div>
						<!-- /.box-body -->
			</div>

		</div>
		</section>
				
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->


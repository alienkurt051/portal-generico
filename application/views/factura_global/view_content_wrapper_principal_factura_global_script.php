<script>

  $(document).ready(function () {
    colocarFechas();
    eventosIniciales();
  });

  /**
   * Mensaje de error al usuario cuando algúna petición ajax falla
   * @type String
   */
  var mensajeError = ("Es posible que su sesión haya caducado, por favor "
            +"ingrese nuevamente y si el problema persiste comuníquese con el "
            +"administrador del portal.");
  /* 
   * Configuración para el rango de fechas 
   */
  function colocarFechas() {
    //Date picker
    $('#fecha_ejecucion').datepicker({
      format: 'yyyy-mm-dd',
      language: "es",
      autoclose: true
    });
  }

  /**
   * Crea eventos necesarios cuando se renderiza la página
   */
  function eventosIniciales() {
    eventosVerDetalles();
  }

  /**
   * Crea los eventos necesarios para el funcionamiento de "Ver detalles" de cada
   * ejecución de la factura global.
   */
  function eventosVerDetalles() {
    var modal = $('#modal-detalles-global');
    var btnDetalles = $('.detalles-global');
    var datosTablaDetalles = $('#datosTablaDetalles tr');
    // Se crea un evento click para cada botón de "Ver detalles"
    btnDetalles.each(function () {
      $(this).click(function (event) {
        event.preventDefault();  // Se detiene evento porque es un link
        // Se obtiene el id de la ejecución y de la entidad
        let ids = $.trim($(this).attr('id'));
        obtenerDetalles(ids);
        dispararModal(modal);
      });
    });
    // Cuando se oculta el modal
    modal.on("hidden.bs.modal", function () {
      // Borrar la tabla
      datosTablaDetalles.remove();
    });
  }

  /**
   * Muestra modal
   * 
   * @param {elementHtml} modal Selector a elemento modal
   */
  function dispararModal(modal) {
    modal.modal('show');
  }

  /**
   * Construye una tabla a partir de la respuesta de la petición previamente hecha
   * @param {json} respuesta Respuesta de tipo JSON de la petición en caso de que 
   *                         sea correcta
   */
  function crearTablaDetalles(respuesta) {
    var tabla = $('#tabla-detalles');
    var url_reporte = '<?php echo $url_descargar_reporte; ?>';
    tabla.empty();  // Vacía la tabla
    var thead = '<thead><tr>';
    // Se agregan los encabezados de la tabla
    thead += '<th>#</th>';
    $.each(respuesta.data[0], function (key, value) {
      thead += '<th>' + key + '</th>';
    });
    thead += '<th>Acciones</th>';
    thead += '</tr></thead>';
    // Se agregan los detalles
    var tbody = '<tbody class="table">';
    var contador = 1;
    $.each(respuesta.data, function (index, detalle) {
      tbody += '<tr>';
      tbody += '<td>' + contador + '</td>';
      $.each(detalle, function (key, valorDetalle) {
        tbody += '<td>' + valorDetalle + '</td>';
      });
      // Si se desea incluir links deben ser al final
      tbody += "<td style='text-align: center;'>" + '<a href="' + url_reporte + detalle['ID Global']
              + '" class="" title="Descargar reporte">'
              + '<i class="fa fa-file-excel-o" style="font-size:22px;color: green;"></i></a>'
              + "</td>" + '</tr>';
      contador = contador + 1;
    });
    tbody += '</tbody>';
    tfoot = thead.replace('thead', 'tfoot'); // Necesario para usar dataTables
    tabla.append(thead).append(tbody).append(tfoot);
    agregarDataTable();
  }

  /**
   * Hace una petición ajax para obtener los detalles de un archivo XML que entro
   * en la factura global
   * 
   * @param {string} datosPeticion id_emi_config_global | id_entidad
   */
  function obtenerDetalles(datosPeticion) {
    var url = '<?php echo $url_consultar_detalles; ?>';
    var loading = $('.overlay');
    loading.show();
    $.ajax({
      url: url,
      type: 'post',
      data: {datos: datosPeticion},
      dataType: 'json',
      success: function (response) {
        if (response.code === 200) {
          crearTablaDetalles(response);
        }
      },
      error: function (xhr) {
        alert(mensajeError);
      },
      complete: function () {
        loading.hide();
      }
    });
  }
  /**
   * Agrega la funcionalidad de la librería dataTables de jquery a la tabla, para
   * que funcione la búsqueda la tabla debe tener tfoot el cual debe ser el mismo
   * contenido que en thead.
   */
  function agregarDataTable() {
    var table = $('#tabla-detalles').DataTable({
      destroy: true, // Permite volver a agregar otra instancia en otra tabla
      paging: false,
      ordering: false,
      info: false,
      "language": {
        "search": "Buscar:"
      }
    });
    $('#tabla-detalles tfoot').hide();
    // Agregar el buscador
    table.columns().every(function () {
      var that = this;
      $('input', this.footer()).on('keyup change clear', function () {
        if (that.search() !== this.value) {
          that.search(this.value)
                  .draw();
        }
      });
    });
  }
</script>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Detalle de ejecución de factura global para <?php echo $entidad->entidad; ?>
			</h1>
		</section>

		<!-- Main content -->
		<section class="content">

		 <div class="box">
			<div class="box-header">
			    <div class="row">
				   <div class="col-md-10">
				       <h3 class="box-title">Detalle de ejecución de factura global</h3>
				   </div>
				   <div class="col-md-2 ">
				       <a href="<?php echo $url_anterior; ?>" class="btn btn-warning pull-right"> Regresar</a>
				   </div>
				</div>
				
				
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				
                <div class="row">
                  <div class="col-md-12">
                        <?php
                        if ( isset($titulo) ) {
                        ?>
                          <div class="<?php echo $tipo_mensaje; ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                            <?php echo $mensaje; ?>
                          </div>
                        <?php
                        }
                        ?>
                  </div>
                </div>
				
				<br/>
				<form id="formulario_consulta" method="post" action="<?php echo $url_alta_ejecucion; ?>">
				<div class="row">
				    <div class="col-md-12">
					    <div class="form-group">
					    	<div class="row">
					    		<div class="col-md-12">
					    			<div class="row">
					    				<div class="col-md-2">
					    					<h4>Tipo de ejecución</h4>
					    				</div>
					    				<div class="col-md-4">
					    					<input type="hidden" name="id_entidad" id="id_entidad" value="<?php echo $entidad->id_entidad; ?>">
                                            <input type="hidden" name="tipo_ejecucion" id="tipo_ejecucion" value="<?php echo $tipo_ejecucion; ?>">
                                            <strong><?php 
                                            
                                            switch ($tipo_ejecucion) {
                                                case 2:
                                                    echo "Mensual";
                                                    break;
                                                    
                                                case 5:
                                                    echo "Diaria";
                                                    break;
                                            }
                                            ?>
                                            </strong>
					    				</div>
					    				<div class="col-md-2">
					    					<h4>* Serie</h4>
					    				</div>
					    	            <div class="col-md-2">
                                            <select class="form-control" id="serie" name="serie" required>
					    			            <option value="" selected>--Serie--</option>
					    				        <?php
                                                foreach ($arr_series_entidad as $serie_entidad) {
                                                    echo '<option value="'.$serie_entidad->serie.'">'.$serie_entidad->serie.'</option>';
                                                }
                                                ?>
					    			        </select>
					    	            </div>
                                        
					    			</div>
					    		</div>
					    	</div>
					    </div>



					    <div class="form-group">
					    	<div class="row">
					    	    <div class="col-md-2">
					    	    	 <label>* Origen Número ticket</label>
					    	    </div>
					    	    <div class="col-md-2">
                                    <select class="form-control" id="origen_num_ticket" name="origen_num_ticket" required>
					    			    <option value="" selected>--Origen--</option>
					    				<option value="idtrx33">ID TRX33 (Clave STO)</option>
					    				<option value="idtrx_erp">ID Transacción ERP</option>
                                        <option value="flex_header">Flex Header</option>
					    			</select>
					    	    </div>
					    	    <div class="col-md-2">
					    	    	 <label>Flex Header</label>
					    	    </div>
					    	    <div class="col-md-2">
					    			<select class="form-control" id="origen_num_ticket_aux" name="origen_num_ticket_aux" disabled>
					    			    <option value="" selected>--Elige flex header--</option>
					    				<?php
                                        foreach ($arr_flex_headers as $flex_header) {
                                            echo '<option value="'.$flex_header->campo_adicional.'">'.$flex_header->descripcion.'</option>';
                                        }
                                        ?>

					    			</select>
					    	    </div>
					    	</div>
					    </div>
					    <div class="form-group">
					    	<div class="row">
					    	    <div class="col-md-2">
					    	    	 <label>* Tipo de factura</label>
					    	    </div>
					    	    <div class="col-md-2">
					    			<select class="form-control" id="tipo_global" name="tipo_global" required>
					    			    <option value="" selected>--Elige tipo--</option>
					    				<option value="0">Previa</option>
					    				<option value="1">Productiva</option>
					    			</select>
					    	    </div>
					    	</div>
					    </div>
                        
					    <div class="form-group">
					    	<div class="row">
					    	    <div class="col-md-12">
                        <?php // ejecucion mensual
                        if ( $tipo_ejecucion == 2 ) {
                            $conteo_mes = 1;
                            
                            echo '<table class="table table-responsive table-hover table-stripped table-condensed">';
                            echo '    <tr>';
                            echo '        <th>#</th>';
                            echo '        <th>Mes</th>';
                            echo '        <th>Fecha inicio</th>';
                            echo '        <th>Hora inicio</th>';
                            echo '        <th>Fecha fin</th>';
                            echo '        <th>Hora fin</th>';
                            echo '        <th>Fecha ejecución</th>';
                            echo '        <th>Hora Ejecución</th>';
//                            echo '        <th>Acciones</th>';
                            echo '    </tr>';
                            
                            
                            for ( $conteo_mes = 1; $conteo_mes <= 12; $conteo_mes++ ) {
                                
                                $continuar = false;
                                foreach($meses_elegidos as $mes_elegido) {
                                    if ( isset($arr_ejecucion[$conteo_mes]["mes_numero"]) && $mes_elegido == $arr_ejecucion[$conteo_mes]["mes_numero"] ) {
                                        $continuar = true;
                                        break;
                                    }
                                }
                                
                                if ( $continuar ) {
                                    echo '<tr>';
                                    echo '    <td>'.$conteo_mes.'</td>';
                                    echo '    <td><input type="hidden" id="id_mes_ejecucion" name="id_mes_ejecucion['.$conteo_mes.']" value="'.$conteo_mes.'" required readonly>'.$arr_ejecucion[$conteo_mes]["mes"].'</td>';
                                    echo '    <td><input type="date" id="fecha_inicio"     name="fecha_inicio['.$conteo_mes.']"     value="'.$arr_ejecucion[$conteo_mes]["fecha_inicio"].'"    required></td>';
                                    echo '    <td><input type="time" id="hora_inicio"      name="hora_inicio['.$conteo_mes.']"      value="'.$arr_ejecucion[$conteo_mes]["hora_inicio"].'"     required step="10"></td>';
                                    echo '    <td><input type="date" id="fecha_fin"        name="fecha_fin['.$conteo_mes.']"        value="'.$arr_ejecucion[$conteo_mes]["fecha_fin"].'"       required></td>';
                                    echo '    <td><input type="time" id="hora_fin"         name="hora_fin['.$conteo_mes.']"         value="'.$arr_ejecucion[$conteo_mes]["hora_fin"].'"        required step="10"></td>';
                                    echo '    <td><input type="date" id="fecha_ejecucion"  name="fecha_ejecucion['.$conteo_mes.']"  value="'.$arr_ejecucion[$conteo_mes]["fecha_ejecucion"].'" required></td>';
                                    echo '    <td><input type="time" id="hora_ejecucion"   name="hora_ejecucion['.$conteo_mes.']"   value="'.$arr_ejecucion[$conteo_mes]["hora_ejecucion"].'"  required></td>';
//                                    echo '    <td>Acciones</td>';
                                    echo '</tr>';
                                } else {
                                    continue;
                                }

                                                              
                            }
                            echo '</table>';
                        }
                        ?>
                        
                        <?php // ejecucion quincenal
                        if ( $tipo_ejecucion == 3 ) {
                            
                        }
                        
                        ?>

                        <?php // ejecucion semanal
                        if ( $tipo_ejecucion == 4 ) {
                            
                        }
                        
                        ?>
                        <?php // ejecucion diaria
                        if ( $tipo_ejecucion == 5 ) {
                            $conteo_dias = 1;
                            
                            echo '<table class="table table-responsive table-hover table-stripped table-condensed">';
                            echo '    <tr>';
                            echo '        <th>#</th>';
                            echo '        <th>Mes</th>';
                            echo '        <th>Fecha inicio</th>';
                            echo '        <th>Hora inicio</th>';
                            echo '        <th>Fecha fin</th>';
                            echo '        <th>Hora fin</th>';
                            echo '        <th>Fecha ejecución</th>';
                            echo '        <th>Hora Ejecución</th>';
//                            echo '        <th>Acciones</th>';
                            echo '    </tr>';
                            
                            $total_dias = count($arr_ejecucion);
                            for ( $conteo_dias = 1; $conteo_dias <= $total_dias; $conteo_dias++ ) {
                                
                                $continuar = false;
                                foreach($meses_elegidos as $mes_elegido) {
                                    if ( isset($arr_ejecucion[$conteo_dias]["mes_numero"]) && $mes_elegido == $arr_ejecucion[$conteo_dias]["mes_numero"] ) {
                                        $continuar = true;
                                        break;
                                    }
                                }
                                
                                if ( $continuar ) {
                                
                                
                                    echo '<tr>';
                                    echo '    <td>'.$conteo_dias.'</td>';
                                    echo '    <td><input type="hidden" id="id_mes_ejecucion" name="id_mes_ejecucion['.$conteo_dias.']" value="'.$conteo_dias.'" required readonly>'.$arr_ejecucion[$conteo_dias]["mes"].'</td>';
                                    echo '    <td><input type="date" id="fecha_inicio"     name="fecha_inicio['.$conteo_dias.']"     value="'.$arr_ejecucion[$conteo_dias]["fecha_inicio"].'"    required></td>';
                                    echo '    <td><input type="time" id="hora_inicio"      name="hora_inicio['.$conteo_dias.']"      value="'.$arr_ejecucion[$conteo_dias]["hora_inicio"].'"     required step="10"></td>';
                                    echo '    <td><input type="date" id="fecha_fin"        name="fecha_fin['.$conteo_dias.']"        value="'.$arr_ejecucion[$conteo_dias]["fecha_fin"].'"       required></td>';
                                    echo '    <td><input type="time" id="hora_fin"         name="hora_fin['.$conteo_dias.']"         value="'.$arr_ejecucion[$conteo_dias]["hora_fin"].'"        required step="10"></td>';
                                    echo '    <td><input type="date" id="fecha_ejecucion"  name="fecha_ejecucion['.$conteo_dias.']"  value="'.$arr_ejecucion[$conteo_dias]["fecha_ejecucion"].'" required></td>';
                                    echo '    <td><input type="time" id="hora_ejecucion"   name="hora_ejecucion['.$conteo_dias.']"   value="'.$arr_ejecucion[$conteo_dias]["hora_ejecucion"].'"  required></td>';
//                                    echo '    <td>Acciones</td>';
                                    echo '</tr>';
                                } else {
                                    continue;
                                }
                                                              
                            }
                            echo '</table>';
                        }
                        
                        ?>
                   
					    	    </div>
					    	</div>
					    </div>
                   
					    <div class="form-group">
					    	<div class="row">
					    		<div class="col-md-12">
                                
					    			<input type="submit" value="Agregar ejecución" id="Buscar" name="btn_procesar_solicitud" class="btn btn-success pull-right"/>
					    		</div>
					    	</div>
					    </div>

				    </div>
				</div>
				</form>

				 <!-- /.box-body -->
			</div>

		</div>
		</section>
				
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->


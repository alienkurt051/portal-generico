<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Configuración de Factura Global
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="box">
      <div class="box-header">
        <div class="row">
          <div class="col-sm-9">
            <h3 class="box-title">En esta ventana puedes consultar y programar la ejecución del programa que genera la factura global por Emisor/sucursal.</h3>
          </div>
          <div class="col-sm-3 ">
            <a href="<?php echo $url_agregar_ejecucion_global; ?>" 
               class="btn btn-success pull-right">
              <i class="fa fa-plus"></i> Agregar ejecución
            </a>
          </div>
        </div>


      </div>
      <!-- /.box-header -->
      <div class="box-body">

        <div class="row">
          <div class="col-sm-12">
            <?php
            if (isset($titulo)) {
              ?>
              <div class="<?php echo $tipo_mensaje; ?>">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                <?php echo $mensaje; ?>
              </div>
              <?php
            }
            ?>
          </div>
        </div>

        <br/>
        <form id="formulario_consulta" method="post" action="<?php echo $url_consultar_ejecucion; ?>">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <div class="row">
                  <label class="control-label col-sm-2" for="id_entidad">* Emisor</label>
                  <div class="col-sm-4">
                    <select id="id_entidad" name="id_entidad" class="form-control" required>
                      <?php
                      foreach ($arr_emisores as $emisor) {
                        echo '<option value="' . $emisor->id_entidad . '">'
                        . $emisor->rfc . " - " . $emisor->entidad . '</option>';
                      }
                      ?>
                    </select>
                  </div>
                  <label class="control-label col-sm-2" for="id_forma_pago">Forma de pago</label>
                  <div class="col-sm-4">
                    <select id="id_forma_pago" name="id_forma_pago" class="form-control" required>
                      <?php
                      foreach ($arr_formas_pago as $forma) {
                        echo '<option value="' . $forma->id_forma_pago . '">'
                        . "($forma->id_forma_pago) $forma->descripcion" . '</option>';
                      }
                      ?>
                      <option value="" selected>Todas</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="control-label col-sm-2" for="tipo_ejecucion">Tipo de ejecución</label>
                  <div class="col-sm-4">
                    <select class="form-control" id="tipo_ejecucion" name="tipo_ejecucion" >
                      <option value="" selected>Todas</option>
                      <option value="2">Mensual</option>
                      <option value="3">Quincenal</option>
                      <option value="4">Semanal</option>
                      <option value="5">Diario</option>
                    </select>
                  </div>
                  <label class="control-label col-sm-2" for="tipo_ejecucion">Fecha</label>
                  <div class="form-group col-sm-4">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input name="fecha_ejecucion" type="text" class="form-control pull-right" 
                             id="fecha_ejecucion" placeholder="Fecha" autocomplete="off"/>
                    </div>
                  </div>    
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-12">
                    <input type="submit" value="Consultar" id="btn_consultar" 
                           name="btn_consultar" class="btn btn-primary pull-right"/>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </form>



        <!-- boton para agregar datos a la tabla peticiones_pendientes_sat -->

        <!-- /.box-body -->

        <?php
        // si ya se hizo una busqueda
        if (isset($arr_ejecucion_global)) {
          ?>
          <hr>
          <h3>Configuración de ejecución de proceso</h3>
          <div class="row">
            <div class="col-sm-12 table-responsive">
              <table id="listado_comprobantes" class="table table-striped table-hover table-condensed">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Entidad</th>
                    <th>Serie</th>
                    <th>Tipo ejecución</th>
                    <th>Fecha/hora inicio<br>Fecha/hora fin</th>
                    <th>Fecha/hora programada<br>de ejecución</th>
                    <th>Origen núm. ticket</th>
                    <th>Tipo Global</th>
                    <th>Estatus</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (count($arr_ejecucion_global) > 0) {

                    $conteo = 0;
                    foreach ($arr_ejecucion_global as $ejecucion) {
                      $conteo++;

                      echo '<tr>';
                      echo '  <td>' . $conteo . '</td>';
                      echo '  <td>' . $ejecucion->id_entidad . " - "
                      . $ejecucion->entidad . '</td>';
                      echo '  <td>' . $ejecucion->serie . '</td>';
                      echo '  <td>' . $ejecucion->d_tipo_ejecucion . '</td>';
                      echo '  <td>' . $ejecucion->hora_inicio . '<br>'
                      . $ejecucion->hora_fin . '</td>';

                      // se calcula la fecha de ejecucion
                      echo '  <td>' . $ejecucion->fecha_ejecucion . '<br>'
                      . $ejecucion->hora_ejecucion . '</td>';
                      echo '  <td>' . $ejecucion->origen_num_ticket . '<br>'
                      . $ejecucion->origen_num_ticket_aux . '</td>';

                      // si es factura previa
                      if ($ejecucion->tipo_global == 0) {
                        echo '  <td>' . $ejecucion->d_tipo_global . '<br>('
                        . $ejecucion->id_emi_config_global . ')</td>';
                      } else {

                        echo '  <td class="bg-aqua-active">' . $ejecucion->d_tipo_global
                        . '<br>(' . $ejecucion->id_emi_config_global . ')</td>';
                      }


                      $estilo = null;
                      switch ($ejecucion->estatus) {
                        case 1:
                          $estilo = "bg-green-active";
                          break;

                        case 2:
                          $estilo = "bg-yellow-active";
                          break;

                        case 100:
                          $estilo = "bg-red-active";
                          break;
                      }


                      if ($estilo == null) {
                        echo '  <td>' . $ejecucion->d_estatus . '</td>';
                      } else {
                        echo '  <td class="' . $estilo . '">' . $ejecucion->d_estatus . '</td>';
                      }

                      $url_cancelar_ejecucion_por_id = $url_cancelar_ejecucion
                              . "/" . $ejecucion->id_emi_config_global;
                      $url_ver_resultados_ejecucion_por_id = $url_ver_resultados_ejecucion
                              . "/" . $ejecucion->id_emi_config_global . "/" . $ejecucion->tipo_global;
                      $url_ver_cifras_control_por_id = $url_ver_cifras_control
                              . "/" . $ejecucion->id_emi_config_global . "/" . $ejecucion->tipo_global;

                      // botones de acciones
                      // si aun no ejecuta la global, se pone boton para cancelar
                      if ($ejecucion->estatus == 0) {
                        echo '  <td><a href="' . $url_cancelar_ejecucion_por_id
                        . '" title="Cancelar ejecución"><i class="fa fa-times text-danger"></i></a>';
                      } else {
                        if ($ejecucion->estatus != 100) {
                          // si ya esta ejecutado, se pone el boton para ver resultados
                          echo '  <td><a href="' . $url_ver_resultados_ejecucion_por_id
                          . '"><i class="fa fa-file-excel-o" title="Reporte excel"></i></a>';
                          echo $ejecucion->tipo_global != 0 ? // si está ejecutada y no es previa
                          '<a href="#" class="detalles-global" id="' . $ejecucion->id_emi_config_global
                          . '|' . $ejecucion->id_entidad . '">'
                          . '<i class="fa fa-eye" title="Ver detalle"></i></a>': '';
                        }
                      }
                      echo '</td>';

                      echo '</tr>';
                    }
                  } else {
                    echo '<tr>';
                    echo '<td colspan="8">No se encontraron registros con los '
                    . 'parámetros de búsqueda indicados. Intente de nuevo por favor con otros valores.</td>';
                    echo '<tr>';
                  }
                }
                ?>
              </tbody>
            </table>


          </div>
        </div>
        <?php $this->load->view('factura_global/view_content_wrapper_modal_factura_global'); ?>   
        <!-- /.box-body -->
      </div>

    </div>
  </section>

  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


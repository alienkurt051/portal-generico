	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Agregar ejecución de generación de Factura Global
			</h1>
		</section>

		<!-- Main content -->
		<section class="content">

		 <div class="box">
			<div class="box-header">
			    <div class="row">
				   <div class="col-md-10">
				       <h3 class="box-title">En esta ventana se agrega ejecución de factura global.</h3>
				   </div>
				   <div class="col-md-2 ">
				       <a href="<?php echo $url_anterior; ?>" class="btn btn-warning pull-right"> Regresar</a>
				   </div>
				</div>
				
				
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				
                <div class="row">
                  <div class="col-md-12">
                        <?php
                        if ( isset($titulo) ) {
                        ?>
                          <div class="<?php echo $tipo_mensaje; ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                            <?php echo $mensaje; ?>
                          </div>
                        <?php
                        }
                        ?>
                  </div>
                </div>
				
				<br/>
				<form id="formulario_agregar_ejecucion" method="post" action="<?php echo $url_detalle_ejecucion; ?>">
				<div class="row">
				    <div class="col-md-12">
					    <div class="form-group">
					    	<div class="row">
					    		<div class="col-md-12">
					    			<div class="row">
					    				<div class="col-md-2">
					    					<h4>* Emisor</h4>
					    				</div>
					    				<div class="col-md-4">
					    					<select id="id_entidad" name="id_entidad" class="form-control" required>
					    					   <option value="" selected>--Elige un RFC emisor--</option>
					    					   <?php
					    					   foreach ($arr_emisores as $emisor) {
                                                   $descripcion_entidad = $emisor->rfc." - ".$emisor->entidad;
                                                   
                                                   if ( $emisor->id_tipo_entidad == 1 ) {
                                                       // emisor
                                                       $descripcion_entidad .= " (emisor)";
                                                   } else  {
                                                       // sucursal
                                                       $descripcion_entidad .= " (sucursal)";
                                                   }
                                                   
					    						   echo '<option value="'.$emisor->id_entidad.'">'.$descripcion_entidad.'</option>';
					    					   }
					    					   /*
					    					   ?>
                                               <option value="todose">Todos los emisores (SIN SUCURSALES)</option>
                                               <option value="todass">Todas las sucursales (SIN EMISORES)</option>
                                               <option value="todas">Todos los emisores y sucursales</option>
                                               */
                                               ?>
					    					</select>
					    				</div>
					    			</div>
					    		</div>
					    	</div>
					    </div>
					    <div class="form-group">
					    	<div class="row">
					    	    <div class="col-md-2">
					    	    	 <label>* Tipo de ejecución</label>
					    	    </div>
					    	    <div class="col-md-2">
					    			<select class="form-control" id="tipo_ejecucion" name="tipo_ejecucion" required>
					    			    <option value="" selected>--Elige rango de selección de tickets--</option>
					    				<?php
                                        /*
                                        <option value="1">Rango de fecha</option>
                                        */
                                        ?>
					    				<option value="2">Mensual</option>
                                        <?php
                                        /*
                                        <option value="3">Quincenal</option>
                                        <option value="4">Semanal</option>
                                        */
                                        ?>
                                        <option value="5">Diario</option>
					    			</select>
					    	    </div>
					    	    <div class="col-md-2">
					    	    	 <label>* Hora de ejecución</label>
					    	    </div>
					    	    <div class="col-md-2">
                                    <input type="time" id="hora_ejecucion" name="hora_ejecucion" value="23:00" required class="form-control" >
					    	    </div>
					    	    <div class="col-md-2">
					    	    	 <label>Dia de ejecución</label>
					    	    </div>
					    	    <div class="col-md-2">
                                    <select id="dia_ejecucion" name="dia_ejecucion" class="form-control">
                                        <option value="">--Opcional--</option>
                                        <option value="md">Mismo día</option>
                                        <option value="sd">Siguiente día</option>
                                        <?php
                                        for($i = 2; $i <=30; $i++) {
                                            echo '<option value="'.$i.'">'.$i.' días después del siguiente mes</option>';
                                        }
                                        ?>
                                    </select>
					    	    </div>
                            </div>
                        </div>
					    <div class="form-group">
					    	<div class="row">
					    	    <div class="col-md-10">
					    	    	 <label>Elige para qué meses aplica la configuración</label>
					    	    </div>
                                <div class="col-md-2">
                                   <a href="javascript:void(0)" id="seleccionar_todos" class="btn btn-default">Todos</a> <a href="javascript:void(0)" id="seleccionar_ninguno" class="btn btn-default">Ninguno</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
					    	    <div class="col-md-12">
                                    <table class="table table-condensed table-hover table-stripped table-responsive">
                                        <tr>
                                           <th>#</th>
                                           <th>Mes</th>
                                           <th>Elegir</th>
                                        </tr>
                                        <tr>
                                           <td>1</td>
                                           <td>Enero</td>
                                           <td><input type="checkbox" id="mes_1" name="mes[1]" value="1"></td>
                                        </tr>
                                        <tr>
                                           <td>2</td>
                                           <td>Febrero</td>
                                           <td><input type="checkbox" id="mes_2" name="mes[2]" value="2"></td>
                                        </tr>
                                        <tr>
                                           <td>2</td>
                                           <td>Marzo</td>
                                           <td><input type="checkbox" id="mes_3" name="mes[3]" value="3"></td>
                                        </tr>
                                        <tr>
                                           <td>4</td>
                                           <td>Abril</td>
                                           <td><input type="checkbox" id="mes_4" name="mes[4]" value="4"></td>
                                        </tr>
                                        <tr>
                                           <td>5</td>
                                           <td>Mayo</td>
                                           <td><input type="checkbox" id="mes_5" name="mes[5]" value="5"></td>
                                        </tr>
                                        <tr>
                                           <td>6</td>
                                           <td>Junio</td>
                                           <td><input type="checkbox" id="mes_6" name="mes[6]" value="6"></td>
                                        </tr>
                                        <tr>
                                           <td>7</td>
                                           <td>Julio</td>
                                           <td><input type="checkbox" id="mes_7" name="mes[7]" value="7"></td>
                                        </tr>
                                        <tr>
                                           <td>8</td>
                                           <td>Agosto</td>
                                           <td><input type="checkbox" id="mes_8" name="mes[8]" value="8"></td>
                                        </tr>
                                        <tr>
                                           <td>9</td>
                                           <td>Septiembre</td>
                                           <td><input type="checkbox" id="mes_9" name="mes[9]" value="9" ></td>
                                        </tr>
                                        <tr>
                                           <td>10</td>
                                           <td>Octubre</td>
                                           <td><input type="checkbox" id="mes_10" name="mes[10]" value="10"></td>
                                        </tr>
                                        <tr>
                                           <td>11</td>
                                           <td>Noviembre</td>
                                           <td><input type="checkbox" id="mes_11" name="mes[11]" value="11"></td>
                                        </tr>
                                        <tr>
                                           <td>12</td>
                                           <td>Diciembre</td>
                                           <td><input type="checkbox" id="mes_12" name="mes[12]" value="12"></td>
                                        </tr>
                                    </table>
					    	    </div>
                            </div>
                        </div>
					    <div class="form-group">
					    	<div class="row">
					    		<div class="col-md-12">
                                
					    			<input type="submit" value="Definir parámetros" id="btn_definir_parametros" name="btn_definir_parametros" class="btn btn-success pull-right"/>
					    		</div>
					    	</div>
					    </div>

				    </div>
				</div>
				</form>

				 <!-- /.box-body -->
			</div>

		</div>
		</section>
				
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->


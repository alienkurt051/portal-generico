<script>
function agrega_quitar_propiedad(campo, propiedad, agregar_propiedad) {
    var elcampo = "#" + campo;
    
    if ( agregar_propiedad ) {
        $(elcampo).prop(propiedad,true);
    } else {
        $(elcampo).prop(propiedad,false);
    }
}

function f_cambiar_cond_ejecucion() {
    var cond_ejecucion = $("#cond_ejecucion").val();
    
    if ( cond_ejecucion == 1 ) {
        agrega_quitar_propiedad("dia_ejecucion", 'required', true);
        agrega_quitar_propiedad("dia_ejecucion", 'readonly', false);
    } else {
        agrega_quitar_propiedad("dia_ejecucion", 'required', false);
        agrega_quitar_propiedad("dia_ejecucion", 'readonly', true);

    }
}

function f_cambiar_origen_ticket() {
    var origen_ticket = $("#origen_num_ticket").val();
    
    if ( origen_ticket == "flex_header" ) {
        agrega_quitar_propiedad("origen_num_ticket_aux", 'required', true);
        agrega_quitar_propiedad("origen_num_ticket_aux", 'disabled', false);
    } else {
        agrega_quitar_propiedad("origen_num_ticket_aux", 'required', false);
        agrega_quitar_propiedad("origen_num_ticket_aux", 'disabled', true);

    }
}

function f_valida_mes_elegido() {
    var conteo;
    var mes;
    var elegidos = 0;
    
    for (conteo = 1; conteo <= 12; conteo++)
    {
        mes = "#mes_" + conteo;
        if ( $(mes).prop("checked") == true ) {
            elegidos++;
        }
    }
    
    if ( elegidos < 1 ) {
        alert("Debes elegir al menos un mes");   
        return false;
    }
    
    return true;
}


$( document ).ready(function() {
    $( "#cond_ejecucion" ).change(function() {
      f_cambiar_cond_ejecucion();
    });
    
    $( "#origen_num_ticket" ).change(function() {
      f_cambiar_origen_ticket();
    });
    
    $( "#formulario_agregar_ejecucion" ).submit(function (){
        if ( !f_valida_mes_elegido() ) {
            return false;
        }
    });
    
    $("#seleccionar_todos").click(function(){
        var conteo;
        var mes;
        
        for (conteo = 1; conteo <= 12; conteo++)
        {
            mes = "#mes_" + conteo;
            $(mes).prop( "checked", true );
        }
            
        
    });

    $("#seleccionar_ninguno").click(function(){
        var conteo;
        var mes;
        
        for (conteo = 1; conteo <= 12; conteo++)
        {
            mes = "#mes_" + conteo;
            $(mes).prop( "checked", false );
        }
    });

});

</script>
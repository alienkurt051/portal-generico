<!-- Modal editar propietario -->
<div class="modal fade" id="modal-detalles-global" role="dialog">
  <div class="modal-dialog modal-lg">    
    <!-- Modal content-->                
    <div class="row">
      <div class="col-md-12">
        <div class="box box-solid container">
          <div class="box-header with-border">
            <h4>Detalles factura global</h4>
          </div>
          <!-- /.box-header --> 
            <div class="col-sm-12 table-responsive">
              <table id="tabla-detalles" class="table table-striped table-hover table-condensed">
                <thead>
                  <tr>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          <div class="overlay" style="display: none;">
            <i class="fa fa-refresh fa-spin"></i>
          </div>                
        </div>
        <!-- /.box -->
      </div>
      <!-- ./col -->
    </div>
  </div>
</div>

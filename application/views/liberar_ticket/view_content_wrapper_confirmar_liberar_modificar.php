<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Liberación de Tickets
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#"><i class="fa fa-dashboard"></i> Liberación de Tickets
                </a>
            </li>
            <li class="active">Confirmar operación</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Confirme los datos de su transacción</h3>
            </div>
            <?php
            echo '<form id="form_editar_liberar_ticket" method="post" action="' . 
                    $url_liberar_ticket . '">';
            ?>
            <input type="hidden" name="id_trx33" id="id_trx33" 
                   value="<?php echo $transaccion->id_trx33_r; ?>">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        if (isset($titulo)) {
                        ?>
                            <div class="<?php echo $tipo_mensaje; ?>">
                                <button type="button" class="close" data-dismiss="alert" 
                                        aria-hidden="true">×</button>
                                <h4>
                                    <i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?>
                                </h4>
                                <?php echo $mensaje; ?>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">          
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Condiciones de pago y datos del encabezado</h4>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="control-label">Método de pago</label>
                                </div>
                                <div class="col-sm-4">
                                    <?php
                                    echo '<input type="text" '
                                    . 'name="metodo_pago" id="metodo_pago" '
                                    . 'class="form-control" value="'.$transaccion->id_metodo_pago
                                    . '" readonly>';
                                    ?>
                                </div>
                                <div class="col-sm-2">
                                    <label class="control-label">Forma de pago</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" name="forma_pago" 
                                           id="forma_pago" class="form-control" 
                                           <?php echo 'value ="' . $transaccion->id_forma_pago . '"';?> 
                                           readonly />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="control-label">Uso de CFDi</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" value="<?php echo $transaccion->uso_cfdi; ?>" 
                                           class="form-control" readonly />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="control-label">Moneda</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" value="<?php echo $transaccion->id_moneda; ?>" 
                                           class="form-control" readonly />
                                </div>
                                <div class="col-sm-2">
                                    <label class="control-label">Tipo cambio</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" value="<?php echo $transaccion->tipo_cambio; ?>" 
                                           class="form-control" readonly />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Conceptos</h4>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">  
                            <table class="table table-hover table-stripped table-condensed text-center">
                                <tr>
                                    <th>#</th>
                                    <th>CveProdServ<br>Num.Ident.</th>
                                    <th width="5%">Cantidad</th>
                                    <th>Unidad</th>
                                    <th>Descripción</th>
                                    <th width="8%">P.U.</th>
                                    <th width="8%">Importe</th>
                                    <th>Descuento</th>
                                </tr>
                                <?php
                                $cont = 1;
                                foreach ($conceptos as $concepto) {
                                    $id_linea = "linea" . $cont;
                                    echo '<tr id="' . $id_linea . '">';
                                    $numero_linea = "numero_linea" . $cont;
                                    echo '<td>' . $concepto->id_trx33_concepto_r . '</td>';
                                    echo '<td>' . $concepto->id_claveprodserv . '<br>' 
                                            . $concepto->numero_identificacion . '</td>';
                                    echo '<td>' . $concepto->cantidad . '</td>';                                   
                                    echo '<td>' . $concepto->id_clave_unidad . '<br>' 
                                            . $concepto->unidad . '</td>';
                                    echo '<td>' . $concepto->descripcion . '</td>';
                                    echo '<td>' . $concepto->valor_unitario . '</td>';
                                    echo '<td>' . $concepto->importe . '</td>';
                                    echo '<td>' . number_format($concepto->descuento, 2) . '</td>';
                                    echo '</tr>';
                                    $cont++;
                                }
                                ?>
                            </table>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-6">
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label">Subtotal</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="id_subtotal" 
                                                   value="<?php echo $transaccion->subtotal; ?>" 
                                                   class="form-control text-right" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-6">
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label">Descuento</label>
                                        <div class="col-sm-6">
                                            <input type="text" 
                                                   value="<?php echo number_format($transaccion->descuento, 2); ?>" 
                                                   class="form-control text-right" readonly />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-6">
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label">Impuestos retenidos</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="id_impuestos_retenidos" 
                                                   value="<?php echo $transaccion->totalImpuestosRetenidos; ?>" 
                                                   class="form-control text-right" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-6">
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label">Impuestos trasladados</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="id_impuestos_trasladados" 
                                                   value="<?php echo $transaccion->totalImpuestosTrasladados; ?>" 
                                                   class="form-control text-right" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-6">
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label">Total</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="id_total" 
                                                   value="<?php echo $transaccion->total; ?>" 
                                                   class="form-control text-right" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <?php
                // si el ticket fue cancelado con antelacion
                if ($transaccion->id_receptor == -1) {
                ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <span class="bg-red">ATENCIÓN: -- El ticket ha sido cancelado con anterioridad. Puede usar el botón liberar para poder usarlo nuevamente --</span>
                        </div>
                    </div>
                <?php
                }
                ?>
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        $url_liberar_ticket .= "/" . $transaccion->id_trx33_r;
                        echo '<input class="btn btn-success" type="submit" '
                        . 'value="Liberar ticket"/>';
                        ?>
                        <a href="<?php echo $url_anterior; ?>" class="btn btn-warning">Regresar</a>
                    </div>
                </div>
            </div>
            <!-- /.box-footer-->
            </form>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
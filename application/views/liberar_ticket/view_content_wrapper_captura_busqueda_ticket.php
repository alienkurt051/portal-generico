<!-- Content Wrapper -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Liberación de Tickets
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Liberación de Tickets</a></li>
            <li class="active">Buscar ticket</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Datos de la transacción a buscar</h3>
            </div>
            <form class="form-horizonal" role="form" method="POST" action="<?php echo $url_vista_previa; ?>">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php
                            if (isset($titulo)) {
                                ?>
                                <div class="<?php echo $tipo_mensaje; ?>">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                    <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                                    <?php echo $mensaje; ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        Teclee aquí la información del ticket que desea liberar
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Sucursal</label>
                                            <div class="col-sm-9">
                                                <select id="sucursal" name="sucursal" class="form-control" required>
                                                    <?php
                                                    foreach ($arr_entidades as $unaEntidad) {
                                                        echo '<option value="' . $unaEntidad->id_entidad . '">' . $unaEntidad->entidad . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            // para cada campo de captura
                            if (count($arr_campos_transaccion) > 0 && $arr_campos_transaccion != null) {
                                foreach ($arr_campos_transaccion as $campo) {
                                    // si es campo numerico o de texto
                                    if ($campo->clave_tipo_dato != "date" && $campo->clave_tipo_dato != "time") {
                            ?>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?php echo $campo->etiqueta_flex_header; ?></label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" 
                                                                   name="<?php echo $campo->campo_adicional; ?>" 
                                                                   id="<?php echo $campo->campo_adicional; ?>" 
                                                                   placeholder="<?php echo $campo->placeholder; ?>" 
                                                                   maxlength="128" required />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            <?php   // campo de fecha
                                    } else if ($campo->clave_tipo_dato == "time") {
                            ?>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12">

                                                    <label class="col-sm-2 control-label"><?php echo $campo->etiqueta_flex_header; ?></label>
                                                    <div class="col-sm-10">

                                                        <input type="text" class="form-control selector" 
                                                               id="<?php echo $campo->campo_adicional; ?>" 
                                                               name="<?php echo $campo->campo_adicional; ?>" 
                                                               placeholder="hh:mm" maxlength="5" required />

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    } else { // campo de fecha
                                    ?>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label class="col-sm-2 control-label"><?php echo $campo->etiqueta_flex_header; ?></label>
                                                    <div class="col-sm-10">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" 
                                                                   id="<?php echo $campo->campo_adicional; ?>" 
                                                                   name="<?php echo $campo->campo_adicional; ?>" 
                                                                   placeholder="yyyy-mm-dd" maxlength="10" required />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            <?php
                                    }
                                }
                            } else { // si no hay campos definidos
                            ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <span>No se han configurado los campos para buscar transacción.</span>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <input type="submit" class="btn btn-success" value="Buscar"> 
                    <a href="<?php echo $url_anterior; ?>" class="btn btn-danger">Cancelar</a>
                </div>
            </form>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script src="<?php echo base_url() . "assets/plugins/input-mask/jquery.inputmask.js"; ?>"></script>
<script src="<?php echo base_url() . "assets/plugins/input-mask/jquery.inputmask.extensions.js"; ?>"></script>
<script src="<?php echo base_url() . "assets/plugins/input-mask/jquery.inputmask.date.extensions.js"; ?>"></script>
<script src="<?php echo base_url() . "assets/plugins/datepicker/bootstrap-datepicker.js"; ?>"></script>
<script src="http://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<link href="http://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>

<script>

    $(document).ready(function () {        
        <?php
        // para cada campo de captura
        foreach ($arr_campos_transaccion as $campo) {
            // Coloca elemento datepicker si lo hay 
            if ($campo->clave_tipo_dato == "date") {
        ?>
                colocarDatePicker('<?php echo $campo->campo_adicional; ?>');
        <?php
            }
            // Coloca elemento timepicker si lo hay 
            if ($campo->clave_tipo_dato == "time") {
        ?>
                colocarTimePicker('<?php echo $campo->campo_adicional; ?>');
        <?php
            }
        }
        ?>
    });

    /* Coloca timepicker a un elemento por su id */
    function colocarTimePicker(id){
        if ($("#" + id).length) {
            // Se crea elemento timepicker
            var timepicker = new TimePicker(id, {
                lang: 'pt',
                theme: 'blue-grey'
            });
            // Evento para que coloque la hora con el formato
            timepicker.on('change', function (evt) {
                var value = (evt.hour || '00') + ':' + (evt.minute || '00');
                evt.element.value = value;

            });
        }
    }

    /* Coloca datepicker a un elemento por su id */
    function colocarDatePicker(id){
        var elementoFecha = $("#"+id);
        if (elementoFecha.length) {
            elementoFecha.datepicker({
                format: 'yyyy-mm-dd',
                language: "es",
                autoclose: true
            });
        }
    }

</script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Perfil
        <small>Consulta de datos de conteo</small>
      </h1>

    </section>
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">

            <!-- /.box-header -->
            <!-- form start -->
            
              <div class="box-body">
              
                <div class="row">
                  <div class="col-md-12">
                        <?php
                        if ( isset($titulo) ) {
                        ?>
                          <div class="<?php echo $tipo_mensaje; ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i> <?php echo $titulo; ?></h4>
                            <?php echo $mensaje; ?>
                          </div>
                        <?php
                        }
                        ?>
                  </div>
                </div>
                
                <div class="row">
                  <!-- emisor -->
                  <div class="col-md-11">
                    <h4 class="box-title">Datos de la cuenta de usuario</h4>
                  </div>
                  <!--<div class="col-md-1">
                    <a href="<?php //echo $url_editar_cuenta; ?>" class="btn btn-warning pull-right"><i class="fa fa-pencil"></i> Editar</a>
                  </div>-->
                </div>
              
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-2">
                      <label>Nombre</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="nombre" name="nombre" class="form-control" value="<?php echo $pss_usuario->nombre; ?>" readonly>
                    </div>
                    <div class="col-md-3">
                      <input type="text" id="apellido_paterno" name="apellido_paterno" class="form-control" value="<?php echo $pss_usuario->apellido_paterno; ?>" readonly>
                    </div>
                    <div class="col-md-3">
                      <input type="text" id="apellido_materno" name="apellido_materno" class="form-control" value="<?php echo $pss_usuario->apellido_materno; ?>" readonly>
                    </div>

                  </div>
                </div>
              
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-2">
                      <label>Login</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="login" name="login" class="form-control" value="<?php echo $pss_usuario->login; ?>" readonly>
                    </div>
                    <div class="col-md-2">
                      <Label>eMail contacto</label>
                    </div>
                    <div class="col-md-4">
                      <input type="text" id="email_contacto" name="email_contacto" class="form-control" value="<?php echo $pss_usuario->email; ?>" readonly>
                    </div>
                  </div>
                </div>
                
                <?php 
                  // no se muestran estos campos cuando el portal esta configurado a usar el RFC como login
                  if ( $config_portal->usar_contrasena != CONTROL_ACCESO_SOLO_LOGIN_RFC ) {
                  ?>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-2">
                        <label>Pista</label>
                      </div>
                      <div class="col-md-4">
                        <input type="text" id="id_pista_recuperar_contrasena" name="id_pista_recuperar_contrasena" class="form-control" value="<?php echo $pregunta_recuperacion; ?>" readonly>
                      </div>
                      <div class="col-md-2">
                        <Label>Respuesta</label>
                      </div>
                      <div class="col-md-4">
                        <input type="text" id="respuesta_recuperar_contrasena" name="respuesta_recuperar_contrasena" class="form-control" value="<?php echo $pss_usuario->respuesta_recuperar_contrasena; ?>" readonly>
                      </div>
                    </div>
                  </div>
                  <?php
                  } 
                ?>

              <hr>
              <table id="tabla_conteo" class="table table-striped table-bordered table-condensed" style="width:100%">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>RFC Emisor</th>
                          <th style="text-align:center;">Número de instancia</th>
                          <th style="text-align:center;">Contador</th>
                          <th style="text-align:center;">Conteo acumulado</th>
                          <th>Fecha de lectura</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php

                      if($pss_con_consulta_conteo!=null){
                        $contador = 1;
                        foreach($pss_con_consulta_conteo as $consulta_conteo) {
                          
                          if($pss_usuario->id_usuario_pss == $consulta_conteo->id_usuario_pss){
                            echo  "<tr>
                                <td>".$contador."</td>
                                <td>".$consulta_conteo->rfc_timbrado."</td>
                                <td style='text-align:center;'>".$consulta_conteo->instancia."</td>
                                <td style='text-align:center;'>".$consulta_conteo->contador."</td>
                                <td style='text-align:center;'>".$consulta_conteo->conteo_acumulado."</td>
                                <td>".$consulta_conteo->fecha_actualizacion."</td>
                              </tr>";
                              $contador++;
                          }
                        }
                      }else{
                    ?>
                      <tr><td colspan="7" align="center"><h4>Sin familia relacionada</h4></td></tr>
                    <?php
                      }
                    ?>

                  </tbody>
              </table>
          </div>
          
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
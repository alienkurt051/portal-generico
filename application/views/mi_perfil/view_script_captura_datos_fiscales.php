<script>

$(document).ready(function() {
url_ajax_rfc
   var url_ajax_cp = "<?php echo $url_ajax_cat_cp; ?>";
   var url_ajax_rfc = "<?php echo $url_ajax_rfc; ?>";
   

   $('#no_col').click(function(){  
      var htmlColonia= '<input type="text" id="colonia" name="colonia" class="form-control" value="">';
      $('#div_colonia').html(htmlColonia);
           });
   $('#rfc').keyup(function(){
       
       // si ya se capturaron los 5 digitos del CP

       if ( $('#rfc').val().length > 11 ) {
           var url_final = url_ajax_rfc + $("#rfc").val();
           //alert(url_final);

         $.ajax({
              url: url_final,
              method: "GET",
              success: function(result){
            //alert(result);
                var datosCP = JSON.parse(result);
                console.log(datosCP)
              
                  if(datosCP != false){

                  var w = 0;
                    $.each(datosCP,function(){
						
						$("#cliente").val(datosCP[w].cliente);
						$("#num_reg_id_trib").val(datosCP[w].num_reg_id_trib);
						$("#calle").val(datosCP[w].calle);
						$("#num_exterior").val(datosCP[w].numero_exterior);
						$("#num_interior").val(datosCP[w].numero_interior);
						$("#colonia").val(datosCP[w].colonia);
						$("#municipio").val(datosCP[w].municipio);
						$("#localidad").val(datosCP[w].localidad);
						$("#estado").val(datosCP[w].estado);
						$("#cp").val(datosCP[w].codigo_postal);
						$("#pais").val(datosCP[w].pais);

					
					  return;
                    });

                  }
              
              }
            });
       }

   });
   $('#cp').keyup(function(){
       
      var htmlColonia= '<input type="text" id="colonia" name="colonia" class="form-control" value="">';
      var htmlMunicipio= '<input type="text" id="municipio" name="municipio" class="form-control" value="">';
      var htmlLocalidad= '<input type="text" id="localidad" name="localidad" class="form-control" value="">';
      var htmlEstado = '<input type="text" id="estado" name="estado" class="form-control" value="">';
      var htmlPais= '<input type="text" id="pais" name="pais" class="form-control" value="">';
       // si ya se capturaron los 5 digitos del CP
       
       if ( $('#cp').val().length > 4 ) {
           var url_final = url_ajax_cp + $("#cp").val();
           //alert(url_final);

	       $.ajax({
           		url: url_final,
           		method: "GET",
           		success: function(result){
	       		
           			var datosCP = JSON.parse(result);
           			//console.log(datosCP)
	       			
               		if(datosCP != false){
                        // se llena el combo de las colonias
	       				var htmlColonia = '<select name="colonia" required class="form-control" id="colonia">';
	       				htmlColonia += '<option value="0">Seleccione una colonia</option>';
	       				$.each(datosCP,function(index,data){
	       					htmlColonia += '<option value="'+data.d_asenta+'">'+data.d_asenta+'</option>';
	       				});
                        
                        $('#div_colonia').html(htmlColonia);
                        
                        // combo de delegacion y localidad
	       				var htmlMunicipio = '<select name="municipio" required class="form-control" id="municipio">';
                        var htmlLocalidad = '<select name="localidad" required class="form-control" id="localidad">';
	       				htmlMunicipio += '<option value="'+datosCP[0].d_mnpio+'">'+datosCP[0].d_mnpio+'</option>';
                        htmlLocalidad += '<option value="'+datosCP[0].d_mnpio+'">'+datosCP[0].d_mnpio+'</option>';
                        $('#div_municipio').html(htmlMunicipio);
                        $('#div_localidad').html(htmlLocalidad);
           
                        // combo de estado
	       				var htmlEstado = '<select name="estado" required class="form-control" id="estado">';
	       				htmlEstado += '<option value="'+datosCP[0].d_estado+'">'+datosCP[0].d_estado+'</option>';
                        $('#div_estado').html(htmlEstado);
               			//38422
               		}else{
                        $('#div_colonia').html(htmlEstado);
                        $('#div_municipio').html(htmlEstado);
                        $('#div_localidad').html(htmlEstado);
                        $('#div_estado').html(htmlEstado);
                        $('#div_pais').html(htmlPais);
                        /*
               			$('#container_colonia').html('<label for="txtCalle">Colonia *:</label><input name="txtColonia" type="text" required class="form-control" id="txtColonia" placeholder="Colonia" maxlength="240">');
               			$('#txtDelegacion').val('');
               			$('#txtLocalidad').val('');
               			$('#cmbEstado').val('');
                        */
               		}
	       			
           		}
           	});
       }
       else{
        $('#div_colonia').html(htmlColonia);
        $('#div_municipio').html(htmlMunicipio);
        $('#div_localidad').html(htmlLocalidad);
        $('#div_estado').html(htmlEstado);
        $('#div_pais').html(htmlPais);
       }
       
       
   });
    valida_rfc();
});
function valida_rfc(){
    // Coloca todas las letras del campo RFC en mayúsculas
    $("#rfc").on("keyup", function () {
        $(this).val($(this).val().toUpperCase());
    });

    $("#rfc").on("keyup", function () {
        //event.preventDefault();
        var rfc = $("#rfc"),
        patron = {
            fisica: '[A-Z&Ñ]{4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]',
            moral: '[A-Z&Ñ]{3}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]'
        },
        mensajes = {
            longuitud: "Longitud mínima 12 y máxima 13",
            fisca: " RFC Persona Fisica 4 caracteres letras *6 caracteres siguiente fecha nacimiento AA/MM/DD *3 caracteres Homoclave",
            moral: "RFC Persona Moral *3 caracteres letras *6 caracteres siguiente fecha registro AA/MM/DD *3 carateres Homoclave"

        };
        // Quita el patrón y el mensaje que muestra en caso de error
        $("#rfc").removeAttr("pattern")
                .removeAttr("title");
        //Validacion para longuitud mínima de 12
        if (rfc.val().length < 12) {
            $("#rfc").attr("pattern", patron.fisica)
                    .attr("title", mensajes.longuitud);
        }
        //Validacion para RFC con logitud de 13 caracteres
        if (rfc.val().length === 13) {
            $("#rfc").attr("pattern", patron.fisica)
                    .attr("title", mensajes.fisca);
        }
        //Validacion para RFC con logitud de 12 caracteres
        if (rfc.val().length === 12) {
            $("#rfc").attr("pattern", patron.moral)
                    .attr("title", mensajes.moral);
        }
    });
}
function valida_mail(){
  var mail          = document.getElementById("email").value;
  var mail_confirm  = document.getElementById("email_confirma").value;
  if (mail != mail_confirm) {
    alert("los correos no coinciden");
    document.getElementById("email_confirma").value = "";
  }
  
}

</script>
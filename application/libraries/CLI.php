<?php
/** Conjunto de funciones útiles cuando se trabaja en desarrollos que se deben
 *  ejecutar desde la línea de comando (CLI).
 * 
 * Ejecución:
 *      Cargar librería: $this->load->library('cli');
 *      Ejecutar métodos: echo $this->cli->calcular_memoria_usada();
 * 
 * Author: Iván Mondragón Sotres
 *  */
defined('BASEPATH') OR exit('No direct script access allowed');

class CLI {

    public function __construct() {
        
    }

    /* Hace un cálculo de la memoria usada hasta el momento en que se ejcuta la
     * función.
     * 
     * @param string mensaje Mensaje que se desplegará
     * @param int codigo_error Opcional el código con el que termina la ejecución
     *      */

    function calcular_memoria_usada() {
        $memoria_usada = memory_get_usage(true);
        if ($memoria_usada < 1024) {
            $memoria_usada .= ' bytes';
        } elseif ($memoria_usada < 1048576) {
            $memoria_usada = round($memoria_usada / 1024, 2) . ' kilobytes';
        } else {
            $memoria_usada = round($memoria_usada / 1048576, 2) . ' megabytes';
        }
        return $memoria_usada;
    }

    /* Muestra un mensaje de error cuando se corre script en línea de comando
     * 
     * @param string mensaje Mensaje que se desplegará
     * @param int codigo_error Opcional el código con el que termina la ejecución
     *      */

    function coloar_mensaje_error_cli($mensaje, $codigo_error = 2) {
        echo "\n\t****************************************************************\n";
        echo "\n\t********* $mensaje ************\n";
        echo ("\n\t****************************************************************\n");
        exit($codigo_error);
    }

    /* Muestra el progreso de un ciclo con un echo.
     * 
     * @param int avance Índice incremental + 1 del ciclo (min 1)
     * @param int total longuitud total o hasta donde se recorre el ciclo.
     *      */

    function mostrar_progreso($done, $total, $size = 30) {
        static $start_time;
        if ($done > $total)
            return;
        if (empty($start_time))
            $start_time = time();
        $now = time();
        $perc = (double) ($done / $total);
        $bar = floor($perc * $size);
        $status_bar = "\r[";
        $status_bar .= str_repeat("=", $bar);
        if ($bar < $size) {
            $status_bar .= ">";
            $status_bar .= str_repeat(" ", $size - $bar);
        } else {
            $status_bar .= "=";
        }
        $disp = number_format($perc * 100, 0);
        $status_bar .= "] $disp%  $done/$total";
        $rate = ($now - $start_time) / $done;
        $left = $total - $done;
        $eta = round($rate * $left, 2);
        $elapsed = $now - $start_time;
        $status_bar .= " restante: " . number_format($eta) . " sec.  transcurrido: " . number_format($elapsed) . " sec.";
        echo "$status_bar  \r";
        flush();
        if ($done == $total) {
            echo "\n";
        }
    }
    /* Valida que los datos de entrada sea diferentes de false
     * 
     * @param array Arreglo de datos a validar
     * 
     * @return bool True si contienen valor diferente de false, False de lo contrario
     *      */

    function validar_datos_entrada($datos) {
        foreach ($datos as $valor) {
            if (!$valor) {
                return false;
            }
        }
        return true;
    }

    /* Valida formato de un arreglo de fechas.
     * 
     * @param array Arreglo de fechas a validar
     * 
     * @return bool True si las fechas son correctas, False de lo contrario
     *      */

    function validar_formato_fechas($fechas, $formato = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/") {
        foreach ($fechas as $fecha) {
            if (!preg_match($formato, $fecha)) {
                return false;
            }
        }
        return true;
    }
    
    /* Obtiene la ruta absoluta del sistema
     * 
     * @param sistema bool true si es para windows, false para linux
     * 
     * @return string cadena con ruta absoluta sin saltos
     *      */

    function obtener_ruta_absoluta($sistema=TRUE) {
        $cmd_ruta = $sistema ? "cd" : "pwd";
        return preg_replace("/[\r\n|\n|\r]+/", "", shell_exec($cmd_ruta));
    }

}

<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Pss_devoluciones extends ORM {

    public $table = "pss_devoluciones";
    public $primary_key = 'id_devolucion';

    function _init() {

        self::$fields = array(
            "id_devolucion"             => ORM::field('auto'),
            "numero_identificacion"     => ORM::field('char[30]'),
            "cantidad"                  => ORM::field('char[30]'),
            "precio"                    => ORM::field('decimal'),
            "fecha"                     => ORM::field('datetime'),
            "id_sucursal"               => ORM::field('int')
        );
    }

}

?>

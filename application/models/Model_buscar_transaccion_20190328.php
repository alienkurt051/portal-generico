<?php

// modelo para buscar las transacciones para facturar

class model_buscar_transaccion extends CI_model {

  // listado de entidades segun el tipo de entidad ORGANIZATION, CUSTOMER, SUCURSAL, ETC
  public function obtener_idtrx33_transaccion($arr_campos, $id_entidad)
  {
      // se verifica si el id_entidad recibido es una matriz o una sucursal
      $sql_id_tipo_entidad = "select id_tipo_entidad from c_entidades where id_entidad = ".$id_entidad;
      $result_tipo_entidad = $this->db->query($sql_id_tipo_entidad);
      $row_entidad = $result_tipo_entidad->row();
      
      $id_tipo_entidad = $row_entidad->id_tipo_entidad;
      
      $busqueda_transaccion  = " select distinct emi.id_emisor, adic.id_trx33 from emi_trx33_inf_adic adic 
                                 inner join emi_trx33_r emi
                                     on adic.id_trx33 = emi.id_trx33_r
                                 where ";
                                 
      // si es la matriz
      if ( $id_tipo_entidad == 1 ) {
          $busqueda_transaccion .= " emi.id_emisor = ".$id_entidad;
      } else {
          // es la sucursal
          $busqueda_transaccion .= " emi.id_sucursal = ".$id_entidad;
      }
      
      $busqueda_transaccion .= " and emi.id_tipo_de_comprobante = 'I' ";
      
      for ( $i = 1; $i <= count($arr_campos); $i++ ) {
          $busqueda_transaccion .= ' and adic.id_trx33 in (select id_trx33 from emi_trx33_inf_adic where id_flex_header = "'.$arr_campos[$i]["campo_adicional"].'" and valor = "'.$arr_campos[$i]["valor"].'")';
      }
      
    //echo "<br>Query: ".$busqueda_transaccion;
    //die();

    $result_transaccion = $this->db->query($busqueda_transaccion);

    // se inicia el contador de transacciones
    $num_transacciones = 1;
    
    $arr_transacciones = array();
    
    // se obtienen los datos de cada transaccion
    foreach ( $result_transaccion->result() as $unaTransaccion)
    {
    
      $arr_transacciones[$num_transacciones] = array("id_emisor" => $unaTransaccion->id_emisor     ,
                                                     "id_trx33"  => $unaTransaccion->id_trx33      );

      // se incrementa el contador
      $num_transacciones = $num_transacciones + 1;
    }
    
    // se libera el cursor
    $result_transaccion->free_result();
  
    // se devuelve el arreglo con la lista de transacciones encontradas
    return $arr_transacciones;
  }

    public function obtener_simbolos($etiqueta_flex_header)
  {
      $arr_simbol_fh = "SELECT simbol FROM pss_fh_transaccion WHERE etiqueta_flex_header = '".$etiqueta_flex_header."';";
      //echo $arr_campos_fh;

      $result_simbol_fh = $this->db->query($arr_simbol_fh);

      $row_simbol_fh = $result_simbol_fh->result();

      return $row_simbol_fh;
  }

      public function obtener_estatus_campos_fh()
  {
      $arr_campos_fh = "SELECT * FROM emi_c_info_adicionales WHERE estatus = 1;";
      $result_campos_fh = $this->db->query($arr_campos_fh);

      $row_campos_fh = $result_campos_fh->result();

      return $row_campos_fh;
  }

      public function obtener_conceptos_transaccion($id_trx33_r)
  {
      $arr_conceptos = "SELECT * FROM emi_trx33_concepto_r WHERE id_trx33_r =".$id_trx33_r;
      $result_conceptos = $this->db->query($arr_conceptos);

      $row_conceptos = $result_conceptos->result();

      return $row_conceptos;
  }

    //inserta informacion de los flexheader
    public function insertar_info_flexheader($name_fh, $id_trx33_r, $arry_name) {
      $array_nombre_fh = "INSERT INTO emi_trx33_inf_adic VALUES ('$name_fh', $id_trx33_r, '$arry_name');";

      $row_nombre_fh = $this->db->query($array_nombre_fh);

      //echo print_r($row_nombre_fh);

      return $row_nombre_fh;
    }

    //inserta informacion de los flexheader
    public function insertar_info_r_flexheader($name_fh, $id_trx33_r, $arry_name) {
      $array_nombre_fh_r = "INSERT INTO emi_trx33_inf_adic_r VALUES ('$name_fh', $id_trx33_r, '$arry_name');";

      $row_nombre_fh_r = $this->db->query($array_nombre_fh_r);

      //echo print_r($row_nombre_fh);

      return $row_nombre_fh_r;
    }

  

}



?>
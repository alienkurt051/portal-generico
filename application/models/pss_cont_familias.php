<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Pss_cont_familias extends ORM {
        public $table_name = "pss_cont_familias";
        public $primary_key = 'id_familia';

        function _init()
        {
                self::$fields = array(
                        'id_familia'        => ORM::field('int'),
                        'id_cliente_ebs'    => ORM::field('char[10]'),
                        'rfc'               => ORM::field('char[20]'),
                        'num_instancia'     => ORM::field('char[10]'),
                        'contador'          => ORM::field('char[100]'),
                        'fecha_alta'        => ORM::field('char[20]')
                );
        }
}

?>
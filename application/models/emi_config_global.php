<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Emi_config_global extends ORM {
        public $table = "emi_config_global";
        public $primary_key = 'id_emi_config_global';
        
        function _init()
        {

                self::$fields = array(
                        'id_emi_config_global'  => ORM::field('int'),
                        'id_entidad'            => ORM::field('int'),
                        'serie'                 => ORM::field('char[40]'),
                        'id_receptor'           => ORM::field('int'),
                        'tipo_ejecucion'        => ORM::field('int'),
                        'rango_dia_ejecucion'   => ORM::field('int'),
                        'hora_inicio'           => ORM::field('time'),
                        'hora_fin'              => ORM::field('time'),
                        'hora_ejecucion'        => ORM::field('time'),
                        'cond_ejecucion'        => ORM::field('int'),
                        'dia_ejecucion'         => ORM::field('int'),
                        'origen_num_ticket'     => ORM::field('char[50]'),
                        'origen_num_ticket_aux' => ORM::field('char[50]'),
                        'estatus'               => ORM::field('int'),
                        'fecha_ejecucion'       => ORM::field('datetime'),
                        'tipo_global'           => ORM::field('int')
                        
                );
        }
}

?>

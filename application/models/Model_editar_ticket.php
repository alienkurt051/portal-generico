<?php

// modelo para editar los tickets

class model_editar_ticket extends CI_model {

  // listado de entidades segun el tipo de entidad ORGANIZATION, CUSTOMER, SUCURSAL, ETC
  public function respalda_transaccion($id_trx33, $id_usuario_firmado)
  {
      // queries para depurar respaldo
      $sql_borra_trx33               = "delete from pss_emi_trx33_r where id_trx33_r = ".$id_trx33;
      $sql_borra_trx33_con_impuestos = "DELETE imp_r.* FROM pss_emi_trx33_con_impuestos_r imp_r INNER JOIN pss_emi_trx33_concepto_r con_r ON imp_r.id_trx33_concepto_r = con_r.id_trx33_concepto_r AND con_r.id_trx33_r=$id_trx33";
      $sql_borra_trx33_conceptos     = "delete from pss_emi_trx33_concepto_r where id_trx33_r = ".$id_trx33;
      $sql_borra_trx33_impuestos     = "delete from pss_emi_trx33_impuestos_r where id_trx33_r = ".$id_trx33;
      
      $this->db->query($sql_borra_trx33);
      $this->db->query($sql_borra_trx33_con_impuestos);
      $this->db->query($sql_borra_trx33_conceptos);
      $this->db->query($sql_borra_trx33_impuestos);
      
      // queries para insertar los datos de respaldo
      $sql_inserta_trx33               = "insert into pss_emi_trx33_r               select *, now(),".$id_usuario_firmado." from emi_trx33_r where id_trx33_r = ".$id_trx33;
      $sql_inserta_trx33_conceptos     = "insert into pss_emi_trx33_concepto_r      select * from emi_trx33_concepto_r      where id_trx33_r = ".$id_trx33;
      $sql_inserta_trx33_con_impuestos = "insert into pss_emi_trx33_con_impuestos_r select * from emi_trx33_con_impuestos_r where id_trx33_concepto_r in (select id_trx33_concepto_r from emi_trx33_concepto_r where id_trx33_r = ".$id_trx33.")";
      $sql_inserta_trx33_impuestos     = "insert into pss_emi_trx33_impuestos_r     select * from emi_trx33_impuestos_r     where id_trx33_r = ".$id_trx33;
      
      $this->db->query($sql_inserta_trx33);
      $this->db->query($sql_inserta_trx33_conceptos);
      $this->db->query($sql_inserta_trx33_con_impuestos);
      $this->db->query($sql_inserta_trx33_impuestos);
      
  }
  
  public function modifica_cantidad_transaccion_concepto($id_trx33, $id_trx33_concepto_r, $cantidad) {
      $sql_actualiza_cantidad = "update emi_trx33_concepto_r set cantidad = ".$cantidad." where id_trx33_r = ".$id_trx33." and id_trx33_concepto_r = ".$id_trx33_concepto_r;
      $this->db->query($sql_actualiza_cantidad);
      
      $this->recalcula_impuestos_y_totales($id_trx33);
      
  }
  
  public function elimina_transaccion_concepto($id_trx33, $id_trx33_concepto_r) {
      // se eliminan los impuestos asociados
      $sql_borra_trx33_con_impuestos     = "delete from emi_trx33_con_impuestos_r where id_trx33_concepto_r = ".$id_trx33_concepto_r;
      $this->db->query($sql_borra_trx33_con_impuestos);
      
      // query para eliminar el concepto
      $sql_borra_trx33_conceptos     = "delete from emi_trx33_concepto_r where id_trx33_r = ".$id_trx33." and id_trx33_concepto_r = ".$id_trx33_concepto_r;
      $this->db->query($sql_borra_trx33_conceptos);
     
      $this->recalcula_impuestos_y_totales($id_trx33);
      
  }
  
  // funcion que actualiza el estatus de un ticket a no facturable
  public function cancelar_ticket($id_trx33) {
      $sql_cancela_ticket = "update emi_trx33_r set id_receptor = -1 where id_trx33_r = ".$id_trx33."";
      $this->db->query($sql_cancela_ticket);
  }
  
  public function recalcula_impuestos_y_totales($id_trx33) {
      // se declaran las constantes
      $IMPUESTO_TRASLADADO = 1;
      $IMPUESTO_RETENIDO   = 2;
      
      //recalcula importes
      $sql_recalcula_importes = "update emi_trx33_concepto_r set importe = ROUND((cantidad * valor_unitario), 2) where id_trx33_r = ".$id_trx33."";
      $this->db->query($sql_recalcula_importes);
      
      // query para recalcular impuestos
      $this->db->where("id_trx33_r",$id_trx33);
      $conceptos = Model\Emi_trx33_concepto_r::all();
      
      foreach ($conceptos as $concepto) {
          // se obtienen sus impuestos
          $this->db->where("id_trx33_concepto_r",$concepto->id_trx33_concepto_r);
          $impuestos_concepto = Model\Emi_trx33_con_impuestos_r::all();
          
          // se recalcula el impuesto
          foreach ($impuestos_concepto as $impuesto_concepto) {
              $nueva_base    = $concepto->importe;
              $concepto->importe = round(($concepto->cantidad * $concepto->valor_unitario),2);
              $nuevo_importe = round(($concepto->importe * $impuesto_concepto->tasa_cuota),2);
              
              $sql_actualiza_impuestos = "update emi_trx33_con_impuestos_r set base = ".$nueva_base.", importe = ".$nuevo_importe." where id_trx33_concepto_r = ".$concepto->id_trx33_concepto_r."";
              $this->db->query($sql_actualiza_impuestos);
          }
      }
      // se actualizan los subtotales de impuestos trasladados y retenidos   
      $sql_acumulados = "SELECT tipo_impuesto, impuesto, tasa_cuota, tipo_factor, 
                         SUM(importe) as importe
                         FROM  emi_trx33_con_impuestos_r 
                         WHERE id_trx33_concepto_r in (select id_trx33_concepto_r from emi_trx33_concepto_r where id_trx33_r = ".$id_trx33.")
                         GROUP BY tipo_impuesto, impuesto, tasa_cuota, tipo_factor
                         ";

      $sum_acumulados = $this->db->query($sql_acumulados);

      foreach ($sum_acumulados->result() as $acumula_impuesto) {
          $sql_acumula_impuestos = "UPDATE emi_trx33_impuestos_r set importe = '".$acumula_impuesto->importe."' where id_trx33_r = '".$id_trx33."' and tipo_impuesto = '".$acumula_impuesto->tipo_impuesto."' and impuesto = '".$acumula_impuesto->impuesto."' and tasa_o_cuota = '".$acumula_impuesto->tasa_cuota."' and tipo_factor = '".$acumula_impuesto->tipo_factor."'";
          $this->db->query($sql_acumula_impuestos);
      }     

      $sum_acumulados->free_result();

      //se actualiza el subtotal y descuento
      $sql_subtotales = "SELECT id_trx33_r, SUM(importe) as importe ,SUM(descuento) as descuento 
                      FROM emi_trx33_concepto_r 
                      WHERE id_trx33_r = ".$id_trx33."";

      $sql_subtotales = $this->db->query($sql_subtotales);

      $sql_subtotal  =  $sql_subtotales->row();

      if (isset($sql_subtotal)) {   
          $sql_actualiza_subtotales = "UPDATE emi_trx33_r SET subtotal = '".$sql_subtotal->importe."', descuento = '".$sql_subtotal->descuento."' WHERE id_trx33_r = ".$id_trx33."";
        
          $this->db->query($sql_actualiza_subtotales);
      }

      $sql_subtotales->free_result();

      //suma los tipo de impuesto, ya sea retenido o trasladado
      $sql_tipo_impuesto = "SELECT tipo_impuesto, SUM(importe) as importe
                         FROM  emi_trx33_impuestos_r 
                         WHERE id_trx33_r = ".$id_trx33."
                         GROUP BY tipo_impuesto
                         ";


      $sel_tipo_impuesto = $this->db->query($sql_tipo_impuesto);

      foreach ($sel_tipo_impuesto->result() as $s_tipo_impuestos) {

        if($s_tipo_impuestos->tipo_impuesto == 1){

          $sql_impuesto_trasladado = "UPDATE emi_trx33_r set totalImpuestosTrasladados = '".$s_tipo_impuestos->importe."'  WHERE id_trx33_r = ".$id_trx33."";
          
          $this->db->query($sql_impuesto_trasladado);

        }elseif ($s_tipo_impuestos->tipo_impuesto == 2) {

          $sql_impuesto_retenido = "UPDATE emi_trx33_r set totalImpuestosRetenidos = '".$s_tipo_impuestos->importe."'  WHERE id_trx33_r = ".$id_trx33."";
          
          $this->db->query($sql_impuesto_retenido);        
        }  
      
      }  

      $sel_tipo_impuesto->free_result();

      //se actualiza el total
      $sql_actualiza_totales = "UPDATE emi_trx33_r SET total = ROUND((ifnull(subtotal,0) - ifnull(descuento,0) + ifnull(totalImpuestosTrasladados,0) - ifnull(totalImpuestosRetenidos,0)),2) WHERE id_trx33_r = ".$id_trx33."";
        
      $this->db->query($sql_actualiza_totales);

  }

  public function borrar_trx33 ($id_trx33) {
      //Borra impuestos de los conceptos
      $sql_borra_con_impuestos = "DELETE imp.* FROM emi_trx33_con_impuestos imp INNER JOIN emi_trx33_conceptos con ON imp.id_trx33_conceptos = con.id_trx33_conceptos and con.id_trx33=$id_trx33";
      $this->db->query($sql_borra_con_impuestos);

      //Borra concepto parte
      $sql_borra_con_parte = "DELETE par.* FROM emi_trx33_con_parte par INNER JOIN emi_trx33_conceptos con ON par.id_trx33_concepto = con.id_trx33_conceptos and con.id_trx33=$id_trx33";
      $this->db->query($sql_borra_con_parte);
      
      //Borra concepto complemento 
      $sql_borra_con_complemento = "DELETE con.* FROM emi_trx33_con_complemento con INNER JOIN emi_trx33_conceptos conc ON con.id_trx33_conceptos = conc.id_trx33_conceptos and conc.id_trx33=$id_trx33";
      $this->db->query($sql_borra_con_complemento);
	  
      //Borra conceptos
      $sql_borra_conceptos = "DELETE FROM emi_trx33_conceptos WHERE id_trx33 = ".$id_trx33."";
      $this->db->query($sql_borra_conceptos);
      
      //Borra concepto informacion adicional
      $sql_borra_con_inf_adic = "DELETE inf.* FROM emi_trx33_con_inf_adic inf INNER JOIN emi_trx33_conceptos con ON inf.id_trx33_conceptos=con.id_trx33_conceptos and con.id_trx33=$id_trx33";
      $this->db->query($sql_borra_con_inf_adic);

      //Borra informacion adicional
      $sql_borra_inf_adic = "DELETE FROM emi_trx33_inf_adic WHERE id_trx33=".$id_trx33."";
      $this->db->query($sql_borra_inf_adic);
      
      //Borra impuestos
      $sql_borra_impuestos = "DELETE FROM emi_trx33_impuestos WHERE id_trx33 = ".$id_trx33."";
      $this->db->query($sql_borra_impuestos);
      
      //Borra cfdi relacionado
      $sql_borra_cfdirelacionado = "DELETE FROM emi_trx33_cfdirelacionado WHERE id_trx33 = ".$id_trx33."";
      $this->db->query($sql_borra_cfdirelacionado);
      
      //Borra XML
      $sql_borra_xml = "DELETE FROM emi_trx33_xml WHERE id_trx33 = ".$id_trx33."";
      $this->db->query($sql_borra_xml);
      
      //Borra PDF
      $sql_borra_pdf = "DELETE FROM emi_trx33_pdf WHERE id_trx33 = ".$id_trx33."";
      $this->db->query($sql_borra_pdf);
      
      //Borra complementos
      $sql_borra_complementos = "DELETE FROM emi_trx33_complementos WHERE id_trx33 = ".$id_trx33."";
      $this->db->query($sql_borra_complementos);
      
      //Borra transaccion
      $sql_borra_trx33 = "DELETE FROM emi_trx33 WHERE id_trx33 = ".$id_trx33.""; 
      $this->db->query($sql_borra_trx33);
      
      //Borra addendas
      $sql_borra_addendas = "DELETE FROM emi_trx33_addendas WHERE id_trx33 = ".$id_trx33."";
      $this->db->query($sql_borra_addendas);
      
  }

  public function ejecuta_sp_mueve_trx ($id_trx33) {

      //Exporta los datos a las tablas que no tienen _r en el nombre
      $sql_stored_procedure = "call sp_mueve_trx(".$id_trx33.")";

      $this->db->query($sql_stored_procedure);

  }
}


?>
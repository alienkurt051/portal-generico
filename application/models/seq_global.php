<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Seq_global extends ORM {
        public $table_name = "seq_global";
        public $primary_key = 'id_global';

        function _init()
        {
                self::$fields = array(
                        'id_global'        => ORM::field('int')                        
                );
        }
}

?>

<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Pss_bitacora extends ORM {
        public $table = "pss_bitacora";
        public $primary_key = 'id_bitacora';
        
        function _init()
        {
            
                self::$fields = array("id_bitacora"   => ORM::field('auto'),
                                      "id_accion"     => ORM::field('int'),
                                      "id_usuario"    => ORM::field('int'),
                                      "fecha_hora"    => ORM::field('datetime'),
                                      "dir_ip"        => ORM::field('char[20]'),
                                      "observaciones" => ORM::field('string')
                                      );
                
        }

}

?>

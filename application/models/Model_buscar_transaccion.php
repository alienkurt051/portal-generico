<?php

// modelo para buscar las transacciones para facturar

class model_buscar_transaccion extends CI_model {

  // listado de entidades segun el tipo de entidad ORGANIZATION, CUSTOMER, SUCURSAL, ETC
  public function obtener_idtrx33_transaccion($arr_campos, $id_entidad)
  {
      // se verifica si el id_entidad recibido es una matriz o una sucursal
      $sql_id_tipo_entidad = "select id_tipo_entidad from c_entidades where id_entidad = ".$id_entidad;
      $result_tipo_entidad = $this->db->query($sql_id_tipo_entidad);
      $row_entidad = $result_tipo_entidad->row();
      
      $id_tipo_entidad = $row_entidad->id_tipo_entidad;
      
      $busqueda_transaccion  = " select distinct emi.id_emisor, adic.id_trx33 from emi_trx33_inf_adic adic 
                                 inner join emi_trx33_r emi
                                     on adic.id_trx33 = emi.id_trx33_r
                                 where ";
                                 
      // si es la matriz
      if ( $id_tipo_entidad == 1 ) {
          $busqueda_transaccion .= " emi.id_emisor = ".$id_entidad;
      } else {
          // es la sucursal
          $busqueda_transaccion .= " emi.id_sucursal = ".$id_entidad;
      }
      
      $busqueda_transaccion .= " and emi.id_tipo_de_comprobante = 'I' ";
      
      for ( $i = 1; $i <= count($arr_campos); $i++ ) {
          $busqueda_transaccion .= ' and adic.id_trx33 in (select id_trx33 from emi_trx33_inf_adic where id_flex_header = "'.$arr_campos[$i]["campo_adicional"].'" and valor = "'.$arr_campos[$i]["valor"].'")';
      }
      
    //echo "<br>Query: ".$busqueda_transaccion;
    //die();

    $result_transaccion = $this->db->query($busqueda_transaccion);

    // se inicia el contador de transacciones
    $num_transacciones = 1;
    
    $arr_transacciones = array();
    
    // se obtienen los datos de cada transaccion
    foreach ( $result_transaccion->result() as $unaTransaccion)
    {
    
      $arr_transacciones[$num_transacciones] = array("id_emisor" => $unaTransaccion->id_emisor     ,
                                                     "id_trx33"  => $unaTransaccion->id_trx33      );

      // se incrementa el contador
      $num_transacciones = $num_transacciones + 1;
    }
    
    // se libera el cursor
    $result_transaccion->free_result();
  
    // se devuelve el arreglo con la lista de transacciones encontradas
    return $arr_transacciones;
  }

    public function obtener_simbolos($etiqueta_flex_header)
  {
      $arr_simbol_fh = "SELECT simbol FROM pss_fh_transaccion WHERE etiqueta_flex_header = '".$etiqueta_flex_header."';";
      //echo $arr_campos_fh;

      $result_simbol_fh = $this->db->query($arr_simbol_fh);

      $row_simbol_fh = $result_simbol_fh->result();

      return $row_simbol_fh;
  }

    public function obtener_estatus() {
      $array_estatus_fh = "SELECT info.id_info_adicional, info.campo_adicional, info.descripcion, info.nivel, estatus.estatus, estatus.flexheader
                                              FROM emi_c_info_adicionales AS info
                                              INNER JOIN pss_campos_adicionales AS estatus
                                              ON info.id_info_adicional = estatus.id_info_adicional
                                              WHERE estatus.estatus = 1;";
      $result_estatus_fh = $this->db->query($array_estatus_fh);

      $row_estatus_fh = $result_estatus_fh->result();
      //echo print_r($row_campos_fh);

      return $row_estatus_fh;
    }

      public function obtener_conceptos_transaccion($id_trx33_r)
  {
      $arr_conceptos = "SELECT * FROM emi_trx33_concepto_r WHERE id_trx33_r =".$id_trx33_r;
      $result_conceptos = $this->db->query($arr_conceptos);

      $row_conceptos = $result_conceptos->result();

      return $row_conceptos;
  }

    //inserta informacion de los flexheader
    public function insertar_info_flexheader($name_fh, $id_trx33_r, $arry_name) {
      $array_nombre_fh = "INSERT INTO emi_trx33_inf_adic VALUES ('$name_fh', $id_trx33_r, '$arry_name');";

      $row_nombre_fh = $this->db->query($array_nombre_fh);

      //echo print_r($row_nombre_fh);

      return $row_nombre_fh;
    }

    //inserta informacion de los flexheader
    public function insertar_info_r_flexheader($name_fh, $id_trx33_r, $arry_name) {
      /*
	  $array_nombre_fh_r = "INSERT INTO emi_trx33_inf_adic_r VALUES ('$name_fh', $id_trx33_r, '$arry_name');";

      $row_nombre_fh_r = $this->db->query($array_nombre_fh_r);

      //echo print_r($row_nombre_fh);
      */
      //return $row_nombre_fh_r;
	  return true;
    }

    /*  Obtiene una lista de entidades ordenadas alfabéticamente que si tienen 
     *  serie definida para facturar.
     * 
     *  @return array Arreglo de objetos con entidades
     *      */
    
    public function obtener_entidades_con_serie() {
        $series_entidades = Model\Pss_series_entidades::all();
        $entidades_in = array();
        $i = 0;
        foreach ($series_entidades as $serie_entidad) {
            $entidades_in[$i] = $serie_entidad->id_entidad;
            $i++;
        }
        // arreglo de entidades
        $this->db->where_in("id_entidad", $entidades_in);
        return Model\V_pss_entidades::order_by('entidad', 'ASC')->all();
    }  

    /*  Actualiza las tablas emi_trx33_inf_adic y emi_trx33_inf_adic_r colocando
     *  un diferenciador (L) para que ya no se encuentre dicha transacción cuando
     *  se intente buscar nuevamente para facturar y/o liberar.
     * 
     *  @param int $id_trx33 Identificador de la transacción a actualizar
     *  @param string $diferenciador Cadena que se concatena a flex de transacc.
     * 
     *  @return bool true si fue posible actualizar, false de lo contrario
     *      */
    
    public function actualizar_flex_liberacion($id_trx33,$diferenciador = "L") {
        $update = "UPDATE emi_trx33_inf_adic inf INNER JOIN emi_trx33_inf_adic_r inf_r 
            ON (inf.id_Trx33 = inf_r.id_trx33_r)
                SET
                    inf.valor 	= concat(inf.valor, '$diferenciador'),
                    inf_r.valor = concat(inf_r.valor, '$diferenciador')
                WHERE inf.id_Trx33 = $id_trx33";
        $this->db->query($update);
        return $this->db->affected_rows() > 0 ? TRUE : FALSE;
    }  

    /*  Busca una transacción por id y regresa su estatus en el que se encuentra,
     *  los cuales pueden ser; pendiente, facturada, cancelada.
     * 
     *  @param int $id_trx33 Identificador de la transacción a actualizar
     * 
     *  @return int 0 es pendiente, 1 facturada, 2 cancelada, 3 error.
     *      */
    
    public function buscar_estatus_transaccion($id_trx33) {
        $this->db->select('x.codigo as codigo,count(*) as registros');
        $this->db->from('emi_trx33_xml x');
        $this->db->join('emi_trx33_r r', 'x.id_trx33 = r.id_trx33_r');
        $this->db->where('x.id_trx33',$id_trx33);
        $resultado  = $this->db->get();
        $registro   = $resultado->row();
        // Si no hay registros es decir que la transacción está pendiente
        if($registro->registros < 1){
            return 0;
        }
        // De lo contrario se valida estatus de código      
        $codigo     = (int)$registro->codigo;
        return $codigo === 10 ? 1 :( ($codigo === 201 || $codigo === 202) ? 2 : 3);
    }  

}



?>
<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class pss_con_instancia_contador extends ORM {
        public $table_name = "pss_con_instancia_contador";
        public $primary_key = 'id_usuario_pss';

        function _init()
        {
                self::$fields = array(
                        'id_usuario_pss'                => ORM::field('int'),
                        'id_cliente_ebs'                => ORM::field('int'),
                        'nombre_cliente'                => ORM::field('char[255]'),
                        'rfc_cliente'                   => ORM::field('char[20]'),
                        'email_cliente'                 => ORM::field('char[255]'),
                        'fecha_contratacion'            => ORM::field('date()'),
                        'tipo_contrato'                 => ORM::field('varchar[100]'),
                        'volumen_contratado'            => ORM::field('int'),
                        'precio_por_transaccion'        => ORM::field('decimal'),
                        'estatus'                       => ORM::field('int'),
                        'id_archivo_creacion'           => ORM::field('int'),
                        'fecha_creacion'                => ORM::field('datetime'),
                        'id_archivo_actualizacion'      => ORM::field('int'),
                        'fecha_actualizacion'           => ORM::field('datetime')


                );
        }
}

?>
<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class V_pss_bitacora extends ORM {
        public $table = "V_pss_bitacora";
        public $primary_key = 'id_bitacora';
        
        function _init()
        {
            
                self::$fields = array("id_bitacora"   => ORM::field('int'),
				                      "id_usuario"    => ORM::field('int'),
                                      "login"         => ORM::field('string'),
									  "nombre"        => ORM::field('string'),
									  "email"         => ORM::field('string'),
                                      "fecha_hora"    => ORM::field('datetime'),
									  "id_accion"     => ORM::field('int'),
                                      "d_accion"      => ORM::field('string'),
									  "dir_ip"        => ORM::field('char[20]'),
									  "observaciones" => ORM::field('string')
                                      );
                
        }
		
}

?>
<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Pss_con_consulta_conteo extends ORM {
        public $table_name = "pss_con_consulta_conteo";
        public $primary_key = 'id_usuario_pss';

        function _init()
        {
                self::$fields = array(
                        'id_usuario_pss'                   => ORM::field('int[11]'),
                        'id_cliente_ebs'                   => ORM::field('int[11]'),
                        'rfc_timbrado'                     => ORM::field('char[20]'),
                        'instancia'                        => ORM::field('char[50]'),
                        'contador'                         => ORM::field('char[50]'),
                        'fecha_actualizacion'              => ORM::field('datetime'),
                        'conteo_acomulado'                 => ORM::field('int[11]'),
                        'id_archivo'                       => ORM::field('int[11]'),
                        'fecha_actualizacion_datos'        => ORM::field('datetime')
                );
        }
}

?>
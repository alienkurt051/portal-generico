<?php

// modelo para buscar las transacciones para facturar

class model_facturar_pss extends CI_model {

  // listado de clientes asociado al usuario firmado sin considerar el cliente utilizado para autofactura
  public function buscar_clientes_por_usuario($id_usuario_pss, $tipo_usuario, $id_cliente_autofactura, $rfc, $razon_social, $max_num_registros)
  {

      // se construye la busqueda de clientes
	  $query_buscar_clientes_por_usuario = "select 
                                            cli.id_cliente        ,
                                            cli.cliente           ,
                                            cli.rfc               ,
                                            cli.numero_cliente    ,
                                            cli.numero_exterior   ,
                                            cli.numero_interior   ,
                                            cli.calle             ,
                                            cli.colonia           ,
                                            cli.localidad         ,
                                            cli.referencia        ,
                                            cli.municipio         ,
                                            cli.estado            ,
                                            cli.pais              ,
                                            cli.codigo_postal     ,
                                            cli.email             ,
                                            cli.estatus           ,
                                            cli.num_reg_id_trib   
											from pss_r_usuario_cliente uc inner join c_clientes cli on uc.id_cliente = cli.id_cliente
											where uc.id_usuario = ".$id_usuario_pss." and cli.id_cliente <> ".$id_cliente_autofactura;
											
        // si se recibio rfc/razon social para buscar
		if ( $rfc != null ) {
			$query_buscar_clientes_por_usuario .= ' and cli.rfc like "%'.$rfc.'%" ';
		}

		if ( $razon_social != null ) {
			$query_buscar_clientes_por_usuario .= ' and cli.cliente like "%'.$razon_social.'%" ';
		}

        $query_buscar_clientes_por_usuario .= ' order by cli.id_cliente desc limit '.$max_num_registros;
											
        $result_transaccion = $this->db->query($query_buscar_clientes_por_usuario);
        
        // se inicia el contador de transacciones
        $num_transacciones = 1;
        
        $arr_clientes = array();
        
        // se obtienen los datos de cada transaccion
        foreach ( $result_transaccion->result() as $unaTransaccion)
        {
        
          $arr_clientes[$num_transacciones] = array("id_cliente"      => $unaTransaccion->id_cliente        ,
                                                    "cliente"         => $unaTransaccion->cliente           ,
                                                    "rfc"             => $unaTransaccion->rfc               ,
                                                    "numero_cliente"  => $unaTransaccion->numero_cliente    ,
                                                    "numero_exterior" => $unaTransaccion->numero_exterior   ,
                                                    "numero_interior" => $unaTransaccion->numero_interior   ,
                                                    "calle"           => $unaTransaccion->calle             ,
                                                    "colonia"         => $unaTransaccion->colonia           ,
                                                    "localidad"       => $unaTransaccion->localidad         ,
                                                    "referencia"      => $unaTransaccion->referencia        ,
                                                    "municipio"       => $unaTransaccion->municipio         ,
													"estado"          => $unaTransaccion->estado            ,
													"pais"            => $unaTransaccion->pais              ,
													"codigo_postal"   => $unaTransaccion->codigo_postal     ,
													"email"           => $unaTransaccion->email             ,
													"estatus"         => $unaTransaccion->estatus           ,
													"num_reg_id_trib" => $unaTransaccion->num_reg_id_trib   
                                                    );                                                     
                                                         
          // se incrementa el contador
          $num_transacciones = $num_transacciones + 1;
        }
        
        // se libera el cursor
        $result_transaccion->free_result();
       
        // se devuelve el arreglo con la lista de clientes
        return $arr_clientes;

  }
  
  

}



?>
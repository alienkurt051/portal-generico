<?php
/**
 * 
 */
class Mcontador extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function getcontador($id_usuario){

		$this->db->select('pss_co.fecha_actualizacion,pss_co.instancia,pss_co.contador,pss_inst.rfc_cliente,pss_co.conteo_acumulado');
		$this->db->from('pss_usuario usu');
		$this->db->join('pss_con_consulta_conteo pss_co','usu.id_usuario_pss=pss_co.id_cliente_ebs','inner');
		$this->db->join('pss_con_instancia_contador pss_inst','usu.id_usuario_pss=pss_inst.id_cliente_ebs','inner');
		$this->db->where('usu.id_usuario_pss',$id_usuario);
		$r = $this->db->get();
		return $r->result();

	}
}
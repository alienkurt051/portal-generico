<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Pss_cont_detalle extends ORM {
        public $table_name = "pss_cont_detalle";
        public $primary_key = 'id_cont_detalle';

        function _init()
        {
                self::$fields = array(
                        'id_cont_detalle'        => ORM::field('auto[10]'),
                        'id_tipo_contador'       => ORM::field('char[10]'),
                        'rfc'                    => ORM::field('char[20]'),
                        'num_instancia'          => ORM::field('char[10]'),
                        'contador'               => ORM::field('char[100]'),
                        'conteo_acumulado'       => ORM::field('char[20]'),
                        'fecha_lectura'          => ORM::field('char[20]')
                );
        }
}

?>

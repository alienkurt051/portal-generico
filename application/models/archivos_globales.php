<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Archivos_globales extends ORM {
        public $table_name = "archivos_globales";
        public $primary_key = 'id_archivo';

        function _init()
        {
                self::$fields = array(
                        'id_archivo'      => ORM::field('int'),      
                        'id_global'       => ORM::field('int'), 
                        'ruta_archivo'    => ORM::field('string'),
                        'id_forma_pago'   => ORM::field('string')
                        
                );
        }
}

?>

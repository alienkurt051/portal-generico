<?php

// modelo para consultas en factura gobal

class model_factura_global extends CI_model {
    
    /* Datos por default a escribir en bitácora */
    private $datos_bitacora = array(
        'id_accion'     => null,
        'id_usuario'    => null,
        'dir_ip'        => '127.0.0.1',
        'observaciones' => null
    );    
    /*  Con estatus cero (no han entrado a ninguna global) facturado en cero
     *  (tickets que no se han facturado, por lo tanto pueden entrar en global )
     * 
     * @param $sucursal         int Identificador de la sucursal
     * @param $fecha            int Fecha con formato aaaamm ej. 201801
     * @param $cantidad_tickets int Límite de tickets a obtener
     * 
     * @return mix Si hay resultados un objeto, null de lo contrario
     *          */

    public function obtener_tickets($sucursal, $fecha, $cantidad_tickets) {
        $consulta_tickets = "
            select * from control_global_$sucursal
                   where
                       sucursal = $sucursal
                       and fecha_num_ticket = $fecha
                       and estatus = 0
                       and facturado = 0
                       and id_trx33 IS NOT NULL
                       limit $cantidad_tickets;
                  ";
        return $this->db->query($consulta_tickets);
    }

    /*  Se obtiene la serie de la tabla emi_config_global dependiendo del 
     *  identidicador de la sucursal, si no hay se regresa vacía.
     * 
     * @param $sucursal         int Identificador de la sucursal
     * 
     * @return string Si hay resultados regresa serie, sino vacía.
     *          */

    public function obtener_serie($sucursal) {
        $consulta_serie = ""
                . "select serie from emi_config_global "
                . "where id_entidad = $sucursal "
                . "limit 1";
        $serie = $this->db->query($consulta_serie);
        /* Si hay resultados */
        if(!empty($serie->result_array())){
            return $serie->result_array()[0]['serie'];
        }
        /* Sino regresa la serie vacía */
        return "";        
    }
    
    /*  Valida si el id de la entidad es una organización o una sucursal.
     * 
     *  @param int $sucursal id de la entidad a validar
     * 
     *  @return bool True si es organización, False si es sucursal.
     *      */    
    public function validar_entidad($sucursal) {
        $entidad = Model\C_entidades::find($sucursal,false);
        $id_entidad_padre = $entidad->id_entidad_padre;
        /* Si es organiación */
        if($id_entidad_padre == null || $id_entidad_padre == ""){
            return true;
        }
        /* De lo contrario es sucursal */
        return false;
    }
    
    /* Se hace registro en la bitácora del portal ss. No se utiliza la función
     * registrar_evento_bitacora ya que el script no se corre en un servidor,
     * por lo cual no se puede obtener la ip.
     * 
     * @param int id_accion identificador de la accion que se realiza
     * @param object Identificador del usuario que realiza la acción (admin)
     * 
     * @return mix True si se actualizó bien, de lo contrario mensaje de error.
     *      */
    
    public function registrar_bitacora($id_accion, $observaciones, $id_usuario = 1) {
        $this->datos_bitacora['id_accion']      = $id_accion;
        $this->datos_bitacora['id_usuario']     = $id_usuario;
        $this->datos_bitacora['observaciones']  = $observaciones;
        $this->db->insert('pss_bitacora', $this->datos_bitacora);
    }    

}

?>
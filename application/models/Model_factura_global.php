<?php

// modelo para buscar los resultados de una ejecucion de factura global

class model_factura_global extends CI_model {

  // resultado de ejecucion de factura gloabl
  public function resultados_factura_global($id_emi_config_global, $es_factura_global_previa, $limite_paginacion, $offset)
  {
      // se obtiene la entidad del id_config_global
      $config_global = Model\Emi_config_global::find($id_emi_config_global);
      
      $id_entidad = $config_global->id_entidad;
      
      $tabla_control_global = "control_global_".$id_entidad;

      // se construye la busqueda de clientes
	  $query_buscar_resultados = "select 
                                    id               ,
                                    sucursal         ,
                                    numero_ticket    ,
                                    fecha            ,
                                    subtotal         ,
                                    descuento        ,
                                    iva              ,
                                    total            ,
                                    fecha_num_ticket ,
                                    id_trx33         ,
                                    facturado        ,
                                    estatus          ,
                                    id_global        
                                  from ".$tabla_control_global."
                                  where id_global = 1
                                  order by numero_ticket
                                  limit ".$offset.", ".$limite_paginacion;

											
        $result_transaccion = $this->db->query($query_buscar_resultados);
        
        // se inicia el contador de transacciones
        $num_transacciones = 1;
        
        $arr_clientes = array();
        
        // se obtienen los datos de cada transaccion
        foreach ( $result_transaccion->result() as $unaTransaccion)
        {
        
          $arr_global[$num_transacciones] = array("id"               => $unaTransaccion->id                 ,
                                                  "sucursal"         => $unaTransaccion->sucursal           ,
                                                  "numero_ticket"    => $unaTransaccion->numero_ticket      ,
                                                  "fecha"            => $unaTransaccion->fecha              ,
                                                  "subtotal"         => $unaTransaccion->subtotal           ,
                                                  "descuento"        => $unaTransaccion->descuento          ,
                                                  "iva"              => $unaTransaccion->iva                ,
                                                  "total"            => $unaTransaccion->total              ,
                                                  "fecha_num_ticket" => $unaTransaccion->fecha_num_ticket   ,
                                                  "id_trx33"         => $unaTransaccion->id_trx33           ,
                                                  "facturado"        => $unaTransaccion->facturado          ,
                                                  "estatus"          => $unaTransaccion->estatus            ,
                                                  "id_global"        => $unaTransaccion->id_global          ,
                                                  );                                                     
                                                         
          // se incrementa el contador
          $num_transacciones = $num_transacciones + 1;
        }
        
        // se libera el cursor
        $result_transaccion->free_result();
       
        // se devuelve el arreglo con los resultados
        return $arr_global;

  }
  
  
  public function tickets_en_factura_global($id_emi_config_global)
  {
      // se obtiene la entidad del id_config_global
      $config_global = Model\Emi_config_global::find($id_emi_config_global);
      
      $id_entidad = $config_global->id_entidad;
            
      // si es factura previa
      if ( $config_global->tipo_global == 0 ) {
          // la tabla de control se llama con previa
          $tabla_control_global = "control_global_".$id_entidad."_prev";
      } else {
          $tabla_control_global = "control_global_".$id_entidad;

      }
      
      // (1) se obtienen los id_global de las globales
      $query_id_global = "select distinct id_global from ".$tabla_control_global." where id_emi_config_global = ".$id_emi_config_global;
      
      $result_id_global = $this->db->query($query_id_global);

      $ids_globales = "";
      $num_linea = 1;
      $hay_ids_globales = false;
      // se obtienen los datos de cada transaccion
      foreach ( $result_id_global->result() as $unIdGlobal)
      {
          if ( $num_linea == 1 ) {
              $ids_globales = '"'.$unIdGlobal->id_global.'"';
              $hay_ids_globales = true;
          } else {
              $ids_globales .= ',"'.$unIdGlobal->id_global.'"';
          }         
                                                       
        // se incrementa el contador
        $num_linea = $num_linea + 1;
      }
      
      // se libera el cursor
      $result_id_global->free_result();

      // se buscan los trx33 de las globales
      $trx33_globales = "";
      $hay_trx33_globales = false;
      
      // si se tienen ids globales 
      if ( $hay_ids_globales ) {
          // (2) se obtienen los idtrx33r de las globales
          $query_trx33_global = "select id_trx33_r from emi_trx33_r where id_trx_erp in (".$ids_globales.")";
          
          $result_trx33_global = $this->db->query($query_trx33_global);
          
          
          $num_linea = 1;
          // se obtienen los datos de cada transaccion
          foreach ( $result_trx33_global->result() as $unTrx33Global)
          {
              if ( $num_linea == 1 ) {
                  $hay_trx33_globales = true;
                  $trx33_globales = '"'.$unTrx33Global->id_trx33_r.'"';
              } else {
                  $trx33_globales .= ',"'.$unTrx33Global->id_trx33_r.'"';
              }            
                                                           
            // se incrementa el contador
            $num_linea = $num_linea + 1;
          }
          
          // se libera el cursor
          $result_trx33_global->free_result();
      }

      // se construye la busqueda de clientes
      $query_buscar_resultados = "";
      if ( $hay_trx33_globales ) {
	      $query_buscar_resultados = "select 
                                        cg.id                   ,
                                        r.serie  as serie_r     ,
                                        r.id_trx_erp            ,
                                        r.folio  as folio_global,
                                        r.id_trx33_r as trx33rg ,
                                        x.uuid                  ,
                                        cg.sucursal             ,
                                        cg.numero_ticket        ,
                                        cg.fecha                ,
                                        cg.subtotal             ,
                                        cg.descuento            ,
                                        cg.iva                  ,
                                        cg.total                ,
                                        cg.fecha_num_ticket     ,
                                        cg.id_trx33             ,
                                        cg.facturado            ,
                                        cg.estatus              ,
                                        cg.id_global            ,
                                        ag.ruta_archivo         ,
                                        ag.id_forma_pago    
                                      from emi_config_global ecg
                                      inner join ".$tabla_control_global." cg on ecg.id_emi_config_global = cg.id_emi_config_global
                                      and ecg.id_emi_config_global = ".$id_emi_config_global." 
                                      left  join archivos_globales ag on ag.id_global = cg.id_global
                                      left  join emi_trx33_r r on ag.id_global = r.id_trx_erp
                                      and r.id_trx33_r in (".$trx33_globales.")
                                      left  join emi_trx33_xml x on r.id_trx33_r = x.id_trx33
                                      and ecg.serie = r.serie
                                      order by numero_ticket";
      } else {
	     $query_buscar_resultados = "select 
                                       cg.id                          ,
                                       ecg.serie  as serie_r          ,
                                       'No disponible' as id_trx_erp  ,
                                       'No disponible' as folio_global,
                                       'No disponible' as trx33rg     ,
                                       'No disponible' as uuid        ,
                                       cg.sucursal                    ,
                                       cg.numero_ticket               ,
                                       cg.fecha                       ,
                                       cg.subtotal                    ,
                                       cg.descuento                   ,
                                       cg.iva                         ,
                                       cg.total                       ,
                                       cg.fecha_num_ticket            ,
                                       cg.id_trx33                    ,
                                       cg.facturado                   ,
                                       cg.estatus                     ,
                                       cg.id_global                   ,
                                       ag.ruta_archivo                ,
                                       ag.id_forma_pago    
                                     from emi_config_global ecg
                                     inner join ".$tabla_control_global." cg on ecg.id_emi_config_global = cg.id_emi_config_global
                                     and ecg.id_emi_config_global = ".$id_emi_config_global." 
                                     left  join archivos_globales ag on ag.id_global = cg.id_global
                                     order by numero_ticket";
      }

											
        $result_transaccion = $this->db->query($query_buscar_resultados);
        
        // se inicia el contador de transacciones
        $num_transacciones = 1;
        
        $arr_clientes = array();
        
        // se obtienen los datos de cada transaccion
        foreach ( $result_transaccion->result() as $unaTransaccion)
        {
        
          $arr_global[$num_transacciones] = array("id"               => $unaTransaccion->id                ,
                                                  "serie_r"          => $unaTransaccion->serie_r           ,
                                                  "id_trx_erp"       => $unaTransaccion->id_trx_erp        ,
                                                  "folio_global"     => $unaTransaccion->folio_global      ,
                                                  "trx33rg"          => $unaTransaccion->trx33rg           ,
                                                  "uuid"             => $unaTransaccion->uuid              ,
                                                  "sucursal"         => $unaTransaccion->sucursal          ,
                                                  "numero_ticket"    => $unaTransaccion->numero_ticket     ,
                                                  "fecha"            => $unaTransaccion->fecha             ,
                                                  "subtotal"         => $unaTransaccion->subtotal          ,
                                                  "descuento"        => $unaTransaccion->descuento         ,
                                                  "iva"              => $unaTransaccion->iva               ,
                                                  "total"            => $unaTransaccion->total             ,
                                                  "fecha_num_ticket" => $unaTransaccion->fecha_num_ticket  ,
                                                  "id_trx33"         => $unaTransaccion->id_trx33          ,
                                                  "facturado"        => $unaTransaccion->facturado         ,
                                                  "estatus"          => $unaTransaccion->estatus           ,
                                                  "id_global"        => $unaTransaccion->id_global         ,
                                                  "ruta_archivo"     => $unaTransaccion->ruta_archivo      ,
                                                  "id_forma_pago"    => $unaTransaccion->id_forma_pago          
                                                  );                                                     

                                                      
          // se incrementa el contador
          $num_transacciones = $num_transacciones + 1;
        }
        
        // se libera el cursor
        $result_transaccion->free_result();
       
        // se devuelve el arreglo con los resultados
        return $arr_global;

  }
  
  public function cifras_control_global($id_emi_config_global)
  {
      // se obtiene la entidad del id_config_global
      $config_global = Model\Emi_config_global::find($id_emi_config_global);
      
      $id_entidad = $config_global->id_entidad;
            
      // si es factura previa
      if ( $config_global->tipo_global == 0 ) {
          // la tabla de control se llama con previa
          $tabla_control_global = "control_global_".$id_entidad."_prev";
      } else {
          $tabla_control_global = "control_global_".$id_entidad;

      }

      // se construye la busqueda de clientes
	  $query_buscar_resultados = "select 
                                    id               ,
                                    sucursal         ,
                                    numero_ticket    ,
                                    fecha            ,
                                    subtotal         ,
                                    descuento        ,
                                    iva              ,
                                    total            ,
                                    fecha_num_ticket ,
                                    id_trx33         ,
                                    facturado        ,
                                    estatus          ,
                                    id_global        
                                  from ".$tabla_control_global."
                                  where id_emi_config_global = ".$id_emi_config_global." 
                                  order by numero_ticket";

											
        $result_transaccion = $this->db->query($query_buscar_resultados);
        
        // se inicia el contador de transacciones
        $num_transacciones = 1;
        
        $arr_clientes = array();
        
        // se obtienen los datos de cada transaccion
        foreach ( $result_transaccion->result() as $unaTransaccion)
        {
        
          $arr_global[$num_transacciones] = array("id"               => $unaTransaccion->id                 ,
                                                  "sucursal"         => $unaTransaccion->sucursal           ,
                                                  "numero_ticket"    => $unaTransaccion->numero_ticket      ,
                                                  "fecha"            => $unaTransaccion->fecha              ,
                                                  "subtotal"         => $unaTransaccion->subtotal           ,
                                                  "descuento"        => $unaTransaccion->descuento          ,
                                                  "iva"              => $unaTransaccion->iva                ,
                                                  "total"            => $unaTransaccion->total              ,
                                                  "fecha_num_ticket" => $unaTransaccion->fecha_num_ticket   ,
                                                  "id_trx33"         => $unaTransaccion->id_trx33           ,
                                                  "facturado"        => $unaTransaccion->facturado          ,
                                                  "estatus"          => $unaTransaccion->estatus            ,
                                                  "id_global"        => $unaTransaccion->id_global          ,
                                                  );                                                     
                                                         
          // se incrementa el contador
          $num_transacciones = $num_transacciones + 1;
        }
        
        // se libera el cursor
        $result_transaccion->free_result();
       
        // se devuelve el arreglo con los resultados
        return $arr_global;

  }
  
  public function obtener_arreglo_programacion($tipo_ejecucion, $meses_elegidos, $hora_ejecucion, $dia_ejecucion) {
      // genera el arreglo de ejecucion que se usara para programar las facturas globales
      $arr_ejecucion = array();
            
      switch ( $tipo_ejecucion ) {
          // intervalo
          case 1:
          

          break;
          
          // mensual
          case 2:
          
          for($conteo_mes = 1; $conteo_mes <= 12; $conteo_mes++) {
              
              $continuar = false;
              
              // si el mes esta entre los elegidos
              foreach($meses_elegidos as $mes_elegido) {
                  if ( $conteo_mes == $mes_elegido ) {
                      $continuar = true;
                      break;
                  }
              }
              
              if ( $continuar ) {
                  $arr_ejecucion[$conteo_mes]["mes_numero"]       = $conteo_mes;
                  $arr_ejecucion[$conteo_mes]["mes"]              = $this->mes_por_numero($conteo_mes);
                  
                  $fecha_inicio = date("Y")."-".$conteo_mes."-01";
                  $arr_ejecucion[$conteo_mes]["fecha_inicio"]     = date("Y-m-d", strtotime($fecha_inicio));
                  $arr_ejecucion[$conteo_mes]["hora_inicio"]      = "00:00:00";
                  $arr_ejecucion[$conteo_mes]["fecha_fin"]        = date("Y-m-t", strtotime($fecha_inicio));
                  $arr_ejecucion[$conteo_mes]["hora_fin"]         = "23:59:59";
                  
                  $fecha_ejecucion = date("Y-m-t", strtotime($fecha_inicio));
                  if ( $dia_ejecucion != null ) {
                      if ( $dia_ejecucion != "md" ) {
                          if ( $dia_ejecucion != "sd" ) {
                              $dias_extra = '+'.$dia_ejecucion.' day';
                              $fecha_ejecucion = date("Y-m-d", strtotime($fecha_ejecucion.$dias_extra));
                          } else {
                              // es el dia siguiente
                              $dias_extra = '+1 day';
                              $fecha_ejecucion = date("Y-m-d", strtotime($fecha_ejecucion.$dias_extra));                              
                          }
                      }
                  }
                  
                  $arr_ejecucion[$conteo_mes]["fecha_ejecucion"]  = $fecha_ejecucion;
                  $arr_ejecucion[$conteo_mes]["hora_ejecucion"]   = $hora_ejecucion;
              } else {
                  continue;
              }
          }
          break;
          
          // quincenal
          case 3:
          
          
          break;
          
          
          // semanal
          case 4:
          
          
          break;
          
          // diario
          case 5:
          
          // se verifica si es ano bisiesto
          $es_ano_bisiesto = false;
          $total_dias = 365;
          $year = date("Y");
          if ((0 == $year % 4) & (0 != $year % 100) | (0 == $year % 400)) {
              $es_ano_bisiesto = true;
              $total_dias = 366;
          }
          
          $conteo_mes  = 1;
          $conteo_dias = 1;
          $dia_mes = 1;
          $dia_fin_mes = 31;
          
          for($conteo_mes = 1; $conteo_mes <= 12; $conteo_mes++) {
              
              // meses con 31 dias
              if ( $conteo_mes == 1 || $conteo_mes == 3 || $conteo_mes == 5 || $conteo_mes == 7 || $conteo_mes == 8 || $conteo_mes == 10 || $conteo_mes == 12 ) {
                  $dia_fin_mes = 31;
              } 

              // meses con 30 dias
              if ( $conteo_mes == 4 || $conteo_mes == 6 || $conteo_mes == 9 || $conteo_mes == 11 ) {
                  $dia_fin_mes = 30;
              } 
              
              // febrero
              if ( $conteo_mes == 2 ) {
                  $dia_fin_mes = 28;
                  if ( $es_ano_bisiesto ) {
                      $dia_fin_mes = 29;
                  }
              }

              for ($i = 1; $i <= $dia_fin_mes; $i++) {
                  $fecha_inicio    = date("Y")."-".$conteo_mes."-".$i;
                  
                  // si el mes esta entre los elegidos
                  $continuar = false;
                  foreach($meses_elegidos as $mes_elegido) {
                      if ( $conteo_mes == $mes_elegido ) {
                          $continuar = true;
                          break;
                      }
                  }
                  
                  if ( $continuar ) {
                      $arr_ejecucion[$conteo_dias]["mes_numero"]       = $conteo_mes;
                      $arr_ejecucion[$conteo_dias]["mes"]              = $this->mes_por_numero($conteo_mes);
                      $arr_ejecucion[$conteo_dias]["fecha_inicio"]     = date("Y-m-d", strtotime($fecha_inicio));
                      $arr_ejecucion[$conteo_dias]["hora_inicio"]      = "00:00:00";
                      $arr_ejecucion[$conteo_dias]["fecha_fin"]        = date("Y-m-d", strtotime($fecha_inicio));
                      $arr_ejecucion[$conteo_dias]["hora_fin"]         = "23:59:59";
                      
                      $fecha_ejecucion = date("Y-m-d", strtotime($fecha_inicio));
                      if ( $dia_ejecucion != null ) {
                          if ( $dia_ejecucion != "md" ) {
                              if ( $dia_ejecucion != "sd" ) {
                                  $fecha_ejecucion = date("Y-m-t", strtotime($fecha_inicio));
                                  $dias_extra = '+'.$dia_ejecucion.' day';
                                  $fecha_ejecucion = date("Y-m-d", strtotime($fecha_ejecucion.$dias_extra));
                              } else {
                                  // es el dia siguiente
                                  $dias_extra = '+1 day';
                                  $fecha_ejecucion = date("Y-m-d", strtotime($fecha_ejecucion.$dias_extra));                              
                              }
                          }
                      }
                      
                      $arr_ejecucion[$conteo_dias]["fecha_ejecucion"]  = $fecha_ejecucion;
                      $arr_ejecucion[$conteo_dias]["hora_ejecucion"]   = $hora_ejecucion;
                      
                      $conteo_dias++;
                  } else {
                      continue;
                  }
              }
          }
          
          break;
      }
      
      return $arr_ejecucion;
  }

  public function mes_por_numero($id_mes) {
      $mes = "";
      
      if ($id_mes == 1) {
          $mes = "Enero";
      } 
      
      if ($id_mes == 2) {
          $mes = "Febrero";
      } 
      
      if ($id_mes == 3) {
          $mes = "Marzo";
      } 
      
      if ($id_mes == 4) {
          $mes = "Abril";
      } 
      
      if ($id_mes == 5) {
          $mes = "Mayo";
      } 
      
      if ($id_mes == 6) {
          $mes = "Junio";
      } 
      
      if ($id_mes == 7) {
          $mes = "Julio";
      } 
      
      if ($id_mes == 8) {
          $mes = "Agosto";
      } 
      
      if ($id_mes == 9) {
          $mes = "Septiembre";
      } 
      
      if ($id_mes == 10) {
          $mes = "Octubre";
      } 
      
      if ($id_mes == 11) {
          $mes = "Noviembre";
      } 
      
      if ($id_mes == 12) {
          $mes = "Diciembre";
      } 
      
      return $mes;
  }
  
  /**
   * Obtiene una lista de proceso/ejecuciones de factura global a partir de un 
   * arreglo de filtros.
   * 
   * @param array $datos_filtro Arreglo con datos de filtro para la consulta
   * @return array Arreglo de objectos de la lista de procesos/ejecuciones
   */
  public function buscar_ejecuciones($datos_filtro = [] ) {
    /* si hay filtro de forma de pago se hace join con la control_global_n y con 
     * la archivos_globales para verificar la forma de pago
     *      */
    if (!empty($datos_filtro['id_forma_pago']) && isset($datos_filtro['id_forma_pago'])) {
      $this->db->join('control_global_' . $datos_filtro['id_entidad'], 'id_emi_config_global')
              ->join('archivos_globales', 'id_global')
              ->group_by('id_global')
              ->select("archivos_globales.id_global");
    }
    // Se recorre todo el arreglo y se agregan los filtros como clave - valor
    foreach ($datos_filtro as $key => $dato) {
      if (!empty($dato) && isset($dato)) {
        $this->db->where($key, $dato);
      }
    }
    $this->db->order_by("id_emi_config_global","desc");
    $this->db->select("v_emi_config_global.*");
    $this->db->limit(1000);
    return Model\V_emi_config_global::all();
  }
  
  /**
   * Obtiene los detalles (cada xml que se forma) de un proceso de ejecución de 
   * una factura global.
   * 
   * @param array $datos_filtro Arreglo con datos de filtro para la consulta
   * @return array Arreglo de objectos de la lista de detalles/xmls
   */
  public function obtener_detalles($datos_filtro = []) {
    $this->db->join('control_global_' . $datos_filtro['id_entidad'], 'id_emi_config_global')
            ->join('archivos_globales', 'id_global')
            ->join('v_c_forma_pago', 'id_forma_pago')
            ->group_by('id_global')
            ->order_by("id_emi_config_global","desc")
            ->select("archivos_globales.id_global as 'ID Global',
                      archivos_globales.ruta_archivo as 'Ruta archivo XML',
                      v_c_forma_pago.descripcion as 'Forma de pago'");
    // Se recorre todo el arreglo y se agregan los filtros como clave - valor
    foreach ($datos_filtro as $key => $dato) {
      if (!empty($dato) && isset($dato)) {
        $this->db->where($key, $dato);
      }
    }
    $query = $this->db->get('v_emi_config_global');
    log_message('debug', $this->db->last_query());
    return $query->result();
  }
  
  /**
   * Obtiene una lista de datos de los tickets que entraron en el reporte de  la 
   * factura global a partir del id_global (identificador de cada XML).
   * 
   * @param int $id_global Identificador de la factura global
   * @return array Arreglo de objectos para listar en el reporte 
   */
  public function consultar_reporte($id_global) {
    // Se obtienen los id_trx33 que le pertencen al id_global separados por comas
    $this->db->select('GROUP_CONCAT(idtrx33) as ids_trx33')
            ->from('emi_trx33_global')
            ->where('id_global', $id_global)
            ->group_by('id_global');
    $ids_trx33 = $this->db->get();
    $resul = $ids_trx33->row();

    // Si no está vacío el resultado es que si hay tickets asociados
    if (!empty($resul->ids_trx33) && isset($resul->ids_trx33)) {
      $arr_ids = explode(',',$resul->ids_trx33);
      $this->db->where_in('id_trx33', $arr_ids);
    } else {
      $this->db->where_in('id_trx33', 0);
    }
    // Se obtienen los resultados de la vista del reporte de la factura global
    // dependiendo de los ids_trx33
    $query = $this->db->get('v_reporte_factura_global');
    return $query->result();
  }

  /**
   * Obtiene una propiedad que será el nombre para el reporte de la factura global
   * la propiedad debe existir en la tabla adm_c_propiedades con el nombre de 
   * factura.global.nombre.reporte
   * 
   * @param int $id_global Identificador de la factura global
   * @return string Nombre con el que aparecerá el reporte
   */
  public function consultar_nombre_reporte($id_global) {
    // Se obtiene el query a partir de la propiedad factura.global.nombre.reporte
    $this->db->select('valor_omision')
            ->from('adm_c_propiedades')
            ->like('propiedad', 'factura.global.nombre.reporte');
    $query_propiedad = $this->db->get();
    // Si la propiedad no existe
    if($query_propiedad->num_rows() < 1){
      return "PROPIEDAD_NO_EXISTE_EN_ADM_C_PROP";
    }
    // Se adquiere el primer registro encontrado y se asigna el id_global
    $resul = $query_propiedad->row();
    $query_nombre = str_replace('?', $id_global, $resul->valor_omision);
    // Si el query de la propiedad no se ejecuta correctamente
    if (!$this->db->simple_query($query_nombre)){
      return "QUERY_MAL_FORMADO_EN_PROPIEDAD";
    }
    $query = $this->db->query($query_nombre);
    $registro_nomnbre = $query->row(); // EL primer resultado
    // Primer columna que se proyecte
    foreach ($registro_nomnbre as $nombre) {
      return $nombre; 
    }
  }

}



?>
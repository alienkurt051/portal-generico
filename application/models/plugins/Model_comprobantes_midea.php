<?php

// modelo para buscar las transacciones para facturar

class model_comprobantes_midea extends CI_model {
    /* Datos que se van a seleccionar en la consulta */

    private $datos_comprobantes = array(
        'id_trx33', 'rfc_emisor',
        'nombre_emisor', 'rfc_receptor',
        'nombre_receptor', 'serie',
        'folio', 'uuid', 'fecha_timbrado',
        'moneda', 'total'
    );
    private $datos_informacion_documento = array(
        'nombre_emisor', 'serie', 'folio',
        'nombre_receptor', 'moneda', 'total'
    );
    private $datos_direccion_sucursal = array(
        'entidad','estado','calle','codigo_postal' 
    );    
    private $view_mis_comprobantes = "v_pss_listado_comprobantes_autofactura v ";
    private $datos_encabezado = array(
        'orden_compra','gln_comprador','seccion_departamento',
        'gln_proveedor','numero_proveedor','gln_entrega','dias_plazo_pago'
    );    
    private $datos_conceptos = array(
        'clave_prod_serv','(select numero_identificacion from pss_emi_trx33_conceptos pc where c.id_trx33_conceptos=pc.id_trx33_conceptos) as numero_identificacion',
        'unidad','descripcion',
        'cantidad','descripcion','valor_unitario','importe','id_trx33_conceptos',
        '(select orden_compra from pss_emi_trx33_conceptos pc where c.id_trx33_conceptos=pc.id_trx33_conceptos) as orden_compra',
        'numero_linea',
    );
    private $datos_encabezado_trx33 = array(
        'emi.fecha', 'emi.serie', 'emi.folio', 'emi.subtotal',
        'emi.total', 'imp.tasa_o_cuota', 'imp.importe','emi.importe_letra'
    );
    private $datos_xml_timbrado = array(
        'xml_timbrado'
    );

    // Fetch records
    public function buscar_comprobantes($rowno, $rowperpage, $datos_consulta) {
        /* Se obtienen los datos a seleccionar en forma de lista */
        $datos_select = implode(",", $this->datos_comprobantes);

        $query_transacciones = " select $datos_select  
                                    from $this->view_mis_comprobantes 
                                 where ";

        $query_transacciones .= $this->agregar_clausulas($datos_consulta);
        $query_transacciones .= " LIMIT $rowno,$rowperpage";
        $query = $this->db->query($query_transacciones);

        $resultados = $query->result_array();
        $query->free_result();
        return $resultados;
    }

    // Select total records
    public function total_registros($datos_consulta) {

        $query_transacciones = " select count(*) as allcount 
                                    from $this->view_mis_comprobantes
                                  where ";

        $query_transacciones .= $this->agregar_clausulas($datos_consulta);

        $query = $this->db->query($query_transacciones);
        $total = $query->result_array()[0]['allcount'];

        $query->free_result();
        return $total;
    }

    /* Se agregan las claúslas where al query que hace el filtro de búsqueda */

    public function agregar_clausulas($datos_filtro) {
        $clausulas = "";
        /* Clausulas para verificar que sean documentos timbrados */
        $clausulas .= "((length(v.uuid) = 36) or v.pdf is not null )";

        if ($datos_filtro['rfc_emisor'] != null && $datos_filtro['rfc_emisor'] != "") {
            $clausulas .= " and v.rfc_emisor = '" . $datos_filtro['rfc_emisor'] . "'";
        }

        if ($datos_filtro['nombre_emisor'] != null && $datos_filtro['nombre_emisor'] != "") {
            $clausulas .= " and v.nombre_emisor = '" . $datos_filtro['nombre_emisor'] . "'";
        }

        // si se tiene receptor
        if ($datos_filtro['rfc_receptor'] != null && $datos_filtro['rfc_receptor'] != "") {
            $clausulas .= " and v.rfc_receptor = '" . $datos_filtro['rfc_receptor'] . "'";
        }

        if ($datos_filtro['nombre_receptor'] != null && $datos_filtro['nombre_receptor'] != "") {
            $clausulas .= " and v.nombre_receptor = '" . $datos_filtro['nombre_receptor'] . "'";
        }

        // si se tiene serie y rango de folios
        if ($datos_filtro['serie'] != null && $datos_filtro['serie'] != "") {
            $clausulas .= " and v.serie = '" . $datos_filtro['serie'] . "'";
        }

        if ($datos_filtro['folio_inicio'] != null && $datos_filtro['folio_inicio'] != "" ) {            
            if ($datos_filtro['folio_fin'] != null && $datos_filtro['folio_fin'] != "") {
                $clausulas .= " and v.folio between " . $datos_filtro['folio_inicio'] . " AND " . $datos_filtro['folio_fin'] . "";
            }else{
                $clausulas .= " and v.folio = " . $datos_filtro['folio_inicio'] . " "; 
            }            
        }
        return $clausulas;
    }
    
    public function obtener_informacion_documento($id_trx33) {
        /* Se obtienen los datos de información del documento en forma de lista */
        $datos_select = implode(",", $this->datos_informacion_documento);
        $query_transacciones = " select $datos_select "
                . "from emi_trx33 "
                . "where id_trx33=$id_trx33";
        $query = $this->db->query($query_transacciones);
        $resultados = $query->result_array();
        $query->free_result();
        return $resultados;
    }
    public function direccion_sucursal($id_trx33) {

        $datos_select = implode(",", $this->datos_direccion_sucursal);
        /* Primeramente se verifica si ya existe algún registro en la tabla
         * pss_emi_trx33_addenda, si no se obtiene de la tabla c_entidades
         *          */
        $this->db->select($datos_select)
                ->from('pss_emi_trx33_addenda')
                ->where('id_trx33', $id_trx33);

        $consulta_pss = $this->db->get();

        if ($consulta_pss->num_rows() > 0) {
            return $consulta_pss->row_array();
        }
        /* Sino se obtiene la dirección de la sucursal donde se hizo la transacción  */
        $this->db->select('id_sucursal')
                ->from('emi_trx33_r')
                ->where('id_trx33_r', $id_trx33);

        $subquery = $this->db->get_compiled_select();
        $this->db->select($datos_select)
                ->from('v_c_entidades')
                ->where("`id_entidad` = ($subquery)", null, false);

        $consulta_emi = $this->db->get();
        /* SI hay resultados no selecciona a la matriz */
        if ($consulta_emi->num_rows() > 0) {
            return $consulta_emi->row_array();
        }        
        /* Si no hay resultados se buscará la dirección de la matriz */
        $this->db->select($datos_select)
                ->from('v_c_entidades')
                ->where('id_entidad_padre is NULL', NULL, FALSE)
                ->limit(1);
        $consulta_matriz = $this->db->get();       
        return $consulta_matriz->row_array();
    }

    public function obtener_datos_addenda($id_trx33) {
        /* Se obtiene la dirección de la sucursal donde se hizo la transacción  */
        $datos_select = implode(",", $this->datos_encabezado);
        $query_transacciones = "select $datos_select "
                . "from pss_emi_trx33_addenda "
                . "where id_trx33=$id_trx33";
        $query = $this->db->query($query_transacciones);
        $resultados = $query->result_array();
        $query->free_result();
        return empty($resultados) ? false : $resultados;
    }
    public function obtener_conceptos($id_trx33) {
        /* Se obtiene la dirección de la sucursal donde se hizo la transacción  */
        $datos_select = implode(",", $this->datos_conceptos);
        $query_transacciones = "select $datos_select "
                . "from emi_trx33_conceptos c "
                . "where id_trx33=$id_trx33 order by id_trx33_conceptos";
        $query = $this->db->query($query_transacciones);
        $resultados = $query->result_array();
        $query->free_result();
        return empty($resultados) ? false : $resultados;
    }
    
    public function obtener_datos_encabezado_nodo($id_trx33) {
        /* Se obtiene la dirección de la sucursal donde se hizo la transacción  */
        $datos_select = implode(",", $this->datos_encabezado_trx33);
        $query_transacciones = "select $datos_select "
                . "from emi_trx33 emi "
                . "inner join emi_trx33_impuestos_r imp "
                . "on(emi.id_trx33=imp.id_trx33_r) "
                . "where id_trx33=$id_trx33";
        $query = $this->db->query($query_transacciones);
        $resultados = $query->result_array();
        $query->free_result();
        return empty($resultados) ? false : $resultados[0];
    }
    public function obtener_concepto($id_trx33_conceptos) {
        /* Se obtiene la dirección de la sucursal donde se hizo la transacción  */
        $datos_select = implode(",", $this->datos_conceptos);
        $query_transacciones = "select $datos_select "
                . "from emi_trx33_conceptos c "
                . "where id_trx33_conceptos=$id_trx33_conceptos";
        $query = $this->db->query($query_transacciones);
        $resultados = $query->result_array();
        $query->free_result();
        return empty($resultados) ? false : $resultados[0];
    }
    
    public function obtener_xml_timbrado($id_trx33) {        
        /* Se obtiene el xml timbrado de la transacción  */
        $datos_select = implode(",", $this->datos_xml_timbrado);
        $query_transacciones = "select $datos_select "
                . "from emi_trx33_xml  "
                . "where id_trx33=$id_trx33";
        $query = $this->db->query($query_transacciones);
        $resultados = $query->result_array();
        $query->free_result();
        return empty($resultados) ? false : $resultados[0]['xml_timbrado'];        
    }
    public function actualizar_xml($id_trx33,$nuevo_xml) {
        $this->db->set('xml_timbrado', $nuevo_xml);
        $this->db->where('id_trx33', $id_trx33);
        $this->db->update('emi_trx33_xml');
    }
    public function actualizar_addenda($id_trx33, $encabezado, $conceptos) {
        /* ENCABEZADO ADDENDA */
        $this->crear_actualizar_addenda($id_trx33, $encabezado);
        /* CONCEPTOS */
        foreach ($conceptos as $concepto) {
            $this->crear_actualizar_concepto($id_trx33, $concepto);
        }
    }

    public function crear_actualizar_addenda($id_trx33, $encabezado) {
        /* Se construye instrucción INSERT ON DUPLICATE KEY UPDATE 
         * para actualizar el registro y si no existe insertarlo
         *          */
        $datos_insert_update = "";
        foreach ($encabezado as $columna => $dato_encabezado) {
            $datos_insert_update .= " $columna='$dato_encabezado', ";
        }
        $datos_insert_update .= " id_trx33='$id_trx33' ";

        $query_transacciones = "INSERT INTO pss_emi_trx33_addenda SET ";
        $query_transacciones .= $datos_insert_update;
        $query_transacciones .= " ON DUPLICATE KEY UPDATE ";
        $query_transacciones .= $datos_insert_update;
        /* Regresa verdadero si se ejecutó correctamente */
        return $this->db->query($query_transacciones);
    }

    public function crear_actualizar_concepto($id_trx33, $concepto) {
        /* Se construye instrucción INSERT ON DUPLICATE KEY UPDATE 
         * para actualizar el registro y si no existe insertarlo
         *          */        
        $datos_insert_update = "";
        foreach ($concepto as $columna => $dato_encabezado) {
            $datos_insert_update .= " $columna='$dato_encabezado',";
        }
        $datos_insert_update .= " id_trx33='$id_trx33' ";

        $query_transacciones = "INSERT INTO pss_emi_trx33_conceptos SET ";
        $query_transacciones .= $datos_insert_update;
        $query_transacciones .= " ON DUPLICATE KEY UPDATE ";
        $query_transacciones .= $datos_insert_update;
        /* Regresa verdadero si se ejecutó correctamente */
        return $this->db->query($query_transacciones);
    }

}
?>
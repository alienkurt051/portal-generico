<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Model_ticket extends CI_Model {
    /* Datos que se van a seleccionar en la consulta */

    private $datos_solicitados = array(
        'emi.id_trx33_r',
        'emi.id_trx_erp',
        '"IDFLEX" AS id_flex_header',
        '"VALOR" AS valor',
        'emi.fecha',
        'ce.entidad as id_sucursal',
        'emi.subtotal',
        'emi.totalImpuestosTrasladados',
        'emi.total',
		'if(emi.id_receptor <> 10000,"Facturado","Pendiente") as estatus_facturacion',
		'if(emi.id_receptor <> 10000,cli.rfc,"") as rfc_receptor',
		'if(emi.id_receptor <> 10000,cli.cliente,"") as nombre_receptor'
    );
    // nombre del flex header en campo de búsqueda Transacción
    private $ids_flex_consulta = [
        "transaccion" => "transaccion" 
    ];
    public function __construct() {
        parent::__construct();
    }

    // Fetch records
    public function obtener_tickets($rowno, $rowperpage, $datos_consulta) {
        /* Se obtienen los datos a seleccionar en forma de lista */
        $datos_select = $this->colocar_id_cliente(implode(",", $this->datos_solicitados));

        $query_transacciones = " select distinct $datos_select  
                                 from emi_trx33_r emi 
                                 inner join c_entidades ce on emi.id_sucursal = ce.id_entidad 
                                 inner join emi_trx33_inf_adic adic on emi.id_trx33_r = adic.id_trx33
                                 inner join c_clientes cli on emi.id_receptor = cli.id_cliente ";
								 
		// si se eligio buscar por caja
		if ( isset($datos_consulta['caja']) ) {
			$query_transacciones .= " inner join emi_trx33_inf_adic adic2 on emi.id_trx33_r = adic2.id_trx33 ";
		}
                                 

        $query_transacciones .= " where emi.id_tipo_de_comprobante = 'I' ";

        $query_transacciones .= $this->validar_datos($datos_consulta);
        $query_transacciones .= " LIMIT $rowno,$rowperpage";
		
		 $logquery = "Query para buscar transacciones: ".$query_transacciones;
		 log_message('debug', $logquery);
		
        $query = $this->db->query($query_transacciones);

//        var_dump($query->result_array());
//        die("HEIL");
        $resultados = $query->result_array();
        $query->free_result();
        return $resultados;
    }

    // Select total records
    public function total_registros($datos_consulta) {

        $query_transacciones = " select count(distinct emi.id_trx33_r) as allcount 
                                    from emi_trx33_inf_adic adic 
                                    inner join emi_trx33_r emi on adic.id_trx33 = emi.id_trx33_r ";

		// si se eligio buscar por caja
		if ( isset($datos_consulta['caja']) ) {
			$query_transacciones .= " inner join emi_trx33_inf_adic adic2 on emi.id_trx33_r = adic2.id_trx33 ";
		}
									

        $query_transacciones .= " where emi.id_tipo_de_comprobante = 'I' ";

        $query_transacciones .= $this->validar_datos($datos_consulta);

        $query = $this->db->query($query_transacciones);        
        $total = $query->result_array()[0]['allcount'];
        
        $query->free_result();
        return $total;
    }
    /* Se agregan las claúslas where al query que hace el filtro de búsqueda */
    public function validar_datos($datos_consulta) {
        $clausulas = "";
        if ($datos_consulta['sucursal'] != "" || $datos_consulta['sucursal'] != null) {
            $clausulas .= " and emi.id_sucursal = " . $datos_consulta['sucursal'];
        }
        if (!empty($datos_consulta['fecha_inicio']) && !empty($datos_consulta['fecha_fin']) ) {
                /* Si hay fecha inicio y fecha fin */
                $clausulas .= " and (date(emi.fecha) between '".$datos_consulta['fecha_inicio']."' and '".$datos_consulta['fecha_fin']."')";           
        }
        if (!empty($datos_consulta['fecha_inicio']) && empty($datos_consulta['fecha_fin']) ) {
                /* Si hay fecha inicio y no hay fecha fin */
                $clausulas .= " and (date(emi.fecha) ='".$datos_consulta['fecha_inicio']."')";                       
        }        
        if (!empty($datos_consulta['hora_inicio']) && !empty($datos_consulta['hora_fin']) ) {
                /* Si hay hora inicio y hora fin */
                $clausulas .= " and (DATE_FORMAT(emi.fecha, '%H:%i') between TIME_FORMAT('".$datos_consulta['hora_inicio']."','%H:%i') and TIME_FORMAT('".$datos_consulta['hora_fin']."','%H:%i'))";                           
        }
        if (!empty($datos_consulta['hora_inicio']) && empty($datos_consulta['hora_fin']) ) {
                /* Si hay hora inicio y no hay hora fin */
                $clausulas .= " and (DATE_FORMAT(emi.fecha, '%H:%i') = TIME_FORMAT('".$datos_consulta['hora_inicio']."','%H:%i'))";
        }
        if ($datos_consulta['caja'] != "" || $datos_consulta['caja'] != null) {
            $clausulas .= " and (adic2.id_flex_header = 'caja' and adic2.valor = '" . $datos_consulta['caja'] . "')";
        }
        if ($datos_consulta['transaccion'] != "" || $datos_consulta['transaccion'] != null) {
            $clausulas .= " and (adic.id_flex_header = '" .
                    $this->ids_flex_consulta["transaccion"] . "' and adic.valor = '" . $datos_consulta['transaccion'] . "')";
        }

        return $clausulas;
    }
    /*  Sustituye el id del cliente para verificar si se ha facturado.
     * 
     *      */
    private function colocar_id_cliente($rows_select) {
        $config_portal = Model\Pss_config_portal::find(1);        
        return str_replace("10000", $config_portal->id_cliente_autofactura, $rows_select);
    }

}

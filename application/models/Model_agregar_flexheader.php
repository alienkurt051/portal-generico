<?php

// modelo para buscar las transacciones para facturar

class model_agregar_flexheader extends CI_model {

  // Listado de los id de los flexheaders
  public function obtener_id_flexheader() {
      $array_id_fh = "SELECT id_flex_header FROM pss_fh_transaccion;";
      $result_campos_fh = $this->db->query($array_id_fh);

      $row_campos_fh = $result_campos_fh->result();
      //echo print_r($row_campos_fh);

      return $row_campos_fh;
  	}
  	//obtiene la informacion de los flexheader que no se encuentran dentro de la lista de array ids
    public function obtener_info_flexheader($array_ids) {
      $array_info_fh = "SELECT * from emi_c_info_adicionales WHERE id_info_adicional NOT IN (".$array_ids.");";
      $result_info_fh = $this->db->query($array_info_fh);

      $row_info_fh = $result_info_fh->result();
      //echo print_r($row_campos_fh);

      return $row_info_fh;
  	}

    public function obtener_flexheaders() {

      $result_flexheaders = $this->db->query("SELECT * from emi_c_info_adicionales;");

      foreach ($result_flexheaders->result() as $value) {
          //echo "string ".$value->id_info_adicional;
          $var_query ="SELECT * FROM pss_campos_adicionales WHERE id_info_adicional =".$value->id_info_adicional.";";
          //echo $var_query."<br>";
          $query_datos = $this->db->query($var_query);
            $existe = 0;
            foreach ($query_datos->result() as $key) {
              //echo $key->id_info_adicional;
              if ($key->id_info_adicional != null) {
                $existe = 1;
                //echo "existe<br>";
              }
            }
            if ($existe == 0) {
                //echo "NO EXISTE<br>";
                $insert_estatus = "INSERT INTO pss_campos_adicionales VALUES(0,".$value->id_info_adicional.",0,'');";

                $row_estatus = $this->db->query($insert_estatus);
                
            }
       }
      
      $result_flexheaders_all = $this->db->query("SELECT info.id_info_adicional, info.campo_adicional, info.descripcion, info.nivel, estatus.estatus, estatus.flexheader
                                              FROM emi_c_info_adicionales AS info
                                              INNER JOIN pss_campos_adicionales AS estatus
                                              ON info.id_info_adicional = estatus.id_info_adicional
                                              WHERE nivel = 1;");

      $row_flexheaders_all = $result_flexheaders_all->result();
      

      return $row_flexheaders_all;
    }

  	//obtiene toda la informacion de los flex headers
  	public function obtener_all_info_flexheader() {
      $array_all_info_fh = "SELECT * from emi_c_info_adicionales";
      $result_all_info_fh = $this->db->query($array_all_info_fh);

      $row_all_info_fh = $result_all_info_fh->result();
      //echo print_r($row_campos_fh);

      return $row_all_info_fh;
  	}
  	//obtine el estatus de los flexheaders
  	public function obtener_estatus_flexheaders() {
      $array_estatus_fh = "SELECT info.id_info_adicional, info.campo_adicional, info.descripcion, info.nivel, estatus.estatus, estatus.flexheader
                                              FROM emi_c_info_adicionales AS info
                                              INNER JOIN pss_campos_adicionales AS estatus
                                              ON info.id_info_adicional = estatus.id_info_adicional
                                              WHERE estatus.estatus = 1;";
      $result_estatus_fh = $this->db->query($array_estatus_fh);

      $row_estatus_fh = $result_estatus_fh->result();
      //echo print_r($row_campos_fh);

      return $row_estatus_fh;
  	}
  	//actualiza el nombre de los flexheader
  	public function actualizar_nombre_fh($valor,$id_info) {
      $array_nombre_fh = "UPDATE pss_campos_adicionales SET flexheader = '".$valor."' WHERE id_info_adicional = ".$id_info.";";
      echo $array_nombre_fh;
      $row_nombre_fh = $this->db->query($array_nombre_fh);

      //echo print_r($row_nombre_fh);

      return $row_nombre_fh;
  	}

    public function update_estatus_fh($id_fh_editado, $estatus) {



      $array_update_estatus = "UPDATE `pss_campos_adicionales` 
                                SET `estatus` = '".$estatus."' 
                                WHERE `id_info_adicional` = '".$id_fh_editado."'";
      //echo $array_update_estatus;
      $row_update_estatus = $this->db->query($array_update_estatus);

      //echo print_r($row_nombre_fh);

      return $row_update_estatus;
    }


  	

}



?>
<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class C_procesos extends ORM {
        public $table_name = "c_procesos";
        public $primary_key = 'id_proceso';

        function _init()
        {
                self::$fields = array(
                        'id_proceso'       => ORM::field('auto[11]'),
                        'nombre'           => ORM::field('char[100]'),
                        'descripcion'      => ORM::field('char[255]'),
                        'estatus'          => ORM::field('int'),
                        'id_grupo_proceso' => ORM::field('int'),
                        'id_prioridad'     => ORM::field('int')
                );
        }
}

?>

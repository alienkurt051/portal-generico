<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Peticiones_pendientes_sat extends ORM {
        public $table_name = "peticiones_pendientes_sat";
        public $primary_key = 'id';

        function _init()
        {
                self::$fields = array(
                        'id'              => ORM::field('auto[11]'),
                        'emisor_rfc'      => ORM::field('varchar[15]'),
                        'receptor_rfc'    => ORM::field('varchar[15]'),
                        'fecha_inicial'   => ORM::field('datetime[0]'),
                        'fecha_final'     => ORM::field('datetime[0]'),
                        'uuid'            => ORM::field('varchar[50]'),
                        'codigo'          => ORM::field('varchar[255]'),
                        'mensaje'         => ORM::field('varchar[255]')
                        
                );
        }
}

?>
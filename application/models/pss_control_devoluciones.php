<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Pss_control_devoluciones extends ORM {

    public $table = "pss_control_devoluciones";
    public $primary_key = 'id_control_devolucion';
    public $foreign_key = 'id_devolucion';

    function _init() {
        self::$relationships = array(
            'id_devolucion' => ORM::belongs_to('\\Model\\Pss_devoluciones')
        );
        self::$fields = array(
            "id_control_devolucion"     => ORM::field('auto'),
            "id_devolucion"             => ORM::field('int'),
            "id_trx33_r"                => ORM::field('int'),
            "id_trx33_concepto_r"       => ORM::field('int'),
            "numero_identificacion"     => ORM::field('char[100]'),
            "cantidad_devolucion"       => ORM::field('char[30]'),
            "cantidad_concepto"         => ORM::field('char[30]'),
            "descripcion"               => ORM::field('string')
        );
    }

}

?>

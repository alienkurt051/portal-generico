<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class C_entidades extends ORM {
        public $table_name = "c_entidades";
        public $primary_key = 'id_entidad';

        function _init()
        {
                self::$fields = array(
                        'id_entidad'        => ORM::field('int'),      
                        'entidad'           => ORM::field('char[360]'), 
                        'numero_entidad'    => ORM::field('char[30]'), 
                        'rfc'               => ORM::field('char[20]'), 
                        'numero_exterior'   => ORM::field('char[20]'), 
                        'calle'             => ORM::field('char[100]'), 
                        'numero_interior'   => ORM::field('char[20]'), 
                        'colonia'           => ORM::field('char[100]'), 
                        'localidad'         => ORM::field('char[60]'), 
                        'referencia'        => ORM::field('char[150]'), 
                        'municipio'         => ORM::field('char[60]'), 
                        'estado'            => ORM::field('char[60]'), 
                        'pais'              => ORM::field('char[60]'), 
                        'codigo_postal'     => ORM::field('char[20]'), 
                        'email'             => ORM::field('char[300]'), 
                        'id_tipo_entidad'   => ORM::field('int'), 
                        'id_entidad_padre'  => ORM::field('int'),
                        'id_regimen'        => ORM::field('int'), 
                        'estatus'           => ORM::field('int')
                        
                );
        }
}

?>

<?php

namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Pss_frecuencia_max_fac extends ORM {

    public $table_name = "pss_frecuencia_max_fac";
    public $primary_key = 'id_frecuencia_max_fac';

    function _init() {
        self::$fields = array(
            'id_frecuencia_max_fac' => ORM::field('int'),
            'frecuencia'            => ORM::field('string'),
            'dias'                  => ORM::field('int'),
            'dias_posteriores'      => ORM::field('int'),
            'cambio_anio'           => ORM::field('int')
        );
    }

}

?>

-- vista para consulta de bitacora
drop view if exists v_pss_bitacora;

create view v_pss_bitacora as
select
bic.id_bitacora     ,
bic.id_usuario      ,
usu.login           ,
concat(usu.nombre, " ",ifnull(usu.apellido_paterno,""), " ",ifnull(usu.apellido_materno,"")) as nombre,
usu.email           ,
bic.fecha_hora      ,
bic.id_accion       ,
acc.d_accion        ,
bic.dir_ip          ,
bic.observaciones   
FROM pss_bitacora bic left outer join pss_usuario usu on bic.id_usuario = usu.id_usuario_pss
inner join c_acciones_bitacora acc on bic.id_accion = acc.id_accion
ORDER BY bic.id_bitacora DESC;
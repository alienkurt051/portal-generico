-- campos para dar informacion de soporte, sea STO o sea por parte del cliente
Alter table pss_config_portal add leyenda_info_soporte  MEDIUMTEXT;
Alter table pss_config_portal add instrucciones_soporte MEDIUMTEXT;
Alter table pss_config_portal add email_soporte         MEDIUMTEXT;
Alter table pss_config_portal add telefono_soporte      MEDIUMTEXT;

update pss_config_portal
set
leyenda_info_soporte = '<h2>Centro de Atención y Soporte</h2><br><br>
En STO DCC estamos comprometidos con nuestros clientes, por lo que hemos creado el CASS (Centro de Atención y Soporte STO Consulting), con la finalidad de brindarles un soporte directo y efectivo para la resolución de problemas de los diferentes productos que manejamos.',
instrucciones_soporte = 'Para levantar un caso (Ticket de seguimiento), por favor de un clic en el siguiente enlace:<br>
CASS – <a href="http://isupport.stoapps.com">http://isupport.stoapps.com</a>
<br><br>
Recuerde tener a la mano su usuario y contraseña. En caso de no tenerlos, envíenos un correo electrónico a la dirección de Help Desk.',
email_soporte = 'soporte@stoconsulting.com',
telefono_soporte = 'T. 55.5284.5060 Opción 3 Soporte|T. 01.800.786.4636 (Lada sin costo)'
;
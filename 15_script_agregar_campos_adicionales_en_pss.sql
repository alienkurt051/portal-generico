-- tabla para definir campos adicionales en ventana de facturacion

CREATE TABLE IF NOT EXISTS pss_campos_adicionales (

  id_estatus_fh int(11) NOT NULL AUTO_INCREMENT,
  id_info_adicional int(11) NOT NULL,
  estatus decimal(1,0) DEFAULT NULL,
  flexheader varchar(255) ,
  PRIMARY KEY (id_estatus_fh)
);
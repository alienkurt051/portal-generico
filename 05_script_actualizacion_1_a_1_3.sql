-- registro de error y facturacion correcta
INSERT IGNORE INTO c_acciones_bitacora VALUES (29,"Ocurrio un error al facturar");
INSERT IGNORE INTO c_acciones_bitacora VALUES (30,"facturacion o evento de facturacion correcto");

-- campo para indicar si al terminar de facturar se sale del portal (para kiosko)
ALTER TABLE pss_config_portal add activar_modo_kiosko SMALLINT DEFAULT 0;
ALTER TABLE pss_config_portal add modo_kiosko_tiempo INT DEFAULT 300000;
-- registro de entrada a liberacion y modificacion de tickets
INSERT IGNORE INTO c_acciones_bitacora VALUES (31,"Entro a liberar-modificar ticket");
INSERT IGNORE INTO c_acciones_bitacora VALUES (32,"Liberación de un ticket");
INSERT IGNORE INTO c_acciones_bitacora VALUES (33,"Modificación de un ticket");

-- Campo para asignar el id de proceso de importacion que se usa para importar los archivos que se generan de la liberacion de ticket. Por omision se usa el 8
ALTER TABLE pss_config_portal add id_proceso_liberacion_ticket SMALLINT DEFAULT 8;

-- tablas de respaldo para cuando se editan los tickets
CREATE TABLE IF NOT EXISTS pss_emi_trx33_r (
  id_trx33_r bigint(20) NOT NULL,
  version varchar(5)  NOT NULL,
  serie varchar(25)  DEFAULT NULL,
  folio varchar(40)  DEFAULT NULL,
  fecha datetime NOT NULL,
  id_forma_pago varchar(5)  DEFAULT NULL,
  condiciones_de_pago varchar(200)  DEFAULT NULL,
  tipo_cambio varchar(30) DEFAULT NULL,
  id_moneda varchar(5)  NOT NULL,
  id_metodo_pago varchar(5)  DEFAULT NULL,
  id_lugar_expedicion varchar(10)  NOT NULL,
  id_tipo_de_comprobante varchar(2)  NOT NULL,
  subtotal decimal(24,6) NOT NULL,
  descuento decimal(24,6) DEFAULT NULL,
  total decimal(24,6) NOT NULL,
  confirmacion varchar(5)  DEFAULT NULL,
  id_trx_erp varchar(45)  DEFAULT NULL,
  envia_xml smallint(1) DEFAULT '0',
  envia_pdf smallint(1) DEFAULT '0',
  envia_zip smallint(1) DEFAULT '0',
  email_envio varchar(255)  DEFAULT NULL,
  id_emisor int(11) NOT NULL,
  id_sucursal int(11) DEFAULT NULL,
  id_receptor int(11) NOT NULL,
  id_destinatario int(11) DEFAULT NULL,
  totalImpuestosRetenidos decimal(24,6) DEFAULT NULL,
  totalImpuestosTrasladados decimal(24,6) DEFAULT NULL,
  id_ejecucion int(11) DEFAULT NULL,
  id_proceso int(11) DEFAULT NULL,
  id_lote_proceso int(11) DEFAULT NULL,
  uso_cfdi varchar(3)  NOT NULL,
  tipo_perfil varchar(45)  DEFAULT NULL,
  id_tipo_documento smallint(6) DEFAULT NULL,
  residencia_fiscal varchar(5) DEFAULT NULL,
  num_reg_idi_trib varchar(20) DEFAULT NULL,
  fecha_edicion TIMESTAMP,
  id_usuario_pss INTEGER,
  PRIMARY KEY (id_trx33_r)
) ;


CREATE TABLE IF NOT EXISTS pss_emi_trx33_concepto_r (
  id_trx33_concepto_r int(11) NOT NULL,
  id_trx33_r bigint(20) NOT NULL,
  id_claveprodserv varchar(10)  NOT NULL,
  cantidad varchar(30)  DEFAULT NULL,
  id_clave_unidad varchar(5)  NOT NULL,
  unidad varchar(20)  DEFAULT NULL,
  numero_identificacion varchar(100)  DEFAULT NULL,
  descripcion text ,
  valor_unitario decimal(24,6) DEFAULT NULL,
  importe decimal(24,6) DEFAULT NULL,
  descuento decimal(24,6) DEFAULT NULL,
  info_aduanera_num_ped varchar(21)  DEFAULT NULL,
  cuenta_predial varchar(150)  DEFAULT NULL,
  numero_linea int(11) DEFAULT NULL,
  PRIMARY KEY (id_trx33_concepto_r)
) ;

CREATE TABLE IF NOT EXISTS pss_emi_trx33_con_impuestos_r (
  id_trx33_con_impuesto int(11) NOT NULL,
  id_trx33_concepto_r int(11) NOT NULL,
  tipo_impuesto varchar(1)  NOT NULL,
  base decimal(24,6) NOT NULL,
  impuesto varchar(3)  NOT NULL,
  tipo_factor varchar(10)  NOT NULL,
  tasa_cuota varchar(30)  DEFAULT NULL,
  importe decimal(24,6) DEFAULT NULL,
  PRIMARY KEY (id_trx33_con_impuesto)
) ;

CREATE TABLE IF NOT EXISTS pss_emi_trx33_impuestos_r (
  id_trx33_impuestos_r int(11) NOT NULL,
  id_trx33_r bigint(20) NOT NULL,
  tipo_impuesto varchar(1)  DEFAULT NULL,
  impuesto varchar(3)  DEFAULT NULL,
  tasa_o_cuota varchar(30)  DEFAULT NULL,
  tipo_factor varchar(10)  DEFAULT NULL,
  importe decimal(24,6) DEFAULT NULL,
  PRIMARY KEY (id_trx33_impuestos_r)
) ;